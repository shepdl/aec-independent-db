// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html
var webpack = require('webpack');

module.exports = function(config) {
  config.set({

    files: [
        'test/frontend/test-main.js'
    ],

    preprocessors: {
        'test/frontend/test-main.js' : ['webpack']
    },

    webpack: require(__dirname + '/webpack.config'),
    webpackMiddleware: {
        noInfo: true,
         stats: {
            chunks: false
         }
    },

    // testing framework to use (jasmine/mocha/qunit/...)
    frameworks: ['jasmine'],

    // web server port
    port: 8080,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    plugins: [
        require('karma-webpack'), require('karma-jasmine'), require('karma-chrome-launcher')
    ],

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: ['Chrome']
  });
};
