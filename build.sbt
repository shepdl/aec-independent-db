import play.sbt.PlayImport.PlayKeys.playRunHooks


name := """aec-editor"""

version := "0.0.2"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  cache,
  ws,
  "javax.inject" % "javax.inject" % "1",
  "com.google.inject" % "guice" % "4.0",
  "ch.qos.logback" % "logback-classic" % "1.1.+",
  "de.svenkubiak" % "jBCrypt" % "0.4.1"
)

libraryDependencies ++= Seq(
  "org.neo4j.driver" % "neo4j-java-driver" % "1.1.0"
)

val neo4jVersion = "3.0.7"

libraryDependencies ++= Seq(
  specs2 % Test,
  "org.specs2" %% "specs2-matcher-extra" % "3.8.4" % Test,
  "org.neo4j" % "neo4j" % neo4jVersion % "test" classifier "tests",
  "org.neo4j" % "neo4j-cypher" % neo4jVersion % "test" classifier "tests",
  "org.neo4j" % "neo4j-kernel" % neo4jVersion % "test" classifier "tests",
  "org.neo4j.test" % "neo4j-harness" % neo4jVersion % "test" classifier "tests",
  // http://stackoverflow.com/questions/35998000/temporary-neo4j-database-to-test-service
  "org.neo4j.app" % "neo4j-server" % neo4jVersion % "test" classifier "tests",
  "org.neo4j" % "neo4j-io" % neo4jVersion % "test" classifier "tests",
  "org.hamcrest" % "hamcrest-all" % "1.3" % "test"
)

playRunHooks += RunSubProcess("gulp")

