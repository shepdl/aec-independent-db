#!/usr/local/bin/fish
set TEST_DB_VOL $WORK/AEC/test-db/

echo "Starting test database server"
docker run --publish=7474:7474 --publish=7687:7687  \
    --volume=$TEST_DB_VOL \
    neo4j

