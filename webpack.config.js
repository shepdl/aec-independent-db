require('imports-loader');
require('exports-loader');
require('url-loader');

module.exports = {
    context: __dirname + "/app/frontend/",
    entry: {
        main: './app',
    },
    output: {
        filename: '[name].entry.js',
        path: __dirname + '/public/javascripts/',
        publicPath: '/assets/javascripts/'
    },
    module: {
        loaders: [
            { test: /angular\/angular/, loader: 'imports?jquery!exports?angular'},
            { test: /angular-/, loader: 'imports?jquery,angular,window=>{angular: angular%2C jQuery:jquery}' },
            { test : /\/jquery\/jquery/, loader: 'exports?jQuery'},
            // { test: /three-obj-loader/, loader: 'imports?THREE=>THREE' },
            { test: /OBJLoader/, loader: 'imports?THREE=threejs'},
            { test: /MTLLoader/, loader: 'imports?THREE=threejs'},
            { test: /ColladaLoader/, loader: 'imports?THREE=threejs'},
            { test: /three-controls/, loader: 'imports?THREE=threejs'}

        ]
    },
    resolve : {
        modulesDirectories: [
            'app/frontend/',
            'node_modules/'
        ],
        alias : {
            'angular-bootstrap' : 'angular-ui-bootstrap',
            'threejs' : 'three',
            'ihtpl' : 'views/ihtpl',
            'three-controls' : 'three-controls/controls'
        }
    }
};
