'use strict';

var gulp = require("gulp");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var ngHtml2Js = require('gulp-ng-html2js');
var minifyHtml = require('gulp-minify-html');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var webpack = require('webpack-stream');
var WebpackDevServer = require('webpack-dev-server');
var replace = require('gulp-replace');
var compass = require('gulp-compass');
// var gulpSass = require('gulp-sass');
var gulpCopy = require('gulp-copy');

var path = {
  js: ['./app/frontend/**/*.js'],
  template: ['./app/frontend/**/*.html'],
  watch: ['./app/frontend/**/*.*']
};

var requireConfig = {
  baseUrl: __dirname + '/app/frontend/',
};

gulp.task('js:webpack', [], function (x) {
  return gulp.src(requireConfig.baseUrl + 'main.js')
    .pipe(webpack(require('./webpack.config.js')))
    .on('error', function (error) {
        gutil.log('WEBPACK ERROR', error);
        this.emit('end');
    })
    // .pipe(uglify())
    .pipe(gulp.dest('./public/javascripts/'))
});

gulp.task('template:compileMinify', function () {
  return gulp.src('./app/frontend/**/*.html')
    .pipe(ngHtml2Js({
      moduleName: 'ih.tpl',
      prefix: '/'
    }))
    .pipe(concat('ihtpl.js'))
    .pipe(gulp.dest('./app/frontend/views/'))
});

gulp.task('fonts', function () {
    return gulp.src([
        './node_modules/**/*.woff2',
        './node_modules/**/*.woff',
        './node_modules/**/*.ttf',
        './app/frontend/**/*.ttf'
    ])
        .pipe(gulpCopy('./public/fonts/', {
            prefix: 10
        }))
    ;

});

// gulp.task('sass', function () {
//     return gulp.src('./app/frontend/**/*.scss')
//         .pipe(gulpSass().on('error', gulpSass.logError))
//         .pipe(concat('main.css'))
//         .pipe(gulp.dest('./public/styles/'))
// });


// gulp.task('sass:watch', function () {
//     gulp.watch('./app/frontend/**/*.scss', ['sass'])
// });


gulp.task('webpack-dev-server', function (callback) {

    var compiler = webpack(require('./webpack.config.js'));

    new WebpackDevServer(compiler, {
    }).listen(8080, 'localhost', function (err) {
        if (err) {
            throw new gutil.PluginError('webpack-dev-server', err);
        }
        gutil.log('[webpack-dev-server]', 'http://localhost:8080/webpack-dev-server/index.html');
    });
});

// gulp.task('default', [/*'fonts',*/ 'js:webpack'], function () {
//     gulp.watch(path.watch, ['js:webpack', 'template:compileMinify',  /* 'sass' */]);
// });


gulp.task('default', [], function () {
    gulp.watch(path.template, ['template:compileMinify']);
    gulp.watch(path.js, ['js:webpack']);
});

gulp.task('test-watch', ['test'], function (done) {
    gulp.watch(['app/frontend/**/*.js', 'app/frontend/**/*.html', 'test/**/*.js'], ['test']);
});

gulp.task('test', ['template:compileMinify'], function (done) {
    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});
