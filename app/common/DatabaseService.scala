package common

import javax.inject.Inject
import com.google.inject.Singleton

import org.neo4j.driver.v1.Config.{ConfigBuilder, EncryptionLevel}
import org.neo4j.driver.v1._
import play.api.Configuration
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future

/**
  * Created by dave on 12/12/16.
  */
trait DatabaseService {

  val driver:Driver

  def withSession[A](block: Session => A) = {
    val session = driver.session()
    try {
      val result = block(session)
      result
    } finally {
      session.close()
    }
  }

  def withTransaction[A](block: Transaction => A) = {
    val session = driver.session()
    val transaction = session.beginTransaction()
    try {
      block(transaction)
    } finally {
      transaction.close()
      session.close()
    }
  }

  def clear:Unit

  def destroy:Unit

}

@Singleton
class Neo4jDatabaseService @Inject()(config:Configuration, lifecycle:ApplicationLifecycle) extends DatabaseService {

  private val dbPath = config.getString("db.default.url").get
  private val dbUsername = config.getString("db.default.username").get
  private val dbPassword = config.getString("db.default.password").get

  private val neo4jConfig = Config.build().withEncryptionLevel(EncryptionLevel.NONE).toConfig

  val driver = GraphDatabase.driver(dbPath, AuthTokens.basic(dbUsername, dbPassword), neo4jConfig)

  lifecycle.addStopHook(
    () => {
      Future.successful(destroy)
    }
  )

  def clear = ???

  def destroy = {
    driver.close()
  }

}

