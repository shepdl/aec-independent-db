package common

import java.util.UUID

/**
  * Created by dave on 12/15/16.
  */
trait GraphEntityProperties {
  val label:String

  val id:String = "id"
}

trait VersionedGraphEntityProperties {

  val current = "Current"
  val previousVersion = "PreviousVersion"
  val versionAction = "VersionAction"
  val updatedBy = s"UpdatedBy"

  val deleted = s"Deleted"

  val deletedBy = s"DeletedBy"
  val undeletedBy = s"UndeletedBy"

  val revertedBy = s"RevertedBy"
  val isReversionOf = "IsReversionOf"

  val versionId = "versionId"
  val nodeId = "nodeId"


}


object VersionActions {

  val original = "Original"
  val updated = "Updated"

  val deleted = s"Deleted"
  val undeleted = "Undeleted"
  val reverted = "Reverted"
  val deletedBy = "DeletedBy"
  val undeletedBy = "UndeletedBy"

}

trait Resource {
  val id:UUID
  val resourceName:String
}

trait TimestampsProperties {

  val createdAt = "createdAt"
  val updatedAt = "updatedAt"

}

object TimestampsProperties extends TimestampsProperties

