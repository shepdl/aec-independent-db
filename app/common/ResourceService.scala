/*
import java.util.UUID
import javax.inject.Inject
import com.google.inject.Singleton

import accessControl.{ConvertsNodesToUsers, User}
import common.DatabaseService
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.Future


trait Resource {

  val id: UUID

}

class ResourceRequest[A <: Resource](val resource:A, val request:Request[A]) extends WrappedRequest[A](request)


trait Repository[R] {
  
  val typeDescription:String
  
  def getById(id:UUID):Option[R]
  def save(resource:R):R
  def delete(resource:R)
  def list(page:Int, sortBy:String, rpp:Int):List[R]

}

// Example:
//
@Singleton
class UserRepository @Inject()(db:DatabaseService) extends Repository[User] with ConvertsNodesToUsers {

  val typeDescription = "User"

  override def getById(id: UUID): Option[User] = ???

  override def save(resource: User): User = ???

  override def delete(resource: User): Unit = ???

  override def list(page: Int, sortBy: String, rpp: Int): List[User] = ???
}

object NoResource extends Resource {
  override val id = null
}


class ResourceService[Resource, ResourceRepo <: Repository[Resource]](repo:ResourceRepo) {

  def ResourceAwareAction(resourceId:UUID) =
    new ActionBuilder[Request] with ActionTransformer[Request, ResourceRequest] {

      override def transform[A](request: Request[A]): Future[ResourceRequest[A]] = Future.successful {
        repo.getById(resourceId) match {
          case None => new ResourceRequest(NoResource, request)
            // NotFound(resourceNotFoundError(resourceId))
          case Some(resource) => new ResourceRequest(resource, request)
        }
      }
  }

  def ChecksIfResourceExists = new ActionFilter[ResourceRequest] with Results {

    override def filter[A](request: ResourceRequest[A]) = Future.successful({
      request.resource match {
        case NoResource => Some(NotFound(Json.obj(
            "error" -> "Resource not found",
            "type" -> repo.typeDescription
          )))
        case _ => None
      }
    })

  }

}
*/
