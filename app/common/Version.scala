package common

import java.util.{Date, UUID}

/**
  * Created by daveshepard on 3/10/17.
  */
trait Version[A] {

  val item:A
  val versionId:UUID
  val updatedAt:Date
  val action:String

}
