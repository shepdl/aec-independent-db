package common

import java.util.UUID

import accessControl.User
import play.api.libs.json.Json

/**
  * Created by dave on 12/14/16.
  */
trait JsonResponses {

  def error(message:String) = Json.obj(
    "error" -> message
  )

  def missingObject(objectType:String, objectId:UUID) = Json.obj(
    "error" -> "Not found",
    "objectType" -> objectType,
    "objectId" -> objectId
  )

  def inadequatePermissions(permissionRequired:String, user:User) = Json.obj(
    "error" -> "Not enough permissions",
    "permissionRequired" -> permissionRequired,
    "userId" -> user.uuid
  )

  def message(message:String) = Json.obj(
    "message" -> message
  )

}
