package common

import com.google.inject.AbstractModule


class IHViewer extends AbstractModule {

  override def configure() = {
    bind(classOf[common.DatabaseService]) to classOf[Neo4jDatabaseService]

//    bind(classOf[annotations.AnnotationRepository]) to classOf[annotations.Neo4jAnnotationRepository]
//    bind(classOf[annotations.AnnotationController]) to classOf[annotations.DefaultController]
//    bind(classOf[annotations.AnnotationSearchController]) to classOf[annotations.DefaultAnnotationSearchController]
//
//    bind(classOf[artifacts.ArtifactRepository]) to classOf[artifacts.Neo4jArtifactRepository]
//    bind(classOf[artifacts.ArtifactController]) to classOf[artifacts.DefaultController]
//
//    bind(classOf[artifacts.MediaRepository]) to classOf[artifacts.Neo4jMediaRepository]
//    bind(classOf[artifacts.MediaController]) to classOf[artifacts.DefaultMediaController]
//
//    bind(classOf[projects.ProjectRepository]) to classOf[projects.Neo4jProjectRepository]
//    bind(classOf[projects.ProjectController]) to classOf[projects.DefaultController]
//
//    bind(classOf[annotations.EntityRepository]) to classOf[annotations.Neo4jEntityRepository]
//    bind(classOf[annotations.EntityController]) to classOf[annotations.DefaultEntityController]
//
//    bind(classOf[schemas.SchemaRepository]) to classOf[schemas.Neo4jSchemaRepository]
//    bind(classOf[schemas.SchemaController]) to classOf[schemas.DefaultController]
  }

}
