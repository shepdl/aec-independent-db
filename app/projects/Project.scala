package projects

import java.util.UUID

import common.Resource

/**
 * Created by dave on 10/9/15.
 */

case class Project(id:UUID, title:String) extends Resource {
  def this(title:String) = this(null, title)

  override val resourceName: String = "Project"

}



