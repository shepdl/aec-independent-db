package projects

import java.util.UUID
import javax.inject.{Inject, Singleton}

import accessControl.{AuthenticationService, RendersGroupAsJson, UserRepository, User}
import backend.approvals.ApprovalRepository
import common.JsonResponses
import play.api.data.Forms._
import play.api.data._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json._
import play.api.mvc.{Action, Controller}


/**
 * Created by dave on 10/9/15.
 */
trait ProjectController extends I18nSupport with JsonResponses with RendersGroupAsJson {

  this: Controller =>

  val authentication:AuthenticationService
  val userRepo:UserRepository
  val projectRepo:ProjectRepository
  val projectService:ProjectService
  val approvalRepo:ApprovalRepository

  val messagesApi:MessagesApi

  import play.api.libs.functional.syntax._
  import play.api.libs.json.Reads._
  import play.api.libs.json.Writes._

  implicit val projectReads: Reads[Project] = (
    (JsPath \ "id").read[UUID] and (JsPath \ "title").read[String]
    )(Project.apply _)

  implicit val projectForm:Form[ProjectData] = Form(
    mapping(
      "title" -> nonEmptyText
    )(ProjectData.apply)(ProjectData.unapply)
  )

  implicit val projectWrites:Writes[Project] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "title").write[String]
  )(unlift(Project.unapply _))

  def toJson(project:Project) = Json.obj(
    "id" -> project.id,
    "title" -> project.title,
    "endorsements" -> approvalRepo.getForItemId(project.id).map(approval => Json.obj(
      "id" -> approval.id,
      "group" -> groupToJson(approval.group, List())
    ))
  )

  def toJson(project:Project, user:User) = Json.obj(
    "id" -> project.id,
    "title" -> project.title,
    "endorsements" -> approvalRepo.getForItemId(project.id).map(approval => Json.obj(
      "id" -> approval.id,
      "group" -> groupToJson(approval.group, List())
    )),
    "permissions" -> projectRepo.getRightsInProject(project, user)
  )


  def get(id: UUID) = Action {
    projectRepo getById id match {
      case Some(project) => Ok(toJson(project))
      case None => NotFound(Json.obj("message" -> s"Project ${id} not found"))
    }
  }

  def list = authentication.UserAwareAction { implicit request =>
    val projects = projectRepo.list()
    Ok(Json.obj(
      "projects" -> projects.map(toJson(_, request.user))
    ))
  }

  def create = authentication.LoginRequiredAction(parse.urlFormEncoded) { implicit request =>
    projectForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(formWithErrors.errorsAsJson)
      },
      projectData => {
        val newProject = Project(null, projectData.title)
        val savedProject = projectRepo.save(newProject)
        projectRepo.setRightsInProjectForUser(savedProject, request.user, ProjectUserRights.ALL_RIGHTS)
        Created(Json toJson savedProject)
      }
    )
  }

  def setRolesInProjectForUser(projectId:UUID, userId:UUID) = projectService.PermissionRequiredAction(projectId, ProjectUserRights.canAddUsers)(parse.json) { implicit request =>
    val rightsToAssign = (request.body \ "rights").as[Set[String]]
    val misnamedRights = rightsToAssign &~ ProjectUserRights.ALL_RIGHTS
    if (misnamedRights.nonEmpty) {
      BadRequest(error(s"Unrecognized rights: ${misnamedRights.mkString(", ")}"))
    } else {
      projectRepo.getById(projectId) match {
        case None => NotFound(error(s"Project ${projectId} not found"))
        case Some(project) => userRepo.getById(userId) match {
          case None => NotFound(error(s"User ${userId} not found"))
          case Some(userToUpdate) => {
            val activeUserRights = projectRepo.getRightsInProject(project, request.user)
            if (activeUserRights.contains(ProjectUserRights.canEditUsers)) {
              projectRepo.setRightsInProjectForUser(project, userToUpdate, rightsToAssign)
              Ok(
                Json.obj(
                  "project" -> projectId,
                  "user" -> userId,
                  "rights" -> rightsToAssign
                )
              )
            } else {
              Forbidden(Json.obj(
                "message" -> "User is not allowed to alter other users' rights in project",
                "user" -> userId,
                "project" -> projectId
              ))
            }
          }
        }
      }
    }
  }

  def getRolesInProjectForUser(projectId:UUID, userId:UUID) = Action { implicit request =>
    projectRepo.getById(projectId) match {
      case None => NotFound(error("Project does not exist"))
      case Some(project) => userRepo.getById(userId) match {
        case None => NotFound(error("User does not exist"))
        case Some(user) => {
          val rights = projectRepo.getRightsInProject(project, user)
          Ok(Json.obj(
            "project" -> projectId,
            "user" -> userId,
            "rights" -> rights
          ))
        }
      }
    }
  }

  def update(projectId:UUID) = projectService.PermissionRequiredAction(projectId, ProjectUserRights.canEditMetadata)(parse.json) { implicit projectRequest =>
    val projectResult = projectRequest.body.validate[Project]
    projectResult.fold(
      errors => BadRequest(Json.obj(
        "status" -> "KO",
        "message" -> JsError.toJson(errors)
      )),
      project => {
        val newProject = Project(project.id, project.title)
        Ok(Json toJson projectRepo.save(newProject))
      }
    )
  }

    def delete(id: UUID) = projectService.PermissionRequiredAction(id, ProjectUserRights.canDeleteProject) { implicit request =>
    projectRepo getById id match {
      case Some(project) => {
        projectRepo.delete(project, request.user)
        Ok(Json.obj("status" -> "OK", "message" -> s"${project.title} deleted"))
      }
      case None => NotFound(Json.obj("message" -> s"Project ${id} not found"))
    }
  }

  def listVersions(projectId:UUID, page:Int, rpp:Int) = projectService.PermissionRequiredAction(projectId, ProjectUserRights.canEditMetadata) {
    implicit request => Ok(
      Json.obj(
        "versions" -> projectRepo.listVersions(projectId, page, rpp).map(version => Json.obj(
         "results" -> Json.obj(
           "version" -> Json.obj(
             "versionId" -> version.versionId,
             "updatedAt" -> version.updatedAt,
             "action" -> version.action
           ),
           "project" -> version.item
          )
        )
      )
    ))
  }

  def revertTo(projectId:UUID, versionId:UUID) = projectService.PermissionRequiredAction(projectId, ProjectUserRights.canEditMetadata) { implicit request =>
    projectRepo.revertTo(projectId, versionId, request.user)
    Ok(
      Json.obj(
        "status" -> "OK",
        "projectId" -> projectId,
        "versionId" -> versionId
      )
    )
  }


}


case class ProjectData(title:String)

case class ProjectRightsForm(rights:List[String])


@Singleton
class DefaultController @Inject()(
   val projectRepo: ProjectRepository,
   override val authentication:AuthenticationService,
   override val projectService: ProjectService,
   override val userRepo:UserRepository,
   override val approvalRepo:ApprovalRepository,
   override val messagesApi: MessagesApi
) extends ProjectController with Controller

