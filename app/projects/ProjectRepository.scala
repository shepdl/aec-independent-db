package projects

import java.util.{Date, UUID}
import javax.inject.Inject

import accessControl.{User, UserProperties, Group, GroupProperties}
import annotations.{AnnotationProperties, ProjectRelationship}
import artifacts.ArtifactProperties
import com.google.inject.Singleton
import common._
import org.neo4j.driver.v1.Transaction
import org.neo4j.driver.v1.types.Node
import schemas.SchemaProperties


object ProjectProperties extends GraphEntityProperties with VersionedGraphEntityProperties with TimestampsProperties {

  val label = "Project"
  val title = "title"

}


object ProjectUserRights extends GraphEntityProperties with TimestampsProperties {

  val label = "HasRightsInProject"

  val canEditMetadata = "canEditMetadata"
  val canDeleteProject = "canDeleteProject"

  val canAddUsers = "canAddUsers"
  val canEditUsers = "canEditUsers"

  val canAddNewArtifacts = "canAddNewArtifacts"
  val canEditOthersArtifacts = "canEditOthersArtifacts"
  val canDeleteArtifacts = "canDeleteArtifacts"

  val canAddNewSchemas = "canAddNewSchemas"
  val canEditOthersSchemas = "canEditOthersSchemas"
  val canDeleteSchemas = "canDeleteSchemas"

  val canAddAnnotations = "canAddAnnotations"
  val canEditOthersAnnotations = "canEditOthersAnnotations"
  val canDeleteAnnotations = "canDeleteAnnotations"

  val canApproveOthersAnnotations = "canApproveOthersAnnotations"
  val canRemoveApprovedAnnotations = "canRemoveApprovedAnnotations"


  val ALL_RIGHTS = Set(
    canEditMetadata, canAddUsers, canEditUsers, canDeleteProject,
    canAddNewArtifacts, canEditOthersArtifacts, canDeleteArtifacts,
    canAddNewSchemas, canEditOthersSchemas, canDeleteSchemas,
    canAddAnnotations, canEditOthersAnnotations, canDeleteAnnotations,
    canApproveOthersAnnotations, canRemoveApprovedAnnotations
  )

}

@Singleton
class ProjectRepository @Inject() (db:DatabaseService) extends ConvertsNodesToProjects {

  import scala.collection.JavaConversions._

  def getById(id:UUID):Option[Project] = db.withTransaction { tx =>
      tx.run(
        s"""
           |MATCH (p:${ProjectProperties.label}:${ProjectProperties.current} {
           |  ${ProjectProperties.id} : {${ProjectProperties.id}}
           |})
           |WHERE NOT p:${ProjectProperties.deleted}
           |RETURN p
         """.stripMargin, Map(
          ProjectProperties.id -> id.toString
        )
      ).map(row => nodeToProject(row.get("p").asNode())).toIterable.headOption
  }


  def list() = db.withTransaction { tx => {
      tx.run(
        s"""
           |MATCH (p:${ProjectProperties.label}:${ProjectProperties.current})
           |WHERE NOT p:${ProjectProperties.deleted}
           |  AND p.${ProjectProperties.title} <> "${UserProperties.SCRATCHPAD_NAME}"
           |RETURN p
           |""".stripMargin).map(p => nodeToProject(p.get("p").asNode())).toList
    }
  }


  def forUser(user:User):List[Project] = db.withTransaction { tx => {
    tx.run(s"""MATCH (u:${UserProperties.label} {
         |  ${UserProperties.id} : {${UserProperties.id}}
         |})-[:{${ProjectUserRights.label}]->(p:${ProjectProperties.label}:${ProjectProperties.current})
         |RETURN p""".stripMargin, Map(
      UserProperties.id -> user.uuid.toString
    )).map(row => nodeToProject(row.get("p").asNode())).toList
    }
  }

  type ProjectRight = String

  def getUserScratchpad(user:User):Project = db.withTransaction({ tx => {
    tx.run(
      s"""
         |MATCH (user:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})-[:${ProjectUserRights.label}]->(project:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.title} : {title}
         |})
         |RETURN project
       """.stripMargin, Map(
        "userId" -> user.uuid.toString,
        "title" -> UserProperties.SCRATCHPAD_NAME
      )).map(
      row => nodeToProject(row.get("project").asNode())
    ).toList.head
  }})

  def setRightsInProjectForUser(project:Project, user:User, rights:Set[String], tx:Transaction):Unit = {
    tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})-[pr:${ProjectUserRights.label}]->(p:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId}
         |})
         |WHERE NOT p:${ProjectProperties.deleted}
         |DELETE pr
       """.stripMargin, Map(
        "userId" -> user.uuid.toString,
        "projectId" -> project.id.toString
      )
    )
    val status = tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |    ${UserProperties.id} : {userId}
         |  }), (p:${ProjectProperties.label}:${ProjectProperties.current} {
         |    ${ProjectProperties.id} : {projectId}
         |  })
         |WHERE NOT p:${ProjectProperties.deleted}
         |CREATE (u)-[:${ProjectUserRights.label} {
         |  ${ rights.map(role => s"${role} : true").mkString(",\n") }
         |}]->(p)
       """.stripMargin, Map(
        "userId" -> user.uuid.toString,
        "projectId" -> project.id.toString
      )
    )
  }

  def setRightsInProjectForUser(project:Project, user:User, rights:Set[String]):Unit = db.withTransaction(tx => {
    setRightsInProjectForUser(project, user, rights, tx)
    tx.success()
  })

  def getRightsInProject(project:Project, user:User):Set[String] = db.withTransaction { tx => {
    val rights = tx.run(
      s"""
         |MATCH (p:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId}
         |})<-[pr:${ProjectUserRights.label}]-(u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |RETURN pr
       """.stripMargin, Map(
        "projectId" -> project.id.toString,
        "userId" -> user.uuid.toString
      )).map(row => row.get("pr").asRelationship()).toList

    if (rights.nonEmpty) {
      val realRights = rights.head
      realRights.keys().map({
        case (key) => key
      }).toSet
    } else {
      Set()
    }
  }}

  def getRightsInProjectForGroup(project:Project, group:Group) = db.withTransaction( tx => {
    val rights = tx.run(
      s"""
         |MATCH (p:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId}
         |})<-[pr:${ProjectUserRights.label}]-(g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})
         |RETURN pr
       """.stripMargin, Map(
        "projectId" -> project.id.toString,
        "groupId" -> group.id.toString
      )).map(row => row.get("pr").asRelationship()).toList

    if (rights.nonEmpty) {
      val realRights = rights.head
      realRights.keys().map({
        case (key) => key
      }).toSet
    } else {
      Set()
    }
  })

  def save(project:Project):Project = db.withTransaction({ tx =>
    val newProject = save(project, tx)
    tx.success()
    newProject
  })

  def save(project:Project, tx:Transaction):Project = {
    val versionId = UUID.randomUUID()
    val now = new Date().getTime
    val id = if (project.id == null) {
      val newId = UUID.randomUUID()
      tx.run(
        s"""
           |CREATE (p:${ProjectProperties.label}:${ProjectProperties.current} {
           |  ${ProjectProperties.id} : {${ProjectProperties.id}},
           |  ${ProjectProperties.title} : {${ProjectProperties.title}},
           |  ${ProjectProperties.versionId} : {${ProjectProperties.versionId}},
           |  ${ProjectProperties.createdAt} : {${ProjectProperties.createdAt}},
           |  ${ProjectProperties.updatedAt} : {${ProjectProperties.updatedAt}},
           |  ${ProjectProperties.versionAction} : {${ProjectProperties.versionAction}}
           |})
         """.stripMargin, Map(
          ProjectProperties.id -> newId.toString,
          ProjectProperties.title -> project.title,
          ProjectProperties.versionId -> versionId.toString,
          ProjectProperties.createdAt -> now.asInstanceOf[Object],
          ProjectProperties.updatedAt -> now.asInstanceOf[Object],
          ProjectProperties.versionAction -> VersionActions.original
        )
      )
      newId
    } else {
      val status = tx.run(
        s"""
           |MATCH (oldVersion:${ProjectProperties.label}:${ProjectProperties.current} {
           |  ${ProjectProperties.id} : {${ProjectProperties.id}}
           |})
           |REMOVE oldVersion:${ProjectProperties.current}
           |CREATE (newVersion:${ProjectProperties.label}:${ProjectProperties.current} {
           |  ${ProjectProperties.id} : {${ProjectProperties.id}},
           |  ${ProjectProperties.versionId} : {newVersionId},
           |  ${ProjectProperties.title} : {${ProjectProperties.title}},
           |  ${ProjectProperties.updatedAt} : {now},
           |  ${ProjectProperties.versionAction} : {${ProjectProperties.versionAction}}
           |}),
           | (newVersion)-[:${ProjectProperties.previousVersion}]->(oldVersion)
           |
           |WITH oldVersion, newVersion
           |MATCH
           |  (oldVersion)<-[right:${ProjectUserRights.label}]-(user:${UserProperties.label})
           |CREATE
           |  (newVersion)<-[:${ProjectUserRights.label} {
           |    ${ProjectUserRights.canEditMetadata} : right.${ProjectUserRights.canEditMetadata},
           |    ${ProjectUserRights.canDeleteProject} : right.${ProjectUserRights.canDeleteProject},
           |    ${ProjectUserRights.canEditUsers} : right.${ProjectUserRights.canEditUsers},
           |    ${ProjectUserRights.canAddNewArtifacts} : right.${ProjectUserRights.canAddNewArtifacts},
           |    ${ProjectUserRights.canEditOthersArtifacts} : right.${ProjectUserRights.canEditOthersArtifacts},
           |    ${ProjectUserRights.canDeleteArtifacts} : right.${ProjectUserRights.canDeleteArtifacts},
           |    ${ProjectUserRights.canAddNewSchemas} : right.${ProjectUserRights.canAddNewSchemas},
           |    ${ProjectUserRights.canEditOthersSchemas} : right.${ProjectUserRights.canEditOthersSchemas},
           |    ${ProjectUserRights.canDeleteSchemas} : right.${ProjectUserRights.canDeleteSchemas},
           |    ${ProjectUserRights.canAddAnnotations} : right.${ProjectUserRights.canAddAnnotations},
           |    ${ProjectUserRights.canEditOthersAnnotations} : right.${ProjectUserRights.canEditOthersAnnotations},
           |    ${ProjectUserRights.canDeleteAnnotations} : right.${ProjectUserRights.canDeleteAnnotations},
           |    ${ProjectUserRights.canApproveOthersAnnotations} : right.${ProjectUserRights.canApproveOthersAnnotations},
           |    ${ProjectUserRights.canRemoveApprovedAnnotations} : right.${ProjectUserRights.canRemoveApprovedAnnotations}
           |  }]-(user)
           |
           |
           |
           |
         """.stripMargin, Map(
          ProjectProperties.id -> project.id.toString,
          "newVersionId" -> versionId.toString,
          ProjectProperties.title -> project.title,
          "now" -> now.asInstanceOf[Object],
          ProjectProperties.versionAction -> VersionActions.updated
        )
      )

      val queryProperties = Map(
        ProjectProperties.id -> project.id.toString
      )
      tx.run(
        s"""
           |MATCH (newVersion:${ProjectProperties.label}:${ProjectProperties.current} {
           |  ${ProjectProperties.id} : {${ProjectProperties.id}}
           |})-[:${ProjectProperties.previousVersion}]->(oldVersion),
           |  (oldVersion)<-[:${ArtifactProperties.project}]-(artifact:${ArtifactProperties.label})
           |CREATE
           |  (newVersion)<-[:${ArtifactProperties.project}]-(artifact)
         """.stripMargin, queryProperties
      )
      tx.run(
        s"""
           |MATCH (newVersion:${ProjectProperties.label}:${ProjectProperties.current} {
           |  ${ProjectProperties.id} : {${ProjectProperties.id}}
           |})-[:${ProjectProperties.previousVersion}]->(oldVersion),
           |  (oldVersion)<-[:${ProjectRelationship.label}]-(annotation:${AnnotationProperties.label})
           |CREATE
           |  (newVersion)<-[:${ProjectRelationship.label}]-(annotation)
           |
         """.stripMargin, queryProperties
      )
      tx.run(
        s"""
           |MATCH (newVersion:${ProjectProperties.label}:${ProjectProperties.current} {
           |  ${ProjectProperties.id} : {${ProjectProperties.id}}
           |})-[:${ProjectProperties.previousVersion}]->(oldVersion),
           |  (oldVersion)<-[:${SchemaProperties.project}]-(schema:${SchemaProperties.label})
           |CREATE
           |  (newVersion)<-[:${SchemaProperties.project}]-(schema)
           |
         """.stripMargin, queryProperties
      )
      project.id
    }
    Project(id, project.title)
  }

  def delete(project: Project, deletingUser:User) = db.withTransaction ( tx => {
    val now = new Date().getTime
    val result = tx.run(
    s"""
       |MATCH (oldVersion:${ProjectProperties.label}:${ProjectProperties.current} {
       |  ${ProjectProperties.id} : {projectId}
       |}),
       |(deletingUser: ${UserProperties.label} {
       |  ${UserProperties.id} : {userId}
       |})
       |REMOVE oldVersion:${ProjectProperties.current}
       |CREATE (newVersion:${ProjectProperties.label}:${ProjectProperties.current}:${ProjectProperties.deleted} {
       |  ${ProjectProperties.id} : {projectId},
       |  ${ProjectProperties.title} : oldVersion.${ProjectProperties.title},
       |  ${ProjectProperties.versionId} : {newVersionId},
       |  ${ProjectProperties.createdAt} : oldVersion.${ProjectProperties.createdAt},
       |  ${ProjectProperties.updatedAt} : {now},
       |  ${ProjectProperties.versionAction} : "${VersionActions.deleted}"
       |})-[:${ProjectProperties.deletedBy}]->(deletingUser),
       |(newVersion)-[:${ProjectProperties.previousVersion}]->(oldVersion)
     """.stripMargin, Map(
        "projectId" -> project.id.toString,
        "userId" -> deletingUser.uuid.toString,
        "newVersionId" -> UUID.randomUUID().toString,
        "now" -> now.asInstanceOf[Object]
    ))
    tx.success()
  })

  def listVersions(projectId:UUID, page:Int, rpp:Int) = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (project:${ProjectProperties.label} {
         |  ${ProjectProperties.id} : {projectId}
         |})
         |RETURN project
         |ORDER BY project.${ProjectProperties.updatedAt} DESC
         |SKIP ${page * rpp}
         |LIMIT $rpp
       """.stripMargin, Map(
        "projectId" -> projectId.toString
      )
    ).map(
      row => {
        val versionProperties = row.get("project").asNode()
        new Version[Project] {
          override val item = nodeToProject(versionProperties)
          override val versionId = UUID.fromString(versionProperties.get(ProjectProperties.versionId).asString())
          override val updatedAt = new Date() // new Date(versionProperties.get(ProjectProperties.updatedAt).asLong())
          override val action = versionProperties.get(ProjectProperties.versionAction).asString()
        }
      }
    ).toSeq
  })

  def revertTo(projectId:UUID, versionId:UUID, revertingUser:User) = db.withTransaction(tx => {
    val newVersionId = UUID.randomUUID()
    val now = new Date().getTime
    tx.run(
      s"""
         |MATCH
         |  (oldVersion:${ProjectProperties.label} {
         |    ${ProjectProperties.id} : {projectId},
         |    ${ProjectProperties.versionId} : {oldVersionId}
         |  }),
         |  (user:${UserProperties.label} {
         |    ${UserProperties.id} : {revertingUserId}
         |  }),
         |(currentVersion:${ProjectProperties.label}:${ProjectProperties.current}),
         |(user:${UserProperties.label})-[:${ProjectUserRights.label}]->(currentVersion)
         |
         |REMOVE currentVersion.${ProjectProperties.current}
         |
         |CREATE (newVersion:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId},
         |  ${ProjectProperties.title} : oldVersion.${ProjectProperties.title},
         |  ${ProjectProperties.versionId} : {newVersionId},
         |  ${ProjectProperties.versionAction} : "reverted",
         |  ${ProjectProperties.createdAt} : oldVersion.${ProjectProperties.createdAt},
         |  ${ProjectProperties.title} : oldVersion.${ProjectProperties.title},
         |  ${ProjectProperties.updatedAt} : {now},
         |  ${ProjectProperties.versionAction} : {${ProjectProperties.versionAction}}
         |}),
         |(newVersion)-[:${ProjectProperties.previousVersion}]->(currentVersion)
         |
         |WITH oldVersion, currentVersion, newVersion
         |MATCH
         |  (oldVersion)<-[:${ProjectRelationship.label}]-(annotation:${AnnotationProperties.label})
         |CREATE
         |  (newVersion)<-[:${ProjectRelationship.label}]-(annotation)
         |
         |WITH oldVersion, currentVersion, newVersion
         |MATCH
         |  (oldVersion)<-[:${SchemaProperties.project}]-(schema:${SchemaProperties.label})
         |CREATE
         |  (newVersion)<-[:${SchemaProperties.project}]-(schema)
         |
         |WITH oldVersion, currentVersion, newVersion
         |MATCH
         |  (oldVersion)<-[:${ArtifactProperties.project}]-(artifact:${ArtifactProperties.label})
         |CREATE
         |  (newVersion)<-[:${ArtifactProperties.project}]-(artifact)
         |
         |WITH oldVersion, currentVersion, newVersion
         |MATCH
         |  (oldVersion)<-[right:${ProjectUserRights.label}]-(user)
         |CREATE
         | (newVersion)<-[:${ProjectUserRights.label} {
         |   ${ProjectUserRights.canEditMetadata} : right.${ProjectUserRights.canEditMetadata},
         |   ${ProjectUserRights.canDeleteProject} : right.${ProjectUserRights.canDeleteProject},
         |   ${ProjectUserRights.canEditUsers} : right.${ProjectUserRights.canEditUsers},
         |   ${ProjectUserRights.canAddNewArtifacts} : right.${ProjectUserRights.canAddNewArtifacts},
         |   ${ProjectUserRights.canEditOthersArtifacts} : right.${ProjectUserRights.canEditOthersArtifacts},
         |   ${ProjectUserRights.canDeleteArtifacts} : right.${ProjectUserRights.canDeleteArtifacts},
         |   ${ProjectUserRights.canAddNewSchemas} : right.${ProjectUserRights.canAddNewSchemas},
         |   ${ProjectUserRights.canEditOthersSchemas} : right.${ProjectUserRights.canEditOthersSchemas},
         |   ${ProjectUserRights.canDeleteSchemas} : right.${ProjectUserRights.canDeleteSchemas},
         |   ${ProjectUserRights.canAddAnnotations} : right.${ProjectUserRights.canAddAnnotations},
         |   ${ProjectUserRights.canEditOthersAnnotations} : right.${ProjectUserRights.canEditOthersAnnotations},
         |   ${ProjectUserRights.canDeleteAnnotations} : right.${ProjectUserRights.canDeleteAnnotations},
         |   ${ProjectUserRights.canApproveOthersAnnotations} : right.${ProjectUserRights.canApproveOthersAnnotations},
         |   ${ProjectUserRights.canRemoveApprovedAnnotations} : right.${ProjectUserRights.canRemoveApprovedAnnotations}
         | }]-(user)
         |
       """.stripMargin, Map(
        "projectId" -> projectId.toString,
        "oldVersionId" -> versionId.toString,
        "newVersionId" -> newVersionId.toString,
        "revertingUserId" -> revertingUser.uuid.toString,
        "now" -> now.asInstanceOf[Object],
        ProjectProperties.versionAction -> VersionActions.reverted
      )
    )
    tx.success()
  })

}


trait ConvertsNodesToProjects {

  def nodeToProject(node:Node):Project = {
    Project(
      UUID.fromString(node.get(ProjectProperties.id).asString()),
      node.get(ProjectProperties.title).asString()
    )
  }

}


