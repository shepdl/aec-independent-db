package projects

import java.util.UUID
import javax.inject.Inject
import com.google.inject.Singleton

import accessControl._
import common.JsonResponses
import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.Future

class ProjectRequest[A](val project:Project, val request:Request[A]) extends WrappedRequest[A](request)
class ProjectAndUserRequest[A](val project:Project, val user:User, val request: Request[A]) extends WrappedRequest[A](request)

@Singleton
class ProjectService @Inject() (authenticationService:AuthenticationService, projectRepo:ProjectRepository) extends JsonResponses {

  val logger = Logger("ProjectService")

  private def privateProjectFilter = new ActionFilter[ProjectRequest] with Results {

    override def filter[A](request: ProjectRequest[A]): Future[Option[Result]] = Future.successful {
      if (request.project.title == null) {
        Some(NotFound(missingObject(request.project.resourceName, request.project.id)))
      } else {
        None
      }
    }

  }

  private def privateProjectAction(projectId:UUID) = new ActionBuilder[ProjectRequest] with ActionTransformer[Request, ProjectRequest] {

    override def transform[A](request: Request[A]): Future[ProjectRequest[A]] = Future.successful {
      val project = projectRepo.getById(projectId).getOrElse(Project(projectId, null))
      new ProjectRequest[A](project, request)
    }

  }

  def ProjectRequiredAction(projectId:UUID) = privateProjectAction(projectId) andThen privateProjectFilter


  private def privateAuthenticatedProjectTransformer(projectId:UUID) = new ActionTransformer[UserRequest, ProjectAndUserRequest] {

    override def transform[A](request: UserRequest[A]): Future[ProjectAndUserRequest[A]] = Future.successful {
      val project = projectRepo.getById(projectId).getOrElse(Project(projectId, null))
      new ProjectAndUserRequest[A](project, request.user, request)
    }

  }

  private def privateAuthenticatedProjectFilter = new ActionFilter[ProjectAndUserRequest] with Results {

    override def filter[A](request: ProjectAndUserRequest[A]): Future[Option[Result]] = Future.successful {
      if (request.project.title == null) {
        Some(NotFound(missingObject(request.project.resourceName, request.project.id)))
      } else {
        None
      }
    }
  }


  def AuthenticatedProjectAction(projectId:UUID) =
    authenticationService.LoginRequiredAction andThen privateAuthenticatedProjectTransformer(projectId) andThen
      privateAuthenticatedProjectFilter


  private def privatePermissionRequiredFilter(permission:String) = new ActionFilter[ProjectAndUserRequest] with Results {

    override def filter[A](request: ProjectAndUserRequest[A]): Future[Option[Result]] = Future.successful {
      val userPermissions = projectRepo.getRightsInProject(request.project, request.user)
      if (!userPermissions.contains(permission)) {
        Some(
          Forbidden(Json.obj(
            "message" -> "You must have a different permission on this project",
            "permission" -> permission,
            "project" -> request.project.id,
            "user" -> request.user.uuid
          ))
        )
      } else {
        None
      }
    }
  }

  private def PermissionOrOwnershipRequiredFilter(permission:String, item:Ownable) = new ActionFilter[ProjectAndUserRequest] with Results {

    override def filter[A](request: ProjectAndUserRequest[A]): Future[Option[Result]] = Future.successful {
      val userPermissions = projectRepo.getRightsInProject(request.project, request.user)
      if (item.owner == request.user || userPermissions.contains(permission)) {
        None
      } else {
        Some(Forbidden(Json.obj(
          "message" -> "You do not have the correct rights to edit this object",
          "permission" -> permission,
          "project" -> request.project.id,
          "user" -> request.user.uuid
        )))
      }
    }

  }

  def PermissionRequiredAction(projectId:UUID, permission:String) = AuthenticatedProjectAction(projectId) andThen
    privatePermissionRequiredFilter(permission)


  def PermissionOrOwnershipRequiredAction(projectId:UUID, item:Ownable, permission: String) = AuthenticatedProjectAction(projectId) andThen
    PermissionOrOwnershipRequiredFilter(permission, item)

}
