define(['underscore'], function (_) {

    var ArrayUtils = {
        remove: function (item) {
            var vertices = this;
            var index = vertices.indexOf(item);
            if (index > -1) {
                vertices.splice(index, 1);
            } else {
                throw "Element missing";
            }
        }, contains: function (item) {
            var vertices = this.getVertices();
            for (var i = 0; i < vertices.length; i++) {
                if (_.isEqual(vertices[i], item)) {
                    return true;
                }
            }
            return false;
        }
    };

    return ArrayUtils;
});
