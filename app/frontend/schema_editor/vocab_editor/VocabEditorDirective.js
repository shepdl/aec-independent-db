define([], function () {
    'use strict';

    var scopeFunction = function (scope, element, attrs) {

        var values = scope.field.values;

        for (var i = 0; i < values.length; i++) {
            values.focused = false;
        }

        var keys = [];
        for (var i in scope.field.values[0]) {
            if (i.charAt(0) !== '$') {
                keys.push(i);
            }
        }

        var newObject = function () {
            var newObj = {};
            newObj[keys[i]] = '';
            return newObj;
        };

        var rowIsEmpty = function (term) {
            return keys.map(function (key) {
                return term[key] === '';
            }).filter(function (v) {
                return !v;
            }).length === 0;
        };

        if (rowIsEmpty(values.slice(values.length - 1)) || values.length === 0) {
            values.push(newObject());
        }

        scope.close = function (field) {
            scope.$close(field);
        };

        scope.keyUp = function (term, index, event) {
            switch (event.key) {
                case 'Backspace':
                    if (rowIsEmpty(term)) {
                        scope.field.values.splice(index, 1);
                    }
                    break;
                case 'ArrowUp':
                    if (index > 0) {
                        term.focused = false;
                        values[index - 1].focused = true;
                    }
                break;
                case 'ArrowDown':
                    if (index < values.length) {
                        term.focused = false;
                        values[index + 1].focused = true;
                    }
                break;
                case 'Enter':
                    if (event.shiftKey) {
                        values.splice(index, 0, newObject());
                    } else {
                        values.splice(index + 1, 0, newObject());
                    }
                break;
            }
        };
    };

    var AECVocabEditorDirective = function () {
        return {
            scope: true,
            templateUrl: '/scripts/schema_editor/vocab_editor/aec-vocab-editor-directive.html',
            link: scopeFunction
        };
    };

    return AECVocabEditorDirective;

});
