define(['angular', './list/TypeListController', './editor/TypeEditorController'], function (angular, TypeListController, TypeEditorController) {
    'use strict';

    var module = angular.module('schema_editor.routes', []);
    module.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/schema_editor/:projectId/list', {
            templateUrl: '/schema_editor/list/root.html',
            controller: TypeListController
        });

        $routeProvider.when('/schema_editor/:projectId/types/:typeId', {
            templateUrl: '/schema_editor/editor/root.html',
            controller: TypeEditorController
        });

    }]);
    return module;

});

