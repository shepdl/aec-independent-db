define (['angular', 'angular-resource', 'angular-bootstrap', './routes', './list/TypeListController', './editor/TypeEditorController', 
        './vocab_editor/VocabEditorDirective', './vocab_editor/FocusableDirective'
    ], function (angular, $resource, _, routes, TypeListController, TypeEditorController, VocabEditorDirective, FocusableDirective) {
    'use strict';

    var module = angular.module('gruntFrontApp.schema_editor', ['ngRoute', 'schema_editor.routes', 'ngResource', 'ui.bootstrap']);
    module.controller('TypeListController', TypeListController);
    module.controller('TypeEditorController', TypeEditorController);

    module.directive('aecVocabEditor', VocabEditorDirective);
    module.directive('aecFocusable', FocusableDirective);
    return module;
});
