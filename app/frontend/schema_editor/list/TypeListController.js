define([], function () {
    'use strict';
    
    var TypeListController = function ($scope, $routeParams, $location, $filter, projectService, typeFactoryService) {
        var typeFactory = null;

        // $scope.project = projectService.get($routeParams.projectId);
        // $scope.dataTypes = $scope.project.types.get();

        $scope.project = null;
        $scope.types = [];
        projectService.get($routeParams.projectId).success(function (project) {
            $scope.project = project;
            typeFactory = typeFactoryService.factoryForProject($scope.project);
            typeFactory.get().success(function (projectTypes) {
                $scope.types = projectTypes.schemas;
                $scope.$digest();
            });
        });


        $scope.add = function () {
            $location.path('/schema_editor/' + $scope.project.id + '/types/create');
        };

        $scope.remove = function (dataType) {
            typeFactory.remove(dataType);
            $filter('filter')($scope.types, {id: "!" + dataType.id});
        };

    };

    TypeListController.$inject = ['$scope', '$routeParams', '$location', '$filter', 'ProjectService', 'TypeFactoryService'];

    return TypeListController;
});

