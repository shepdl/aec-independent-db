define(['angular-bootstrap', 'projectAPI/Type', 'projectAPI/Annotation', 'annotation_editor/EventProxy'], function (_, Type, Annotation, EventProxy) {
    'use strict';
    
    var TypeEditorController = function ($scope, $routeParams, $location, $uibModal, projects, typeFactoryService) {

        var typeFactory = null
        ;

        $scope.project = null;
        $scope.type = null;
        $scope.types = [];

        var types = {};

        $scope.noop = function () {};

        var loadType = function () {
            var typeId = $routeParams.typeId;
            if (typeof typeId === 'undefined' || typeId === 'create') {
                $scope.type = typeFactory.create();
            } else {
                typeFactory.get(typeId).success(function (type) {
                    var scopeType = {
                        id: type.id,
                        appliesTo: type.appliesTo,
                        title: type.title,
                        isControlled: type.isControlled,
                        fields: type.fields.map(function (field) {
                            return {
                                id: field.id,
                                title: field.title,
                                type: types[field.typeId],
                                inputElement: field.inputElement,
                                inputStyle: field.inputStyle,
                                displayElement: field.displayElement,
                                displayStyle: field.displayStyle,
                                isControlled: field.isControlled,
                                values: field.values
                            };
                        })
                    };

                    $scope.type = new Type(scopeType);
                    $scope.$digest();
                });
            }
        };

        projects.get($routeParams.projectId).success(function (projectData) {
            $scope.project = projectData;

            typeFactory = typeFactoryService.factoryForProject($scope.project);
            $scope.annotation = new Annotation({
                schema: typeFactory.create()
            });

            typeFactory.getTypesForProject($scope.project).success(function (projectSchemaList) {
                var scopeTypes = [];
                for (var i = 0; i < projectSchemaList.schemas.length; i++) {
                    var thisType = projectSchemaList.schemas[i];

                    var scopeType = {
                        id: thisType.id,
                        title: thisType.title,
                        fields: thisType.fields
                    };
                    scopeTypes.push(scopeType);
                    types[thisType.id] = scopeType;
                }
                var baseTypes = typeFactory.getBaseTypes();
                for (var i = 0; i < baseTypes.length; i++) {
                    var thisType = baseTypes[i];
                    var scopeType = {
                        id: thisType.id,
                        title: thisType.title,
                        fields: thisType.fields
                    };
                    scopeTypes.push(scopeType);
                    types[thisType.id] = scopeType;
                }
                $scope.types = scopeTypes;
                loadType();
            });

        });

        $scope.addField = function (type) {
            type.createField();
        };

        $scope.delete = function (type, field) {
            type.removeField(field);
        };

        $scope.save = function () {
            var typeToSave = {
                title: $scope.type.title,
                appliesTo: $scope.type.appliesTo,
                isControlled: $scope.type.isControlled,
                fields: $scope.type.fields.map(function (field) {
                    var fieldTypeId = field.type.id;
                    var inputElement = field.inputElement.id || 'input';
                    var displayElement = inputElement;
                    var newField = {
                        title: field.title,
                        typeId: fieldTypeId,
                        inputElement: inputElement,
                        inputStyle: field.inputStyle,
                        displayElement: displayElement,
                        displayStyle: field.displayStyle,
                        isControlled: field.isControlled === 1,
                        values: field.values
                    };
                    if (field.id) {
                        newField.id = field.id;
                    }
                    return newField;
                })
            };
            
            var id = $scope.type.id;

            if (id) {
                typeToSave.id = id;
            }

            typeFactory.save(typeToSave).success(function () {
                $location.path('/projects/' + $routeParams.projectId);
            }).error(function (error) {
                alert('Error saving your new schema :' + error);
            });
        };

        $scope.editCV = function () {
            $location.path('/schema_editor/' + $routeParams.projectId + '/vocab/custom');
        };

        $scope.addExtraFields = function () {
            $scope.subType = project.types.create({
                name: $scope.type.name + "Instance"
            });
        };

        $scope.launchVocabEditor = function (field) {
            var newScope = $scope.$new();
            var field = {
                type: field.type,
                values: field.values
            };
            newScope.field = field;
            $uibModal.open({
                scope: newScope,
                template: '<aec-vocab-editor></aec-vocab-editor>'
            });
        };

    };

    TypeEditorController.$inject = ['$scope', '$routeParams', '$location', '$uibModal', 'ProjectService', 'TypeFactoryService'];

    return TypeEditorController;
});

