define(['jquery'], function ($) {
    'use strict';


    var AECAnnotationListWindow = function () {
        return {
            scope: false,
            controller: 'AnnotationListWindowController',
            link: function (scope, element, attrs) {
                $(element).draggable().resizable();
                // TODO: move this to another controller
            }
        };
    };

    return AECAnnotationListWindow;
});
