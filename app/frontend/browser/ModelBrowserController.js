define([], function () {
    'use strict';

    var ModelBrowserController = function ($scope, $routeParams, artifactRepo, projectRepo) {

        $scope.artifact = null;
        $scope.project = null;

        var artifactId = $routeParams.artifactId,
            projectId = $routeParams.projectId
        ;

        artifactRepo.getById(artifactId).then(function (response) {
            $scope.artifact = response.data;
        });

        $scope.windows = {};
        var windowCounter = 0;

        projectRepo.getById(projectId).then(function (response) {
            $scope.project = response.data;
            windowCounter += 1;
            windows['window-' + windowCounter] = $scope.project;
        });
        
        // set up Scene using directive
        //

    };

    ModelBrowserController.$inject = ['$scope'];

    return ModelBrowserController;
});
