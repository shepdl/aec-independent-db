define([], function () {
    'use strict';

    var AnnotationListController = function ($scope, annotationRepo) {
        var searchParameters = {};

        $scope.annotations = [];

        var refreshAnnotationList = function () {
            annotationRepo.search(searchParameters).then(function (response) {
                $scope.annotations = response.data;
            });
        };

        refreshAnnotationList();


        $scope.delete = function (annotation) {
            annotationRepo.delete(annotation);
        };


    };

    AnnotationListController.$inject = ['$scope', 'annotationRepo'];

    return AnnotationListController;
});
