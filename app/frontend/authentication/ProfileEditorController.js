define([], function () {
    'use strict';

    var ProfileEditorController = function ($scope, authService) {

        authService.whoAmI().then(function (response) {
            $scope.name = response.data.name;
            $scope.email = response.data.email;
            $scope.userId = response.data.id;
        });

        $scope.save = function () {
            authService.updateUser({
                id:  $scope.userId,
                name: $scope.name,
                email: $scope.email
            }).then(function (response) {
                $scope.$close();
            });
        };

    };

    ProfileEditorController.$inject = ['$scope', 
        'gruntFrontApp.authentication.AuthenticationService'
    ];

    return ProfileEditorController;
});
