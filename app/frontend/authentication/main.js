define(['angular', './AuthenticationService', './AuthenticationController', './JoinWindowController', 'angular-ui-bootstrap'], function (ng, AuthenticationService, AuthenticationController, JoinWindowController, uiBootstrap) {
    'use strict';

    var module = ng.module('gruntFrontApp.authentication', ['ui.bootstrap.modal', 'ngRoute']),
        ngInjector = ng.injector(['ng'])
    ;

    module.controller('gruntFrontApp.authentication.AuthenticationController', AuthenticationController);

    module.factory('gruntFrontApp.authentication.AuthenticationService', function () {
        return new AuthenticationService(ngInjector.get('$http'));
    });

    module.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/join', {
            templateUrl: '/authentication/join.html',
            controller: JoinWindowController
        });
    }]);
    
    return module;

});
