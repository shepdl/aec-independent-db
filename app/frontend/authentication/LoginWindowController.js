define([], function () {
    'use strict';

    var LoginWindowController = function ($scope, authService) {

        $scope.message = '';

        $scope.login = function () {

            authService.login($scope.username, $scope.password).then(function (response) {
                switch (response.status) {
                    case 200:
                        $scope.$close();
                        break;
                    default:
                        console.log(response);
                }
            }, function (error) {
                switch (error.status) {
                    case 404:
                        $scope.message = 'Incorrect';
                        $scope.$digest();
                        break;
                    default:
                        console.log(response);
                }
            });
        };
    };

    LoginWindowController.$inject = ['$scope',
        'gruntFrontApp.authentication.AuthenticationService'
    ];

    return LoginWindowController;

});
