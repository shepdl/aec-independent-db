define(['./LoginWindowController', './JoinWindowController', './ProfileEditorController'], function (LoginWindowController, JoinWindowController, ProfileEditorController) {
    'use strict';

    var AuthenticationController = function ($scope, $uibModal, authService) {

        $scope.isOpen = false;

        $scope.$watch('isOpen', function () {
            console.log('Toggled isOpen: ' + $scope.isOpen)
        });


        $scope.showLoginWindow = function () {
            var modal = $uibModal.open({
                templateUrl: '/authentication/login_window.html',
                controller: LoginWindowController
            });
            modal.result.then(function (response) {
                authService.whoAmI().then(function (response) {
                    $scope.user = response.data;
                });
            });
        };

        $scope.showJoinWindow = function () {
            var modal = $uibModal.open({
                templateUrl: '/authentication/join.html',
                controller: JoinWindowController
            });

            modal.result.then(function (response) {
                console.log(response);
            });
        };

        $scope.showProfileEditor = function () {
            var modal = $uibModal.open({
                templateUrl: '/authentication/profile_editor.html',
                controller: ProfileEditorController
            });
        };

        authService.whoAmI().then(function (response) {
            $scope.user = response.data;
            $scope.$digest();
        });

        $scope.toggle = function () {
            $scope.isOpen = !$scope.isOpen;
        };

    };

    AuthenticationController.$inject = ['$scope', '$uibModal', 'gruntFrontApp.authentication.AuthenticationService'];

    return AuthenticationController;
});
