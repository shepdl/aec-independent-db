define([], function () {
    'use strict';
    
    var JoinWindowController = function ($scope, authService) {

        $scope.join = function () {
            authService.join(
                $scope.name, $scope.email, $scope.password
            ).then(function (response) {
                $scope.$close();
            }, function (error) {
                switch (error.code) {
                    case 400:
                        console.log(error);
                        break;
                    default:
                        console.log(error);
                        break;
                }
            });
        };
    };


    JoinWindowController.$inject = ['$scope', 'gruntFrontApp.authentication.AuthenticationService'];

    return JoinWindowController;
});
