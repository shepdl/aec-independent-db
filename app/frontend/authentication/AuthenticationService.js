define(['../projectAPI/API', 'jquery'], function (API, $) {
    'use strict';

    var AuthenticationService = function ($http) {

        var currentUser = {};

        var whoAmI = function () {
            return $http({
                method: 'GET',
                url: API.URL('/users/whoAmI')
            })
            // .then(function (response) {
            //     console.log(response);
            //     currentUser = response.data;
            // });
        };

        var postHeader = {
            'Content-Type' : 'application/x-www-form-urlencoded'
        };

        this.currentUser = function () {
            return currentUser;
        };

        this.login = function (username, password) {
            return $http({
                method: 'POST',
                url: API.URL('/users/login'),
                headers: postHeader,
                data: $.param({
                    email: username,
                    password: password
                })
            });
        };

        this.join = function (name, email, password) {
            return $http({
                method: 'POST',
                url: API.URL('/users'),
                headers: postHeader,
                data: $.param({
                    name: name,
                    email: email,
                    password: password
                })
            });
        };

        this.updateUser = function (user) {
            return $http({
                method: 'PUT',
                url: API.URL('/users/' + user.id),
                data : user
            });
        };

        this.getGroups = function () {
            return $http({
                method: 'GET',
                url: API.URL('/groups/myGroups')
            });
        };

        this.whoAmI = whoAmI;
    };

    return AuthenticationService;
});
