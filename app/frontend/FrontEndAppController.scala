package frontend

import play.api.mvc.{Action, Controller}

/**
  * Created by daveshepard on 3/23/17.
  */
class FrontEndAppController extends Controller {

  def application = Action {
    Ok(
      views.html.application()
    )
  }

}
