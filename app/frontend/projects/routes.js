define(['angular', './ProjectListController', './edit/ProjectEditController', './bulkImport/BulkImportController'], function (angular, ProjectListController, ProjectEditController, BulkImportController) {
    'use strict';

    var module = angular.module('projects.routes', []);
    module.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: '/projects/root.html',
            controller: ProjectListController
        });
        $routeProvider.when('/projects', {
            templateUrl: '/projects/root.html',
            controller: ProjectListController
        });
        $routeProvider.when('/projects/:projectId', {
            templateUrl: '/projects/edit/root.html',
            controller: ProjectEditController
        });
        $routeProvider.when('/projects/:projectId/import/:schemaTitle/:schemaId', {
            templateUrl: '/projects/bulkImport/root.html',
            controller: BulkImportController
        });
    }]);
    return module;

});


