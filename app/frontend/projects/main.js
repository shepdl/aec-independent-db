define (['angular',
        './ProjectListController',
        './edit/ProjectSchemaListController',
        './edit/ArtifactListController',
        'angular-bootstrap', 'ng-file-upload',
        './routes', 'angular-resource', 
], function (angular, ProjectListController, ProjectSchemaListController,
    ArtifactListController
    ) {
    'use strict';

    var module = angular.module('gruntFrontApp.projects', [
        'ngRoute', 'projects.routes', 'ngResource', 
        'ngFileUpload', 'gruntFrontApp.api'
    ]);
    module.controller('ProjectListController', ProjectListController);
    
    module.controller('ProjectSchemaListController', ProjectSchemaListController);
    module.controller('ArtifactListController', ArtifactListController);

    return module;
});

