define([], function () {
    'use strict';

    var ProjectController = function ($scope, $filter, projectService) {

        $scope.projects = [];
        
        projectService.get().then(function (projects) {
            var projects = projects.data.projects;
            var newProjects = [];
            for (var i = 0; i < projects.length; i++) {
                var project = projects[i];
                var permissions = {};
                for (var j = 0; j < project.permissions.length; j++) {
                    permissions[project.permissions[j]] = true;
                }
                newProjects.push({
                    id: project.id,
                    title: project.title,
                    editing: false,
                    permissions: permissions,
                    alerts: []
                });
            }
            $scope.projects = newProjects;
            $scope.$digest();
        });

        $scope.save = function (project) {
            projectService.save(project).success(function (newProject) {
                project.editing = false;
                project.id = newProject.id || newProject.data.id;
                $scope.$digest();
            }).error(function (error) {
                project.alerts.push({
                    message: error.message,
                    permission: error.permission,
                    type: 'danger'
                });
                $scope.$digest();
            });
        };

        $scope.edit = function (project) {
            project.editing = true;
        };

        $scope.delete = function (project) {
            projectService.delete(project).then(function (response) {
                $scope.projects = $filter('filter')($scope.projects, {id: "!" + project.id});
            });
        };

        $scope.addProject = function () {
            $scope.projects.push({
                id : null,
                editing: true,
                name: ''
            });
        };

        $scope.stopEditing = function (project) {
            project.editing = false;
        };

    };

    ProjectController.$inject = ['$scope', '$filter', 'ProjectService', 'EndorsementService'];

    return ProjectController;
});
