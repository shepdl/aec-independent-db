define(['xlsx', 'projectAPI/Annotation'], function (XLSX, Annotation) {
    'use strict';

    var BulkImportController = [
            '$scope', '$routeParams', 'Upload', 'AnnotationFactoryService' ,
        function ($scope, $routeParams, Upload, AnnotationFactoryService) {

        var annotationRepo = AnnotationFactoryService.factoryForProjectAndModel(
            {id: $routeParams.projectId},
            {id: $routeParams.artifactId}
        );

        $scope.schema = {
            id: $routeParams.schemaId,
            title: $routeParams.schemaTitle
        };

        $scope.spreadsheetFile = null;

        $scope.columnHeaders = [];
        $scope.dataItems = [];

        $scope.parseFile = function (file) {
            var reader = new FileReader();
            reader.onload = function (result) {
                var spreadsheet = XLSX.read(result.target.result, { type: 'binary'});
                var jsonSheet = XLSX.utils.sheet_to_json(spreadsheet.Sheets[spreadsheet.SheetNames[0]]);
                $scope.columnHeaders = [];
                $scope.dataItems = [];

                // iterate through this and place in a table on the screen
                for (var k in jsonSheet[0]) {
                    $scope.columnHeaders.push(k);
                }
                $scope.dataItems = jsonSheet;
                $scope.$digest();
            };
            reader.readAsBinaryString($scope.spreadsheetFile);
        };

        $scope.commit = function () {
            var itemsToCommit = [];
            for (var i = 0; i < $scope.dataItems.length; i++) {
                var item = $scope.dataItems[i],
                    entityData = {}
                ;
                for (var key in item) {
                    if (key !== 'Model ID' || key !== 'Model Title') {
                        entityData[key] = item[key];
                    }
                }
                var annotation = new Annotation({
                    id: 'u1234',
                    entity:  {
                        schema: {
                            id: $routeParams.schemaId,
                            title: $routeParams.schemaTitle
                        },
                        data: entityData
                    }
                });
                annotation.project = {
                    id: $routeParams.projectId,
                };
                annotation.model = {
                    id: item['Model ID'],
                };
                itemsToCommit.push(annotation);
            }

            saveAnnotation(itemsToCommit);
        };

        var saveAnnotation = function (annotations) {
            var annotationToSave = annotations.shift();
            annotationRepo.save(annotationToSave).success(function (message) {
                if (annotations.length > 0) {
                    annotationToSave.saved = true;
                    saveAnnotation(annotations);
                } else {
                    alert('Annotations successfully saved');
                }
            }).error(function (message) {
                console.error(message);
            });
        };

    }
    ];


    return BulkImportController;
});
