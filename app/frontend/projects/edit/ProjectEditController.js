define(['xlsx', 'file-saver/FileSaver'], function (XLSX, FileSaver) {
    'use strict';

    var ProjectEditController = function ($scope, $location, $filter, $routeParams, Upload, 
                                          projectService, artifactFactoryService, 
                                          modelFactoryService, typeFactoryService, 
                                          endorsementRepo, authService, $uibModal) {

        var artifactFactory,
            typeFactory
        ;

        $scope.project = {
            id: $routeParams.projectId,
            title: '',
            artifacts: [],
            schemas: []
        };

        $scope.bulkEditMode = false;

        $scope.toggleBulkEditMode = function (schema, enabled) {
            $scope.bulkEditSchema = schema;
            $scope.bulkEditMode = enabled;
        };

        function s2ab(s) {
            if(typeof ArrayBuffer !== 'undefined') {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            } else {
                var buf = new Array(s.length);
                for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
        }

        $scope.downloadSpreadsheet = function () {
            var selectedModels = [];
            var artifacts = $scope.project.artifacts;
            var workbook = XLSX.utils.book_new();
            var schema = $scope.bulkEditSchema;
            var columnHeadings = schema.fields.map(function (field) {
                return field.title;
            });
            var sheetData = [
                ['Model ID', 'Model Title',].concat(columnHeadings),
            ];
            for (var i = 0; i < artifacts.length; i++) {
                for (var j = 0; j < artifacts[i].models.length; j++) {
                    var model = artifacts[i].models[j];
                    if (model.checked) {
                        sheetData.push([
                            model.id,
                            model.title
                        ]);
                    }
                }
            }
            XLSX.utils.book_append_sheet(workbook, XLSX.utils.aoa_to_sheet(sheetData), schema.title);
            var asBinary = XLSX.write(workbook, { bookType: 'xlsx', bokSST: true, type: 'binary'});
            FileSaver.saveAs(new Blob([s2ab(asBinary)], { type: 'application/octet-stream' }), schema.title + '.xlsx');
            $scope.toggleBulkEditMode(null, false);
        };

        authService.whoAmI().then(function (response) {
            $scope.user = response.data;
            $scope.$digest();
        }, function (error) {
            $scope.$digest();
        });

        $scope.userGroups = [];

        authService.getGroups().then(function (response) {
            var groups = [];
            for (var i = 0; i < response.data.groups.length; i++) {
                var group = response.data.groups[i].group;
                groups.push({
                    id: group.id,
                    name: group.name
                });
            }
            $scope.userGroups = groups;
            loadProject();
        }, function (error) {
            loadProject();
        });

        var loadSchemas = function () {
            console.info($scope.project);
            typeFactory = typeFactoryService.factoryForProject($scope.project);

            typeFactory.getTypesForProject($scope.project).success(function (projectSchemaList) {
                var scopeTypes = [];
                for (var i = 0; i < projectSchemaList.schemas.length; i++) {
                    var thisType = projectSchemaList.schemas[i];
                    scopeTypes.push({
                        id: thisType.id,
                        title: thisType.title,
                        fields: thisType.fields
                    });
                }
                $scope.project.schemas = scopeTypes;
                $scope.$digest();
            }).error(function (error) {
                console.log(error);
                alert("Error: " + error.error);
            });
        };


        var loadProject = function () {
            projectService.get($scope.project.id).success(function (data) {
                var project = data;
                var idsOfGroupsThatHaveApprovedThisProject = project.endorsements.map(function (approval) {
                    return approval.group.id;
                });
                var groupsThatMayApproveThisProject = $scope.userGroups.filter(function (userGroup) {
                    return idsOfGroupsThatHaveApprovedThisProject.indexOf(userGroup.id) === -1;
                });
                project.groupsThatHaveNotApprovedThis = groupsThatMayApproveThisProject;
                $scope.project = project;
                $scope.project.artifacts = [];
                $scope.project.schemas = [];

                $scope.title = project.title;

                artifactFactory = artifactFactoryService.factoryForProject(project);
                artifactFactory.getForProject(project).success(function (data) {
                    var artifacts = data;
                    for (var i = 0; i < artifacts.length; i++) {
                        var artifact = artifacts[i];
                        var renderArtifact = {
                            id: artifact.id,
                            title: artifact.title,
                            editing: false,
                            models: artifact.models.map(function (model) {
                                return {
                                    id: model.id,
                                    title: model.title,
                                    files: model.files.map(function (filename) {
                                        return {name: filename};
                                    }),
                                    previewImage: model.previewImage,
                                    checked: false
                                };
                            })
                        };
                        renderArtifact.models.push({ title: '', files: [], needsUpload: true });
                        $scope.project.artifacts.push(renderArtifact);
                    }
                    $scope.$digest();
                });

            }).error(function (message) {
                alert('Sorry: project not found');
            }).then(loadSchemas);
        };

       $scope.uploadFile = function (project, artifact, model) {
           Upload.upload({
               url: artifactFactory.uploadUrl(artifact),
               data: {
                   title: model.title,
                   files: model.files,
                   format: model.format
               }
           }).then(function (response) {
               var data = response.data || response;
               model.id = data.model.id;
               model.title = data.model.title;
               model.files = data.model.files.map(function (modelFile) {
                   return {
                       name: modelFile.filename
                   };
               });
               artifact.models.push({ filename: '', title: '', needsUpload: true });
               model.needsUpload = false;
               model.success = true;
           }, function (error) {
               model.error = error;
           }, function (event) {
               var progressPercentage = parseInt(100.0 * event.loaded / event.total);
               console.log('progress:' + progressPercentage + '%');
           });
       };

        $scope.addArtifact = function (project) {
            var newArtifact = artifactFactory.createBlank();
            newArtifact.editing = true;
            newArtifact.models.push({ title : '', files: [], needsUpload: true});
            project.artifacts.push(newArtifact);
        };

        $scope.showListOfArtifacts = function (project) {
            var scope = $scope.$new();
            scope.project = project;
            var dialog = $uibModal.open({
                scope: scope,
                templateUrl: '/projects/edit/aec-artifact-list.html',
                controller: 'ArtifactListController'
                // template: '<aec-artifact-list project-id="\'' + $scope.project.id + '\'"></aec-artifact-list>'
            });
            console.log(dialog);
            dialog.close(function () {
                console.log('Modal closed');
            });
        };

        $scope.saveArtifact = function (project, artifact) {
            artifactFactory.saveForProject(artifact, project).success(function (newArtifactData) {
                artifact.id = newArtifactData.id;
                artifact.editing = false;
                artifact.status = 'Saved';
            }).error(function () {
                artifact.status = 'Error';
            });
        };

        $scope.deleteModel = function (artifact, model) {
            var modelFactory = modelFactoryService.forProjectAndArtifact($scope.project, artifact);
            modelFactory.delete(artifact, model).success(function (message) {
                artifact.models  = artifact.models.filter(function (newModel) {
                    return newModel != model;
                });
                $scope.$digest();
                alert(model.title + ' deleted');
            }).error(function (error) {
                console.debug(error);
            });

        };

        $scope.deleteArtifact = function (artifact, project) {
            artifactFactory.deleteForProject(artifact, project).success(function () {
                project.artifacts = project.artifacts.filter(function (a) {
                    return a != artifact;
                });
            }).error(function (error) {
                alert("Error deleting artifact " + artifact.title);
            });
        }


        // Schema Editing
        $scope.addSchema = function () {
            $location.path('/schema_editor/' + $scope.project.id + '/types/create');
        };

        var showSchemaMessage = function (message) {
            $scope.schemaMessage = message;
        };

        var showSchemaError = function (error) {
            $scope.schemaError = error;
        };


        $scope.loadSchema = function (schema) {
            $location.path('/schema_editor/' + $scope.project.id + '/types/' + schema.id);
        };

        $scope.removeSchema = function (dataType) {
            typeFactory.remove(dataType).success(function () {
                $filter('filter')($scope.project.schemas, {id: "!" + dataType.id});
                $scope.$digest();
            }).error(function (error) {
                showSchemaError(error);
            });
        };

        $scope.disapproveProject = function (endorsement, group) {
            var result = confirm('Are you sure you want to remove the approval of ' 
                    + group.name + '?');
            if (result) {
                endorsementRepo.removeEndorsement(endorsement.id, group).then(function (response) {
                    loadProject();
                }, function (error) {
                    console.log(error);
                });
            }
        };

        $scope.approveProjectBy = function (project, group) {
            endorsementRepo.endorse(project, group).then(function (response) {
                loadProject();
            }, function (error) {
                console.log(error);
            });

        };
    };

    ProjectEditController.$inject = ['$scope', '$location', '$filter', '$routeParams', 'Upload', 'ProjectService', 'ArtifactFactoryService', 'ModelFactory', 'TypeFactoryService', 'EndorsementService', 'gruntFrontApp.authentication.AuthenticationService', '$uibModal'
    ];

    return ProjectEditController;

});
