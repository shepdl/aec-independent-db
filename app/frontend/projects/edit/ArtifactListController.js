define([], function () {
    'use strict';

    var ArtifactListController = function ($scope, ArtifactFactoryService) {
        var project = $scope.$parent.project;
        var artifactFactory = ArtifactFactoryService.factoryForProject(project);

        $scope.artifacts = [];

        $scope.pageNo = 1;
        $scope.rpp = 12;

        console.log('User:');
        console.log($scope.user);

        var refreshArtifactList = function () {
            artifactFactory.listArtifactsNotInProject(project, $scope.pageNo, $scope.rpp).then(function (response) {
                $scope.artifacts = response.data.artifacts;
                $scope.$digest();
            }, function (error) {
                console.log(error);
            });
        }

        refreshArtifactList();

        $scope.addArtifact = function (artifact) {
            artifactFactory.addToProject(artifact, project).then(function (response) {
                refreshArtifactList();
            }, function (error) {
                console.log(error);
            });
        };

        $scope.nextPage = function () {
            if ($scope.artifacts.length < $scope.rpp) {
                $scope.pageNo += 1;
                refreshArtifactList();
            }
        };

        $scope.prevPage = function () {
            if ($scope.pageNo <= 1) {
                $scope.pageNo -= 1;
                refreshArtifactList();
            }
        };

    };

    ArtifactListController.$inject = ['$scope', 'ArtifactFactoryService'];

    return ArtifactListController;
});

