define([], function () {
    'use strict';
    
    var GroupViewController = function ($scope, groupRepo, authenticationService, $routeParams, endorsementRepo) {
        var groupId = $routeParams.groupId;

        groupRepo.getById(groupId).then(function (response) {
            $scope.group = response.data;
            $scope.membership = response.data.membership;
            endorsementRepo.getEndorsementsByGroup($scope.group).then(function (response) {
                var endorsements = response.data.endorsements;
                for (var i = 0; i < endorsements.length; i++) {
                    var item = endorsements[i];
                    if (item.item.type === 'Project') {
                        $scope.endorsedProjects.push(item.item);
                    } else {
                        $scope.endorsedOtherItems.push(item.item);
                    }
                }
                $scope.$digest();
            });
        });

       var user = authenticationService.currentUser();
       $scope.user = user;


        $scope.requestMembership = function (group) {
            groupRepo.requestMembership(group, user).then(function (response) {
                $scope.membership = 'REQUESTED';
            });
        };

        $scope.edit = function (group) {
            group.editing = true;
            group.oldName = group.name;
            group.oldUrl = group.homepage;
        };

        $scope.endorsedProjects = [];
        $scope.endorsedOtherItems = [];


        $scope.cancelEditing = function (group) {
            group.name = group.oldName;
            group.homepage = group.oldUrl;
            group.editing = false;
        };

        $scope.save = function (group) {
            group.message = 'saved';
            groupRepo.save(group).then(function (message) {
                group.editing = false;
                delete group.oldName;
                delete group.oldUrl;
                $scope.$digest();
            });
        };

        $scope.removeUserFromGroup = function (user, group) {

            groupRepo.removeUserFromGroup(user, group).then(function (response) {
                group.users  = group.users.filter(function (userToCompare) {
                    return userToCompare.id !== user.id;
                });
            });

        };

        $scope.addUserToGroup = function (user, group) {
            groupRepo.addUserToGroup(user, group).then(function (response) {
                if (response.data.message === 'ADDED') {
                    user.message = 'added';
                    group.users.push(user);
                } else {
                    console.log(error);
                    alert('Sorry; there was an error and the user could not be added to the group.');
                }
            });
        };

        $scope.approveUserMembershipRequest = function (user, group) {
            groupRepo.addUserToGroup(user, group).then(function (response) {
                group.users.push(user);
                $scope.group.userRequests = $scope.group.userRequests.filter(function (userRequest) {
                    return userRequest.id !== user.id;
                });
            });
        };

        $scope.denyUserMembershipRequest = function (user, group) {
            groupRepo.denyUserMembershipRequest(user, group).then(function () {
                $scope.group.userRequests = $scope.group.userRequests.filter(function (userRequest) {
                    return userRequest.id !== user.id;
                });
            });
        };

        $scope.removeEndorsement = function (item, group) {
            console.log(item);
            endorsementRepo.removeEndorsement(item.id, group);
        };

    };

    GroupViewController.$inject = [
        '$scope', 
        'gruntFrontApp.groups.GroupRepository', 
        'gruntFrontApp.authentication.AuthenticationService',
        '$routeParams',
        'EndorsementService'
    ];

    return GroupViewController;

});
