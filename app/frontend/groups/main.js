define(['angular', './GroupListController', './GroupRepository', './GroupViewController'], function (ng, GroupListController, GroupRepository, GroupViewController) {
    'use strict';

    var module = ng.module('gruntFrontApp.groups', ['ngRoute', 'gruntFrontApp.api', 'ui.bootstrap', 'ngMessages']),
        ngInjector = ng.injector(['ng'])
    ;

    module.controller('gruntFrontApp.groups.GroupListController', GroupListController);
    module.factory('gruntFrontApp.groups.GroupRepository', function () {
        return new GroupRepository(ngInjector.get('$http'));
    });

    module.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/groups', {
            templateUrl: '/groups/group_list.html',
            controller: GroupListController
        });
        $routeProvider.when('/groups/:groupId', {
            templateUrl: '/groups/group_view.html',
            controller: GroupViewController
        });
    }]);

    return module;
});
