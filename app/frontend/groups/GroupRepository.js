define(['../projectAPI/API'], function (API) {
    'use strict';

    var GroupRepository = function ($http) {

        var postTransformRequest = function (obj) {
            var str = [];
            for (var p in obj) {
                str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
            }
            return str.join('&');
        };

        var postHeader = {
            'Content-Type' : 'application/x-www-form-urlencoded'
        };

        this.save = function (group) {
            var payload = {
                name: group.name,
                homepage: group.homepage,
                userIds : group.users.map(function (user) {
                    return user.id;
                }).join(',')
            };

            if (typeof group.id !== 'undefined') {
                payload.id = group.id;
                return $http({
                    method: 'PUT',
                    url: API.URL('/groups/' + group.id),
                    data : payload
                });
            } else {
                return $http({
                    method: 'POST',
                    url: API.URL('/groups'),
                    transformRequest: postTransformRequest,
                    headers: postHeader,
                    data: payload
                });
            }
        };


        this.getById = function (id) {
            return $http({
                method: 'GET',
                url : API.URL('/groups/' + id)
            });
        };

        this.list = function (inOptions) {
            var options = inOptions || {},
                pageIndex = options.page || 0,
                resultCount = options.count || 10
            ;

            return $http({
                method: 'GET',
                url : API.URL('/groups?page=' + pageIndex + '&rpp=' + resultCount)
            });
        };

        this.delete = function (id) {
            return $http({
                method: 'DELETE',
                url: API.URL('/groups/' + id)
            });
        };

        this.addUserToGroup = function (user, group) {
            return $http({
                method: 'POST',
                url: API.URL('/groups/' + group.id + '/members'),
                headers: postHeader,
                data: postTransformRequest({
                    userId: user.id,
                    roles: 'MEMBER'
                })
            });
        };

        this.removeUserFromGroup = function (user, group) {
            return $http({
                method: 'DELETE',
                url: API.URL('/groups/' + group.id + '/members/' + user.id)
            });
        };

        this.requestMembership = function (user, group) {
            return $http({
                method: 'POST',
                url: API.URL('/groups/' + group.id + '/membership_requests'),
                data: postTransformRequest,
                headers: postHeader
            });
        };

        this.getUsersNotInGroup = function (group) {
            return $http({
                method: 'GET',
                url: API.URL('/groups/' + group.id + '/non_members')
            });
        };

        this.approveUserMembershipRequest = function (group, user) {
            return $http({
                method: 'PUT',
                url: API.URL('/groups/' + group.id + '/members/' + user.id),
                data: {
                    status: 'MEMBER'
                }
            });
        };

        this.denyUserMembershipRequest = function (group, user) {
            return $http({
                method: 'PUT',
                url: API.URL('/groups/' + group.id + '/members/' + user.id),
                data: {
                    status: 'DENIED'
                }
            });
        };

    };

    GroupRepository.$inject = ['$http'];

    return GroupRepository;
});
