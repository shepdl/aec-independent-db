define([], function () {
    'use strict';
    
    var UserAddDialogController = function ($scope, groupRepo) {
        $scope.users = [];

        var loadUsers = function () {
            groupRepo.getUsersNotInGroup($scope.group).then(function (response) {
                $scope.users = response.data.users;
            });
        }
        
        $scope.addUser = function (user) {
            groupRepo.addUserToGroup(user, $scope.group).then(function (response) {
                console.log(response);
                loadUsers();
            });
        };

        loadUsers();
    };

    UserAddDialogController.$inject = [
       '$scope',
       'gruntFrontApp.groups.GroupRepository' 
    ];

    return UserAddDialogController;
});
