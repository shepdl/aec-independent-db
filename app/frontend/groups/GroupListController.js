define(['./UserAddDialogController'], function (UserAddDialogController) {
    'use strict';

    var GroupListController = function ($scope, $uibModal, groupRepo) {
        $scope.groups = [ ];

        groupRepo.list({page: 0, resultsPerPage: 10}).then(function (result) {
            if (result.data) {
                $scope.groups = result.data.groups;
                $scope.$digest();
            } else {
                alert('Error loading groups');
            }
        });

        $scope.addGroup = function () {
            $scope.groups.push({
                name: '',
                homepage: '',
                users: [],
                editing: true
            });
        };

        $scope.cancelEditing = function (group) {
            group.name = group.oldName;
            group.homepage = group.oldUrl;
            delete group.oldName;
            delete group.oldUrl;
            group.editing = false;
        };

        $scope.usersNotInGroup = [];

        $scope.showUserPaneForGroup = function (group) {
            // TODO: show user pane here
            
            var scope = $scope.$new();
            scope.group = group;
            var modal = $uibModal.open({
                templateUrl: '/groups/user_add_dialog.html',
                controller: UserAddDialogController,
                scope: scope
            });

            modal.result.then(function (response) {
                console.log(response);
            });
        };

        $scope.edit = function (group) {
            group.editing = true;
            group.oldName = group.name;
            group.oldUrl = group.homepage;
        };

        $scope.save = function (group) {
            group.message = 'saved';
            groupRepo.save(group).then(function (message) {
                console.log(message);
            });
            group.editing = false;
        };

        $scope.removeUserFromGroup = function (user, group) {
            groupRepo.removeUserFromGroup(user, group).then(function (response) {
                if (response.data.message === 'User removed') {
                    group.users = group.users.filter(function (userToCompare) {
                        return userToCompare.id !== user.id;
                    });
                    $scope.$digest();
                } else {
                    alert('Could not remove user from group.');
                }
            });

        };

        $scope.addUserToGroup = function (user, group) {
            groupRepo.addUserToGroup(user, group).then(function (response) {
                if (response.data.message === 'SUCCESS') {
                    user.message = 'added';
                    group.users.push(user);
                    $scope.usersNotInGroup = $scope.usersNotInGroup.filter(function (comparingUser) {
                        return comparingUser.id !== user.id;
                    });
                } else {
                    alert('There was a problem adding the user to the group.');
                }
            });
        };

        $scope.delete = function (group) {
            groupRepo.delete(group.id).then(function (response) {
                if (response.data.message === 'SUCCESS') {
                    $scope.groups = $scope.groups.filter(function (groupToCompare) {
                        return groupToCompare.id !== group.id;
                    });
                } else {
                    alert('There was an error deleting this group.');
                }
            });
        };
    };

    GroupListController.$inject = ['$scope', '$uibModal', 'gruntFrontApp.groups.GroupRepository'];

    return GroupListController;

});
