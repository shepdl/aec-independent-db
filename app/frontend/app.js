/*jshint unused: vars */
define(['angular',
       'angular-messages', 'angular-dragdrop',
    'angular-route', 'angular-cookies', 'angular-sanitize', 'angular-touch',
    'annotation_editor/main',
    'schema_editor/main', 'angular-bootstrap', 'projectAPI/main', 'projects/main', 'mag/main', 'groups/main', 'authentication/main'
]/*deps*/, function (angular)/*invoke*/ {
  'use strict';

  /**
   * @ngdoc overview
   * @name gruntFrontApp
   * @description
   * # gruntFrontApp
   *
   * Main module of the application.
   */
  return angular
    .module('gruntFrontApp', [
      'gruntFrontApp.annotation_editor',
      'gruntFrontApp.schema_editor',
      'gruntFrontApp.api',
      'gruntFrontApp.projects',
      'gruntFrontApp.mag',
      'gruntFrontApp.authentication',
      'gruntFrontApp.groups',

        'ih.tpl',

/*angJSDeps*/
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ngAnimate',
    'ngTouch',
    'ngDragDrop',
    'ngMessages',

    'ui.bootstrap'

  ])
    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider
        .otherwise({
          redirectTo: '/'
        });
    }]);
});
