define(['jquery', 'projectAPI/Annotation', 'projectAPI/AnnotationCollection'], function ($, Annotation, AnnotationCollection) {
    'use strict';

    var MagController = function ($scope, TypeFactoryService, SearchService) {

        var typeFactory = TypeFactoryService.genericFactory();

        $scope.schemas = {};

        typeFactory.get().success(function (types) {
            var schemas = {};
            for (var i = 0; i < types.length; i++) {
                var type = types[i];
                schemas[type.title] = type;                
            }
            $scope.schemas = schemas;
            $scope.$digest();
        }).error(function (error) {
            console.log(error);
        });


        $scope.facets = [{
        }];


        var localIdCounter = 0;

        var createSearch = function () {
            return { 
                id: localIdCounter,
                message: {
                    text : '',
                    type : '',
                },
                color: "#00FF00",
                facets: [
                    {
                        type: {},
                        values: {}
                    }
                ],
                annotations: {
                    "{modelId}" : [
                        { id: 0, /* extra data */ }
                    ]
                }
            };
        }
        
        $scope.searches = { localIdCounter: createSearch() };


        $scope.models = {};

        $scope.addFacet = function (search, typeName) {
            var type = $scope.schemas[typeName];
            var values = {};
            for (var i = 0; i < type.fields.length; i++) {
                values[type.fields[i].title] = '';
            }
            search.facets.push({
                type: type,
                values: values
            });
        };

        $scope.doSearch = function (search) {
            SearchService.search(search.facets).success(function (result) {
                // TODO: This is why the results never change!
                // TODO: update color of search results here
                // TODO: record which search each object comes from
                // TODO: use the search to choose to preserve or remove each annotation
                var results = result.result;

                if (results.length === 0) {
                    for (var id in $scope.models) {
                        var model = $scope.models[id];
                        // delete model.searchResults[search.id];
                        model.searchResults[12000] = {};
                        delete model.searchResults[search.id];
                        /*
                        if ($.isEmptyObject(model.searchResults)) {
                            delete $scope.models[id];
                        }
                        */
                    }
                }

                for (var i = 0; i < results.length; i++) {
                    var result = results[i];

                    var newModel = $scope.models[result.model.id];
                    if (typeof $scope.models[result.model.id] === 'undefined') {
                        newModel = {
                            model: result.model,
                            modelURL: '',
                            textureURL: '',
                            searchResults: {},
                        };

                    }
                    
                    var searchResult = {
                        id: search.id,
                        search: search,
                        annotations: new AnnotationCollection()
                    };

                    for (var j = 0; j < result.annotations.length; j++) {
                        var annotation = new Annotation(result.annotations[j]);
                        searchResult.annotations.push(annotation);
                    }
                    newModel.searchResults[search.id] = searchResult;
                    $scope.models[result.model.id] = newModel;
                }
                $scope.$digest();

            }).error(function (error) {
                search.message = {
                    'text' : 'No results found',
                    'type' : 'danger'
                };
                setTimeout(function () {
                    search.message = {};
                    $scope.$digest();
                }, 5000);
            });
        };

    };

    MagController.$inject = ['$scope', 'TypeFactoryService', 'SearchService'];

    return MagController;
});
