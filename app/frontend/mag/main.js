/**
 * 
 * Design:
 *  -- aec-search-results-viewer renders things. 
 *      -- Parameters:
 *          -- id of model to render
 *      -- watches:
 *          -- $scope.model.annotations
 *      
 * @param {type} angular
 * @param {type} MagController
 * @param {type} SearchService
 * @param {type} routes
 * @param {type} MagSearchResultViewer
 * @returns {unresolved}
 */
define(['angular', './MagController', './SearchService', './routes', './MagSearchResultViewer'], function (angular, MagController, SearchService, routes, MagSearchResultViewer) {
    'use strict';

    var module = angular.module('gruntFrontApp.mag', ['mag.routes']),
        ngInjector = angular.injector(['ng'])
    ;

    module.controller('MagController', MagController);
    module.directive('aec-mag-facet', {
        templateUrl: '/mag/aec-mag-facet.html'
    });
    module.directive('aecMagSearchResultViewer', MagSearchResultViewer);
    module.factory('SearchService', function () { 
        return new SearchService(ngInjector.get('$http')); 
    });

    return module;
});
