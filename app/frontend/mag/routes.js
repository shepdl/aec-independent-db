define(['angular', './MagController'], function (angular, MagController) {
    'use strict';

    var module = angular.module('mag.routes', []);
    module.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/mag', {
            templateUrl: '/mag/root.html',
            controller: MagController
        });
    }]);
    return module;

});
