define([], function () {
    'use strict';

    var SearchResultViewer = function () {

       return {
           scope: false,
           link: function (scope, element, attrs) {

                scope.$watch(
                    // 'models[' + attrs.modelId + '].searchResults',
                    function ($scope) { 
                        return $scope.models[attrs.modelId].searchResults; 
                    }, 
                    function (newSearchResults, oldSearchResults) { 
                        if (typeof newSearchResults === 'undefined' && typeof oldSearchResults === 'undefined') {
                            return;
                        }

                        scope.searchResults = newSearchResults;
                    }
                );
           }
       };
   }

   return SearchResultViewer;
});
