define(['projectAPI/API'], function (API) {
    'use strict';

    var SearchService = function ($http) {

        var modelsToSearch = function (dataAsModels) {
            var facets = [];

            for (var i = 0; i < dataAsModels.length; i++) {
                var inFacet = dataAsModels[i];
                var schemaId = inFacet.type.id;

                // TODO: add field ID
                for (var key in inFacet.values) {
                    if (inFacet.values[key]) {
                        facets.push({
                            type : 'quality',
                            schemaId: schemaId,
                            fieldName: key,
                            queryType: 'like',
                            value: inFacet.values[key]
                        })
                        facets.push({'type': 'operator', 'name': 'AND'});
                    }
                }
            }
            return facets;
        }
        
        this.searchObject = function (object) {
            return $http.post(API.URL('annotations/search'), {request: object});
        };
        this.search = function (dataAsModels) {

            var searchData = {
                'request' : {
                    'entities' : modelsToSearch(dataAsModels),
                    'projects' : [],
                    'approvedBy' : []
                },
            };
            return $http.post(API.URL('annotations/search'), searchData);
        };

    };

    SearchService.$inject = ['$http'];

    return SearchService;
});
