define(['./API', 'PlayUtils'], function (API, PlayUtils) {
    'use strict';

    var EndorsementService = function ($http) {
    
        this.endorse = function (item, group) {
            return $http({
                method: 'POST',
                url: API.URL('/groups/' + group.id + '/endorsements'), 
                headers : { 
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With' : 'XMLHttpRequest'
                },
                data: PlayUtils.serialize({
                    itemId: item.id,
                    itemType: 'anything'
                })
            });
        };

        this.removeEndorsement = function (endorsementId, group) {
            return $http({
                method: 'DELETE',
                url: API.URL('/groups/' + group.id + '/endorsements/' + endorsementId)
            });
        };

        this.getEndorsementsByGroup = function (group) {
            return $http({
                method: 'GET',
                url: API.URL('/groups/' + group.id + '/endorsements')
            });
        };
    };

    return EndorsementService;
});
