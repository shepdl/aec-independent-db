define(['./API', './ModelFactoryService', 'jquery'], function (API, ModelFactoryService, $) {

    var Artifact = function (cParams) {
        var params = cParams || {},
            self = this,

            id = params.id,
            title = params.title || '',
            project = cParams.project || '',
            filename = cParams.filename || ''
        ;

        this.title = title;
        this.id = id;
        this.models = [];
        this.modelFactory = new ModelFactoryService(cParams.$http).forProjectAndArtifact(project, this);
        this.filename = filename;

        this.createModel = function () {
            this.modelFactory.createModel(); 
        };


    };


    var ProjectArtifactFactory = function ($http, project) {

        var artifacts = [];

        var ARTIFACTS_URL = function (url) {
            if (typeof url === 'undefined') {
                url = '';
            }
            if ((url.length > 0 && url.substr(0, 1) != '/') || typeof url === 'number') {
                url = '/' + url;
            }
            return API.URL('activities/' + project.id + '/' + 'artifacts' + url);
        };

        this.uploadUrl = function (artifact) {
            return ARTIFACTS_URL(artifact.id + '/model');
        };

        this.get = function (id) {
            return $http.get(ARTIFACTS_URL('/' + id));
        };

        this.getArtifacts = function () {
            return artifacts;
        };

        this.getModelsForArtifact = function (artifact) {
            return $http.get(API.URL('/artifacts/' + artifact.id));
        };

        this.uploadModel = function (project, artifact, modelFile) {
            return $http.post(API.URL('activities/' + project.id + '/artifacts/' + artifact.id + '/model'));
        };

        this.getForProject = function (project) {
            return $http.get(ARTIFACTS_URL());
        };

        this.createBlank = function () {
            var artifact = new Artifact({
                project: project,
                $http: $http
            });
            artifacts.push(artifact);
            return artifact;
        };

        this.listArtifactsNotInProject = function (project, page, rpp) {
            var projectId;
            if (typeof project === 'string') {
                projectId = project;
            } else {
                projectId = project.id;
            }
            return $http.get(API.URL('/artifacts/not_in_project/' + projectId + '?page=' + page + '&rpp=' + rpp));
        };

        this.addToProject = function (artifact, project) {
            var projectId = project.id,
                artifactId = artifact.id
            ;

            if (typeof project === 'string') {
                projectId = project;
            }

            if (typeof artifact === 'string') {
                artifactId = artifact.id;
            }

            return $http({
                method: 'POST',
                url: API.URL('/activities/' + projectId + '/artifacts/' + artifactId)
            });
        };

        this.saveForProject = function (artifact, project) {
            if (artifact.id) {
                return $http({
                    method: 'PUT',
                    url: ARTIFACTS_URL(artifact.id),
                    data: { 
                        id: artifact.id,
                        title: artifact.title,
                    }
                });
            } else {
                return $http({
                    method: 'POST',
                    url: ARTIFACTS_URL(),
                    headers : { 
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-Requested-With' : 'XMLHttpRequest'
                    },
                    data: $.param({ 
                        title: artifact.title,
                    })
                });
            }

        };

        this.deleteForProject = function (artifact, project) {

            return $http({
                method: 'DELETE',
                // url: ARTIFACTS_URL(artifact.id)
                url: API.URL('/activities/' + project.id + '/artifacts/' + artifact.id + '/link')
            });
        };

    };

    var ArtifactFactoryService = function ($http) {
        
        this.factoryForProject = function (project) {
            return new ProjectArtifactFactory($http, project);
        };

    }

    ArtifactFactoryService.$inject = ['$http'];


    return ArtifactFactoryService;
});
