define(['ArrayUtils'], function (ArrayUtils) {
    'use strict';

    /**
     * Annotation for an object
     * 
     * Consists of the visual representation of that annotation 
     * and also the data associated with it.
     * 
     * @returns {undefined}
     */
    var Annotation = function (constructorParams) {

        var params = constructorParams || {};

        var id = params.id || null,
            self = this,
            entity = params.entity || {
                schema: params.schema || null,
                data: params.data || {}
            },
            approvedBy = params.approvedBy,

            vertices = params.vertices || [],
            userVertices = params.vertices || [],
            imageData = params.imageData,

            editing = params.editing || false,
            observers = {},
            observerCounter = 0
        ;

        if (params.location) {
            for (var i = 0; i < params.location.length; i++) {
                var loc = params.location[i];
                var vertex = {
                    x: loc[0],
                    y: loc[1],
                    z: loc[2],
                    faceIndex: loc[3]
                };
                vertices.push(vertex);
                userVertices.push(vertex);
            }
        }

        this.id = id;
        this.entity = entity;
        this.approvedBy = approvedBy;
        this.imageData = imageData;

        this.checked = false;
        this.dirty = false;
        this.editing = editing;

        var self = this;


        var informObserversOfUpdate = function () {
            for (var key in observers) {
                observers[key].wasUpdated(self);
            }
        };

        // Vertices
        this.addVertex = function (vertex, isUserVertex) {
            vertices.push(vertex);
            if (isUserVertex) {
                vertex.isUserDefined = true;
                userVertices.push(vertex);
                this.dirty = true;
            }
            informObserversOfUpdate();
        };


        this.removeVertex = function (vertexIndex) {
            userVertices.splice(vertexIndex, 1);
            vertices.splice(vertexIndex, 1);
            informObserversOfUpdate();
        };

        this.insertVertex = function (indexOfPreceedingVertex, vertex) {
            vertices.splice(indexOfPreceedingVertex, 0, vertex);
            informObserversOfUpdate();
        };

        this.updateVertex = function (vertexIndex, newValue) {
            vertices[vertexIndex] = newValue;
            userVertices[vertexIndex] = newValue;
            self.dirty = true;
            informObserversOfUpdate();
        };

        this.getVertices = function () {
            return vertices;
        };

        this.getUserVertices = function () {
            return userVertices;
        };

        this.addObserver = function (observer) {
            var observerId = 'observer-' + observerCounter;
            observers[observerId] = observer;
            observerCounter += 1;
            return observerId;
        };

        this.removeObserver = function (observerId) {
            delete observers[observerId];
        };

        this.toJSON = function () {
            var json = {
                location: userVertices.map(function (vertex) {
                    return [vertex.x, vertex.y, vertex.z];
                }),
                entity: entity
            };
            if (id) {
                json.id = id;
            }
            return json;
        };

    };


    return Annotation;

});

