define([], function () {
    'use strict';

    var Field = function (cParams) {
        var params = cParams || {};
        this.id = params.id || null;
        this.name = params.name || '';
        this.title = params.title || '';
        this.type = params.type || '';
        this.isControlled = params.isControlled || false;
        this.values = params.values || [];

        this.inputElement = params.inputElement;
        this.inputStyle = params.inputStyle;
        this.displayElement = params.displayElement;
        this.displayStyle = params.displayStyle;

        var self = this;

        var vocabulary = params.vocabulary || null;

        this.controlledVocabulary = function (newVocab) {
            if (typeof newVocab === 'undefined') {
                return vocabulary;
            } else {
                vocabulary = newVocab;
            }
        };

        this.toJSON = function () {
            var possibleMatch = self.type.match(/\((\d+)\)/),
                type = null
            ;

            if (possibleMatch) {
                type = possibleMatch[1];
            } else {
                type = self.type;
            }
            var json = {
                title: self.title,
                type: type
            };
            if (self.id) {
                json.id = self.id;
            }
            return json;
        };

    };

    var Type = function (cParams) {
        var id = cParams.id || null,
            project = cParams.project || {},
            rawFields = cParams.fields || [],
            isControlled = cParams.isControlled || false,
            vocabulary = false
        ;

        var fields = [];
        for (var i = 0; i < rawFields.length; i++) {
            fields.push(new Field(rawFields[i]));
        }
        this.fields = fields;

        this.name = cParams.name || cParams.title || '';
        this.title = this.name;
        this.appliesTo = 'annotation';
        this.isControlled = isControlled;

        this.controlledVocabulary = function (newVocab) {
            if (typeof newVocab === 'undefined') {
                return vocabulary;
            } else {
                vocabulary = newVocab;
            }
        };

        this.id = id;

        this.createField = function (params) {
            var field = new Field(params);
            fields.push(field);
            return field;
        };

        this.removeField = function (item) {
            var index = fields.indexOf(item);
            if (index > -1) {
                fields.splice(index, 1);
            }
        };

        this.toJSON = function () {
            var json = {
                title: this.title,
                fields : fields.map(function (field) {
                    return field.toJSON();
                })
            };

            if (this.id) {
                json.id = this.id;
            }
            return json;
        };

    };

    return Type;

});
