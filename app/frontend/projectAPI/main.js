define (['angular', 'angular-resource', './ProjectFactory', './ModelFactoryService', './TypeFactory', './AccessControl', './ArtifactFactory', './AnnotationFactoryService', './EndorsementService'], function (angular, $resource, ProjectFactory, ModelFactoryService, TypeFactoryService, AccessControl, ArtifactFactoryService, AnnotationFactoryService, EndorsementService) {
    'use strict';

    var module = angular.module('gruntFrontApp.api', ['ngRoute', 'ngResource']),
        ngInjector = angular.injector(['ng'])
    ;

    module.factory('AccessControl', function () {
        return new AccessControl(ngInjector.get('$http'));
    });
    module.factory('ProjectService', function () {
        return new ProjectFactory(ngInjector.get('$http'));
    });
    module.factory('ModelFactory', function () {
        return new ModelFactoryService(ngInjector.get('$http'));
    });
    module.factory('TypeFactoryService', function () {
        return new TypeFactoryService(ngInjector.get('$http'));
    });
    module.factory('ArtifactFactoryService', function () {
        return new ArtifactFactoryService(ngInjector.get('$http'));
    });
    module.factory('AnnotationFactoryService', function () {
        return new AnnotationFactoryService(ngInjector.get('$http'), ngInjector.get('$q'));
    });

    module.factory('EndorsementService', function () {
        return new EndorsementService(ngInjector.get('$http'));
    });

    return module;
});

