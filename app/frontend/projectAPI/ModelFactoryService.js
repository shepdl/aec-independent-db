define(['./API', './Model'], function (API, Model) {
    'use strict';

    var models = [];

    var ModelFactory = function ($http, $location, project, artifact) {

        var cache = models;

        var projectId = window.location.hash.split('/')[2];

        var MODEL_URL = function (url) {
            return API.URL('/artifacts/' + artifact.id + '/models/' + url);
        };
        
        this.get = function (id) {
            if (typeof id != 'undefined') {
                return $http.get(MODEL_URL(id));
            }
        };


        this.createBlank = function (inProject, cParams) {
            if (typeof inProject === 'undefined') {
                inProject = project;
            }
            var params = cParams || {};
            params.project = inProject;

            var model = new Model(params);
            cache.push(model);
            return model;
        };

        this.delete = function (artifact, model) {
            return $http({
                method: 'DELETE',
                url: API.URL('/activities/' + projectId + '/artifacts/' + artifact.id + '/models/' + model.id)
            });
        };

        this.setPreviewImage = function (model, imageData) {
            return $http({
                method: 'PUT',
                data: {
                    imageData: imageData
                },
                url: API.URL('/activities/' + projectId + '/artifacts/' + 
                             artifact.id + '/models/' + model.id + '/preview'
                )
            });
        };
    };


    var ModelFactoryService = function ($http, $location) {
        
        this.forProjectAndArtifact = function (inProject, inArtifact) {
            var project = {},
                artifact = {}
            ;
            if (typeof inProject == 'number' || typeof inProject == 'string') {
                project.id = inProject;
            }
            if (typeof inArtifact == 'number' || typeof inArtifact == 'string') {
                // artifact.id = parseInt(inArtifact);
                artifact.id = inArtifact;
            }
            return new ModelFactory($http, $location, project, artifact);
        };

    };

    ModelFactoryService.$inject = ['$http', '$location'];

    return ModelFactoryService;

});
