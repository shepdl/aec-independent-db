define([], function () {

    var API = {};

    API.URL = function (url) {
        var separator = '';

        if (url.substr(0, 1) !== '/') {
            separator = '/';
        }
        return '' + separator + url;
    };

    return API;
});
