define(['./API', '../PlayUtils', './Annotation'], function (API, PlayUtils, Annotation) {
    'use strict';

    var ProjectAndModelAnnotationFactory = function ($http, $q, project, model) {

        this.createBlank = function () {
            return new Annotation();
        };

        this.get = function (id) {
        };

        this.getForModel = function (model) {
            return $http.get(API.URL('/activities/' + project.id + '/artifacts/' + model.id + '/annotations'));
        };
        this.getForModelFromSchema = function (model, schema) {};
        this.getForWholeModel = function (model) {
            return $http.get(API.URL('/activities/' + project.id + '/models/' + model.id + '/annotations'));
        };

        this.list = function () {
            var projectId = project.id,
                modelId = model.id
            ;

            return $http.get(API.URL('/activities/' + projectId + '/artifacts/' + modelId + '/annotations')).then(function (response) {
                var processedAnnotations = response.data.annotations.map(function (annotation) {
                    return new Annotation(annotation);
                });
                return $q(function (resolve, _) {
                    resolve(processedAnnotations);
                });
            });
        };

        this.getForModelAndProject = function (model) {
            return $http.get(API.URL('/activities/' + project.id + '/artifacts/' + model.id + '/annotations')).then(function (response) {
                var processedAnnotations = response.data.annotations.map(function (annotation) {
                    return new Annotation(annotation);
                });
                console.log(processedAnnotations);
                return $q(function (resolve, _) {
                    resolve(processedAnnotations);
                });
            });
        };

        this.approve = function (annotation, group) {
            return $http({
                method: 'POST',
                url: API.URL('/annotations/' + annotation.id + '/approvals'), 
                headers : { 
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With' : 'XMLHttpRequest'
                },
                data: PlayUtils.serialize({
                    groupId: group.id
                })
            });
        };

        this.removeApproval = function (endorsementId, group) {
            return $http({
                method: 'DELETE',
                url: API.URL('/groups/' + group.id + '/endorsements/' + endorsementId)
            });
        };

        this.save = function (annotation) { 
            var projectId, modelId;
            if (typeof project === 'undefined' || typeof project.id === 'undefined') {
                projectId = annotation.project.id;
            } else {
                projectId = project.id;
            }
            if (typeof model === 'undefined' || typeof model.id === 'undefined') {
                modelId = annotation.model.id;
            } else {
                modelId = model.id;
            }
            if (typeof annotation.id === 'undefined' || annotation.id.charAt(0) === 'u') {
                var data = [];
                for (var i in annotation.entity.data) {
                    var item = annotation.entity.data[i];
                    var value = item;
                    if (typeof item.value !== 'undefined') {
                        value = item.value;
                    }
                    data.push({name: i, value: value});
                }
                var annotationData = {
                    data: data,
                    location: annotation.getUserVertices().map(function (vertex) {
                        return {x: vertex.x, y: vertex.y, z: vertex.z, faceIndex: vertex.faceIndex};
                    }),
                    imageData: annotation.imageData
                };

                return $http({
                    method: 'POST',
                    url: API.URL('/activities/' + projectId + '/models/' + modelId + '/' +
                        // annotation.entity.schema.title + '-' + annotation.entity.schema.id
                        'schema' + '/' + annotation.entity.schema.id
                    ) ,
                    headers : { 
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-Requested-With' : 'XMLHttpRequest'
                    },
                    data: PlayUtils.serialize(annotationData)
                });
            } else {
                var schema = annotation.entity.schema;
                return $http.put(
                    API.URL(
                        '/activities/' + projectId + '/models/' + modelId + '/' +
                            schema.title + '/' + schema.id + 
                                '/' + annotation.id
                    ),
                    annotation
                );
            }
        };

        this.delete = function (annotation, model) {
            if (typeof annotation.model !== 'undefined') {
                model = annotation.model;
            }
            return $http({
                method: 'DELETE',
                url: API.URL('/activities/' + project.id + '/models/' + model.id + '/' +
                        annotation.entity.schema.title + '/' + annotation.entity.schema.id + '/' + annotation.id
                    )
                }
            );
        };

    };

    var GeneralAnnotationFactory = function ($http, $q) {

        this.createBlank = function () {
            return new Annotation();
        };

        this.getForModel = function (model) {
            return $http.get(API.URL('/artifacts/' + model.id + '/annotations'));
        };

        this.getForModelOfSchemas = function (model, schemas) {
            var modelId = model.id,
                schemaId = schema.id
            ;

            if (typeof model === 'string') {
                modelId = model;
            }
            if (typeof schema === 'string') {
                schemaId = schema;
            }

            return $http.get(API.URL('/models/' + model.id + '?schemas=' + schemas.map(function (schema) {
                return schema.id;
            }).join(',')));
        };

        this.getForWholeModel = function (model) {
            return $http.get(API.URL('/models/' + model.id + '/annotations'));
        };

        this.getForModelAndProject = function (project, model) {
            var projectId = project.id,
                modelId = model.id
            ;

            if (typeof model === 'string') {
                modelId = model;
            }
            if (typeof project === 'string') {
                projectId = project;
            }
            return $http.get(API.URL('/activities/' + projectId + '/artifacts/' + modelId + '/annotations')).then(function (response) {
                var processedAnnotations = response.data.annotations.map(function (annotation) {
                    return new Annotation(annotation);
                });
                return $q(function (resolve, _) {
                    resolve(processedAnnotations);
                });
            });
        };

        this.save = function (annotation) { 

            var projectId = annotation.project.id,
                modelId = annotation.model.id
            ;

            if (typeof annotation.id === 'undefined' || !annotation.id || annotation.id.charAt(0) === 'u') {
                var data = [];
                for (var i in annotation.entity.data) {
                    var item = annotation.entity.data[i];
                    data.push({name: i, value: item.value});
                }
                var annotationData = {
                    data: data,
                    imageData: annotation.imageData,
                    location: annotation.getUserVertices().map(function (vertex) {
                        return {x: vertex.x, y: vertex.y, z: vertex.z, faceIndex: vertex.faceIndex};
                    })
                };

                return $http({
                    method: 'POST',
                    url: API.URL('/activities/' + projectId + '/models/' + modelId + '/' +
                        // annotation.entity.schema.title + '-' + annotation.entity.schema.id
                        'schema' + '/' + annotation.entity.schema.id
                    ) ,
                    headers : { 
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-Requested-With' : 'XMLHttpRequest'
                    },
                    data: PlayUtils.serialize(annotationData)
                });
            } else {
                var schema = annotation.entity.schema;
                var entityData = {};
                for (var key in annotation.entity.data) {
                    entityData[key] = annotation.entity.data[key].value;
                }
                var annotationData = {
                    entity: {
                        id: null,
                        schema: annotation.entity.schema,
                        data: entityData,
                    },
                    imageData: annotation.imageData,
                    location: annotation.getUserVertices().map(function (vertex) {
                        return [vertex.x, vertex.y, vertex.z, vertex.faceIndex];
                    })
                };

                return $http.put(
                    API.URL(
                        '/activities/' + projectId + '/models/' + modelId + '/' +
                            schema.title + '/' + schema.id + 
                                '/' + annotation.id
                    ),
                    annotationData
                );
            }
        };

        this.delete = function (annotation, model) {
            var modelId, projectId;
            if (typeof model === 'undefined') {
                modelId = annotation.model.id;
            } else if (typeof model === 'string') {
                modelId = model;
            }
            if (typeof annotation.project !== 'undefined') {
                projectId = annotation.project.id;
            } else if (typeof annotation.projectId === 'string') {
                projectId = annotation.projectId;
            }
            return $http({
                method: 'DELETE',
                url: API.URL('/activities/' + projectId + '/models/' + modelId + '/' +
                        annotation.entity.schema.title + '/' + annotation.entity.schema.id + '/' + annotation.id
                    )
                }
            );
        };

        this.approve = function (annotation, group) {
            return $http({
                method: 'POST',
                url: API.URL('/groups/' + group.id + '/endorsements'), 
                headers : { 
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'X-Requested-With' : 'XMLHttpRequest'
                },
                data: PlayUtils.serialize({
                    itemId: annotation.id,
                    itemType: 'anything'
                })
            });
        };

        this.removeApproval = function (endorsementId, group) {
            return $http({
                method: 'DELETE',
                url: API.URL('/groups/' + group.id + '/endorsements/' + endorsementId)
            });
        };
    };

    var AnnotationFactoryService = function ($http, $q) {

        this.factoryForProjectAndModel = function (project, model) {
            return new ProjectAndModelAnnotationFactory($http, $q, project, model);
        };

        this.factory = function () {
            return new GeneralAnnotationFactory($http, $q);
        };
    };

    AnnotationFactoryService.$inject = ['$http', '$q'];

    return AnnotationFactoryService;

});
