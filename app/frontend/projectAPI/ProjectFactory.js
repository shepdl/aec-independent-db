define(['angular', './Model', './ArtifactFactory', './TypeFactory', './API', 'jquery'], function (angular, Model, ArtifactFactory, TypeFactory, API, $) {
    'use strict';

    var projects = {},
        idCounter = 1
    ;


    /**
    * Method: all objects (model, data type) recieve the project ID from the URL
    * 
    * They request the data from this service, by submitting the project ID, 
    * which returns a Project object, which has models and types attributes.
    * 
    * Model objects have a modelURL and annotations, which can be queried.
    * 
    * A type has a name, fields, and vocabularies.
    * 
    */


    var Project = function (cParams) {
        var params = cParams || {},
            self = this,
            id = params.id,
            name = params.name || '',
            types = params.types || new TypeFactory(cParams.$http).factoryForProject(this),
            title = params.title || ''
        ;

        this.title = title;
        this.schemas = types;

        this.id = function () {
            return id;
        };

        this.name = function () {
            return name;
        };

        this.artifacts = new ArtifactFactory(cParams.$http).factoryForProject(this);

        this.createArtifact = function () {
            var artifact = new Artifact();
            this.artifacts.push(artifact);
            return artifact;
        };

    };

    var newId = idCounter++;

    var ProjectService = function ($http) {

        var ACTIVITIES_BASE_URL = API.URL('activities');

        this.get = function (id) {
            var url = ACTIVITIES_BASE_URL;
            if (typeof id !== 'undefined') {
                url += '/' + id;
            } 

            return $http.get(url);
        };

        this.createBlank = function () {
            var newId = ++idCounter;
            var project = new Project({
                id: newId,
                $http: $http
            });
            projects[newId] = project;
            return project;
        };

        this.save = function (project) {
            if (project.id === null || typeof project.id === 'undefined') {
                return $http({
                    url : ACTIVITIES_BASE_URL,
                    method: 'POST',
                    headers : { 
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-Requested-With' : 'XMLHttpRequest'
                    },
                    data: $.param({ title: project.title})
                });
            } else {
                return $http.put(ACTIVITIES_BASE_URL + '/' + project.id, {
                    id: project.id,
                    title: project.title
                });
            }
                

        };

        this.delete = function (project) {
            return $http.delete(ACTIVITIES_BASE_URL + '/' + project.id);

        };

    };

    ProjectService.$inject = ['$http'];

    return ProjectService;

});

