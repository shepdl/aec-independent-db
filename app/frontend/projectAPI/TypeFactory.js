define(['./API', './Type', 'jquery', 'PlayUtils'], function (API, Type, $, PlayUtils) {
    'use strict';

    var TypeFactory = function ($http) {

        var TYPE_URL_FROM = function (inUrl) {
            var url = '/schemas';
            if (typeof inUrl !== 'undefined') {
                url = inUrl;
            }
            return API.URL(url);
        };


        var baseTypes = [];

        $http.get(API.URL('/schemas/base')).success(function (schemas) {
            baseTypes = schemas;
        });

        this.getBaseTypes = function () {
            return baseTypes;
        };

        this.create = function (cParams) {
            var params = cParams || {};
            var newType = new Type(params);
            return newType;
        };

        this.save = function (type) {
            var postData = {
                title: type.title, 
                appliesTo: type.appliesTo, 
                fields: type.fields, 
                isControlled: type.isControlled
            };

            if (type.id) {
                postData.id = type.id;
                return $http({
                    url: TYPE_URL_FROM('/' + type.id),
                    method: 'PUT',
                    data: postData
                });
            } else {
                return $http({
                    url: TYPE_URL_FROM(),
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'X-Requested-With' : 'XMLHttpRequest'
                    },
                    data: PlayUtils.serialize(postData)
                });
            }
        };

        this.get = function (id) {
            var url = TYPE_URL_FROM(id);
            if (typeof id === 'string' || typeof id === 'number') {
                url += '/' + id;
            }  
            return $http.get(url);
        };

        this.remove = function (type) {
            return $http({
                url: TYPE_URL_FROM(type.id),
                method: 'DELETE'
            });
        };

    };


    var ProjectTypeFactory = function (project, $http) {

        var API_BASE_URL = '/',
            TYPE_URL = API_BASE_URL + 'activities/' + project.id + '/schemas',
            self = this
        ;

        var TYPE_URL_FROM = function (url) {
            return API.URL('/activities/' + project.id + '/' + url);
        };

        var baseTypes = [];

        $http.get(API.URL('/schemas/base')).success(function (schemas) {
            baseTypes = schemas;
        });


        this.getBaseTypes = function () {
            return baseTypes;
        };

        this.getTypesAnd = function (then) {
            $http.get(TYPE_URL).success(function (data) {
                var types = data.schemas.map(function (item) {
                    return new Type(item);
                });
                then(types);
            });
        };

        this.getTypesForProject = function (inProject) {
            var targetProject = project;
            if (typeof inProject !== 'undefined') {
                if (typeof inProject === 'number') {
                    targetProject = {};
                    targetProject.id = inProject;
                } else if (typeof inProject === 'string') {
                    targetProject = {};
                    targetProject.id = parseInt(inProject);
                } else {
                    targetProject = inProject;
                }
            }
            return $http.get(API.URL('/activities/' + targetProject.id + '/schemas'));
        };


        var project = project,
            http = $http
        ;

        this.create = function (cParams) {
            var params = cParams || {};
            var newType = new Type(params);
            return newType;
        };

        this.save = function (type) {
            var postData = {title: type.title, appliesTo: type.appliesTo, fields: type.fields, isControlled: type.isControlled};
            if (type.id) {
                postData.id = type.id;
                return $http({
                    url: TYPE_URL + '/' + type.id,
                    method: 'PUT',
                    data: postData
                });
            } else {
                return $http({
                    url: TYPE_URL,
                    method: 'POST',
                    data: postData // PlayUtils.serialize(postData)
                });
            }
        };

        this.get = function (id) {
            var url = API_BASE_URL + 'activities/' + project.id + '/schemas';
            if (typeof id === 'string' || typeof id === 'number') {
                url += '/' + id;
            }  
            return $http.get(url);
        };


        this.remove = function (type) {
            return $http({
                url: API_BASE_URL + 'activities/' + project.id + '/schemas/' + type.id,
                method: 'DELETE'
            });
        };

    };

    var TypeFactoryService = function ($http) {

        this.http = $http;
        
        this.get = function (id) {
            var instance = null;
            if (id < cache.length - 1) {
                instance = cache[id];
            } else {
                instance = new Type(id);
            }
            return instance;
        };

        this.createBlank = function (project) {
            var type = new Type({
                project: project
            });
            cache.push(type);
            return type;
        };

        this.factoryForProject = function (project) {
            return new ProjectTypeFactory(project, $http);
        };

        this.genericFactory = function () {
            return new TypeFactory($http);
        };

    };

    TypeFactoryService.$inject = ['$http'];

    return TypeFactoryService;

});

