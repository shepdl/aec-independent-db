define([], function () {
    'use strict';

    var Model = function (cParams) {
        var params = cParams || {};

        var url = params.url || '';

        this.annotations = params.annotations || [];
        this.title = params.title || '';
        this.filename = params.filename || '';
        
        this.getURL = function () {
            return url;
        };
    };

    return Model;

});

