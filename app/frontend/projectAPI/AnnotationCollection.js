define(['jquery', 'underscore', './Annotation', 'ArrayUtils'], function ($, _, Annotation, ArrayUtils) {
    'use strict';

    var AnnotationCollection = function (inputAnnotations) {

        if (typeof inputAnnotations === 'undefined') {
            inputAnnotations = [];
        }

        var localIDCounter = 0;

        for (var i = 0; i < inputAnnotations.length; i++) {
            this.push(inputAnnotations[i]);
        }

        this.createBlank = function () {
            var annotation = new Annotation();
            this.push(annotation);
            return annotation;
        };

    };

    AnnotationCollection.prototype = [];

    AnnotationCollection.byId = function (targetId) {
        for (var i = 0; i < this.length; i++) {
            if (this[i].id == targetId) {
                return this[i];
            }
        }
        return false;
    };

    AnnotationCollection.prototype.add = function (item) {
        var renderableAnnotation = new Annotation(item);
        this.push(renderableAnnotation);
        return renderableAnnotation;
    };

    AnnotationCollection.prototype.remove = function (item) {
        var index = this.indexOf(item);
        if (index > -1) {
            this.splice(index, 1);
        } else {
            throw "Element missing";
        }
    };

    AnnotationCollection.prototype.contains = function (item) {
        for (var i = 0; i < this.length; i++) {
            if (_.isEqual(this[i], item)) {
                return true;
            }
        }
        return false;
    };

    AnnotationCollection.prototype.differenceFrom = function (other) {
        return this.filter(function (item) {
            return $.inArray(item, other) === -1;
        });
    };

    AnnotationCollection.prototype.complement = function (other) {
        var self = this;
        return new AnnotationCollection(other.filter(function (item) {
            return !self.contains(item);
        }));
    };

    AnnotationCollection.prototype.intersection = function (other) {
        return new AnnotationCollection(this.filter(function (item) {
            return other.contains(item);
        }));
    };

    AnnotationCollection.prototype.getById = function (id) {
        for (var i = 0; i < this.length; i++) {
            if (this[i].localID === id) {
                return this[i];
            }
        }
    };

    return AnnotationCollection;

});
