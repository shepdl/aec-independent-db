define([], function () {
    'use strict';

    var User = function (inId, inUsername) {

        var username = inUsername,
            id = inId
        ;

        this.getId = function () {
            return id;
        };

        this.getName = function () {
            return username;
        };

    };

    var AccessControl = function ($http) {

        this.currentUser = null;

        this.login = function (username, password) {
            $http.post(
                'http://localhost:8082/structr/rest/login', 
                {
                    name: username,
                    password: password
                }
            ).then(function (successResponse) {
                var data = successResponse.data,
                    statusCode = successResponse.status
                ;

                if (statusCode === 200) {
                    this.currentUser = new User(data.id, data.name);
                    alert("Login succeeded");
                } else {
                    alert("Login failed; status code was wrong");
                }

            }, function (errorResponse) {
                alert("It failed");
            });
        };
    };

    AccessControl.$inject = ['$http'];

    return AccessControl;

});

