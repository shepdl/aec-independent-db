define([], function () {
    'use strict';

    var DrawTool = function (annotation, eventProxy) {

        this.onSelect = function () {
            eventProxy.addEventListener('scene.mouseup', onMouseUp);
        };

        var onMouseUp = function (intersectedObject) {

            if (!intersectedObject.userData) {
                // currentAnnotation.addVertex(intersectedObject.point);
                var point = intersectedObject.point;
                annotation.addVertex(
                    {
                        x: point.x,
                        y: point.y,
                        z: point.z,
                        faceIndex: intersectedObject.faceIndex,
                    },
                    true
                );
            }

        };

        this.onUnselect = function () {
            eventProxy.removeEventListeners();
        };

    };

    return DrawTool;
});
