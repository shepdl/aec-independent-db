define (['angular', 'angular-resource', './routes',
        './RootController', 
        // './AnnotationEditController', 
        '../projectAPI/ProjectFactory',
        // './AccessControlController', 
        // './LoginController',
        './Scene',
        './AnnotationEdit',
        // './AnnotationListWindowController', 
        './AnnotationListWindow',
        './AnnotationView', 
        // './AnnotationViewController',
        './SharingPane',
        'angular-bootstrap',
        'angular-animate',
        'ihtpl'
], function (angular, $resource, routes,
    RootController,
             // AnnotationEditorController, AnnotationEditController, 
             ProjectFactory,
    // AccessControlController, LoginController, 
    Scene,
    AnnotationEditor,
     // AnnotationListWindowController, 
     AnnotationListWindow,
     AnnotationView, 
     // AnnotationViewController, 
     SharingPane
    ) {
    'use strict';

    var module = angular.module('gruntFrontApp.annotation_editor', ['ngRoute', 
            'annotation_editor.routes', 'ngResource', 'gruntFrontApp.api', 
            'ui.bootstrap','ui.bootstrap.tabs',
        'ih.tpl'
    ]);
    // module.controller('AccessControlController', AccessControlController);
    // module.controller('AnnotationEditController', AnnotationEditController);
    // module.controller('LoginController', LoginController);
    // module.controller('AnnotationEditorController', AnnotationEditorController.AnnotationEditorController);
    // module.controller('AnnotationViewController', AnnotationViewController);

    module.controller('RootController', RootController);
    module.directive('aecScene', Scene);
    module.directive('aecAnnotationEditor', AnnotationEditor);
    module.directive('aecAnnotationListWindow', AnnotationListWindow);
    module.directive('aecSharingPane', SharingPane);
    module.directive('aecAnnotationView', AnnotationView);

    return module;
});
