define(['jquery', 'projectAPI/Annotation', 'uuid/uuid', 'jquery-ui-npm/jquery-ui'], function ($, Annotation, uuid) {
    'use strict';

    var AnnotationListWindowController = ['$scope', '$element', '$filter',
            '$uibModal',
            'AnnotationFactoryService',
            'gruntFrontApp.authentication.AuthenticationService' ,
            'gruntFrontApp.groups.GroupRepository',
            'SearchService',
            'TypeFactoryService',
        function (
        $scope, $el, $filter, $modal,
        AnnotationFactoryService, authService, groupRepo, searchService, typeFactoryService
    ) {
        var typeFactory = typeFactoryService.factoryForProject({ id: $scope.projectId });

        $scope.editMode = false;
        $scope.setEditMode = function (status) {
            $scope.editMode = status;
        };

        setTimeout(function () {
            $('[data-toggle="tooltip"]', $el).tooltip();
        }, 100);

        var annotationRepo = AnnotationFactoryService.factoryForProjectAndModel(
                {id: $scope.projectId},
                {id: $scope.artifactId}
            ),
            annotationCache = null
        ;

        $scope.$watch(['projectId', 'artifactId'], function () {
            annotationRepo = AnnotationFactoryService.factoryForProjectAndModel(
                {id: $scope.projectId},
                {id: $scope.artifactId}
            );
        });

        $scope.user = null;
        authService.whoAmI().then(function (response) {
            $scope.user = response.data;
        });

        var searchParameters = {},
            windowId = $scope.windowId,
            artifactId = $scope.artifactId,
            projectId = $scope.projectId
        ;

        $scope.userGroups = [];

        $scope.filters = {
            byGroups: []
        };

        $scope.cloneWindow = function () {
        };

        $scope.addGroupFilter = function (group) {
            $scope.filters.byGroups.push(group);
            group.selected = true;
            refreshAnnotationList();
        };

        $scope.removeGroupFilter = function (group) {
            group.selected = false;
            var index = $scope.filters.byGroups.indexOf(group);
            $scope.filters.byGroups.splice(index, 1);
            refreshAnnotationList();
        };

        $scope.createAnnotation = function (schema) {
            var newAnnotation = new Annotation({
                id: 'u' + uuid.v4(),
                entity: {
                    schema: schema
                },
                checked: true,
                editing: true
            });
            newAnnotation.project = {
                id: $scope.projectId
            };
            newAnnotation.model = {
                id: $scope.artifactId
            };

            $scope.windowAnnotations.push(newAnnotation);
            $scope.addAnnotation()(newAnnotation);
        };

        $scope.allGroups = [];

        authService.getGroups().then(function (response) {
            var groups = [];
            for (var i = 0; i < response.data.groups.length; i++) {
                var group = response.data.groups[i].group;
                groups.push({
                    id: group.id,
                    name: group.name
                });
            }
            $scope.userGroups = groups;
            refreshAnnotationList();
        }, function (error) {
            refreshAnnotationList();
        });

        groupRepo.list().then(function (response) {
            var groups = [];
            for (var i = 0; i < response.data.groups.length; i++) {
                var group = response.data.groups[i];
                groups.push({
                    id: group.id,
                    name: group.name
                });
            }
            $scope.allGroups = groups;
            $scope.$digest();
        });

        $scope.windowAnnotations = [];

        $scope.setAllAnnotations = function () {
            searchParameters = {};
            refreshAnnotationList();
        };

        $scope.justThisProject = function () {
            searchParameters = {
                project: project
                // TODO: add grouping and sorting
            };
            refreshAnnotationList();
        };

        $scope.noGrouping = function () {
            delete searchParameters.grouping;
            refreshAnnotationList();
        };

        $scope.groupByProjects = function () {
            searchProjects.grouping = 'projects';
            refreshAnnotationList();
        };

        $scope.groupBySchemas = function () {
            searchProjects.grouping = 'schemas';
            refreshAnnotationList();
        };


        var processAnnotations = function (ann) {
            var newData = {};
            for (var key in ann.entity.data) {
                if (key === 'id' || key === 'versionId') {
                    continue;
                }
                var keyData = ann.entity.schema.fields.filter(function (f) {
                    return f.title === key;
                })[0];

                newData[key] = {
                    value: ann.entity.data[key],
                    type: keyData.type
                };
            }
            ann.entity.data = newData;
            ann.project = {
                id: $scope.projectId
            };
            ann.model = {
                id: $scope.artifactId
            };
            var groupIds = $scope.userGroups.map(function (group) {
                return group.id;
            });
            var idsOfGroupsThatHaveApprovedThisAnnotation = ann.approvedBy.map(function (approval) {
                return approval.group.id;
            });
            var groupsThatMayApproveThisAnnotation = $scope.userGroups.filter(function (userGroup) {
                return idsOfGroupsThatHaveApprovedThisAnnotation.indexOf(userGroup.id) === -1;
            });
            ann.groupsThatHaveNotApprovedThis = groupsThatMayApproveThisAnnotation;
            return ann;
        };

        $scope.schemas = [];

            typeFactory.get().then(function (types) {
                $scope.schemas = types.data.schemas;
            });

        var refreshAnnotationList = function () {
            // annotationRepo.list(searchParameters).then(function (response) {
            //     $scope.windowAnnotations = response.data;
            // });
            // TODO: Implement this API
            var searchParameters = {
                projects: [$scope.projectId],
                models: [$scope.artifactId],
                // schemaIds: [schemaId],
                entities: [],
                // approvedBy: $scope.filters.byGroups.map(function (group) {
                //     return group.id;
                // })
                // TODO: add grouping and sorting
            };
            searchService.searchObject(searchParameters).then(function (result) {
                if (result.data.result.length === 0) {
                    $scope.windowAnnotations = [];
                    $scope.$digest();
                    return;
                }
                var processedAnnotations = result.data.result[0].annotations.map(function (ann) {
                    return new Annotation(ann);
                }).map(processAnnotations);
                $scope.allAnnotations = processedAnnotations;
                var wholeModelAnnotations = processedAnnotations.filter(function (annotation) {
                    return annotation.entity.schema.appliesTo == "whole" || annotation.getUserVertices().length == 0;
                });
                var locatedAnnotations = processedAnnotations.filter(function (annotation) {
                    return (
                            annotation.entity.schema.appliesTo == "annotation" ||
                            annotation.entity.schema.appliesTo == "either" 
                       ) && annotation.getUserVertices().length > 0;
                });
                if ($scope.annotationFilter === 'whole') {
                    $scope.windowAnnotations = wholeModelAnnotations;
                } else {
                    $scope.windowAnnotations = locatedAnnotations;
                }
                $scope.$digest();
            });
        };

        $scope.close = function () {
            $scope.onClose();
            // TODO: remove annotations if they are in this project
        };

        $scope.showAnnotation = function (annotation) {
            var renderer = $scope.addAnnotation()(annotation);
            annotation.selected = true;
        };

        $scope.hideAnnotation = function (annotation) {
            annotation.selected = false;
            $scope.removeAnnotation()(annotation);
        };
        
    }];

    var AnnotationListWindow = function () {
        return {
            scope: {
                windowId: '=',
                eventProxy: '=',
                addAnnotation: '&',
                removeAnnotation: '&',
                title: '=',
                query: '=',
                artifactId: '=',
                projectId: '=',
                closeable: '=',
                editable: '=',
                onClose: '&'
            },
            controller: AnnotationListWindowController,
            templateUrl: '/annotation_editor/annotation-list-window.html',
            link: function (scope, element, attrs) {
                $(element).draggable().resizable();
            }
        };
    };

    return AnnotationListWindow;
});
