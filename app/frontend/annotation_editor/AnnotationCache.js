define(['./AnnotationRenderer', 'threejs'], function (AnnotationRenderer, THREE) {
    'use strict';

    var AnnotationCache = function () {
        var annotations = {},
            contouringFunction = null,
            self = this,
            scene = null
        ;

        var consolidatedGeometry = [],
            geometryUUIDReferences = {}
        ;


        this.setContouringFunction = function (func) {
            contouringFunction = func;
        };

        this.setScene = function (theScene) {
            scene = theScene;
        };

        this.updateAnnotationGeometry = function (annotationId, geometry) {
            geometryUUIDReferences[annotationId] = geometry;
        };

        this.add = function (annotation) {
            if (typeof annotations[annotation.id] === 'undefined') {
                var renderer = new AnnotationRenderer(annotation, contouringFunction, scene, self);
                annotations[annotation.id] = renderer;
                annotation.addObserver(renderer);

                var objectsToAdd = renderer.render();
                geometryUUIDReferences[annotation.id] = objectsToAdd;
            }
            return annotations[annotation.id];
        };

        this.remove = function (annotation) {
            annotations[annotation.id].remove();

            delete annotations[annotation.id];
            consolidatedGeometry.splice(geometryUUIDReferences[annotation.id], 1);
            delete geometryUUIDReferences[annotation.id];
        };

        this.getGeometry = function () {
            var geometryToReturn = [];
            for (var key in geometryUUIDReferences) {
                var geometry = geometryUUIDReferences[key];
                geometryToReturn = geometryToReturn.concat(geometry);
            }
            return geometryToReturn;
        };

    };

    return AnnotationCache;
});
