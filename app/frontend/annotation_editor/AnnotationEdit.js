define(['jquery', './DrawTool', './MoveTool', './EraserTool',], function ($, DrawTool, MoveTool, EraserTool) {
    'use strict';

    var AnnotationEditController = ['$scope', '$element', 'AnnotationFactoryService', 
        function ($scope, $element, AnnotationFactoryService) {

        $scope.alerts = [];

        $scope.setEditMode(true);

        var annotationRepo = AnnotationFactoryService.factory();

        for (var i = 0; i < $scope.annotation.entity.schema.fields.length; i++) {
            var field  = $scope.annotation.entity.schema.fields[i],
                cssRule = field.displayStyle,
                cssRules = cssRule.split('}'),
                outerRules = cssRules.pop()
            ;
            field.displayStyle = cssRules.join('\n') +  '} \n ' + 
                ".field-" + field.id + " { " + outerRules + "}"
            ;
        }

        $scope.delete = function () {
            if (confirm('Are you sure you want to delete this ' + $scope.annotation.entity.schema.title + '?')) {
                annotationRepo.delete($scope.annotation, $scope.annotation.modelId).then(function (response) {
                    $scope.onRemove();
                    $scope.onHide($scope.annotation);
                });
            }
        };

        $scope.save = function () {
            var annotation = $scope.annotation;

            var callback = function (annotation) {
                    annotationRepo.save($scope.annotation).success(function () {
                        $scope.alerts.push({
                            type: 'success',
                            message: annotation.entity.schema.title + " saved successfully."
                        });
                        activeTool.onUnselect();
                        $scope.onHide($scope.annotation);
                        $scope.annotation.editing = false;
                        $scope.setEditMode(false);
                    }).error(function (error) {
                        console.error(error);
                        $scope.alerts.push({
                            type: 'error',
                            message: 'Saving annotation failed'
                        });
                        $scope.$digest();
                    });
                };
            if (!annotation.imageData && (
                    annotation.entity.schema.appliesTo === 'annotation' ||
                    annotation.entity.schema.appliesTo === 'either'
                )) {
                $scope.setPreviewImage(callback);
            } else {
                callback($scope.annotation);
            }

        };

        $scope.cancelEditing = function () {
            $scope.annotation.editing = false;
            activeTool.onUnselect();
            $scope.onHide($scope.annotation);
            $scope.setEditMode(false);
        };

        $scope.setPreviewImage = function (callback) {
            var previewCanvas = $('.preview-image', $element)[0];

            var tempImage = document.createElement('img');
            var imageData = $('.visual-editor > canvas')[0].toDataURL();
            tempImage.src = imageData;
            tempImage.onload = function () {
                previewCanvas.getContext('2d').drawImage(tempImage, 0, 0, 80, 60);
                $scope.annotation.imageData = previewCanvas.toDataURL();
                callback($scope.annotation);
            };
        };

        $scope.controls = {
            draw: new DrawTool($scope.annotation, $scope.eventProxy),
            move: new MoveTool($scope.annotation, $scope.eventProxy),
            erase: new EraserTool($scope.annotation, $scope.eventProxy)
        };

        var activeTool = null;

        $scope.selectTool = function (toolName) {
            if (activeTool) {
                activeTool.status = '';
                activeTool.onUnselect();
            }
            activeTool = $scope.controls[toolName];
            activeTool.onSelect();
        };

        $scope.selectTool('draw');

    }];


    return function () {
        return {
            templateUrl: '/annotation_editor/annotation-edit.html',
            controller: AnnotationEditController,
            scope: {
                annotation: '=',
                eventProxy: '=',
                setEditMode: '=',
                getCurrentImage: '=',
                onHide: '&',
                onRemove: '&'
            }
        };
    };

});
