define(['angular', './RootController'], function (angular, RootController) {
    'use strict';

    var module = angular.module('annotation_editor.routes', []);
    module.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/editor/:projectId', {
            templateUrl: '/annotation_editor/root.html',
            controller: RootController
        });
        $routeProvider.when('/projects/:projectId/artifacts/:artifactId/editor/:modelId', {
            templateUrl: '/annotation_editor/root.html',
            controller: RootController
        });
    }]);
    return module;

});

