define([
    'threejs', 'jquery', 'underscore',
    './ContouringFunction',

    // Stuff that attaches to already-existing objects goes below this line
    './ColladaLoader',
    './MTLLoader', './OBJLoader',
    './TrackballControls'
], function (THREE, $, _, ContouringFunction) {
    'use strict';

    var SceneController = ['$scope', '$element', function ($scope, $element) {
        var canvas = $element[0];

        $scope.saveImageData = function () {
            $scope.saveSnapshot(canvas.toDataURL());
        };

        $scope.$watch('model', function (newValue) {
        });

    }];

    var Scene = function () {

        return {
            scope : {
                setContouringFunction: '&',
                setScene: '&',
                getGeometry: '&',
                eventProxy: '=',
                artifactUrl: '=',
                modelData: '=',
                height: '=',
                width: '='
            },
            controller: SceneController,
            templateUrl: '/annotation_editor/scene.html',
            link: function (scope, element, attrs) {

                var modelURL = scope.artifactUrl,
                    height = scope.height || 900
                ;

                // $(element[0]).css('height', height);
                // $(element[0]).css('width', 600);
                //
                var uniTool = null;


                var customHeight = 1000;

                var container = element[0].childNodes[0],
                    scene = new THREE.Scene(),
                    renderer,
                    camera,
                    controls,
                    mouse = new THREE.Vector2(),
                    clock = new THREE.Clock(),
                    raycaster = new THREE.Raycaster(),
                    plane = new THREE.Plane(),
                    
                    modelObjects = []
                ;
                
                function onWindowResize() {
                    camera.aspect = container.innerWidth / container.innerHeight;
                    camera.updateProjectionMatrix();
                    renderer.setSize( container.innerWidth, container.innerHeight );
                }

                function animate() {
                    window.requestAnimationFrame( animate, renderer.domElement );
                    var delta = clock.getDelta();
                    // THREE.AnimationHandler.update( delta );
                    renderer.render( scene, camera );
                }


                camera = new THREE.PerspectiveCamera(60, container.clientWidth / customHeight, 1, 10000 );
                camera.position.set( -50, -50, 50 );
                camera.up.set( 1, 0, 0 );

                scene.fog = new THREE.FogExp2( 0xfff4e5, 0.0003 );

                var light = new THREE.DirectionalLight( 0xffffff, 1.5 );
                light.position.set( 0, -4, -4 ).normalize();
                scene.add( light );
                

                renderer = new THREE.WebGLRenderer({ 
                    preserveDrawingBuffer: true, // Enabled for saving image previews
                    antialias: true 
                });
                renderer.setClearColor( scene.fog.color, 1 );
                renderer.setPixelRatio(window.devicePixelRatio);
                renderer.setSize( container.clientWidth, customHeight);
                // renderer.setSize(1600, 112200);
                renderer.sortObjects = false;
                // onWindowResize();

                container.appendChild( renderer.domElement );


                var loadDAE = function (newURL) {
                    var loader = new THREE.ColladaLoader();
                    var modelURL = newURL.replace(/\/app-data/, '');
                    loader.load(modelURL, function (geometry, materials) { 
                    
                        scene.add( geometry.scene );

                        // T.P. adapted this to use un-flattened DAE hierarchy; see ColladaLoader.js
                        // for more information
                        var skin = geometry.scene; 
                        camera.lookAt(skin.position);

                        var parseModelIntoObjects = function ($model) {
                            modelObjects.push($model);
                            if ($model.children && $model.children.length > 0) {
                                for (var i = 0; i < $model.children.length; i++) {
                                    parseModelIntoObjects($model.children[i]);
                                }
                            }
                        };

                        parseModelIntoObjects(geometry.scene);

                    });
                };

                scope.$watch('modelData', function (newValue) {
                    if (!newValue) {
                        return;
                    }
                    // TODO: fix this
                    var modelData = {
                        texture: '',
                        model: '',
                    };
                    for (var i = 0; i < newValue.files.length; i++) {
                        var filename = newValue.files[i];
                        if (filename.indexOf('.obj') > -1) {
                            modelData.model = filename;
                            break;
                        } else if (filename.indexOf('.mtl') > -1) {
                            modelData.texture = filename;
                        }
                    }

                    loadOBJ(modelData);
                });

                var loadOBJ = function (modelData) {

                    var mtlLoader = new THREE.MTLLoader(),
                        modelLoader = new THREE.OBJLoader(),

                        textureURL = modelData.texture.replace(/\/app-data/, ''),
                        modelURL = modelData.model.replace(/\/app-data/, ''),
                        url = textureURL.split('/')
                    ;

                    url.pop(); // discard last
                    mtlLoader.setTexturePath(url.join('/') + '/');
                    mtlLoader.load(textureURL, function (materials) {
                        materials.preload();
                        modelLoader.materials = materials;
                        modelLoader.load(modelURL, function (object) {
                            var parseModelIntoObjects = function ($model) {
                                modelObjects.push($model);
                                if (typeof $model.geometry !== 'undefined') {
                                    var vertices = $model.geometry.attributes.position.array;
                                    var geometry = new THREE.Geometry();
                                    geometry.fromBufferGeometry($model.geometry);

                                    var verticesToFI = {};

                                    for (var i = 0, len = geometry.vertices.length; i < len; i++) {
                                        var vertex = geometry.vertices[i],
                                            hash = vertex.x + '/' + vertex.y + '/' + vertex.z
                                        ;
                                        verticesToFI[hash] = [];
                                    }

                                    var faceGraph = {};
                                    for (var i = 0, len = geometry.faces.length; i < len; i++) {
                                        var face = geometry.faces[i],
                                            a = geometry.vertices[face.a],
                                            b = geometry.vertices[face.b],
                                            c = geometry.vertices[face.c]
                                        ;
                                        
                                        verticesToFI[a.x + '/' + a.y + '/' + a.z].push(i);
                                        verticesToFI[b.x + '/' + b.y + '/' + b.z].push(i);
                                        verticesToFI[c.x + '/' + c.y + '/' + c.z].push(i);

                                    }

                                    for (var i = 0, len = geometry.faces.length; i < len; i++) {
                                        var face = geometry.faces[i];
                                        var edges = [
                                            [face.a, face.b],
                                            [face.b, face.c],
                                            [face.c, face.a]
                                        ];

                                        for (var j = 0; j < edges.length; j++) {
                                            var edge = edges[j];
                                            var vertex1 = geometry.vertices[edge[0]],
                                                vertex2 = geometry.vertices[edge[1]],
                                                vertex1Neighbors = verticesToFI[
                                                    vertex1.x + '/' + vertex1.y + '/' + vertex1.z
                                                ],
                                                vertex2Neighbors = verticesToFI[
                                                    vertex2.x + '/' + vertex2.y + '/' + vertex2.z
                                                ],
                                                neighboringFaces = _.intersection(
                                                    vertex1Neighbors, vertex2Neighbors
                                                ),
                                                targetFaceIndex = _.difference(
                                                    neighboringFaces, [i]
                                                )[0],
                                                targetFace = geometry.faces[targetFaceIndex]
                                            ;

                                            if (neighboringFaces.length > 1) {
                                                if (typeof faceGraph[neighboringFaces[0]] === 'undefined') {
                                                    faceGraph[neighboringFaces[0]] = {};
                                                }
                                                if (typeof faceGraph[neighboringFaces[1]] === 'undefined') {
                                                    faceGraph[neighboringFaces[1]] = {};
                                                }

                                                if (typeof targetFace !== 'undefined') {

                                                    var va = geometry.vertices[targetFace.a],
                                                        vb = geometry.vertices[targetFace.b],
                                                        vc = geometry.vertices[targetFace.c],

                                                        ab = vb.clone().sub(va),
                                                        ac = vc.clone().sub(va),

                                                        cross = new THREE.Vector3()
                                                    ;

                                                    cross.crossVectors(ab, ac);
                                                    faceGraph[neighboringFaces[0]][neighboringFaces[1]] = cross.length() / 2;
                                                    // faceGraph[neighboringFaces[1]][neighboringFaces[0]] = 1;
                                                }

                                                faceGraph[neighboringFaces[0]][neighboringFaces[1]] = 1;
                                                faceGraph[neighboringFaces[1]][neighboringFaces[0]] = 1;
                                                }
                                        }

                                        if (geometry.faces) {
                                            $model.userData.faces = geometry.faces;
                                        }

                                        if (geometry.vertices) {
                                            $model.userData.originalVertices = geometry.vertices;
                                        }

                                        if (faceGraph) {
                                            $model.userData.faceGraph = faceGraph;
                                        }
                                    }
                                }
                                if ($model.children && $model.children.length > 0) {
                                    for (var i = 0; i < $model.children.length; i++) {
                                        parseModelIntoObjects($model.children[i]);
                                    }
                                }

                                if ($model.geometry) {
                                    scope.setContouringFunction()(
                                        new ContouringFunction(
                                            $model,
                                            $model.userData.faces,
                                            $model.userData.originalVertices,
                                            $model.userData.faceGraph
                                        )
                                    );
                                }
                            };

                            scene.add(object);
                            parseModelIntoObjects(object);
                        });
                    });
                };

                var eventListeners = {};

                var hitTest = function (event) {
                    event.preventDefault();

                    mouse.x = (event.offsetX / renderer.domElement.width) * 2 - 1;
                    mouse.y = -(event.offsetY / renderer.domElement.height) * 2 + 1;
                    raycaster.setFromCamera(mouse, camera);
                    var intersectedUserObjects = raycaster.intersectObjects(scope.getGeometry()());
                    var intersectedModelObjects = raycaster.intersectObjects(modelObjects);
                    var intersects = raycaster.intersectObjects(scope.getGeometry()()).concat(
                        raycaster.intersectObjects(modelObjects)
                    );

                    var intersects = intersectedUserObjects.concat(intersectedModelObjects);

                    if (intersects.length > 0) {
                        if (intersects.length === 1) {
                            return intersects[0];
                        } else {
                            // prefer polnts
                            var selectedIntersection = null;
                            for (var i = 0; i < intersects.length; i++) {
                                var objectToConsider = intersects[i];
                                if (objectToConsider.object.userData && objectToConsider.object.userData.renderClasses && objectToConsider.object.userData.renderClasses.userVertex) {
                                    selectedIntersection = objectToConsider;
                                    break;
                                }
                            }
                            if (!selectedIntersection) {
                                for (var i = 0; i < intersects.length; i++) {
                                    var objectToConsider = intersects[i];
                                    // NOTE: we are no longer preferring lines.
                                    // We are now preferring the model itself
                                    if (objectToConsider.object.userData && objectToConsider.faceIndex) {
                                        selectedIntersection = objectToConsider;
                                        break;
                                    }
                                }
                            }
                            return selectedIntersection;
                        }
                    } else {
                        return null;
                    }
                };

                var intersection = new THREE.Vector3(),
                    offset = new THREE.Vector3()
                ;

                scope.eventProxy.addEventListener = function (event, handler) {
                    var pieces = event.split('.');
                    var targetElement = element[0];
                    if (pieces[0] === 'scene') {
                        var eventName = pieces[1];
                        switch (eventName) {
                            case 'mousedown':
                                eventListeners[eventName] = function (event) {
                                    var intersectedObject = hitTest(event);
                                    if (intersectedObject) {
                                        handler(intersectedObject, event);
                                    }
                                    if (raycaster.ray.intersectPlane(plane, intersection)) {
                                        offset.copy(intersection).sub(intersectedObject.point);
                                    }
                                };
                                targetElement.addEventListener(eventName, eventListeners[eventName]);
                                break;
                            case 'mousemove':
                                eventListeners[eventName] = function (event) {
                                    if (raycaster.ray.intersectPlane(plane, intersection)) {
                                        handler(intersection, offset);
                                    }
                                }
                                targetElement.addEventListener(eventName, eventListeners[eventName]);
                                break;
                            case 'mouseup':
                                eventListeners[eventName] = function (event) {
                                    var intersectedObject = hitTest(event);
                                    if (intersectedObject) {
                                        handler(intersectedObject);
                                    }
                                }
                                targetElement.addEventListener(eventName, eventListeners[eventName]);
                                break;
                            default:
                                eventListeners[eventName] = function (event) {
                                    handler(event);
                                };
                                targetElement.addEventListener(eventName, eventListeners[eventName]);
                                break;
                        }
                    } else {
                        eventListeners[event] = handler;
                        targetElement.addEventListener(event, handler);
                    }
                };

                scope.eventProxy.removeEventListeners = function () {
                    for (var key in eventListeners) {
                        if (typeof element.off !== 'undefined') {
                            element[0].removeEventListener(key, eventListeners[key]);
                        } else {
                            element.removeEventListener(key, eventListeners[key]);
                        }
                    }
                };

                container.addEventListener('resize', onWindowResize, false);

                controls = new THREE.TrackballControls(camera, renderer.domElement);
                scene.add(camera);

                scope.setScene()(scene);
                animate();
            }
        };

    };

    return Scene;

});
