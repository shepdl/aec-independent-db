define(['threejs'], function (THREE) {
    'use strict';

    var ContouringFunction = function (model, faces, originalVertices, faceGraph) {

        var cache = {};

        this.contourPath = function (startPoint, endPoint) {
            var startPointFaceIndex = startPoint.faceIndex,
                endPointFaceIndex = endPoint.faceIndex,
                leftKey = Math.min(startPointFaceIndex, endPointFaceIndex),
                rightKey = Math.max(startPointFaceIndex, endPointFaceIndex),
                key = leftKey + '/' + rightKey
            ;

            if (typeof cache[key] !== 'undefined') {
                return cache[key];
            }
            var path = calculateIntermediaryPoints(startPoint, endPoint);
            cache[key] = path;
            return path;
        };

        var raycaster = new THREE.Raycaster();
        raycaster.set(model, new THREE.Vector3(0, 0, 0));

        var calculateIntermediaryPoints = function (startPoint, endPoint) {
            var points = [];

            if (startPoint.faceIndex == undefined) {
                var intersection = raycaster.intersectObjects(startPoint);
                // console.log(intersection);
            }

            var path = shortestPathBetween(
                faceGraph, 
                startPoint.faceIndex / 3, 
                endPoint.faceIndex / 3
                // modelAgainst.faceIndex / 3
            );

            for (var i = 0; i < path.length - 1; i++) {
                var face1Index = path[i],
                    face2Index = path[i + 1];
                var face1 = faces[face1Index],
                    face2 = faces[face2Index],

                    face1Vertices = [face1.a, face1.b, face1.c].map(function (vertexIndex) {
                        return originalVertices[vertexIndex];
                    }),
                    face2Vertices = [face2.a, face2.b, face2.c].map(function (vertexIndex) {
                        return originalVertices[vertexIndex];
                    }),
                    sharedVertices = face1Vertices.filter(function (vertex) {
                        for (var j = 0; j < face2Vertices.length; j++) {
                            var comparedVertex = face2Vertices[j];
                            if (comparedVertex.x === vertex.x &&
                                    comparedVertex.y === vertex.y &&
                                    comparedVertex.z === vertex.z
                               ) {
                                return true;
                            }
                        }
                        return false;
                    }),
                    sharedEdge = new THREE.Line3(
                        sharedVertices[0],
                        sharedVertices[1]
                    ),
                    mutualPoint = sharedEdge.closestPointToPoint(endPoint, true)
                ;
                points.push(mutualPoint);
            }

            return points;
        };

        var extractKeys = function (obj) {
            var keys = [],
                key
            ;

            for (key in obj) {
                Object.prototype.hasOwnProperty.call(obj, key) && keys.push(key);
            }
            return keys;
        };

        var sorter = function (a, b) {
            return parseFloat(a) - parseFloat(b);
        };

        var findPaths = function (graph, start, end) {
            var costs = {},
                open = {'0': [start]},
                predecessors = {},
                keys
            ;

            var addToOpen = function (cost, vertex) {
                var key = '' + cost;
                if (!open[key]) {
                    open[key] = [];
                }
                open[key].push(vertex);
            };

            costs[start] = 0;

            while (open) {
                if (!(keys = extractKeys(open)).length) break;

                keys.sort(sorter);

                var key = keys[0],
                    bucket = open[key],
                    node = bucket.shift(),
                    currentCost = parseFloat(key),
                    adjacentNodes = graph[node] || {}
                ;

                if (!bucket.length) delete open[key];

                for (var vertex in adjacentNodes) {
                    if (Object.prototype.hasOwnProperty.call(adjacentNodes, vertex)) {
                        var cost = adjacentNodes[vertex],
                            totalCost = cost + currentCost,
                            vertexCost = costs[vertex]
                        ;

                        if ((typeof vertexCost === 'undefined') || (vertexCost > totalCost)) {
                            costs[vertex] = totalCost;
                            addToOpen(totalCost, vertex);
                            predecessors[vertex] = node;
                        }
                    }
                }
            }

            if (typeof costs['' + end] === 'undefined') {
                return null;
            } else {
                return predecessors;
            }
        };

        var extractShortest = function (predecessors, end) {
            var nodes = [],
                u = end
            ;

            while (u) {
                nodes.push(u);
                u = predecessors[u];
            }

            nodes.reverse();
            return nodes;
        };

        var shortestPathBetween = function (graph, startPoint, endPoint) {
            var predecessors,
                path = [],
                shortest,
                nodes = [endPoint],
                start = startPoint,
                end
            ;

            while (nodes.length) {
                end = nodes.shift();
                predecessors = findPaths(graph, start, end);

                if (predecessors) {
                    shortest = extractShortest(predecessors, end);
                    if (nodes.length) {
                        path.push.apply(path, shortest.slice(0, -1));
                    } else {
                        return path.concat(shortest);
                    }
                } else {
                    return null;
                }
                start = end;
            }
        };

    };

    return ContouringFunction;
});
