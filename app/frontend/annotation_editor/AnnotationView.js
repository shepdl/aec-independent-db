define([], function () {
    'use strict';

    var AnnotationViewController = ['$scope', 'AnnotationFactoryService', function ($scope, AnnotationFactoryService) {

        var self = this,
            annotationRepo = AnnotationFactoryService.factory()
        ;

        for (var i = 0; i < $scope.annotation.entity.schema.fields.length; i++) {
            var field  = $scope.annotation.entity.schema.fields[i],
                cssRule = field.displayStyle,
                cssRules = cssRule.split('}'),
                outerRules = cssRules.pop()
            ;
            field.displayStyle = cssRules.join('\n') +  '} \n ' + 
                ".field-" + field.id + " { " + outerRules + "}"
            ;
        }


        $scope.alerts = [];

        $scope.edit = self.edit;

        $scope.approveBy = function (group) {
            annotationRepo.approve($scope.annotation, group).then(function (response) {
                $scope.annotation.approvedBy.push(group);
                $scope.$digest();
            }, function (error) {
                $scope.alerts.push({
                    type: 'danger',
                    message: error
                });
                $scope.$digest();
            });
        };

        $scope.disapproveAnnotation = function (endorsementId, group, $index) {
            var result = confirm('Are you sure you want to remove the approval of ' 
                    + group.name + '?');
            if (result) {
                annotationRepo.removeApproval(endorsementId, group).then(function (response) {
                    $scope.annotation.approvedBy.splice($index, 1);
                    $scope.$digest();
                }, function (error) {
                    var message = 'An unknown error occurred';
                    if (typeof error.error !== 'undefined') {
                        message = error.error;
                    }
                    $scope.alerts.push({
                        type: 'danger',
                        message: message
                    });
                    $scope.$digest();
                });
            }
        };

        $scope.edit = function () {
            $scope.annotation.editing = true;
            if (!$scope.annotation.renderer) {
                $scope.onShow();
            }
        };

        $scope.delete = function () {
            if (confirm('Are you sure you want to delete this ' + $scope.annotation.entity.schema.title + '?')) {
                annotationRepo.delete($scope.annotation, $scope.annotation.modelId).then(function (response) {
                    $scope.onRemove();
                });
            }
        };

    }];


    var AnnotationView = function () {
        return {
            templateUrl: '/annotation_editor/annotation-view.html',
            controller: AnnotationViewController,
            restrict: 'E',
            scope: {
                annotation: '=',
                approveBy: '=',
                onRemove: '&',
                onShow: '&',
                onHide: '&'
            }
        }
    };

    return AnnotationView;

});
