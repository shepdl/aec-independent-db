define(['./EventProxy', './AnnotationCache'], function (EventProxy, AnnotationCache) {
    'use strict';

    var RootController = function ($scope, $routeParams, modelFactoryService) {

        $scope.eventProxy = new EventProxy();
        $scope.bus = new AnnotationCache();
        $scope.windows = {};
        //

        $scope.artifactUrl = '/artifacts/' + $routeParams.artifactId;
        
        var modelFactory = modelFactoryService.forProjectAndArtifact($routeParams.projectId, $routeParams.artifactId);
        // var typeFactory = typeFactoryService.factoryForProject($routeParams.projectId);
        modelFactory.get($routeParams.modelId).success(function (model) {
            $scope.model = model;
            $scope.$digest();
        });

        $scope.windows['window-0'] = {
            title : 'Whole Model',
            closeable: false,
            editable: true,
            projectId: $routeParams.projectId,
            artifactId: $routeParams.modelId,
            annotationFilter: 'whole',
            windows: $scope.windows
        };
        $scope.windows['window-1'] = {
            title: 'Project Annotations',
            closeable: true,
            editable: true,
            projectId: $routeParams.projectId,
            artifactId: $routeParams.modelId,
            annotationFilter: 'none',
            windows: $scope.windows
        };

    };

    RootController.$inject = ['$scope', '$routeParams', 'ModelFactory'];

    return RootController;
});
