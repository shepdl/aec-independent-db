/**
 * Created by daveshepard on 3/27/17.
 */

define([], function (annotation) {
    'use strict';

    var SharingPaneController = ['$scope', function ($scope) {

    }];

    return function () {
        return {
            restrict: 'E',
            scope: {
                contentType: '=',
                permissions: '=',
                users: '=',
                groups: '=',
                item: '&'
            },
            templateUrl: '/annotation_editor/sharing-pane.html',
            controller: SharingPaneController
        };
    };
});

