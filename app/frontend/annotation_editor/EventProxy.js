define([], function () {
    'use strict';

    var EventProxy = function (addListenerFunction, removeListenerFunction) {
        this.addListener = addListenerFunction;
        this.removeListener = removeListenerFunction;
    };

    return EventProxy;
});
