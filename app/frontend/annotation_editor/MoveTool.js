define([], function () {
    'use strict';

    var MoveTool = function (annotation, eventProxy) {

        var currentVertex = null;

        this.onSelect = function () {
            eventProxy.addEventListener('scene.mouseup', onMouseUp);
        };

        this.onUnselect = function () {
            currentVertex = null;
            eventProxy.removeEventListeners();
        };

        function onMouseUp (intersectedObject) {
            var object = intersectedObject.object,
                point = intersectedObject.point
            ;
            if (object.userData.renderClasses && object.userData.renderClasses.userVertex) {
                if (currentVertex === object) {
                    object.userData.renderClasses.selected = false;
                    annotation.updateVertex(
                        currentVertex.userData.vertexIndex,
                        {
                            x: currentVertex.point.x,
                            y: currentVertex.point.y,
                            z: currentVertex.point.z,
                            faceIndex: currentVertex.faceIndex,
                            selected: false,
                            isUserVertex: true
                        }
                    );
                } else {
                    if (currentVertex) {
                        var currentVertexData = annotation.getUserVertices()[currentVertex.userData.vertexIndex];
                        currentVertexData.selected = false;
                        annotation.updateVertex(
                            currentVertex.userData.vertexIndex,
                            currentVertexData
                        );
                    }
                    currentVertex = object;
                    var currentVertexData = annotation.getUserVertices()[currentVertex.userData.vertexIndex];
                    currentVertexData.selected = true;
                    annotation.updateVertex(
                        currentVertex.userData.vertexIndex,
                        currentVertexData
                    );
                }
            } else {
                if (currentVertex) {

                    var point = intersectedObject.point,
                        newPoint = {
                            x: point.x,
                            y: point.y,
                            z: point.z,
                            faceIndex: intersectedObject.faceIndex,
                            selected: true
                        }
                    ;
                    annotation.updateVertex(
                        currentVertex.userData.vertexIndex,
                        newPoint
                    );
                }
            }
        }

    };

    return MoveTool;

});
