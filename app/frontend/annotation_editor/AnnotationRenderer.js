define(['threejs'], function (THREE) {
    'use strict';
    var AnnotationRenderer = function (annotation, contouringFunction, scene, parent) {

        var geometry = new THREE.Group(),
            objects = [],
            params = params || {},
            self = this
        ;

        this.annotation = annotation;

        var unselectedAnnotationColor = params.unselectedAnnotationColor || 0xff0000,
            selectedAnnotationColor = params.selectedAnnotationColor || 0x00ff00
        ;

        this.wasUpdated = function (annotation) {
            this.render();
        };


        var renderVertices = function (vertices, color) {
            var particles = [] //,
                //group = new THREE.Group()
            ;
            for (var i = 0; i < vertices.length; i++) {
                var vertex = vertices[i],
                    vertexGeometry = new THREE.BoxGeometry(0.5, 0.5, 0.5),
                    color = 0x0000ff,
                    renderClasses = { userVertex: true}
                ;

                if (vertex.selected) {
                    renderClasses.selected = true;
                    color = 0xED66B8;
                }

                var particle = new THREE.Mesh(
                    vertexGeometry,
                    new THREE.MeshBasicMaterial( {color: color})
                );

                if (i === 0) {
                    renderClasses.aecStart = true;
                }

                particle.position.x = vertices[i].x;
                particle.position.y = vertices[i].y;
                particle.position.z = vertices[i].z;
                particle.userData.renderClasses = renderClasses;
                particle.userData.renderer = self;
                particle.userData.vertexIndex = i;
                particles.push(particle);
                // geometry.add(particle);
                objects.push(particle);
            }
            // return group;
        };

        this.selected = false;

        var remove = function () {
            scene.remove(geometry);
            for (var i = 0; i < objects.length; i++) {
                scene.remove(objects[i]);
            }
            objects = [];
        };

        this.remove = function () {
            remove();
        };

        this.render = function () {
            // scene.removeGeometry(geometry);
            remove();
            geometry = new THREE.Group();
            var meshProperties = {
                linewidth: 3
            };

            var color = null;

            if (annotation.selected) {
                color = selectedAnnotationColor;
            } else {
                color = unselectedAnnotationColor;
            }

            // annotation.addObserver(self);
            // annotation.renderer(self);
            // parent.removeGeometry(geometry.children[i]);

            var vertices = annotation.getUserVertices();

            var properties = {
                linewidth: 3,
                color: color
            };
            
            var lineGeometry = new THREE.Geometry();

            if (vertices.length > 0) {
                for (var i = 0; i < vertices.length - 1; i++) {
                    var start = vertices[i],
                        end = vertices[i + 1],
                        intermediaryPoints = contouringFunction.contourPath(start, end)
                    ;
                    for (var j = 0; j < intermediaryPoints.length; j++) {
                        var vertex = intermediaryPoints[j];
                        lineGeometry.vertices.push(
                            new THREE.Vector3(vertex.x, vertex.y, vertex.z)
                        );
                    }
                }

                intermediaryPoints = contouringFunction.contourPath(vertices[vertices.length - 1], vertices[0]);
                for (var j = 0; j < intermediaryPoints.length; j++) {
                    var vertex = intermediaryPoints[j];
                    lineGeometry.vertices.push(
                        new THREE.Vector3(vertex.x, vertex.y, vertex.z)
                    );
                }
            }

            var lineStyle = new THREE.LineBasicMaterial(properties);
            var line = new THREE.Line(lineGeometry, lineStyle);
            line.userData.renderer = self;
            line.userData.renderClasses = { aecLine : true };
            // parent.add(line);
            geometry.add(line);
            objects.push(line);

            var vertexRing = renderVertices(vertices, color);
            // geometry.add(vertexRing);
            // scene.add(geometry);
            for (var i = 0; i < objects.length; i++) {
                scene.add(objects[i]);
            }
            parent.updateAnnotationGeometry(annotation.id, objects);
            return objects;
        };

    };

    return AnnotationRenderer;

});
