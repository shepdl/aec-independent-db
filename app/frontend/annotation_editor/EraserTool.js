define([], function () {
    'use strict';

    var EraserTool = function (annotation, eventProxy) {
        this.onSelect = function () {
            eventProxy.addEventListener('scene.mouseup', onMouseUp);
        };

        this.onUnselect = function () {
                eventProxy.removeEventListeners();
        };

        function onMouseUp (intersectedObject) {
            var object = intersectedObject.object;

            var renderClasses = object.userData.renderClasses;

            if (renderClasses && renderClasses.userVertex) {
                annotation.removeVertex(object.userData.vertexIndex);
            }
        }
    };


    return EraserTool;

});
