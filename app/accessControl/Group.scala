package accessControl

import java.util.UUID

/**
  * Created by dave on 11/17/16.
  */
case class Group (id:UUID, name:String, homepage:String, isSystem:Boolean = false)

case object Group {
  def apply(id:UUID, name:String, homepage:String):Group = Group(id, name, homepage, false)
  def homeGroup(user:User) = Group(null, s"Home group for ${user.name}", "No home page", true)
}

case class GroupMembership(user:User, group:Group, roles:Set[String])
