package accessControl

import java.util.{Date, UUID}
import javax.inject.Inject

import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json._
import play.api.mvc.{Action, Controller}
import play.api.libs.functional.syntax._
import projects.ProjectRepository

trait UserController extends I18nSupport with RendersUserAsJson {

  this:Controller =>

  val userRepo:UserRepository
  val projectRepo:ProjectRepository
  val authentication:AuthenticationService
  val messagesApi:MessagesApi

  case class UserData(name:String, email:String, password:String)

  val joinForm:Form[UserData] = Form(
    mapping(
      "name" -> nonEmptyText,
      "email" -> nonEmptyText,
      "password" -> nonEmptyText
    )(UserData.apply)(UserData.unapply)
  )

  def join = Action(parse.urlFormEncoded) { implicit request =>
    joinForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      userData => {
        val user = new User(userData.name, userData.email)
        val savedUser = userRepo.save(user)
        userRepo.setPasswordForUser(savedUser, userData.password)
        Created(userToJson(savedUser))
      }
    )
  }

  def validate = Action { implicit request =>
    NotFound("Not implemented yet")
  }

  implicit val userWrites:Writes[User] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "name").write[String] and (JsPath \ "email").write[String]
    )(unlift(User.unapply))

  def update(id:UUID) = authentication.LoginRequiredAction(parse.json) { implicit request =>
    userRepo.getById(id) match {
      case None => NotFound(Json.obj("error" -> s"User ${id} does not exist"))
      case Some(user) => {
        if (user.uuid != id) {
          Forbidden(Json.obj(
            "error" -> "You are not allowed to update this user"
          ))
        } else {
          val userResult = request.body.validate[User]
          userResult.fold(
            errors => BadRequest(JsError.toJson(errors)),
            newUser => {
              userRepo.save(newUser)
              Ok(userToJson(newUser))
            }
          )
        }
      }
    }
  }

  def whoAmI = authentication.LoginRequiredAction { implicit request =>
    Ok(Json.obj(
      "user" -> userToJson(request.user),
      "scratchPadId" -> projectRepo.getUserScratchpad(request.user).id.toString
    ))
  }


  def getById(id:UUID) = Action { implicit request =>
    userRepo.getById(id) match {
      case Some(user) => {
        Ok(userToJson(user))
      }
      case None => NotFound(Json.obj(
        "error" -> s"User ${id} not found"
      ))
    }
  }

  val loginForm:Form[LoginRequest] = Form(
    mapping(
      "email" -> nonEmptyText,
      "password" -> nonEmptyText
    )(LoginRequest.apply)(LoginRequest.unapply)
  )

  def login = Action(parse.urlFormEncoded) { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      loginData => {
        userRepo.getByEmailAndPassword(loginData.email, loginData.password) match {
          case None => NotFound(Json.obj("message" -> "User with email and password does not exist"))
          case Some(user) => {
            Ok(Json.obj(
              "message" -> "Login successful"
            )).withSession("userId" -> user.uuid.toString)
          }
        }
      }
    )
  }

  def delete(id:UUID) = authentication.LoginRequiredAction {
    userRepo.getById(id) match {
      case None => NotFound(s"User ${id} does not exist")
      case Some(user) => {
        userRepo.delete(user)
        Ok(Json.obj("message" -> "User deleted"))
      }
    }
  }

  def list(page:Int, sortBy:String, rpp:Int) = (authentication.UserAwareAction andThen authentication.LoginRequiredAction) {
    Ok(Json.arr(userRepo.list(page, sortBy, rpp).map(userToJson)))
  }

}


case class LoginRequest(email:String, password:String)

trait RendersUserAsJson {

  implicit val userReads:Reads[User] = (
    (JsPath \ "id").read[UUID] and (JsPath \ "name").read[String] and (JsPath \ "email").read[String]
    )(User.apply _)

  def userToJson(user:User):JsValue = Json.obj(
    "id" -> user.uuid,
    "name" -> user.name,
    "email" -> user.email
  )

}


class DefaultUserController @Inject() (
  override val userRepo:UserRepository,
  override val authentication: AuthenticationService,
  override val projectRepo: ProjectRepository,
  override val messagesApi: MessagesApi
) extends UserController with Controller {
}
