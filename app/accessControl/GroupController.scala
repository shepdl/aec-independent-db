package accessControl

import java.util.UUID
import javax.inject.Inject

import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc.{Action, Controller}
import play.api.libs.functional.syntax._
import play.api.libs.json._

/**
  * Created by dave on 11/18/16.
  */
trait GroupController extends I18nSupport with RendersGroupAsJson {

  this:Controller=>

  val userRepo:UserRepository
  val groupRepo:GroupRepository
  val authentication:AuthenticationService

  val createForm:Form[GroupCreationRequest] = Form(
    mapping(
      "name" -> nonEmptyText,
      "homepage" -> nonEmptyText
    )(GroupCreationRequest.apply)(GroupCreationRequest.unapply)
  )

  implicit val groupWrites:Writes[Group] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "name").write[String] and (JsPath \ "homepage").write[String] and (JsPath \ "system").write[Boolean]
  )(unlift(Group.unapply))

  implicit val groupReads:Reads[Group] = (
    (JsPath \ "id").read[UUID] and (JsPath \ "name").read[String] and (JsPath \ "homepage").read[String]
  )(Group.apply _)

  def create = authentication.LoginRequiredAction(parse.urlFormEncoded) { implicit request =>
    createForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      groupData => {
        val newGroup = Group(null, groupData.title, groupData.homepage)
        val savedGroup = groupRepo.save(newGroup)
        groupRepo.addUser(savedGroup, request.user, GroupMembershipProperties.roles.admin)
        groupRepo.addUser(savedGroup, request.user, GroupMembershipProperties.roles.member)
        val usersInGroup = groupRepo.listUsers(savedGroup)
        Created(groupToJson(savedGroup, usersInGroup))
      }
    )
  }

  val ACCEPTABLE_SORT_FIELDS = Set(
    "createdAt", "title", "homepage"
  )

  def list(page:Int, rpp:Int, sortBy:String = "createdAt") = Action { implicit request =>
    val groups = groupRepo.list(page, sortBy, rpp)
    Ok(Json.obj(
      "groups" -> groups.map(group => {
        val usersInGroup = groupRepo.listUsers(group)
        groupToJson(group, usersInGroup)
      }))
    )
  }

  def getUserGroups = authentication.LoginRequiredAction { implicit request =>
    Ok(Json.obj(
      "user" -> userToJson(request.user),
      "groups" -> groupRepo.listGroupsForUser(request.user).map(groupMembership => Json.obj(
        "group" -> groupToJson(groupMembership.group, List()),
        "roles" -> groupMembership.roles
      ))
    ))
  }

  def getById(id:UUID) = Action { implicit request =>
    groupRepo.getById(id) match {
      case None => NotFound(Json.obj(
        "error" -> "Group does not exist"
      ))
      case Some(group) => {
        val usersInGroup = groupRepo.listUsers(group)
        Ok(groupToJson(group, usersInGroup))
      }
    }
  }

  def isUserGroupAdmin(group:Group, user:User):Boolean =
    groupRepo.getRolesInGroup(group, user).roles.contains(GroupMembershipProperties.roles.admin)

  def update(id:UUID) = authentication.LoginRequiredAction(parse.json) { implicit request =>
    groupRepo.getById(id) match {
      case None => NotFound(Json.obj("error" -> s"Group ${id} does not exist"))
      case Some(group) => {
        if (!groupRepo.canEditGroup(group, request.user)) {
          Forbidden(Json.obj( "error" -> s"User must be administrator" ))
        } else {
          request.body.validate[Group].fold(
            errors => BadRequest(JsError.toJson(errors)),
            newGroup => {
              val updatedGroup = groupRepo.save(newGroup)
              Ok(groupToJson(updatedGroup, List()))
            }
          )
        }
      }
    }
  }

  def delete(id:UUID) = authentication.LoginRequiredAction { implicit request =>
    groupRepo.getById(id) match {
      case None => NotFound(Json.obj("error" -> s"Group ${id} does not exist"))
      case Some(group) => {
        if (!groupRepo.canDeleteGroup(group, request.user)) {
          Forbidden(Json.obj( "error" -> s"User must be administrator" ))
        } else {
          groupRepo.delete(group)
          Ok(Json.obj("message" -> "Group deleted"))
        }
      }
    }
  }

  val updateMembershipForm:Form[GroupMembershipUpdateRequest] = Form(
    mapping(
      "userId" -> uuid,
      "roles" -> nonEmptyText
    )(GroupMembershipUpdateRequest.apply)(GroupMembershipUpdateRequest.unapply)
  )

  def addUser(groupId:UUID) = authentication.LoginRequiredAction(parse.urlFormEncoded) { implicit request =>
    groupRepo.getById(groupId) match {
      case None => NotFound(s"Group ${groupId} does not exist")
      case Some(group) => {
        if (!groupRepo.canAddUsersToGroup(group, request.user)) {
          Forbidden(Json.obj("error" -> "Only group administrators may perform this action"))
        } else {
          updateMembershipForm.bindFromRequest.fold(
            formWithErrors => BadRequest(formWithErrors.errorsAsJson),
            membershipData => userRepo.getById(membershipData.userId) match {
              case None => BadRequest(Json.obj("error" -> s"User ${membershipData.userId} does not exist"))
              case Some(user) => {
                if (membershipData.roles.split(",").forall(GroupMembershipProperties.roles.all.contains(_))) {
                  membershipData.roles.split(",").foreach(
                    role => {
                      groupRepo.addUser(group, user, role)
                    }
                  )
                  Created(Json.obj("message" -> "Membership updated"))
                } else {
                  BadRequest(Json.obj("error" -> "Invalid role submitted"))
                }
              }
            }
          )
        }
      }
    }
  }

  def removeUser(groupId:UUID, userId:UUID) = authentication.LoginRequiredAction { implicit request =>
    groupRepo.getById(groupId) match {
      case None => NotFound(s"Group ${groupId} does not exist")
      case Some(group) => {
        userRepo.getById(userId) match {
          case None => NotFound(s"User ${userId} does not exist")
          case Some(userToRemove) =>
            if (!groupRepo.canRemoveUserFromGroup(group, request.user, userToRemove)) {
              Forbidden(Json.obj("error" -> "Only group administrators may remove another user from a group."))
            } else {
              val status = groupRepo.removeUser(group, userToRemove)
              Ok(Json.obj("message" -> "User removed"))
            }
        }
      }
    }
  }

  def updateUserRoles(groupId:UUID, userId:UUID) = authentication.LoginRequiredAction(parse.json) { implicit request =>
    val newRoles = (Json.parse(request.body.toString()) \ "roles").as[Set[String]]
    groupRepo.getById(groupId) match {
      case None => NotFound(Json.obj("error" -> s"Group ${groupId} not found"))
      case Some(group) => userRepo.getById(userId) match {
        case None => NotFound(Json.obj("error" -> s"User {$userId} not found"))
        case Some(userToUpdate) => {
          if (!groupRepo.canChangeUserRolesInGroup(group, request.user)) {
            Forbidden(Json.obj("error" -> s"User is not allowed to update roles for users in group"))
          } else {
            val status = groupRepo.setUserRolesInGroup(group, userToUpdate, newRoles)
            Ok(groupToJson(
              group, List(status)
            ))
          }
        }
      }
    }
  }

  def listUsersNotInGroup(groupId:UUID, page:Int, rpp:Int) = Action { implicit request =>
    groupRepo.getById(groupId) match {
      case None => NotFound(Json.obj("error" -> s"Group ${groupId} does not exist"))
      case Some(group) => {
        val users = groupRepo.listUsersNotInGroup(group, page, UserProperties.name, rpp)
        Ok(Json.obj("users" -> users.map(userToJson)))
      }
    }
  }

}

case class GroupCreationRequest(title:String, homepage:String)
case class GroupMembershipUpdateRequest(userId:UUID, roles:String)


trait RendersGroupAsJson extends RendersUserAsJson {

  def groupToJson(group:Group, users:List[GroupMembership]) = Json.obj(
    "id" -> group.id,
    "name" -> group.name,
    "homepage" -> group.homepage,
    "users" -> users.map(user => Json.obj(
      "roles" -> user.roles,
      "id" -> user.user.uuid,
      "name" -> user.user.name
    ))
  )

}


class DefaultGroupController @Inject()(
    override val userRepo:UserRepository,
    override val groupRepo:GroupRepository,
    override val authentication:AuthenticationService,
    override val messagesApi: MessagesApi
) extends GroupController with Controller

