package accessControl

import java.util.{Date, UUID}
import javax.inject.Inject

import common.DatabaseService
import org.neo4j.driver.v1.Transaction
import org.neo4j.driver.v1.types.Node
import play.api.Logger

/**
  * Created by dave on 11/17/16.
  */
class GroupRepository @Inject()(val db:DatabaseService)
  extends ConvertsNodesToUsers with ConvertsNodesToGroups {

  val logger = Logger("Group Repository")


  import scala.collection.JavaConversions._

  def save(group:Group):Group = db.withTransaction(tx => {
    val newGroup = save(group, tx)
    tx.success()
    newGroup
  })

  def save(group:Group, tx:Transaction):Group = {
    val now = new Date().getTime.toString
    val id = if (group.id == null) {
      val newId = UUID.randomUUID()
      tx.run(
        s"""
           |CREATE (g:${GroupProperties.label}${if (group.isSystem) s":${SystemGroupProperties.label}" else ""} {
           |  ${GroupProperties.id} : {${GroupProperties.id}},
           |  ${GroupProperties.name} : {${GroupProperties.name}},
           |  ${GroupProperties.homepage} : {${GroupProperties.homepage}},
           |  ${GroupProperties.createdAt} : {${GroupProperties.createdAt}}
           |})
         """.stripMargin, Map(
          GroupProperties.id -> newId.toString,
          GroupProperties.name -> group.name,
          GroupProperties.homepage -> group.homepage,
          GroupProperties.createdAt -> now
        )
      )
      newId
    } else {
      tx.run(
        s"""
           |MATCH (g:${GroupProperties.label} {
           |  ${GroupProperties.id} : ${GroupProperties.id}
           |})
           |WITH g SET
           |  g.${GroupProperties.name} = {${GroupProperties.name}},
           |  g.${GroupProperties.homepage} = {${GroupProperties.homepage}},
           |  g.${GroupProperties.updatedAt} = {${GroupProperties.updatedAt}}
           |RETURN g
         """.stripMargin, Map(
          GroupProperties.id -> group.id.toString,
          GroupProperties.name -> group.name.asInstanceOf[Object],
          GroupProperties.homepage -> group.homepage.asInstanceOf[Object],
          GroupProperties.updatedAt -> now
        )
      )
      group.id
    }

    Group(id, group.name, group.homepage)
  }

  def delete(id:Group) = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {${GroupProperties.id}}
         |})<-[:${GroupMembershipProperties.label}]-(u)
         |DETACH DELETE g
       """.stripMargin, Map(
        GroupProperties.id -> id.toString
      )
    )
    tx.success()
  })

  def getById(id:UUID):Option[Group] = db.withTransaction(tx => {
    val result = tx.run(s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})
         |RETURN g
       """.stripMargin, Map(
      "groupId" -> id.toString
    )).map(row => {
      val node = row.get("g").asNode
      node2Group(node)
    }).toList.headOption
    result
  })


  def canViewGroup(group:Group, user:User):Boolean = true

  def canEditGroup(group:Group, user:User):Boolean =
    getRolesInGroup(group, user).roles.contains(GroupMembershipProperties.roles.admin)

  def canDeleteGroup(group:Group, user:User):Boolean =
    getRolesInGroup(group, user).roles.contains(GroupMembershipProperties.roles.admin)

  def addUser(group:Group, user:User, role:String):Boolean = db.withTransaction(tx => {
    val groupMembershipsWithThisRole = tx.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})<-[gp:${GroupMembershipProperties.label} {
         |  ${GroupMembershipProperties.role} : {${GroupMembershipProperties.role}}
         |}]-(u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |RETURN gp
       """.stripMargin, Map[String,Object](
        "groupId" -> group.id.toString,
        GroupMembershipProperties.role -> role,
        "userId" -> user.uuid.toString
      )).size

    val result = if (groupMembershipsWithThisRole > 0) {
      false
    } else {
      val now = new Date().getTime.toString
      tx.run(
        s"""
           |MATCH (g:${GroupProperties.label} {
           |  ${GroupProperties.id} : {groupId}
           |}), (u:${UserProperties.label} {
           |  ${UserProperties.id} : {userId}
           |})
           |CREATE (g)<-[gp:${GroupMembershipProperties.label} {
           |    ${GroupMembershipProperties.role} : {${GroupMembershipProperties.role}},
           |    ${GroupMembershipProperties.joinedOn} : {${GroupMembershipProperties.joinedOn}}
           |  }]-(u)
           |RETURN gp
         """.stripMargin, Map[String,AnyRef](
          "groupId" -> group.id.toString,
          "userId" -> user.uuid.toString,
          GroupMembershipProperties.role -> role,
          GroupMembershipProperties.joinedOn -> now
        )
      )
      true
    }

    tx.success()
    result
  })

  def removeUser(group:Group, user:User):Boolean = db.withTransaction(tx => {
    val groupMembershipsForThisUser = tx.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})
         |      <-[gp:${GroupMembershipProperties.label}]-
         |      (u:${UserProperties.label} {
         |        ${UserProperties.id} : {userId}
         |      })
         |RETURN gp
       """.stripMargin, Map[String,AnyRef](
        "groupId" -> group.id.toString,
        "userId" -> user.uuid.toString
      )).size
    if (groupMembershipsForThisUser == 0) {
      false
    } else {
      tx.run(
        s"""
           |MATCH (g:${GroupProperties.label} {
           |  ${GroupProperties.id} : {groupId}
           |})<-[gp:${GroupMembershipProperties.label}]-
           |(u:${UserProperties.label} {
           |  ${UserProperties.id} : {userId}
           |})
           |DELETE gp
       """.stripMargin, Map(
          "groupId" -> group.id.toString,
          "userId" -> user.uuid.toString
        )
      )
      tx.success()
      true
    }
  })

  def removeUserRole(group:Group, user:User, role:String):Boolean = db.withTransaction(tx => {
    val groupMembershipsWithThisRole = tx.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})
         |      <-[gp:${GroupMembershipProperties.label} {
         |        ${GroupMembershipProperties.role} : {${GroupMembershipProperties.role}}
         |      }]-
         |        (u:${UserProperties.label} {
         |          ${UserProperties.id} : {userId}
         |        })
         |RETURN gp
       """.stripMargin, Map[String,AnyRef](
        "groupId" -> group.id.toString,
        GroupMembershipProperties.role -> role,
        "userId" -> user.uuid.toString
      )).size

    val result = if (groupMembershipsWithThisRole == 0) {
      false
    } else {
      tx.run(
        s"""
           |MATCH (g:${GroupProperties.label} {
           |  ${GroupProperties.id} : {groupId}
           |})
           |      <-[gp:${GroupMembershipProperties.label} {
           |        ${GroupMembershipProperties.role} : {${GroupMembershipProperties.role}}
           |      }]-
           |      (u:${UserProperties.label} {
           |        ${UserProperties.id} : {userId}
           |      })
           |DELETE gp
         """.stripMargin, Map[String,AnyRef](
          GroupProperties.id -> group.id.toString,
          UserProperties.id -> user.uuid.toString,
          GroupMembershipProperties.role -> role
        )
      )
      true
    }

    tx.success()
    result
  })

  def getRolesInGroup(group:Group, user:User):GroupMembership = db.withSession(session => {
    val userRoles = session.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})<-[gp:${GroupMembershipProperties.label}]-(u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |RETURN gp.${GroupMembershipProperties.role} AS roleName
       """.stripMargin, Map[String,AnyRef](
        "groupId" -> group.id.toString,
        "userId" -> user.uuid.toString
      )
    ).map(row => row.get("roleName").asString).toSet
    GroupMembership(user, group, userRoles)
  })

  def setUserRolesInGroup(group:Group, user:User, newRoles:Set[String]): GroupMembership = db.withTransaction(tx => {
    val rolesInDB = tx.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})<-[gp:${GroupMembershipProperties.label}]-(u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |RETURN gp.${GroupMembershipProperties.role}
       """.stripMargin, Map[String,AnyRef](
        "groupId" -> group.id.toString,
        "userId" -> user.uuid.toString
      )
    ).map(row => {
      row.get(s"gp.${GroupMembershipProperties.role}").asString
    }).toSet
    val rolesToAdd = newRoles &~ rolesInDB
    val rolesToRemove = rolesInDB &~ newRoles

    val now = new Date().getTime
    tx.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})<-[gp:${GroupMembershipProperties.label}]-(u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |WHERE gp.${GroupMembershipProperties.role} IN {roles}
         |DELETE gp
       """.stripMargin, Map[String,AnyRef](
        "groupId" -> group.id.toString,
        "userId" -> user.uuid.toString,
        "roles" -> rolesToRemove
      )
    )
    rolesToAdd.foreach(role => {
      tx.run(
        s"""
           |MATCH (g:${GroupProperties.label} {
           |  ${GroupProperties.id} : {groupId}
           |}), (u:${UserProperties.label} {
           |  ${UserProperties.id} : {userId}
           |})
           |CREATE (g)<-[gp:${GroupMembershipProperties.label} {
           |  ${GroupMembershipProperties.role} : {${GroupMembershipProperties.role}},
           |  ${GroupMembershipProperties.joinedOn} : {${GroupMembershipProperties.joinedOn}}
           |}]-(u)
         """.stripMargin, Map[String,AnyRef](
          "groupId" -> group.id.toString,
          "userId" -> user.uuid.toString,
          GroupMembershipProperties.role -> role,
          GroupMembershipProperties.joinedOn -> now.asInstanceOf[Object]
        ))
    })
    tx.success()

    GroupMembership(
      user, group, newRoles
    )
  })

  def canAddUsersToGroup(group:Group, user:User):Boolean = getRolesInGroup(group, user).roles.nonEmpty

  def canRemoveUserFromGroup(group:Group, user:User, userToRemove:User):Boolean =
    user == userToRemove || getRolesInGroup(group, user).roles.contains(GroupMembershipProperties.roles.admin)

  def canChangeUserRolesInGroup(group:Group, user:User):Boolean =
    getRolesInGroup(group, user).roles.contains(GroupMembershipProperties.roles.admin)


  val ALLOWED_SORT_FIELDS = Set(
    GroupProperties.name, GroupProperties.createdAt, GroupProperties.updatedAt,
    GroupMembershipProperties.joinedOn, GroupMembershipProperties.role
  )

  def list(page:Int, sortBy:String, rpp:Int):List[Group] = db.withTransaction(tx => {
    val sortByField = if (ALLOWED_SORT_FIELDS.contains(sortBy)) sortBy else GroupMembershipProperties.joinedOn
    val skip = page * rpp
    val groups = tx.run(
      s"""
        |MATCH (g:${GroupProperties.label})
        |WHERE NOT g:${SystemGroupProperties.label}
        |RETURN g
        |ORDER BY g.${sortByField}
        |SKIP ${skip} LIMIT ${rpp}
      """.stripMargin, Map(
        "sortByField" -> sortByField.asInstanceOf[Object],
        "skip" -> skip.asInstanceOf[Object],
        "rpp" -> rpp.asInstanceOf[Object]
        )
    ).map(row =>  {
        node2Group(row.get("g").asNode)
      }
    ).toList
    groups
  })

  def listGroupsForUser(user:User):List[GroupMembership] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})-[membership:${GroupMembershipProperties.label}]->(group:${GroupProperties.label})
         |RETURN membership.${GroupMembershipProperties.role} AS role, group
       """.stripMargin, Map(
        "userId" -> user.uuid.toString
      )
    ).foldLeft(Map[UUID, GroupMembership]())({
      case (acc, row) => {
        val group = node2Group(row.get("group").asNode())
        val role = row.get("role").asString()
        val newRoles = if (!acc.contains(group.id)) {
          Set(role)
        } else {
          Set(role) ++ acc(group.id).roles
        }
        acc.updated(group.id, GroupMembership(user, group, newRoles))
      }
    }).values.toList
  })

  def listUsers(group:Group, page:Int = 0, sortBy:String = GroupMembershipProperties.joinedOn, rpp:Int = 999999):List[GroupMembership] = db.withTransaction(tx => {
    val sortByField = if (ALLOWED_SORT_FIELDS.contains(sortBy)) sortBy else GroupMembershipProperties.joinedOn
    // BUG: This counts user membership records, not users. Need to fix this.
    val skip = page * rpp
    val userRoles = tx.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |})<-[gp:${GroupMembershipProperties.label}]-(u:${UserProperties.label})
         |RETURN u, gp, gp.${GroupMembershipProperties.role}
         |ORDER BY gp.${sortByField}
         |SKIP ${skip} LIMIT ${rpp}
       """.stripMargin, Map(
        "groupId" -> group.id.toString
      )
    ).foldLeft(Map[User, GroupMembership]()) ({
      case (acc, row) => {
        val user = nodeToUser(row.get("u").asNode)
        val role = row.get(s"gp.${GroupMembershipProperties.role}").asString()
        val newMembership = if (acc.contains(user)) {
          GroupMembership(user, group, acc(user).roles + role)
        } else {
          GroupMembership(user, group, Set(role))
        }
        acc.updated(user, newMembership)
      }
    })
    tx.close()
    userRoles.values.toList
  })

  def listUsersWithRole(group:Group, role:String, page:Int, sortBy:String, rpp:Int):List[User] = db.withSession(session => {
    val sortByField = if (ALLOWED_SORT_FIELDS.contains(sortBy)) sortBy else GroupMembershipProperties.joinedOn
    val skip = page * rpp
    val users = session.run(
      s"""
         |MATCH (g:${GroupProperties.label} {
         |  ${GroupProperties.id} : {${GroupProperties.id}}
         |})<-[
         |  gp:${GroupMembershipProperties.label}}  {
         |    ${GroupMembershipProperties.role} : {${GroupMembershipProperties.role}}
         |  }
         | ]-(u:${UserProperties.label})
         |RETURN u, gp, gp.${GroupMembershipProperties.role}
         |ORDER BY gp.${sortByField}
         |SKIP ${skip} LIMIT ${rpp}
       """.stripMargin, Map[String,AnyRef](
        GroupProperties.id -> group.id.toString,
        GroupMembershipProperties.role -> role
      )
    ).map(row => nodeToUser(row.get("u").asNode)).toList
    users
  })

  def listUsersNotInGroup(group:Group, page:Int, sortBy:String, rpp:Int):List[User] = db.withTransaction(tx => {
    val sortByField = if (ALLOWED_SORT_FIELDS.contains(sortBy)) sortBy else GroupMembershipProperties.joinedOn
    val skip = page * rpp
    val users = tx.run(
        s"""
           |MATCH (u:${UserProperties.label}), (g:${GroupProperties.label} {
           |  ${GroupProperties.id} : {groupId}
           |})
           |WHERE NOT (u)-[:${GroupMembershipProperties.label}]->(g)
           |RETURN u
           |SKIP ${skip} LIMIT ${rpp}
       """.stripMargin, Map(
        "groupId" -> group.id.asInstanceOf[Object]
      )
    ).map(row => {
      nodeToUser(row.get("u").asNode())
    }).toList
    users
  })


}


trait ConvertsNodesToGroups {

  implicit def node2Group(node: Node) = Group(
    UUID.fromString(node.get(GroupProperties.id).asString()),
    node.get(GroupProperties.name).asString(),
    node.get(GroupProperties.homepage).asString()
  )

}

object GroupProperties {

  val label = "Group"

  val id = "id"
  val name = "name"
  val homepage = "homepage"

  val createdAt = "createdAt"
  val updatedAt = "updatedAt"

}

object SystemGroupProperties {

  val label = "System"

}

object GroupMembershipProperties {
  val label = "IsMemberOf"


  object roles {
    val member = "MEMBER"
    val admin = "ADMINISTRATOR"
    val all = Set(member, admin)
  }

  val role = "role"
  val joinedOn = "joinedOn"
}
