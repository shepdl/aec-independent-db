package accessControl

import java.util.UUID
import javax.inject.Inject

import play.api.Logger
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.Future

class UserRequest[A](val user:User, request:Request[A]) extends WrappedRequest[A](request)

class AuthenticationService @Inject() (userRepo:UserRepository) {

  val logger = Logger("AuthenticationService")

  val NoUser = User(null, "NO USER", "user_does_not_exist@local")

  def UserAwareAction = new ActionBuilder[UserRequest] with ActionTransformer[Request, UserRequest] with Results {

    val USER_ID_KEY = "userId"

    override def transform[A](request: Request[A]): Future[UserRequest[A]] = Future.successful {
      if (request.session.get(USER_ID_KEY).isEmpty) {
        new UserRequest(userRepo.NOBODY, request)
      } else {
        val userId = UUID.fromString(request.session(USER_ID_KEY))
        userRepo.getById(userId) match {
          case None => new UserRequest[A](userRepo.NOBODY, request)
          case Some(user) => new UserRequest(user, request)
        }
      }
    }
  }

  private def ActionRequiresLogin = new ActionFilter[UserRequest] with Results {

    override def filter[A](request: UserRequest[A]) = Future.successful({
      request.user match {
        case userRepo.NOBODY => Some(Forbidden(Json.obj(
            "error" -> "You must be logged in to perform this action"
          )))
        case NoUser => {
          Some(Forbidden(Json.obj(
            "error" -> "You must be logged in to perform this action"
          )))
        }
        case _ => {
          None
        }
      }
    })
  }

  def LoginRequiredAction = UserAwareAction andThen ActionRequiresLogin

}

