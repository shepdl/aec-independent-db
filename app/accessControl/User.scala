package accessControl

import java.util.{Date, UUID}

case class User(uuid:UUID, name:String, email:String) {

  def this(name:String, email:String) = this(null, name, email)

}
