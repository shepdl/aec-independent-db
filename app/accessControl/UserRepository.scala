package accessControl

import java.util.{Date, UUID}
import javax.inject.Inject

import com.google.inject.Singleton

import common.{DatabaseService, TimestampsProperties}
import org.mindrot.jbcrypt.BCrypt
import org.neo4j.driver.v1.Record
import org.neo4j.driver.v1.types.Node
import play.api.Logger
import projects.{Project, ProjectProperties, ProjectRepository, ProjectUserRights}

@Singleton
class UserRepository @Inject()(db:DatabaseService, projectRepo:ProjectRepository, groupRepo:GroupRepository) extends ConvertsNodesToUsers {

  import scala.collection.JavaConversions._

  val logger = Logger("User Repository")

  private def initializeAdminUser = db.withTransaction({ implicit tx =>
    val result = tx.run(
      s"""
         |MATCH (user:${UserProperties.label} {${UserProperties.email}: "admin" })
         |RETURN user.${UserProperties.id} AS ${UserProperties.id}
       """.stripMargin).map(row => {
         UUID.fromString(row.get(UserProperties.id).asString())
      }
    )

    val adminUserId = if (result.isEmpty) {
      val user = save(User(
        null, "Admin", "admin"
      ))
      setPasswordForUser(user, "admin")
      logger.info(s"Created Admin user with password 'admin' and UUID ${user.uuid}")
      tx.success()
      user.uuid
    } else {
      result.next()
    }

    adminUserId
  })

  val ADMIN_USER = getById(initializeAdminUser).get

  private def initializeNobody = db.withTransaction({ implicit tx => 
    val result = tx.run(
      s"""
         |MATCH (user:${UserProperties.label} {${UserProperties.email}: "nobody" })
         |RETURN user.${UserProperties.id} AS ${UserProperties.id}
       """.stripMargin).map(row => {
         UUID.fromString(row.get(UserProperties.id).asString())
      }
    )

    val nobodyUserId = if (result.isEmpty) {
      val user = save(User(
        null, "Nobody", "nobody"
      ))
      setPasswordForUser(user, "nobody")
      logger.info(s"Created 'Nobody' user with password 'nobody' and UUID ${user.uuid}")
      tx.success()
      user.uuid
    } else {
      result.next()
    }

    nobodyUserId
  })

  val NOBODY = getById(initializeNobody).get


  def save(user:User):User = {
    val now = new Date().getTime.toString
    db.withTransaction({ tx =>
      val id = if (user.uuid == null) {
        val userId = UUID.randomUUID()
        val groupId = UUID.randomUUID()
        val scratchpadId = UUID.randomUUID()
        val projectNodeId = UUID.randomUUID()
        val salt = BCrypt.gensalt(12)

        tx.run(
          s"""
             |CREATE (user:${UserProperties.label} {
             |  ${UserProperties.id} : {userId},
             |  ${UserProperties.name} : {${UserProperties.name}},
             |  ${UserProperties.email} : {${UserProperties.email}},
             |  ${UserProperties.salt} : {${UserProperties.salt}},
             |  ${UserProperties.password}: {${UserProperties.salt}},
             |  ${UserProperties.createdAt} : {now},
             |  ${UserProperties.updatedAt} : {now}
             |}), (group:${GroupProperties.label}:${SystemGroupProperties.label} {
             |  ${GroupProperties.id} : {groupId},
             |  ${GroupProperties.name} : {groupTitle},
             |  ${GroupProperties.homepage} : "none",
             |  ${GroupProperties.createdAt} : {now}
             |}), (project:${ProjectProperties.label}:${ProjectProperties.current}:${SystemGroupProperties.label} {
             |  ${ProjectProperties.id} : {projectId},
             |  ${ProjectProperties.title} : "${UserProperties.SCRATCHPAD_NAME}",
             |  ${ProjectProperties.nodeId} : {${ProjectProperties.nodeId}}
             |}),
             |
             |(user)-[:${GroupMembershipProperties.label} {
             |  ${GroupMembershipProperties.role} : {role}
             |}]->(group),
             |
             |(user)-[:${ProjectUserRights.label} {
             |   ${ProjectUserRights.canEditMetadata} : false,
             |   ${ProjectUserRights.canDeleteProject} : false,
             |   ${ProjectUserRights.canEditUsers} : false,
             |   ${ProjectUserRights.canAddNewArtifacts} : true,
             |   ${ProjectUserRights.canEditOthersArtifacts} : true,
             |   ${ProjectUserRights.canDeleteArtifacts} : true,
             |   ${ProjectUserRights.canAddNewSchemas} : true,
             |   ${ProjectUserRights.canEditOthersSchemas} : true,
             |   ${ProjectUserRights.canDeleteSchemas} : true,
             |   ${ProjectUserRights.canAddAnnotations} : true,
             |   ${ProjectUserRights.canEditOthersAnnotations} : true,
             |   ${ProjectUserRights.canDeleteAnnotations} : true,
             |   ${ProjectUserRights.canApproveOthersAnnotations} : false,
             |   ${ProjectUserRights.canRemoveApprovedAnnotations} : false
             |}]->(project)
             |
           """.stripMargin, Map(
            "userId" -> userId.toString,
            "groupId" -> groupId.toString,
            "projectId" -> scratchpadId.toString,
            UserProperties.name -> user.name,
            UserProperties.email -> user.email,
            UserProperties.salt -> salt,
            UserProperties.password -> "",
            "now" -> now,
            "role" -> GroupMembershipProperties.roles.admin,
            // Project
            "groupTitle" -> s"${user.name}'s private group",
            ProjectProperties.nodeId -> projectNodeId.toString
          )
        )

        userId
      } else {
        tx.run(
          s"""
             |MATCH (u: ${UserProperties.label} {
             |  ${UserProperties.id} : {${UserProperties.id}}
             |})
             |WITH u SET
             |  u.${UserProperties.name} = {${UserProperties.name}},
             |  u.${UserProperties.email} = {${UserProperties.email}},
             |  u.${UserProperties.updatedAt} = {${UserProperties.updatedAt}}
             |RETURN u
           """.stripMargin, Map(
            UserProperties.id -> user.uuid.toString,
            UserProperties.name -> user.name,
            UserProperties.email -> user.email,
            UserProperties.updatedAt -> now
          )
        )
        user.uuid
      }
      tx.success()
      User(id, user.name, user.email)
    })
  }

  def getById(id:UUID):Option[User] = db.withTransaction({ tx =>
    val result = tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.id} : {${UserProperties.id}}
         |})
         |RETURN u
       """.stripMargin, Map(
        UserProperties.id -> id.toString
      )).toList

    if (result.nonEmpty) {
      val userRecord = result.head
      Some(nodeToUser(userRecord.get("u").asNode()))
    } else {
      None
    }
  })

  def setPasswordForUser(user:User, password:String) = db.withTransaction({ tx =>
    val now = new Date().getTime.toString
    val salt = BCrypt.gensalt(12)
    tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.id} : {${UserProperties.id}}
         |})
         |WITH u SET
         |  u.${UserProperties.salt} = {${UserProperties.salt}},
         |  u.${UserProperties.password} = {${UserProperties.password}},
         |  u.${UserProperties.updatedAt} = {${UserProperties.updatedAt}}
         |RETURN u
       """.stripMargin, Map(
        UserProperties.id -> user.uuid.toString,
        UserProperties.salt -> salt,
        UserProperties.password -> hashPassword(password, salt),
        UserProperties.updatedAt -> now
      )
    )
    tx.success()
  })

  val ACCEPTABLE_SORT_FIELDS = Set(
    UserProperties.createdAt, UserProperties.name
  )

  def hashPassword(password:String, salt:String) = BCrypt.hashpw(password, salt)

  def list(page:Int, sortBy:String, rpp:Int):List[User] = db.withTransaction { tx =>
    val sortByField = if (ACCEPTABLE_SORT_FIELDS.contains(sortBy)) {
      sortBy
    } else {
      "createdAt"
    }
    val skip = page * rpp
    tx.run(
      s"""
         |MATCH (u:${UserProperties.label})
         |RETURN u
         |ORDER BY u.${sortByField}
         |SKIP ${skip} LIMIT ${rpp}
       """.stripMargin)
      .map(row => {
        nodeToUser(row.get("u").asNode())
      }
    ).toList
  }

  def getByEmailAndPassword(email:String, inputPassword:String):Option[User] = db.withTransaction(tx => {
    val result = tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.email} : {${UserProperties.email}}
         |})
         |RETURN u, u.${UserProperties.password}, u.${UserProperties.salt}
       """.stripMargin, Map(
        UserProperties.email -> email
      )).map(row => {
      (
        nodeToUser(row.get("u").asNode()),
        row.get(s"u.${UserProperties.password}").asString(),
        row.get(s"u.${UserProperties.salt}").asString()
      )
    }).toList

    result.headOption match {
      case Some((user, dbPassword, salt)) => if (hashPassword(inputPassword, salt) == dbPassword) {
        Some(user)
      } else {
        None
      }
      case None => None
    }
  })

  def delete(user:User) = db.withTransaction { tx =>
    tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.id} : {${UserProperties.id}}
         |})
         |DETACH DELETE u
       """.stripMargin, Map(
        UserProperties.id -> user.uuid.toString
      )
    )
    tx.success()
  }

}


trait ConvertsNodesToUsers {

  def resultToUser(result:Record):User = {
    User(
      UUID.fromString(result.get(UserProperties.id).asString()),
      result.get(UserProperties.name).asString(),
      result.get(UserProperties.email).asString()
    )
  }

  def nodeToUser(node:Node) = User(
      UUID.fromString(node.get(UserProperties.id).asString()),
      node.get(UserProperties.name).asString(),
      node.get(UserProperties.email).asString()
    )

}

case object UserProperties extends TimestampsProperties {
  val label = "User"


  val id = "id"
  val name = "name"
  val email = "email"
  val password = "password"
  val salt = "salt"

  val lastLogin = "lastLogin"
  val joinedOn = "joinedAt"

  final val SCRATCHPAD_NAME = "My Scratchpad"


}
