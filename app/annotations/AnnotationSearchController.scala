package annotations

import java.util.UUID
import javax.inject.{Inject, Singleton}

import accessControl.GroupRepository
import artifacts.Model
import artifacts.MediaRepository
import backend.approvals.ApprovalRepository
import play.api.libs.json._
import play.api.mvc.{Action, _}
import projects.{Project, ProjectRepository}
import schemas.SchemaRepository

/**
  * Created by dave on 4/18/16.
  */
trait AnnotationSearchController {

  this:Controller =>

  val entityRepo:EntityRepository
  val annotationRepo:AnnotationRepository
  val schemaRepo:SchemaRepository
  val groupRepo:GroupRepository
  val approvalRepo:ApprovalRepository
  val projectRepo:ProjectRepository
  val mediaRepo:MediaRepository

  def toJson(entity:Entity):JsObject = Json.obj(
    "id" -> entity.id,
    "schema" -> Json.obj(
      "id" -> entity.schema.id,
      "title" -> entity.schema.title,
      "appliesTo" -> entity.schema.appliesTo,
      "fields" -> entity.schema.fields.map(field => Json.obj(
        "id" -> field.id,
        "title" -> field.title,
        "dataTypeId" -> field.dataType.id,
        "inputElement" -> field.inputElement,
        "inputStyle" -> field.inputStyle,
        "displayElement" -> field.displayElement,
        "displayStyle" -> field.displayStyle,
        "isControlled" -> field.isControlled,
        "value" -> entity.data(field.title)
      ))
    ),
    "data" -> Json.toJson(entity.data)
  )

  def toJson(annotation:Annotation):JsObject = Json.obj(
    "id" -> annotation.id,
    "entity" -> toJson(annotation.entity),
    "projectId" -> annotation.project.id,
    "modelId" -> annotation.model.id,
    "location" -> JsArray(annotation.location.map({
      case (x, y, z, faceIndex) => JsArray(Seq(JsNumber(x), JsNumber(y), JsNumber(z), JsNumber(faceIndex)))
    })),
    "imageData" -> annotation.imageData,
    "approvedBy" -> approvalRepo.getForItemId(annotation.id).map(approval => Json.obj(
      "id" -> approval.id,
      "group" -> Json.obj(
        "id" -> approval.group.id,
        "name" -> approval.group.name
      )
    ))
  )

  def toJson(mar:ModelAnnotationsResult):JsObject = {
    Json obj (
      "model" -> Json.obj(
          "id" -> mar.model.id,
          "title" -> mar.model.title,
          "filenames" -> mar.model.files.map(f => s"/models/${mar.model.id}/$f")
      ),
      "annotations" -> mar.annotations.map(toJson)
    )
  }

  def search = Action(parse.json) { implicit request => {
    val json = request.body

    val qualifierFactory = annotationRepo.qualifierFactory
    // iterate through each entity and build a map of qualifiers for each schema
    // at the end, assemble them into qualifiers with multiple criteria
    val withProjects = (json \ "request" \ "projects").asOpt[List[JsString]] match {
      case None => annotationRepo.queryFactory.create
      case Some(projectIds) => {
        val projectResults = projectIds.map {
          projectId => (projectId, projectRepo.getById(UUID.fromString(projectId.value)))
        }
        val projects = projectResults.filter(_._2.isDefined).map(_._2.get)

        projects.foldLeft(annotationRepo.queryFactory.create) {
          case (query, project) => query.addProject(project)
        }
      }
    }
    val withModels = (json \ "request" \ "models").asOpt[List[JsString]] match {
      case None => withProjects
      case Some(modelIds) => {
        val modelResults = modelIds.map {
          modelId => (modelId, mediaRepo.getById(UUID.fromString(modelId.value)))
        }
        val models = modelResults.filter(_._2.isDefined).map(_._2.get)

        models.foldLeft(withProjects) {
          case (query, model) => query.addModel(model)
        }
      }
    }
    val withQualifiers = (json \ "request" \ "entities").as[List[JsObject]].foldLeft(Map[UUID, Qualifier]()) {
      case (acc, entry) => {
        if ((entry \ "type").as[String] == "quality") {
          val schemaId = (entry \ "schemaId").as[UUID]
          val schema = schemaRepo.getById(schemaId).get
          val fieldName = (entry \ "fieldName").as[UUID]
          val qualifier = if (acc.contains(schemaId)) {
            acc(schemaId)
          } else {
            qualifierFactory.createForSchema(schema)
          }
          acc.updated(schema.id, qualifier.addAttribute(
            schema(fieldName),
            qualifierFactory.operandForName((entry \ "queryType").as[String]),
            (entry \ "value").as[String]
          ))
        } else {
          acc
        }
      }
    }.values.foldLeft(withModels) {
      case (query, qualifier) => query.addQualifier(qualifier)
    }

    val groups = (json \ "request" \ "approvedBy").asOpt[List[JsString]].getOrElse(List()).map(groupId => {
      (groupId, groupRepo.getById(UUID.fromString(groupId.value)))
    })

    if (groups.nonEmpty) {
      val missingGroupIds = groups.filter(_._2.isEmpty).map(_._1)
      if (!missingGroupIds.isEmpty) {
        NotFound(
          Json.obj(
            "error" -> "missing groups",
            "groupIds" -> missingGroupIds
          )
        )
      } else {

        val asQuery = groups.foldLeft(withQualifiers) {
          case (query, group) => query.addApprovedByGroup(group._2.get)
        }

        val result = annotationRepo.search(asQuery)

        Ok(Json obj(
          "result" -> result.map(toJson)
          ))
      }
    } else {

      val result = annotationRepo.search(withQualifiers)

      Ok(Json obj(
        "result" -> result.map(toJson)
        )
      )
    }

  }
  }

}

case class ModelAnnotationsResult(model:Model, annotations:List[Annotation])


case class AnnotationSearchForm(json:String)

case class ProjectSearch(projects:List[Long])

case class EntitySearch(schemaId:Long, data:Map[String,String])

sealed trait GroupByQualifier
case object Projects extends GroupByQualifier
case object Entities extends GroupByQualifier

case class AnnotationSearch(projects:List[ProjectSearch], entities:List[EntitySearch], groupBy:GroupByQualifier)


@Singleton
class DefaultAnnotationSearchController @Inject() (
  val entityRepo: EntityRepository,
  val projectRepo:ProjectRepository,
  val schemaRepo: SchemaRepository,
  val annotationRepo: AnnotationRepository,
  val groupRepo:GroupRepository,
  val approvalRepo:ApprovalRepository,
  val mediaRepo:MediaRepository
  ) extends AnnotationSearchController with Controller
