package annotations

import java.util.UUID
import javax.inject.Inject

import accessControl.{OwnableProperties, User, UserProperties}
import common.{DatabaseService, GraphEntityProperties, TimestampsProperties, VersionedGraphEntityProperties}
import org.neo4j.driver.v1.{StatementRunner, Transaction}
import schemas._


object EntityProperties extends GraphEntityProperties with VersionedGraphEntityProperties with OwnableProperties with TimestampsProperties {

  val label = "Entity"

  val schema = "schema"

}

object HasValueRelationshipProperties extends GraphEntityProperties with TimestampsProperties {

  val label = "hasValue"

  val name = "name"

}

object ControlledVocabularyTermProperties extends GraphEntityProperties with TimestampsProperties {

  val label = "controlledVocabularyTerm"
  val field = "field"

  val ordering = "ordering"

}

class EntityRepository @Inject()(val db:DatabaseService, val schemaRepo:SchemaRepository) extends ConvertsNodesToEntities {

  import scala.collection.JavaConversions._

  def save(entity:Entity, tx:Transaction):Entity = {
    if (entity.id == null) {
      val entityId = UUID.randomUUID()
      create(entity, entityId, tx)
    } else {
      update(entity, tx)
    }
  }

  def save(entity:Entity):Entity = db.withTransaction( tx => {
    val newEntity = save(entity, tx) 
    tx.success()
    newEntity
  })

  def getById(id:UUID):Option[Entity] = db.withSession(session => {
    getById(id, session)
  })

  def getById(id:UUID, statementRunner:StatementRunner) = {
    statementRunner.run(
      s"""
         |MATCH (entity: ${EntityProperties.label}:${EntityProperties.current} {
         |  ${EntityProperties.id} : {${EntityProperties.id}}
         |})-[:${EntityProperties.schema}]->(schema:${SchemaProperties.label})
         |RETURN entity, schema.id AS schemaId
       """.stripMargin, Map(
        EntityProperties.id -> id.toString
      )
    ).map(row => {
      val entityNode = row.get("entity").asNode()
      val schema = schemaRepo.getById(UUID.fromString(row.get("schemaId").asString())).get
      Entity(
        UUID.fromString(entityNode.get(EntityProperties.id).asString()),
        schema,
        entityNode.asMap().map({
          case (key, value) => key -> value.toString
        }).toMap
      )
    }).toList.headOption match {
      case None => None
      case Some(entity) => {
        val referenceRows = statementRunner.run(
          s"""
             |MATCH (entity:${EntityProperties.label}:${EntityProperties.current} {
             |  ${EntityProperties.id} : {${EntityProperties.id}}
             |})-[valueLink:${HasValueRelationshipProperties.label}]->(value:${EntityProperties.label})
             |RETURN valueLink, value.id AS valueId
           """.stripMargin, Map(
            EntityProperties.id -> id.toString
          )
        ).map {
          row => {
            val valueLink = row.get("valueLink").asRelationship()
            val valueLinkName = valueLink.get(HasValueRelationshipProperties.name).asString()
            (valueLinkName, row.get("valueId").asString())
          }
        }.toMap
        Some(Entity(
          entity.id,
          entity.schema,
          entity.data ++ referenceRows
        ))
      }
    }
  }

  private def create(entity:Entity, entityId:UUID, tx:Transaction) = {
    val newVersionId = UUID.randomUUID()
    val initialValues = Map[String,AnyRef](
      EntityProperties.id -> entityId.toString,
      "versionId" -> newVersionId.toString,
      "schemaId" -> entity.schema.id.toString
    )
    val (propertySettingQuery, referenceSettingQuery, values) = entity.schema.fields.foldLeft(
      (List[String](), List[String](), initialValues)) {
      case ((propertySettingQuery, referenceSettingQuery, values), field) => {
        if (schemaRepo.isBaseType(field.dataType)) {
          (
            s"  `${field.title}` : {${field.title.replace(" ", "_")}}" :: propertySettingQuery,
            referenceSettingQuery,
            values.updated(field.title.replace(" ", "_"), entity.data(field.title))
          )
        } else {
          (
            // TODO: I removed a colon and a label from the node (3 lines below this). That may cause errors later.
            propertySettingQuery,
            s"""(entity)-[:${HasValueRelationshipProperties.label} {
                 ${HasValueRelationshipProperties.name} : ${field.title.replace(" ", "_")}
              }]->( {
                ${EntityProperties.id} : {${field.title.replace(" ", "_")}}
              })
              """ :: referenceSettingQuery,
            values.updated(field.title.replace(" ", "_"), entity.data(field.title))
          )
        }
      }
    }
    // TODO: handle which version of schema Entity is using ...
    tx.run(
      s"""
         |MATCH (schema:${SchemaProperties.label}:${SchemaProperties.current} {
         |    ${SchemaProperties.id} : {schemaId}
         |})
         |CREATE (entity:${EntityProperties.label}:${EntityProperties.current} {
         |  ${EntityProperties.id} : {${EntityProperties.id}},
         |  ${EntityProperties.versionId} : {versionId},
         |  ${propertySettingQuery.mkString(",\n")}
         |}),
         |  (entity)-[:${EntityProperties.schema}]->(schema)
         |  ${referenceSettingQuery.mkString(",\n")}
         """.stripMargin, values
    )
    Entity(entityId, entity.schema, entity.data)
  }

  private def update(entity:Entity, tx:Transaction) = {
    val entityId = tx.run(
      s"""
         |MATCH (currentVersion:${EntityProperties.label}:${EntityProperties.current} {
         |  ${EntityProperties.id} : {entityId}
         |})
         |REMOVE currentVersion:${EntityProperties.current}
         |RETURN currentVersion.${EntityProperties.versionId}
       """.stripMargin, Map(
        "entityId" -> entity.id.toString
      )
    ).map(row => row.get(s"currentVersion.${EntityProperties.versionId}").asString())
      .toList.head
    val newEntity = create(entity, entity.id, tx)
    // TODO: This doesn't actually update the entity data
    tx.run(
      s"""
         |MATCH (oldVersion:${EntityProperties.label} {
         |  ${EntityProperties.versionId} : {oldVersionId}
         |}),
         |(newVersion:${EntityProperties.label}:${EntityProperties.current} {
         |  ${EntityProperties.id} : {entityId}
         |})
         |CREATE (newVersion)-[:${EntityProperties.previousVersion}]->(oldVersion)
       """.stripMargin, Map(
        "oldVersionId" -> entityId,
        "entityId" -> entity.id.toString
      )
    )
    newEntity
  }

  private def setVocabularyTerms(originId:UUID, originLabel:String, terms:List[Entity], tx:Transaction):List[Entity] = {
    val newTerms = terms.zipWithIndex.map({
      case (term, index) => {
        val termId = UUID.randomUUID()
        val entity = create(term, termId, tx)
        tx.run(
          s"""
             |MATCH
             |  (field:$originLabel {
             |    ${SchemaFieldProperties.id} : {schemaFieldId}
             |  }),
             |  (term:${EntityProperties.label} {
             |    ${EntityProperties.id} : {termId}
             |  })
             |CREATE
             |  (field)<-[:${ControlledVocabularyTermProperties.field} {
             |    ${ControlledVocabularyTermProperties.ordering} : {${ControlledVocabularyTermProperties.ordering}}
             |  }]-(term)
             |SET field:${ControlledVocabularyTermProperties.label}
             """.stripMargin, Map(
            "schemaFieldId" -> originId.toString,
            "termId" -> termId.toString,
            ControlledVocabularyTermProperties.ordering -> index.asInstanceOf[Object]
          )
        )
        entity
      }
    })
    newTerms
  }

  def getTermsForSchemaField(schemaFieldId:UUID):List[Entity] = db.withTransaction(tx => {
    val result = tx.run(
      s"""
         |MATCH (field:${SchemaFieldProperties.label} {
         |  ${SchemaFieldProperties.id} : {${SchemaFieldProperties.id}}
         |})<-[entityLink:${ControlledVocabularyTermProperties.field}]-(term:${EntityProperties.label})
         |RETURN term.${EntityProperties.id} AS termId
         |ORDER BY entityLink.${ControlledVocabularyTermProperties.ordering}
       """.stripMargin, Map(
        SchemaFieldProperties.id -> schemaFieldId.toString
      )).map(row => {
        val termId = row.get("termId").asString()
        getById(UUID.fromString(termId), tx).get
    })
    result.toList
  })

  def setVocabularyTermsForSchemaField(schemaField:SchemaField, terms:List[Entity]):Either[List[ValidationStatus], List[Entity]] =
    db.withTransaction(tx => {
      val newTerms = setVocabularyTermsForSchemaField(schemaField, terms, tx)
      tx.success()
      newTerms
    }
  )

  def setVocabularyTermsForSchemaField(schemaField:SchemaField, terms:List[Entity], tx:Transaction)
      :Either[List[ValidationStatus], List[Entity]] = {

    val validationErrors = terms.map(term => schemaField.dataType.validate(term.data)).filterNot(_ == Pass)

    if (validationErrors.nonEmpty) {
      Left(validationErrors)
    } else {
      Right(setVocabularyTerms(schemaField.id, SchemaFieldProperties.label, terms, tx))
    }
  }

  def setVocabularyTermsForSchema(schema:Schema, terms:List[Entity]):Either[List[ValidationStatus], List[Entity]] = db.withTransaction(tx => {
    val newTerms = setVocabularyTermsForSchema(schema, terms, tx)
    tx.success()
    newTerms
  })

  def setVocabularyTermsForSchema(schema:Schema, terms:List[Entity], tx:Transaction):Either[List[ValidationStatus], List[Entity]] = {
    val validationErrors = terms.map(term => schema.validate(term.data)).filterNot(_ == Pass)

    if (validationErrors.nonEmpty) {
      tx.failure()
      Left(validationErrors)
    } else {
      Right(setVocabularyTerms(schema.id, SchemaProperties.label, terms, tx))
    }
  }

  def getByAnnotationId(id:UUID):Option[Entity] = db.withTransaction(tx => {
    val entityId = tx.run(
      s"""
         |MATCH (a:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |  ${AnnotationProperties.id} : {annotationId}
         |})-->(entity:${EntityProperties.label})
         |RETURN entity.id AS entityId
       """.stripMargin, Map(
        "annotationId" -> id.toString
      )).map(row => UUID.fromString(row.get("entityId").asString())).next
    getById(entityId)
  })

  def getByAnnotationId(id:UUID, versionId:UUID):Option[Entity] = db.withTransaction(tx => {
    val entityId = tx.run(
      s"""
         |MATCH (a:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |  ${AnnotationProperties.id} : {annotationId}
         |  ${if (versionId != null) {s", ${AnnotationProperties.versionId}: {${AnnotationProperties.versionId}} "} else "" }
         |})-[:]->(entity:${EntityProperties.label})
         |RETURN entity.id AS entityId
       """.stripMargin, Map(
        "annotationId" -> id.toString,
        if (versionId != null) {
          AnnotationProperties.versionId -> versionId.toString
        } else null
      ).filter({
        case (k, v) => v != null
      })).map(row => UUID.fromString(row.get("entityId").asString())).next
    getById(entityId)
  })

  def delete(entity:Entity, user:User):Unit = db.withTransaction(tx => {
    delete(entity, user, tx)
  })

  def delete(entity:Entity, user:User, tx:Transaction):Unit = {
    val versionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (oldEntity:${EntityProperties.label}:${EntityProperties.current} {
         |  ${EntityProperties.id} : {entityId}
         |}),
         |(deletingUser: ${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |REMOVE oldEntity:${EntityProperties.current}
         |CREATE (newEntity:${EntityProperties.label}:${EntityProperties.current}:${EntityProperties.deleted} {
         |  ${EntityProperties.id} : {${EntityProperties.id}},
         |  ${EntityProperties.versionId} : {${EntityProperties.versionId}}
         |})-[:${EntityProperties.deletedBy}]->(deletingUser),
         |  (newEntity)-[:${EntityProperties.previousVersion}]->(oldEntity)
       """.stripMargin, Map[String,AnyRef](
          "entityId" -> entity.id.toString,
          EntityProperties.versionId -> versionId.toString,
          "userId" -> user.uuid.toString
    ))
  }

  val TERM_ORDER = "TERM_ORDER"

}


trait ConvertsNodesToEntities {

}

