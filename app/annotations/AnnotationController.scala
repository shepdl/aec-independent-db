package annotations

import java.util.UUID
import javax.inject.{Inject, Singleton}

import accessControl.{AuthenticationService, RendersGroupAsJson, User}
import artifacts.{ArtifactRepository, MediaRepository}
import backend.approvals.ApprovalRepository
import common.JsonResponses
import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, _}
import play.api.mvc.{Action, Controller => BaseController}
import projects.{ProjectRepository, ProjectService, ProjectUserRights}
import schemas._

/**
 * Created by dave on 10/14/15.
 */
trait AnnotationController extends JsonResponses with I18nSupport with RendersGroupAsJson with RendersAnnotationAsJson {

  this:BaseController =>

  val authenticationService:AuthenticationService
  val projectRepo:projects.ProjectRepository
  val projectService:ProjectService
  val schemaRepo:schemas.SchemaRepository
  val entityRepo:EntityRepository
  val artifactRepo:ArtifactRepository
  val mediaRepo:MediaRepository
  val annotationRepo:AnnotationRepository
  val approvalRepo:ApprovalRepository

  import schemas.SchemaField
  implicit val fieldWrites:Writes[SchemaField] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "title").write[String] and (JsPath \ "dataTypeId").write[Schema]
      and (JsPath \ "inputElement").write[String] and (JsPath \ "inputStyle").write[String] and (JsPath \ "displayElement").write[String]
      and (JsPath \ "displayStyle").write[String]
      and (JsPath \ "isControlled").write[Boolean] and (JsPath \ "values").write[List[Entity]]
    )(unlift(SchemaField.unapply))

  implicit val creatorWrites:Writes[User] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "name").write[String] and (JsPath \ "email").write[String]
    )(unlift(User.unapply _))

  implicit val schemaWrites:Writes[Schema] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "title").write[String] and (JsPath \ "appliesTo").write[String] and (JsPath \ "fields").write[List[SchemaField]]
      and (JsPath \ "isControlled").write[Boolean] and (JsPath \ "creator").write[User]
    )(unlift(Schema.unapply _))


  implicit val entityWrites:Writes[Entity] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "schema").write[Schema] and (JsPath \ "data").write[Map[String,String]]
  )(unlift(Entity.unapply _))

  def jsonFromLocation(json:JsValue):List[(Double, Double, Double, Int)] = json.as[Seq[JsArray]].map({
    case item => (item(0).as[JsNumber].as[Double], item(1).as[JsNumber].as[Double], item(2).as[JsNumber].as[Double], item(3).as[JsNumber].as[Int])
  }).toList

  val entityMapping = mapping(
      "name" -> nonEmptyText,
      "value" -> nonEmptyText
    )(EntityEntry.apply)(EntityEntry.unapply)

  val annotationForm:Form[AnnotationForm] = Form(
    mapping(
      "data" -> list[EntityEntry](entityMapping),
      "location" -> list[LocationData](
        mapping(
          "x" -> of(doubleFormat),
          "y" -> of(doubleFormat),
          "z" -> of(doubleFormat),
          "faceIndex" -> of(intFormat)
        )(LocationData.apply)(LocationData.unapply)
      ),
      "imageData" -> optional(nonEmptyText)
    )(AnnotationForm.apply)(AnnotationForm.unapply)
  )

  def toJson(errors:SchemaValidationError):JsObject = Json obj (
    "missingFields" -> Json.arr (errors.missingFields),
    "extraFields" -> errors.extraFields,
    "invalidValues" -> errors.invalidFields
  )

  def toJson(errors:ValidationError):JsObject = {
    val schemaProblems = if (errors.schemaProblems == Pass) Json.arr() else toJson(errors.schemaProblems.asInstanceOf[SchemaValidationError])
    Json obj (
      "errors" -> errors.missingData,
      "schemaErrors" -> schemaProblems
    )
  }


  def extractMissingFields(schema:Schema, data:JsObject):List[String] = ???
  // def extractInvalidValues(schema:Schema, data:JsObject):List[FieldValidationError] = ???

  def create(activityId:UUID, modelId:UUID, schemaName:String, schemaId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canAddAnnotations
  )(parse.urlFormEncoded) { implicit request =>
    schemaRepo.getById(schemaId) match {
      case None => NotFound(error(s"Schema ${schemaId} does not exist"))
      case Some (schema) => {
        mediaRepo.getById(modelId) match {
          case None => NotFound(error(s"Model ${modelId} not found"))
          case Some(model) => {
            annotationForm.bindFromRequest.fold(
              formWithErrors => BadRequest(formWithErrors.errorsAsJson),
              annotationData => {
                val entityData = annotationData.data.foldLeft(Map[String, String]()) {
                  case (acc, (entry)) => acc.updated(entry.name, entry.value)
                }
                val annotationToSave = Annotation(
                  null, request.project, model, Entity(null, schema, entityData),
                  annotationData.location.map(location => (location.x, location.y, location.z, location.faceIndex)), request.user,
                  annotationData.imageData.getOrElse("")
                )
                val savedAnnotation = annotationRepo.save(annotationToSave)
                Created(toJson(savedAnnotation))
              }
            )
          }
        }
      }
    }
  }

  import EntityController.jsonToEntity

  def update(activityId:UUID, modelId:UUID, schemaName:String, schemaId:UUID, annotationId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canEditOthersAnnotations
  )(parse.json) { implicit request =>
    mediaRepo.getById(modelId) match {
      case None => NotFound(missingObject("Model", modelId))
      case Some(model) => schemaRepo.getById(schemaId) match {
        case None => NotFound(missingObject("Schema", schemaId))
        case Some(schema) => annotationRepo.getById(annotationId) match {
          case None => NotFound(missingObject("Annotation", annotationId))
          case Some(oldAnnotation) => {
            val json = request.body
            (json \ "entity" \ "data").asOpt[JsObject] match {
              case None => BadRequest(Json toJson Map("Message" -> "Update failed. No entity found."))
              case Some(_) => {
                val entity = jsonToEntity(schema, (json \ "entity").get)
                val location = jsonFromLocation((json \ "location").get)
                val annotation = Annotation(
                  annotationId,
                  request.project,
                  model,
                  entity,
                  location,
                  request.user,
                  (json \ "imageData").asOpt[String].getOrElse("")
                )
                val updatedAnnotation = annotationRepo.save(annotation)
                Ok(toJson(updatedAnnotation))
              }
            }
          }
        }
      }
    }

  }

  private def renderAnnotation(annotation:Annotation) = toJson(annotation) ++ Json.obj(
    "approvedBy" -> approvalRepo.getForAnnotation(annotation).map(approval => Json.obj(
      "group" -> groupToJson(approval.group, List()),
      "id" -> approval.id
    )).toList
  )

  def get(modelId:UUID, schemaName:String, schemaId:UUID, annotationId:UUID) = Action {
    mediaRepo.getById(modelId) match {
      case None => NotFound(missingObject("Model", modelId))
      case Some(artifact) => schemaRepo.getById(schemaId) match {
        case None => NotFound(missingObject("Schema", schemaId))
        case Some(schema) => {
          annotationRepo.getById(annotationId) match {
            case None => NotFound(missingObject("Annotation", annotationId))
            case Some(annotation) => Ok(toJson(annotation))
          }
        }
      }
    }
  }

  def delete(projectId:UUID, modelId:UUID, schemaName:String, schemaId:UUID, annotationId:UUID) = authenticationService.LoginRequiredAction { implicit request =>
    mediaRepo.getById(modelId) match {
      case None => NotFound(missingObject("Model", modelId))
      case Some(artifact) => schemaRepo.getById(schemaId) match {
        case None => NotFound(missingObject("Schema", schemaId))
        case Some(schema) => annotationRepo.getById(annotationId) match {
          case None => NotFound(missingObject("Annotation", annotationId))
          case Some(annotation) => {
            annotationRepo.delete(annotation, request.user)
            Ok(Json toJson Map("message" -> "Annotation deleted"))
          }
        }
      }
    }
  }


  def getForModel(modelId:UUID) = Action {
    mediaRepo.getById(modelId) match {
      case None => NotFound(missingObject("Model", modelId))
      case Some(model) => {
        val annotations = annotationRepo.getForModel(model)
        Ok(Json obj(
          "modelId" -> JsString(modelId.toString),
          "annotations" -> annotations.map(renderAnnotation)
          ))
        }
      }
    }

  def getForProject(projectId:UUID) = projectService.ProjectRequiredAction(projectId) { implicit request =>
    val annotations = annotationRepo.getForProject(request.project)
    Ok(Json.obj(
      "projectId" -> projectId.toString,
      "annotations" -> annotations.map(toJson)
    ))
  }

  def getForModelAndProject(modelId:UUID, projectId:UUID) = projectService.ProjectRequiredAction(projectId) { implicit request =>
    mediaRepo.getById(modelId) match {
      case None => NotFound(missingObject("Model", modelId))
      case Some(model) => {
        val annotations = annotationRepo.getForModelAndProject(model, request.project)
        Ok(Json obj(
          "projectId" -> JsString(projectId.toString),
          "modelId" -> JsString(modelId.toString),
          "annotations" -> annotations.map(renderAnnotation)
          )
        )
      }
    }
  }

  def getForProjectAndModelAndSchema(projectId:UUID, modelId:UUID, schemaName:String, schemaId:UUID) = projectService.ProjectRequiredAction(
    projectId) { implicit request =>
    mediaRepo getById modelId match {
      case None => NotFound(missingObject("Model", modelId))
      case Some(model) => schemaRepo getById schemaId match {
        case None => NotFound(missingObject("Schema", schemaId))
        case Some(schema) => {
          val annotations = annotationRepo.getForModelAndProjectAndSchema(model, request.project, schema)
          Ok(Json obj(
            "projectId" -> JsString(projectId.toString),
            "modelId" -> JsString(modelId.toString),
            "annotations" -> annotations.map(toJson)
            ))
          }
        }
      }
    }

  def listVersions(activityId:UUID, modelId:UUID, schemaName:String, schemaId:UUID, annotationId:UUID, page:Int, rpp:Int) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canEditOthersAnnotations
  ) { implicit request =>
    mediaRepo getById modelId match {
      case None => NotFound(missingObject("Model", modelId))
      case Some(model) => schemaRepo getById schemaId match {
        case None => NotFound(missingObject("Schema", schemaId))
        case Some(schema) => {
          annotationRepo.getById(annotationId) match {
            case None => NotFound(missingObject("Annotation", annotationId))
            case Some(_) => Ok(Json.obj(
              "id" -> annotationId,
              "versions" -> annotationRepo.listVersions(annotationId, page, rpp).map(version => Json.obj(
                "version" -> Json.obj(
                  "versionId" -> version.versionId,
                  "updatedAt" -> version.updatedAt,
                  "action" -> version.action
                ),
                "annotation" -> toJson(version.item)
              )))
            )
          }
        }
      }
    }
  }

  def revertTo(activityId:UUID, annotationId:UUID, versionId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canEditOthersAnnotations
  ) { implicit request =>
    annotationRepo.getById(annotationId) match {
      case None => NotFound(missingObject("Annotation", annotationId))
      case Some(annotation) => {
        annotationRepo.revertToVersion(annotationId, versionId, request.user)
        Ok(
          Json.obj(
            "message" -> "Reverted"
          )
        )
      }
    }
  }

}

case class LocationData(x:Double, y:Double, z:Double, faceIndex:Int)

case class AnnotationForm(data:List[EntityEntry], location:List[LocationData], imageData:Option[String])

case class EntityEntry(name:String, value:String)


trait RendersAnnotationAsJson {

  def toJson(entity:Entity):JsObject = Json.obj(
    "id" -> entity.id,
    "schema" -> Json.obj(
      "id" -> entity.schema.id,
      "appliesTo" -> entity.schema.appliesTo,
      "title" -> entity.schema.title,
      "fields" -> entity.schema.fields.map(f => {
        Json.obj( "id" -> f.id, "title" -> f.title, "type" -> f.dataType.id )
      })
    ),
    "data" -> Json.toJson(entity.data)
  )

  def toJson(annotation:Annotation):JsObject = Json.obj(
    "id" -> annotation.id,
    "entity" -> toJson(annotation.entity),
    "projectId" -> annotation.project.id,
    "project" -> Json.obj(
      "id" -> annotation.project.id,
      "title" -> annotation.project.title
    ),
    "modelId" -> annotation.model.id,
    "model" -> Json.obj(
      "id" -> annotation.model.id,
      "title" -> annotation.model.title
    ),
    "location" -> JsArray(annotation.location.map({
      case (x, y, z, faceIndex) => JsArray(Seq(JsNumber(x), JsNumber(y), JsNumber(z), JsNumber(faceIndex)))
    })),
    "imageData" -> annotation.imageData
  )

}


@Singleton
class DefaultController @Inject() (
        val messagesApi: MessagesApi,
        val authenticationService:AuthenticationService,
        val projectRepo:ProjectRepository,
        val projectService:ProjectService,
        val entityRepo: EntityRepository,
        val schemaRepo: SchemaRepository,
        val mediaRepo: MediaRepository,
        val artifactRepo: ArtifactRepository,
        val annotationRepo:AnnotationRepository,
        val approvalRepo:ApprovalRepository
      ) extends AnnotationController with BaseController

