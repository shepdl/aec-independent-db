package annotations

import java.util.UUID
import javax.inject.Inject

import accessControl.AuthenticationService
import com.google.inject.Singleton
import common.JsonResponses
import play.api.libs.json._
import play.api.mvc.{Action, Controller}
import projects.ProjectService
import schemas.{Pass, Schema, SchemaRepository, SchemaValidationError}

/**
  * Created by dave on 3/17/16.
  */
trait EntityController extends RendersEntitiesAsJson with JsonResponses {

  this:Controller =>

  val authenticationService:AuthenticationService
  val schemaRepo:SchemaRepository
  val entityRepo:EntityRepository
  val projectService:ProjectService

  def create(activityId:UUID, schemaName:String, schemaId:UUID) = authenticationService.LoginRequiredAction(parse.urlFormEncoded) { implicit request =>
    schemaRepo.getById(schemaId) match {
      case None => NotFound(s"Schema with ID $schemaId not found")
      case Some(schema) => {
        val entityData = request.body.map({
          case (k, v) => k -> v(0)
        })
        schema.validate(entityData) match {
          case SchemaValidationError(extraFields, missingFields, invalidFields) => BadRequest(Json.obj(
            "extraFields" -> extraFields,
            "missingFields" -> missingFields,
            "invalidFields" -> invalidFields
          ))
          case Pass => {
            // TODO: link to user
            val entity = Entity(null, schema, entityData)
            val savedEntity = entityRepo.save(entity)
            Created(toJson(savedEntity))
          }
        }
      }
    }
  }

  def getById(schemaName:String, schemaId:UUID, entityId:UUID) = Action {
    schemaRepo.getById(schemaId) match {
      case None => NotFound(s"Schema with ID $schemaId not found")
      case Some(schema) => entityRepo.getById(entityId) match {
        case None => NotFound(s"$schemaName #$entityId not found")
        case Some(entity) => Ok(toJson(entity))
      }
    }
  }

  def update(schemaName:String, schemaId:UUID, entityId:UUID) = authenticationService.LoginRequiredAction(parse.json) { implicit request =>
    schemaRepo.getById(schemaId) match {
      case None => NotFound(s"Schema with ID $schemaId not found")
      case Some(schema) => {
        val json = request.body
        val data = (json \ "data").as[JsObject].value.foldLeft(Map[String,String]()) {
          case (acc, (k, v)) => acc.updated(k, v.asOpt[String] match {
            case Some(v1) => v1
            case None => v.toString
          })
        }
        schema.validate(data) match {
          case SchemaValidationError(extraFields, missingFields, invalidFields) => BadRequest(Json.obj(
            "extraFields" -> extraFields,
            "missingFields" -> missingFields,
            "invalidFields" -> invalidFields
          ))
          case Pass => {
            val entity = Entity(
              (json \ "id").as[UUID],
              schema,
              data
            )
            val newEntity = entityRepo.save(entity)
            Ok(toJson(newEntity))
          }
        }
      }
    }
  }

  def delete(schemaName:String, schemaId:UUID, entityId:UUID) = authenticationService.LoginRequiredAction { implicit request =>
    schemaRepo.getById(schemaId) match {
      case None => NotFound(s"${schemaName} not found")
      case Some(schema) => {
        entityRepo.getById(entityId) match {
          case None => NotFound(s"${schemaName} #${entityId} not found")
          case Some(entity) => {
            entityRepo.delete(entity, request.user)
            Ok(Json toJson Map("message" -> s"$schemaName #$schemaId deleted"))
          }
        }
      }
    }
  }

}


case object EntityController {

  def jsonToEntity(schema:Schema, json:JsValue) = Entity(
    (json \ "id").asOpt[UUID].getOrElse(null),
    schema,
    (json \ "data").as[JsObject].value.foldLeft(Map[String,String]()) {
      case (acc, (k, v)) => acc.updated(k, v.asOpt[String] match {
        case Some(v1) => v1
        case None => v.toString
      })
    }
  )

}

trait RendersEntitiesAsJson {

  def toJson(entity:Entity) = Json.obj(
    "id" -> entity.id,
    "schema" -> Json.obj(
      "id" -> entity.schema.id,
      "title" -> entity.schema.title
    ),
    "data" -> Json.toJson(entity.data)
  )

  def toJson(errors:SchemaValidationError) = Json obj (
    "missingFields" -> Json.arr (errors.missingFields),
    "extraFields" -> errors.extraFields,
    "invalidValues" -> errors.invalidFields
    )

}


@Singleton
class DefaultEntityController @Inject() (
      val authenticationService: AuthenticationService,
      override val entityRepo: EntityRepository,
      override val projectService: ProjectService,
      override val schemaRepo:SchemaRepository)
  extends EntityController with Controller {
}
