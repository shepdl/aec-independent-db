package annotations

import accessControl.{Group, GroupProperties}
import artifacts.{Model, ModelProperties}
import backend.approvals.ApprovalProperties
import projects.{Project, ProjectProperties}
import schemas.{Schema, SchemaField, SchemaRepository}

/**
  * Created by dave on 4/7/16.
  */


trait Operand {
  val textRepresentation:String
}

trait Equality extends Operand

trait Like extends Operand


case object QualifierFactory {

  val equality = new Equality {
    override val textRepresentation: String = "="
  }

  val like = new Like {
    override val textRepresentation: String = "=~"
  }

  def createForSchema(schema: Schema): Qualifier = Qualifier(schema)

  val operandForName = Map(
    "equality" -> equality,
    "like" -> like
  )

}


case class AttributeQualifier(field:SchemaField, operand:Operand, value:String)


case class Qualifier(
   schema:Schema, fields:List[AttributeQualifier]
 ) {

  def addAttribute(field:SchemaField, operand: Operand, value:String) =
    Qualifier(
      schema, AttributeQualifier(field, operand, value) :: fields
    )
}




case object Qualifier {
  def apply(schema:Schema):Qualifier = Qualifier(schema, List())
}


case class Query(
    projects:Option[List[Project]], models:Option[List[Model]],
    entities:List[Qualifier], groups:Set[Group]
   ) {

  def addProject(project: Project): Query =
    projects match {
      case None => Query(Some(List(project)), models, entities, groups)
      case Some(projects) =>
        Query(Some(project :: projects), models, entities, groups)
    }

  def addModel(model: Model): Query =
    Query(projects, Some(model :: models.getOrElse(List())), entities, groups)

  def addEntity(schema: Schema): Query =
    Query(projects, models, Qualifier(schema) :: entities, groups)

  def addQualifier(qualifier: Qualifier): Query =
    Query(projects, models, qualifier.asInstanceOf[Qualifier] :: entities, groups)

  def addApprovedByGroup(group:Group): Query =
    Query(projects, models, entities, groups + group)

}

trait QueryConverter {

  val schemaRepo:SchemaRepository

  def toCypher(query:Query):String = {
    val enumerated = enumeratedFields(query)

    val matchClauseComponents = List(
      matchClause(enumerated),
      projectsMatchClause(query),
      onMediaMatchClause(query.entities),
      groupMatchClause(query)
    ).filter(_.nonEmpty)

    val whereClauseComponents = List(
      if (query.entities.isEmpty) {
        s"NOT a0:${AnnotationProperties.deleted}"
      } else {
        query.entities.indices.map(index => s" NOT a${index}:${AnnotationProperties.deleted}" ).mkString(" AND ")
      },
      whereClause(enumerated),
      whereClause(query.groups),
      projectsWhereClause(query.projects),
      modelsWhereClause(query.models),
      s"NOT m:${ModelProperties.deleted}"
    ).filter(_.nonEmpty)

    s"""
       |MATCH ${matchClauseComponents.mkString(",")}
       |WHERE ${whereClauseComponents.mkString(" AND ")}
       |RETURN [${returnClause(query.entities)}] AS annotations, m
       |ORDER BY m.${ModelProperties.createdAt}
     """.stripMargin
  }


  private def hasOnlyBaseAttributes(qualifier:Qualifier, schema:Schema) =
    qualifier.fields.forall(f => schemaRepo.isBaseType(schema(f.field.title).dataType))

  private def matchClause(query:List[EnumeratedQualifier]): String = {
    if (query.isEmpty) {
      s"(a0:${AnnotationProperties.label}:${AnnotationProperties.current})-[:${OnMediaProperties.label}]->(m:${ModelProperties.label}:${ModelProperties.current})"
    } else {
      query.zipWithIndex.map({ case (e, index) => {
        val qualifier = e.qualifier
        val schema = qualifier.schema

        if (hasOnlyBaseAttributes(qualifier, schema)) {
          s"(a${index}:${AnnotationProperties.label}:${AnnotationProperties.current})-[:${AnnotationProperties.entity}]->(${e.q}_0)"
        } else {
          qualifier.fields.filterNot(f => schemaRepo.isBaseType(schema(f.field.title).dataType))
            .zipWithIndex.map({
            case (f, index) => s"""(${e.q}_$index)<--(${e.fe})"""
          }
          ).mkString(",") + s""", \n(a${index}:${AnnotationProperties.label}:${AnnotationProperties.current})-[:${AnnotationProperties.entity}]->(${e.fe})"""
        }
      }}).mkString(",\n")
    }
  }

  private def matchClause(projects:Option[List[Project]], annotationNodeCount:Int):String = projects match {
    case None => ""
    case Some(projectList) => projectList match {
      case List() => ""
      case projects => (0 to annotationNodeCount map { index =>
        s"""
           |  (a$index)-[:${ProjectRelationship.label}]->(project:${ProjectProperties.label}:${ProjectProperties.current})
         """.stripMargin
      }).mkString(",\n")
    }
  }

  private def whereClause(groups:Set[Group]): String = {
    if (groups.isEmpty) {
      ""
    } else {
      s"""
         |group.id IN ["${groups.map(_.id).mkString("""" , """")}"]
     """.stripMargin
    }
  }

  private case class EnumeratedQualifier(index: Int, qualifier:Qualifier) {
    val q = s"q$index"
    val fe = s"fe$index"
  }

  private def enumeratedFields(query:Query) = query.entities.zipWithIndex.map({
    /**
      * Input: set of fields and matching values
      * Output:
      *   - links between found entities and annotations
      *   - links between found entities and quality nodes, plus description for link
      *   - property-based qualifiers on found entities
      */
    case (qualifier, index) => EnumeratedQualifier(index, qualifier)
  })

  private def whereClause(qualifiers:List[EnumeratedQualifier]): String = {
    qualifiers.map(e => {
      val qualifier = e.qualifier
      val schema = qualifier.schema

      if (hasOnlyBaseAttributes(qualifier, schema)) {
        qualifier.fields.map(q =>
          s"""
             |${e.q}_0.`${q.field.title}` ${q.operand.textRepresentation} "${q.value}"
           """.stripMargin).mkString("\n AND \n")
      } else {
        (
          qualifier.fields.filter(f => schemaRepo.isBaseType(f.field.dataType))
            .map(q => s"""${e.fe}.`${q.field.title}` ${q.operand.textRepresentation} "${q.value}" """)
            ++
            qualifier.fields.filterNot(f => schemaRepo.isBaseType(f.field.dataType)).zipWithIndex.map({
              case (qualifier, index) => {
                s"""${e.q}_${index}.id = "${qualifier.value}" """
              }
            })
          ).mkString("\n AND ")
      }
    }).mkString("\n AND ")
  }

  private def projectsWhereClause(possibleProjects:Option[List[Project]]) = possibleProjects match {
    case Some(projects) => s"""  project.id IN ["${projects.map(_.id).mkString("\",\"")}"] """
    case None => ""
  }

  private def projectsWhereClause(projects:List[Project]) = {
    s"""
       |  project.id IN ["${projects.map(_.id).mkString("\",\"")}"]
     """.stripMargin
  }

  private def groupsToMatchClause(annotationIndex:Int):String =
      s"""(a$annotationIndex)<-[:${ApprovalProperties.item}]-(approval:${ApprovalProperties.label})
        |-[:${ApprovalProperties.group}]->(group:${GroupProperties.label})
        |""".stripMargin

  private def groupMatchClause(query:Query):String = {
    if (query.groups.nonEmpty) {
      if (query.entities.nonEmpty) {
        "," + query.entities.zipWithIndex.map(e => groupsToMatchClause(e._2)).mkString(",\n")
      } else {
        "," + groupsToMatchClause(0)
      }
    } else {
      ""
    }
  }

  private def returnClause(entities:List[Qualifier]):String = {
    if (entities.isEmpty) {
      "a0"
    } else {
      entities.zipWithIndex.map(a => s"a${a._2}").mkString(",")
    }
  }

  private def modelsWhereClause(possibleModels: Option[List[Model]]) = possibleModels match {
    case Some(models) => s""" m.id IN ["${models.map(_.id).mkString("\",  \"")}"] """
    case None => ""
  }

  private def onMediaMatchClause(entities:List[Qualifier]) = entities.indices.map(index =>
    s"""
       |  (a${index})-[:${OnMediaProperties.label}]->(m:${ModelProperties.label}:${ModelProperties.current})
       |""".stripMargin
    ).mkString(",\n")

  private def projectsMatchClause(query:Query):String = query.projects match {
    case None => ""
    case Some(_) => {
      val annotationCount = query.entities.size - (if (query.entities.isEmpty) 0 else 1)
      (0 to annotationCount).map(index => s"(a${index})-[:${ProjectRelationship.label}]->(project:${ProjectProperties.label}:${ProjectProperties.current})").mkString(", ")
    }
  }

}


case object QueryFactory {
  def create = {
    Query(None, None, List(), Set())
  }
}
