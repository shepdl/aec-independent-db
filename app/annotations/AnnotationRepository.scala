package annotations

import java.util.{Date, UUID}
import javax.inject.Inject

import accessControl._
import artifacts._
import common._
import org.neo4j.driver.v1.Transaction
import org.neo4j.driver.v1.types.Node
import play.api.{Configuration, Logger}
import projects.{ConvertsNodesToProjects, Project, ProjectProperties, ProjectRepository}
import schemas.{Pass, Schema, SchemaProperties, SchemaRepository}

import scala.collection.convert.Wrappers.SeqWrapper

object AnnotationProperties extends GraphEntityProperties with VersionedGraphEntityProperties
  with OwnableProperties with TimestampsProperties {

  val label = "Annotation"

  val imageData = "imageData"

  val entity = "entity"
}

object OnMediaProperties extends GraphEntityProperties {
  val label = "OnMedia"
}

object IsDelimitedByProperties extends GraphEntityProperties {
  val label = "IsDelimitedBy"

  val ordering = "ordering"
}

object DescribesProperties extends GraphEntityProperties {
  val label = "Describes"
}

object ProjectRelationship extends GraphEntityProperties {
  val label = "Project"
}

object VertexProperties extends GraphEntityProperties with TimestampsProperties {
  val label = "Vertex"

  val x = "x"
  val y = "y"
  val z = "z"
  val faceIndex = "faceIndex"

}

object RegionProperties extends GraphEntityProperties with TimestampsProperties {
  val label = "Region"

  val annotation = "annotation"
}
class AnnotationRepository @Inject()(
                val db:DatabaseService,
                val projectRepo:ProjectRepository,
                val artifactRepo:ArtifactRepository,
                val mediaRepo:MediaRepository,
                val schemaRepo:SchemaRepository,
                val entityRepo:EntityRepository
            ) extends ConvertsNodesToAnnotations with ConvertsNodesToProjects with ConvertsNodesToUsers
  with ConvertsNodesToMedia with QueryConverter {


  val logger = Logger("Annotation Repo")

  import scala.collection.JavaConversions._
  import scala.collection.JavaConverters._

  private def create(annotation:Annotation, tx:Transaction) = {
    val now = new Date().getTime.asInstanceOf[Object]
    val versionId = UUID.randomUUID()
    val newEntity = entityRepo.save(annotation.entity, tx)
    val regionUUID = UUID.randomUUID()
    tx.run(
      s"""
         |CREATE (region:${RegionProperties.label} {
         |  ${RegionProperties.id} : {${RegionProperties.id}},
         |  ${RegionProperties.createdAt} : {${RegionProperties.createdAt}}
         |})
         """.stripMargin, Map[String,AnyRef](
        RegionProperties.id -> regionUUID.toString,
        RegionProperties.createdAt -> now,
        RegionProperties.updatedAt -> now
      )
    )
    annotation.location.zipWithIndex.foreach({
      case ((x, y, z, faceIndex), index) => tx.run(
        s"""
           |MATCH (region:${RegionProperties.label} {
           |  ${RegionProperties.id} : {regionId}
           |})
           |CREATE (:${VertexProperties.label} {
           |  ${VertexProperties.x} : {${VertexProperties.x}},
           |  ${VertexProperties.y} : {${VertexProperties.y}},
           |  ${VertexProperties.z} : {${VertexProperties.z}},
           |  ${VertexProperties.faceIndex} : {${VertexProperties.faceIndex}}
           |})<-[:${IsDelimitedByProperties.label} {
           |  ${IsDelimitedByProperties.ordering} : {${IsDelimitedByProperties.ordering}}
           |}]-(region)
           """.stripMargin, Map[String,AnyRef](
          VertexProperties.x -> x.asInstanceOf[Object],
          VertexProperties.y -> y.asInstanceOf[Object],
          VertexProperties.z -> z.asInstanceOf[Object],
          VertexProperties.faceIndex -> faceIndex.asInstanceOf[Object],
          IsDelimitedByProperties.ordering -> index.asInstanceOf[Object],
          "regionId" -> regionUUID.toString
        )
      )
    })
    val annotationId = UUID.randomUUID()
    val result = tx.run(
      s"""
         |MATCH
         |  (entity:${EntityProperties.label}:${EntityProperties.current}  { ${EntityProperties.id} : {entityId} }),
         |  (project:${ProjectProperties.label}:${ProjectProperties.current} {${ProjectProperties.id} : {projectId}}),
         |  (owner:${UserProperties.label} {${UserProperties.id} : {userId}}),
         |  (region:${RegionProperties.label} {${RegionProperties.id} : {regionId}}),
         |  (media: ${ModelProperties.label}:${ModelProperties.current} {${ModelProperties.id} : {modelId}})
         |
         |CREATE (annotation:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |  ${AnnotationProperties.id} : {annotationId},
         |  ${AnnotationProperties.versionId} : {versionId},
         |  ${AnnotationProperties.versionAction} : "${VersionActions.original}",
         |  ${AnnotationProperties.imageData} : {imageData}
         |}),
         |(annotation)-[:${AnnotationProperties.entity}]->(entity),
         |(annotation)-[:${ProjectRelationship.label}]->(project),
         |(annotation)<-[:${AnnotationProperties.owns}]-(owner),
         |(annotation)<-[:${RegionProperties.annotation}]-(region),
         |(annotation)-[:${OnMediaProperties.label}]->(media)
         """.stripMargin,
      Map[String,AnyRef](
        "annotationId" -> annotationId.toString,
        "versionId" -> versionId.toString,
        "entityId" -> newEntity.id.toString,
        "projectId" -> annotation.project.id.toString,
        "userId" -> annotation.creator.uuid.toString,
        "regionId" -> regionUUID.toString,
        "modelId" -> annotation.model.id.toString,
        "imageData" -> annotation.imageData
      )
    )
    tx.success()
    Annotation(
      annotationId,
      annotation.project,
      annotation.model,
      newEntity,
      annotation.location,
      annotation.creator,
      annotation.imageData
    )
  }

  private def update(annotation:Annotation, tx:Transaction) = {
    val now = new Date().getTime.asInstanceOf[Object]
    val versionId = UUID.randomUUID()
    val regionUUID = UUID.randomUUID()
    tx.run(
      s"""
         |CREATE (region:${RegionProperties.label} {
         |  ${RegionProperties.id} : {${RegionProperties.id}},
         |  ${RegionProperties.createdAt} : {${RegionProperties.createdAt}}
         |})
         """.stripMargin, Map[String,AnyRef](
        RegionProperties.id -> regionUUID.toString,
        RegionProperties.createdAt -> now,
        RegionProperties.updatedAt -> now
      )
    )
    val newEntity = entityRepo.save(annotation.entity, tx)
    annotation.location.zipWithIndex.foreach({
      case ((x, y, z, faceIndex), index) => tx.run(
        s"""
           |MATCH (region:${RegionProperties.label} {
           |  ${RegionProperties.id} : {regionId}
           |})
           |CREATE (:${VertexProperties.label} {
           |  ${VertexProperties.x} : {${VertexProperties.x}},
           |  ${VertexProperties.y} : {${VertexProperties.y}},
           |  ${VertexProperties.z} : {${VertexProperties.z}},
           |  ${VertexProperties.faceIndex} : {${VertexProperties.faceIndex}}
           |})<-[:${IsDelimitedByProperties.label} {
           |  ${IsDelimitedByProperties.ordering} : {${IsDelimitedByProperties.ordering}}
           |}]-(region)
           """.stripMargin, Map[String,AnyRef](
          VertexProperties.x -> x.asInstanceOf[Object],
          VertexProperties.y -> y.asInstanceOf[Object],
          VertexProperties.z -> z.asInstanceOf[Object],
          VertexProperties.faceIndex -> faceIndex.asInstanceOf[Object],
          IsDelimitedByProperties.ordering -> index.asInstanceOf[Object],
          "regionId" -> regionUUID.toString
        )
      )
    })
    tx.run(
      s"""
         |MATCH
         |  (entity:${EntityProperties.label}:${EntityProperties.current}  { ${EntityProperties.id} : {entityId} }),
         |  (project:${ProjectProperties.label}:${ProjectProperties.current} {${ProjectProperties.id} : {projectId}}),
         |  (owner:${UserProperties.label} {${UserProperties.id} : {userId}}),
         |  (region:${RegionProperties.label} {${RegionProperties.id} : {regionId}}),
         |  (media: ${ModelProperties.label}:${ModelProperties.current} {${ModelProperties.id} : {modelId}}),
         |  (oldAnnotation:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |    ${AnnotationProperties.id} : {annotationId}
         |  })
         |
         |REMOVE oldAnnotation:${AnnotationProperties.current}
         |CREATE (annotation:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |  ${AnnotationProperties.id} : {annotationId},
         |  ${AnnotationProperties.versionId} : {versionId},
         |  ${AnnotationProperties.versionAction} : "${VersionActions.updated}",
         |  ${AnnotationProperties.imageData} : {imageData}
         |})-[:${AnnotationProperties.entity}]->(entity),
         |  (annotation)-[:${AnnotationProperties.previousVersion}]->(oldAnnotation),
         |  (annotation)-[:${ProjectRelationship.label}]->(project),
         |  (annotation)<-[:${AnnotationProperties.owns}]-(owner),
         |  (annotation)<-[:${RegionProperties.annotation}]-(region),
         |  (annotation)-[:${OnMediaProperties.label}]->(media)
       """.stripMargin, Map[String,AnyRef](
          "annotationId" -> annotation.id.toString,
          "versionId" -> versionId.toString,
          "entityId" -> newEntity.id.toString,
          "projectId" -> annotation.project.id.toString,
          "userId" -> annotation.creator.uuid.toString,
          "regionId" -> regionUUID.toString,
          "modelId" -> annotation.model.id.toString,
          "imageData" -> annotation.imageData
        )
    )
    tx.success()
    Annotation(
      annotation.id,
      annotation.project,
      annotation.model,
      newEntity,
      annotation.location,
      annotation.creator,
      annotation.imageData
    )
  }

  /**
    * What can be updated through this method:
    *   Location
    *
    * What cannot be updated through this method:
    *   Model (Delete and create a new one)
    *   Owner (Delete and re-create)
    * @param annotation
    * @return
    */
  def save(annotation:Annotation) = db.withTransaction(tx => {
    if (annotation.id == null) {
      create(annotation, tx)
    } else {
      update(annotation, tx)
    }
  })

  def getById(id:UUID):Option[Annotation] = db.withTransaction(tx => {
    entityRepo.getByAnnotationId(id) match {
      case None => {
        logger.error(s"Entity for annotation ${id} not found")
        None
      }
      case Some(entity) => {
        val linearRingElements = {
          tx.run(
            s"""
               |MATCH (annotation:${AnnotationProperties.label}:${AnnotationProperties.current} {
               |  ${AnnotationProperties.id} : {${AnnotationProperties.id}}
               |})<-[:${RegionProperties.annotation}]-(region:${RegionProperties.label})
               |
               |MATCH (region)-[link:${IsDelimitedByProperties.label}]->(vertex:${VertexProperties.label})
               |RETURN vertex
               |ORDER BY link.${IsDelimitedByProperties.ordering}
           """.stripMargin, Map(
              AnnotationProperties.id -> id.toString
            )
          ).map(row => row.get("vertex").asNode())
        }
        val annotation = tx.run(
          s"""
             |MATCH (annotation:${AnnotationProperties.label}:${AnnotationProperties.current} {
             |  ${AnnotationProperties.id} : {${AnnotationProperties.id}}
             |}),
             |  (annotation)-[:${OnMediaProperties.label}]->(model:${ModelProperties.label}),
             |  (annotation)-[:${ProjectRelationship.label}]->(project:${ProjectProperties.label}),
             |  (annotation)<-[:${AnnotationProperties.owns}]-(owner:${UserProperties.label})
             |RETURN annotation, model.id AS modelId, project, owner
           """.stripMargin, Map(
            AnnotationProperties.id -> id.toString
          )
        ).map(row => nodeToAnnotation(
            row.get("annotation").asNode(),
            mediaRepo.getById(UUID.fromString(row.get("modelId").asString())).get,
            nodeToLinearRing(linearRingElements.toList),
            entity,
            nodeToProject(row.get("project").asNode()),
            nodeToUser(row.get("owner").asNode())
          )
        )
        if (annotation.hasNext) {
          Some(annotation.next)
        } else {
          None
        }
      }
    }
  })

  def delete(annotation:Annotation, deletingUser:User) = db.withTransaction(tx => {
    val versionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (oldAnnotation:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |  ${AnnotationProperties.id} : {annotationId}
         |})<-[:${AnnotationProperties.owns}]-(creator:${UserProperties.label}),
         |(deletingUser:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |REMOVE oldAnnotation:${AnnotationProperties.current}
         |CREATE (newAnnotation:${AnnotationProperties.label}:${AnnotationProperties.current}:${AnnotationProperties.deleted} {
         |  ${AnnotationProperties.id} : {annotationId},
         |  ${AnnotationProperties.versionId} : {versionId}
         |})-[:${AnnotationProperties.deletedBy}]->(deletingUser),
         |  (newAnnotation)-[:${AnnotationProperties.previousVersion}]->(oldAnnotation),
         |  (creator)-[:${AnnotationProperties.owns}]->(newAnnotation)
         |
       """.stripMargin, Map(
        "annotationId" -> annotation.id.toString,
        "userId" -> deletingUser.uuid.toString,
        "versionId" -> versionId.toString
      )
    )
    tx.success()
  })

  def listVersions(id:UUID, page:Int, rpp:Int):Seq[Version[Annotation]] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (annotation:${AnnotationProperties.label} {
         |  ${AnnotationProperties.id} : ${AnnotationProperties.id}
         |}),
         |  (annotation)-[:${OnMediaProperties.label}]->(model:${ModelProperties.label}),
         |  (annotation)-[:${AnnotationProperties.owns}]->(owner:${UserProperties.label})
         |ORDER BY ${AnnotationProperties.updatedAt}
         |LIMIT $rpp
         |OFFSET ${page * rpp}
         |RETURN annotation, model, owner
       """.stripMargin, Map(
        AnnotationProperties.id -> id.toString
      )
    ).map(row => {
      val annotationNode = row.get("annotation").asNode
      val owner = nodeToUser(row.get("owner").asNode)
      val annotationVersionId = UUID.fromString(annotationNode.get(AnnotationProperties.versionId).asString())
      val linearRingElements = tx.run(
        s"""
           |MATCH (annotation:${AnnotationProperties.label} {
           |  ${AnnotationProperties.versionId} : {${AnnotationProperties.versionId}}
           |})<-[:${RegionProperties.annotation}]-(:${RegionProperties.label})
           |  -[:${IsDelimitedByProperties.label}]->(vertex:${VertexProperties.label})
           |RETURN vertex
           """.stripMargin, Map(
          AnnotationProperties.versionId -> annotationVersionId.toString
        )
      ).map(row => row.get("vertex").asNode())
      new Version[Annotation] {
        val item: Annotation = nodeToAnnotation(annotationNode,
          // null because this is going to be the same across every version of the annotation
          model = null,
          location = nodeToLinearRing(linearRingElements.toList),
          entity = entityRepo.getByAnnotationId(id, annotationVersionId).get,
          // null because users will probably want the restored version of the node to belong to the projects the most recent version belongs to
          project = null,
          owner
        )
        val versionId: UUID = annotationVersionId
        val updatedAt = new Date(annotationNode.get(AnnotationProperties.updatedAt).asLong)
        val action: String = annotationNode.get(AnnotationProperties.versionAction).asString()
      }
    }).toSeq
  })

  def revertToVersion(annotationId:UUID, revertingVersionId:UUID, revertingUser:User) = db.withTransaction(tx => {
    val now = new Date().getTime
    val newVersionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH
         |  (currentVersion:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |    ${AnnotationProperties.id} : {annotationId}
         |  }),
         |  (versionToRestore:${AnnotationProperties.label} {
         |    ${AnnotationProperties.id} : {revertingVersionId}
         |  }),
         |
         |  // Stuff to copy from previous version
         |  (versionToRestore)<-[:${RegionProperties.annotation}]-(regionToRestore:${RegionProperties.label}),
         |  (versionToRestore)-[:${AnnotationProperties.entity}]->(entityToRestore),
         |  (versionToRestore)-[:${OnMediaProperties.label}]->(model),
         |
         |  // Stuff to copy from current version
         |  (currentVersion)-[:${ProjectRelationship.label}]->(project:${ProjectProperties.label}),
         |  (currentVersion)<-[:${AnnotationProperties.owns}]-(owner:${UserProperties.label}),
         |
         |  (revertingUser:${UserProperties.label} {
         |    ${UserProperties.id} : {revertingUserId}
         |  })
         |
         |REMOVE
         |  currentVersion:${AnnotationProperties.current}
         |CREATE
         |  (newVersion:${AnnotationProperties.label}:${AnnotationProperties.current} {
         |    ${AnnotationProperties.id} : {annotationId},
         |    ${AnnotationProperties.versionId} : {newVersionId},
         |    ${AnnotationProperties.createdAt} : currentVersion.${AnnotationProperties.createdAt},
         |    ${AnnotationProperties.updatedAt} : {now}
         |  }),
         |  (newVersion)-[:${AnnotationProperties.previousVersion}]->(currentVersion),
         |  (newVersion)-[:${ProjectRelationship.label}]->(project),
         |  (newVersion)<-[:${AnnotationProperties.owns}]-(owner),
         |  (newVersion)<-[:${RegionProperties.annotation}]-(regionToRestore),
         |  (newVersion)-[:${OnMediaProperties.label}]->(model),
         |  (newVersion)-[:${AnnotationProperties.entity}]->(entityToRestore),
         |  (newVersion)-[:${AnnotationProperties.revertedBy}]->(revertingUser)
         |
       """.stripMargin, Map[String,AnyRef](
        "annotationId" -> annotationId.toString,
        "revertingVersionId" -> revertingVersionId.toString,
        "revertingUserId" -> revertingUser.uuid.toString,
        "newVersionId" -> newVersionId.toString,
        "now" -> now.asInstanceOf[Object]
      )
    )
    tx.success()
  })

  def getForProject(project:Project): List[Annotation] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (p:${ProjectProperties.label}:${AnnotationProperties.current} {
         |  ${ProjectProperties.id} : {${ProjectProperties.id}}
         |})<-[:${ProjectRelationship.label}]-(annotation:${AnnotationProperties.label})
         |WHERE NOT annotation:${AnnotationProperties.deleted}
         |RETURN annotation.id AS annotationId
       """.stripMargin, Map(
        ProjectProperties.id -> project.id.toString
      )).map(row => getById(UUID.fromString(row.get("annotationId").asString())).get).toList
  })

  def getForModel(model:Model):List[Annotation] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (m:${ModelProperties.label}:${ModelProperties.current} {
         |  ${ModelProperties.id} : {${ModelProperties.id}}
         |})<-[:${OnMediaProperties.label}]-(annotation:${AnnotationProperties.label}:${AnnotationProperties.current})
         |WHERE NOT annotation:${AnnotationProperties.deleted}
         |RETURN annotation.id AS annotationId
       """.stripMargin, Map(
        ModelProperties.id -> model.id.toString
      )).map(row => getById(UUID.fromString(row.get("annotationId").asString())).get).toList
  })

  def getForModelAndProject(model:Model, project:Project):List[Annotation] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (m:${ModelProperties.label}:${ModelProperties.current} {
         |  ${ModelProperties.id} : {modelId}
         |})<-[:${OnMediaProperties.label}]-(annotation:${AnnotationProperties.label}:${AnnotationProperties.current})
         |  -[:${ProjectRelationship.label}]->(project:${ProjectProperties.label}:${ProjectProperties.current} {
         |    ${ProjectProperties.id} : {projectId}
         |})
         |WHERE NOT annotation:${AnnotationProperties.deleted}
         |RETURN annotation.id AS annotationId
       """.stripMargin, Map(
        "modelId" -> model.id.toString,
        "projectId" -> project.id.toString
      )).map(row => {
      getById(UUID.fromString(row.get("annotationId").asString())).get
    }).toList
  })

  def getForModelAndSchema(model:Model, schema:Schema):List[Annotation] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (m:${ModelProperties.label} {
         |  ${ModelProperties.id} : {modelId}
         |})<-[:${OnMediaProperties.label}]-(annotation:${AnnotationProperties.label})
         |  <-(:${AnnotationProperties.entity})-(:${EntityProperties.label})
         |  -[:${EntityProperties.schema}]->(:${SchemaProperties.label} {
         |    ${SchemaProperties.id} : {schemaId}
         |  })
         |WHERE NOT annotation:${AnnotationProperties.deleted}
         |RETURN annotation.id AS annotationId
       """.stripMargin, Map(
        "modelId" -> model.id.toString,
        "schemaId" -> schema.id.toString
      )).map(row => getById(UUID.fromString(row.get("annotationId").asString())).get).toList
  })

  def getForModelAndProjectAndSchema(model:Model, project:Project, schema:Schema): List[Annotation] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (m:${ModelProperties.label} {
         |  ${ModelProperties.id} : {modelId}
         |})<-[:${OnMediaProperties.label}]-(annotation:${AnnotationProperties.label})
         |  <-(:${AnnotationProperties.entity})-(:${EntityProperties.label})
         |  -[:${EntityProperties.schema}]->(:${SchemaProperties.label} {
         |    ${SchemaProperties.id} : {schemaId}
         |  }),
         |  (annotation)-[:${ProjectRelationship.label}]->(:${ProjectProperties.label} {
         |    ${ProjectProperties.id} : {projectId}
         |  })
         |WHERE NOT annotation:${AnnotationProperties.deleted}
         |RETURN annotation.id AS annotationId
       """.stripMargin, Map(
        "modelId" -> model.id.toString,
        "schemaId" -> schema.id.toString,
        "projectId" -> project.id.toString
      )).map(row => getById(UUID.fromString(row.get("annotationId").asString())).get).toList
  })

  def getForModelOfSchema(model:Model, schema:Schema): List[Annotation] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (m:${ModelProperties.label}:${ModelProperties.current} {
         |  ${ModelProperties.id} : {modelId}
         |})<-[:${OnMediaProperties.label}]-(annotation:${AnnotationProperties.label}:${AnnotationProperties.current})
         |  -[:${AnnotationProperties.entity}]->(:${EntityProperties.label})
         |  -[:${EntityProperties.schema}]->(:${SchemaProperties.label} {
         |    ${SchemaProperties.id} : {schemaId}
         |  })
         |WHERE NOT annotation:${AnnotationProperties.deleted}
         |RETURN annotation.id AS annotationId
       """.stripMargin, Map(
        "modelId" -> model.id.toString,
        "schemaId" -> schema.id.toString
      )).map(row => getById(UUID.fromString(row.get("annotationId").asString())).get).toList
  })


  val queryFactory = QueryFactory
  val qualifierFactory = QualifierFactory

  def search(query:Query):List[ModelAnnotationsResult] = db.withTransaction(tx => {
    val annotationResult = tx.run(toCypher(query))
    val annotations = annotationResult.foldLeft(Map[UUID,ModelAnnotationsResult]()) {
      case (acc, row) => {
        val artifact = nodeToModel(row.get("m").asNode(), List())
        val annotations = row.get("annotations").asList().map(
          node => {
            val annotationNode = node.asInstanceOf[Node]
            getById(UUID.fromString(annotationNode.get(AnnotationProperties.id).asString())).get
          }
        ).toList

        val modelAnnotationResult = ModelAnnotationsResult(artifact,
          annotations ++ acc.getOrElse(artifact.id, ModelAnnotationsResult(artifact, List())).annotations
        )
        acc.updated(artifact.id, modelAnnotationResult)
      }
    }
    annotations.values.toList
  })

}




trait ConvertsNodesToAnnotations {

  def nodeToAnnotation(node:Node, model:Model, location:List[(Double, Double, Double, Int)], entity:Entity, project:Project, owner:User):Annotation = {
    Annotation(
      UUID.fromString(node.get(AnnotationProperties.id).asString()),
      project,
      model,
      entity,
      location,
      owner,
      node.get(AnnotationProperties.imageData).asString()
    )
  }

  def nodeToLinearRing(nodes:List[Node]):List[(Double, Double, Double, Int)] = {
    nodes.map(node => {
      val possibleFaceIndex = node.get(VertexProperties.faceIndex)
      (
        node.get(VertexProperties.x).asDouble(),
        node.get(VertexProperties.y).asDouble(),
        node.get(VertexProperties.z).asDouble(),
        if (possibleFaceIndex.isNull()) -1 else possibleFaceIndex.asInt()
      )
    })
  }

}
