package annotations

import java.util.UUID

import accessControl.User
import artifacts.Model
import common.Resource
import projects.Project
import schemas.Schema

/**
  * Created by dave on 3/15/16.
  */
case class Annotation(
   id:UUID, project:Project, model:Model, entity:Entity, location:List[(Double, Double, Double, Int)], creator:User,
   imageData: String
) extends Resource {
  override val resourceName = "Annotation"
}

case class Entity(id: UUID, schema:Schema, data:Map[String,String]) extends Resource {
  override val resourceName: String = "Entity"

  def apply(key:String) = data(key)
}

case class ValidationError(missingData:List[String], schemaProblems:schemas.ValidationStatus)
