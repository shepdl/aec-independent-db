package backend.approvals

import java.util.UUID

import accessControl.Group
import common.Resource

/**
  * Created by dave on 1/10/17.
  */
case class Approval(id:UUID, group:Group) {

}


case class Endorsement[A <: Resource](id:UUID, group:Group, item:A)
