package approvals

import java.util.UUID
import javax.inject.Inject

import accessControl.{AuthenticationService, GroupMembershipProperties, GroupRepository, RendersGroupAsJson}
import annotations.{Annotation, AnnotationRepository, RendersAnnotationAsJson}
import backend.approvals.ApprovalRepository
import common.JsonResponses
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import projects.{Project, ProjectProperties, ProjectUserRights}

/**
  * Created by dave on 1/10/17.
  */
trait ApprovalController extends I18nSupport with JsonResponses with RendersGroupAsJson with RendersAnnotationAsJson {

  this:Controller =>

  val messagesApi:MessagesApi

  val authenticationService:AuthenticationService
  val groupRepo:GroupRepository
  val annotationRepo:AnnotationRepository
  val approvalRepo:ApprovalRepository

  case class ApproveData(inGroupId:String) {
    val groupId = UUID.fromString(inGroupId)
  }

  val approveForm:Form[ApproveData] = Form(
    mapping(
      "groupId" -> nonEmptyText
    )(ApproveData.apply)(ApproveData.unapply)
  )

  case class EndorseData(inItemId:String, itemType:String) {
    val itemId = UUID.fromString(inItemId)
  }

  val endorseForm:Form[EndorseData] = Form(
    mapping(
      "itemId" -> nonEmptyText,
      "itemType" -> nonEmptyText
    )(EndorseData.apply)(EndorseData.unapply)
  )

  def approve(groupId:UUID) = authenticationService.LoginRequiredAction(parse.urlFormEncoded) { implicit request =>
    endorseForm.bindFromRequest.fold(
      formWithErrors => BadRequest(formWithErrors.errorsAsJson),
      endorsementData => groupRepo.getById(groupId) match {
        case None => NotFound(error(s"Group ${groupId} not found"))
        case Some(group) => {
          val roles = groupRepo.getRolesInGroup(group, request.user)
          if (roles.roles.contains(GroupMembershipProperties.roles.admin)) {
            approvalRepo.approve(endorsementData.itemId, endorsementData.itemType, group, request.user) match {
              case Left(_) => NotFound(error(s"${endorsementData.itemType} ${endorsementData.itemId} not found"))
              case Right(approval) => Ok(Json.obj(
                "message" -> "Approved",
                "itemId" -> endorsementData.itemId,
                "groupId" -> group.id
              ))
            }
          } else {
            Forbidden(error(s"User does not have permission to approve annotations for ${group.name} (${groupId})"))
          }
        }
      }
    )
  }

  def listGroupEndorsements(groupId:UUID) = Action { implicit request =>
    groupRepo.getById(groupId) match {
      case None => NotFound(missingObject("group", groupId))
      case Some(group) => Ok(
          Json.obj(
            "group" -> groupToJson(group, List()),
            "endorsements" -> approvalRepo.getForGroup(group).map(endorsement => Json.obj(
              "id" -> endorsement.id,
              "groupId" -> endorsement.group.id,
              "item" -> (endorsement.item match {
                case item:Project => Json.obj(
                  "id" -> item.id,
                  "title" -> item.title,
                  "type" -> "Project"
                )
                case item:Annotation => toJson(item) ++ Json.obj(
                  "type" -> "Annotation"
                )
              })
            )
          )
        )
      )
    }
  }

  def delete(groupId:UUID, endorsementId:UUID) = authenticationService.LoginRequiredAction { implicit request =>
    groupRepo.getById(groupId) match {
      case None => NotFound(error(s"Group ${groupId} not found"))
      case Some(group) => {
        val roles = groupRepo.getRolesInGroup(group, request.user)
        if (roles.roles.contains(GroupMembershipProperties.roles.admin)) {
          approvalRepo.remove(endorsementId) match {
            case Left(_) => NotFound(error(s"Endorsement ${endorsementId} not found"))
            case Right(approval) => Ok(Json.obj(
              "message" -> "Approval removed",
              "itemId" -> endorsementId,
              "groupId" -> group.id
            ))
          }
        } else {
          Forbidden(
            inadequatePermissions(ProjectUserRights.canRemoveApprovedAnnotations, request.user)
          )
        }
      }
    }
  }

}



class DefaultApprovalController @Inject() (
  override val messagesApi: MessagesApi,

  override val authenticationService: AuthenticationService,
  override val groupRepo:GroupRepository,
  override val annotationRepo:AnnotationRepository,
  override val approvalRepo:ApprovalRepository
) extends ApprovalController with Controller


