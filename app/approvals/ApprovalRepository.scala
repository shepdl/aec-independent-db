package backend.approvals

import java.util.{Date, UUID}
import javax.inject.Inject

import accessControl._
import annotations.{Annotation, AnnotationProperties, AnnotationRepository, ConvertsNodesToAnnotations}
import common.{DatabaseService, Resource, TimestampsProperties}
import org.neo4j.driver.v1.types.Node
import projects.{ConvertsNodesToProjects, ProjectProperties, ProjectRepository}

object ApprovalProperties extends TimestampsProperties {

  val label = "Approval"

  val id = "id"
  val approvedBy = "approvedBy"
  val group = "group"
  val item = "item"

}

class ApprovalRepository @Inject()(
      db:DatabaseService,
      annotationRepo:AnnotationRepository,
      projectRepo:ProjectRepository
    ) extends ConvertsNodesToApprovals
  with ConvertsNodesToGroups with ConvertsNodesToProjects with ConvertsNodesToAnnotations {

  import scala.collection.JavaConversions._

  def approve(itemId:UUID, itemType:String, group:Group, user:User):Either[ResourceNotFound,Approval] = db.withTransaction(tx => {
    val now = new Date().getTime.toString
    val id = UUID.randomUUID()
    val result = tx.run(
      s"""
          |MATCH (user:${UserProperties.label} {
          |  id: {userId}
          |}), (resource:${AnnotationProperties.current} {
          |  id: {resourceId}
          |}), (group:${GroupProperties.label} {
          |  id: {groupId}
          |})
          |CREATE (approval: ${ApprovalProperties.label} {
          |    ${ApprovalProperties.id} : {approvalId},
          |    ${ApprovalProperties.createdAt} : {now}
          |  }),
          |  (approval)-[:${ApprovalProperties.approvedBy}]->(user),
          |  (approval)-[:${ApprovalProperties.group}]->(group),
          |  (approval)-[:${ApprovalProperties.item}]->(resource)
          |
       """.stripMargin, Map(
        "approvalId" -> id.toString,
        "userId" -> user.uuid.toString,
        "resourceId" -> itemId.toString,
        "groupId" -> group.id.toString,
        "now" -> now
      )
    )
    if (result.summary().counters().nodesCreated() == 1) {
      tx.success()
      Right(Approval(id, group))
    } else {
      Left(ResourceNotFound(itemId))
    }
  })

  def approve(annotation: Annotation, group:Group, user:User) = db.withTransaction(tx => {
    val now = new Date().getTime.toString
    val id = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (annotation:${AnnotationProperties.label} {
         |  ${AnnotationProperties.id} : {annotationId}
         |}), (group:${GroupProperties.label} {
         |  ${GroupProperties.id} : {groupId}
         |}), (user:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |CREATE (approval:${ApprovalProperties.label} {
         |    ${ApprovalProperties.id} : {approvalId},
         |    ${ApprovalProperties.createdAt} : {now}
         | }),
         | (annotation)<-[:${ApprovalProperties.item}]-(approval),
         | (user)<-[:${ApprovalProperties.approvedBy}]-(approval),
         | (group)<-[:${ApprovalProperties.group}]-(approval)
         |
       """.stripMargin, Map(
        "annotationId" -> annotation.id.toString,
        "groupId" -> group.id.toString,
        "userId" -> user.uuid.toString,
        "approvalId" -> id.toString,
        "now" -> now
      )
    )
    tx.success()
    Approval(id, group)
  })

  def getForAnnotation(annotation: Annotation) = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (annotation:${AnnotationProperties.label} {
         |  ${AnnotationProperties.id} : {annotationId}
         |})<-[:${ApprovalProperties.item}]-(approval:${ApprovalProperties.label})
         |  -[:${ApprovalProperties.group}]->(group:${GroupProperties.label})
         |RETURN approval.id AS id, group
       """.stripMargin, Map(
        "annotationId" -> annotation.id.toString
      )
    ).map(row => {
      Approval(
        UUID.fromString(row.get("id").asString()),
        node2Group(row.get("group").asNode())
      )
    })
  })

  def getForItemId(id:UUID) = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (item {
         |  id: {itemId}
         |})<-[:${ApprovalProperties.item}]-(approval:${ApprovalProperties.label})
         |  -[:${ApprovalProperties.group}]->(group:${GroupProperties.label})
         |RETURN approval, group
       """.stripMargin, Map(
        "itemId" -> id.toString
      )
    ).map(row => nodeToApproval(
      row.get("approval").asNode(), node2Group(row.get("group").asNode())
    )).toList
  })

  def getForGroup(group:Group):List[Endorsement[Resource]] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (:${GroupProperties.label} {
         |  ${GroupProperties.id} : {${GroupProperties.id}}
         |})<-[:${ApprovalProperties.group}]-(approval:${ApprovalProperties.label})
         |  -[:${ApprovalProperties.item}]->(item)
         |RETURN approval, item
       """.stripMargin, Map(
        GroupProperties.id -> group.id.toString
      )
    ).map(
      row => {
        val itemNode = row.get("item").asNode()
        val approvalNode = row.get("approval").asNode()
        val label = itemNode.labels().head
        val itemId = UUID.fromString(itemNode.get("id").asString())
        val item = label match {
          case ProjectProperties.label => projectRepo.getById(itemId).get
          case AnnotationProperties.label => annotationRepo.getById(itemId).get
        }
        nodeToEndorsement(approvalNode, group, item)
      }
    ).toList
  })


  def remove(endorsementId:UUID):Either[ResourceNotFound,Approval] = db.withTransaction(tx => {
    val result = tx.run(
      s"""
         |MATCH (approval: ${ApprovalProperties.label} {
         |  ${ApprovalProperties.id} : {${ApprovalProperties.id}}
         |})
         |DETACH DELETE approval
       """.stripMargin, Map(
        ApprovalProperties.id -> endorsementId.toString
      )
    )
    if (result.summary().counters().nodesDeleted() == 1) {
      tx.success()
      Right(Approval(endorsementId, null))
    } else {
      Left(ResourceNotFound(endorsementId))
    }
  })

}

trait ConvertsNodesToApprovals {

  def nodeToApproval(result:Node, group:Group):Approval = {
    Approval(
      UUID.fromString(result.get(ApprovalProperties.id).asString()),
      group
    )
  }

  def nodeToEndorsement(result:Node, group:Group, item:Resource):Endorsement[Resource] = {
    Endorsement[Resource](
      UUID.fromString(result.get(ApprovalProperties.id).asString()),
      group,
      item
    )
  }

}


trait Error

case class ResourceNotFound(id:UUID) extends Error
