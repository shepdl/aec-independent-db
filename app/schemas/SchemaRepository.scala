package schemas

import java.util.{Date, UUID}
import javax.inject.Inject

import accessControl._
import annotations.{ConvertsNodesToEntities, Entity, EntityRepository}
import common._
import org.neo4j.driver.v1.Transaction
import org.neo4j.driver.v1.types.Node
import play.api.Logger
import projects.{Project, ProjectProperties}


object SchemaProperties extends GraphEntityProperties with VersionedGraphEntityProperties
  with OwnableProperties with TimestampsProperties {

  val label = "Schema"

  val title = "title"
  val appliesTo = "appliesTo"

  val isControlled = "isControlled"

  val project = "project"

}

object SchemaFieldProperties extends GraphEntityProperties with VersionedGraphEntityProperties
  with TimestampsProperties {

  val label = "SchemaField"

  val title = "title"

  val dataType = "DataType"
  val isControlled = "isControlled"

  val inputElement = "inputElement"
  val inputStyle = "inputStyle"
  val displayElement = "displayElement"
  val displayStyle = "displayStyle"

  val schema = "schema"
  val order = "fieldOrder"
}

class SchemaRepository @Inject() (val db:DatabaseService, val userRepo:UserRepository) extends ConvertsNodesToSchemas with ConvertsNodesToUsers with ConvertsNodesToEntities  {

  val logger = Logger("SchemaRepository")

  val schemaRepo = this

  import scala.collection.JavaConversions._

  type ParamList = Map[String,AnyRef]


  private val entityRepo = new EntityRepository(db, this)

  private def initializeBaseNodes = db.withTransaction( tx => {
    val adminUser = tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.email} : "admin"
         |})
         |RETURN u
       """.stripMargin
    ).map(row => nodeToUser(row.get("u").asNode)).toSeq.head
    val baseTypes = List("String", "Int", "Transliteration").foldLeft(Map[String,Schema]()) ({
        case (map, name) => {
            val query =
              s"""MATCH (s:${SchemaProperties.label} {
                 |  ${SchemaProperties.title} : {${SchemaProperties.title}}
                 |}) RETURN s""".stripMargin
            val result = tx.run(query, Map(SchemaProperties.title -> name))
            if (result.hasNext) {
                map.updated(name, nodeToSchema(result.next.get("s").asNode, List(), adminUser))
              } else {
                map.updated(name, create(tx, Schema(UUID.randomUUID(), name, "either", List(), false, adminUser)))
              }
          }
      }
    )
    tx.success()

    baseTypes
  })

  val baseTypes = initializeBaseNodes

  def getBaseTypes = baseTypes.values

  val FIELD_ORDER = "FIELD_ORDER"

  val NO_ID = -1

  def isBaseType(schema:Schema):Boolean = baseTypes.contains(schema.title) 

  private def create(tx:Transaction, schema:Schema) = {
    val now = new Date().getTime
    val schemaId = UUID.randomUUID()
    val nodeId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (u:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |CREATE (:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {${SchemaProperties.id}},
         |  ${SchemaProperties.versionId} : {${SchemaProperties.versionId}},
         |  ${SchemaProperties.title} : {${SchemaProperties.title}},
         |  ${SchemaProperties.appliesTo} : {${SchemaProperties.appliesTo}},
         |  ${SchemaProperties.isControlled} : {${SchemaProperties.isControlled}},
         |  ${SchemaProperties.createdAt} : {${SchemaProperties.createdAt}},
         |  ${SchemaProperties.updatedAt} : {${SchemaProperties.updatedAt}},
         |  ${SchemaProperties.nodeId} : {${SchemaProperties.nodeId}}
         |})<-[:${SchemaProperties.owns} {
         |  ${SchemaProperties.createdAt} : {${SchemaProperties.createdAt}},
         |  ${SchemaProperties.updatedAt} : {${SchemaProperties.updatedAt}}
         |}]-(u)
       """.stripMargin, Map[String,AnyRef](
        SchemaProperties.id -> schemaId.toString,
        SchemaProperties.versionId -> nodeId.toString,
        SchemaProperties.title -> schema.title,
        SchemaProperties.appliesTo -> schema.appliesTo,
        SchemaProperties.isControlled -> schema.isControlled.asInstanceOf[Object],
        SchemaProperties.createdAt -> now.asInstanceOf[Object],
        SchemaProperties.updatedAt -> now.asInstanceOf[Object],
        SchemaProperties.nodeId -> nodeId.toString,
        "userId" -> schema.owner.uuid.toString
      )
    )
    val schemaFields = schema.fields.zipWithIndex.map({
      case (schemaField, index) => createField(schemaId, schemaField, now, index, tx)
    })
    Schema(schemaId, schema.title, schema.appliesTo, schemaFields, schema.isControlled, schema.owner)
  }

  private def createField(schemaId:UUID, schemaField:SchemaField, now:Long, index:Int, tx:Transaction) = {
    val schemaFieldId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (schema:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {schemaId}
         |}), (dataType:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {dataTypeId}
         |})
         |
         |
         |CREATE (sf:${SchemaFieldProperties.label} {
         |  ${SchemaFieldProperties.id} : {${SchemaFieldProperties.id}},
         |  ${SchemaFieldProperties.title} : {${SchemaFieldProperties.title}},
         |  ${SchemaFieldProperties.isControlled} : {${SchemaFieldProperties.isControlled}},
         |  ${SchemaFieldProperties.inputElement} : {${SchemaFieldProperties.inputElement}},
         |  ${SchemaFieldProperties.inputStyle} : {${SchemaFieldProperties.inputStyle}},
         |  ${SchemaFieldProperties.displayElement} : {${SchemaFieldProperties.displayElement}},
         |  ${SchemaFieldProperties.displayStyle} : {${SchemaFieldProperties.displayStyle}}
         |})-[:${SchemaFieldProperties.schema} {
         |  ${SchemaFieldProperties.order} : {${SchemaFieldProperties.order}},
         |  ${SchemaFieldProperties.createdAt} : {${SchemaFieldProperties.createdAt}},
         |  ${SchemaFieldProperties.updatedAt} : {${SchemaFieldProperties.updatedAt}}
         |}]->(schema),
         |
         |(sf)-[:${SchemaFieldProperties.dataType}]->(dataType)
         |RETURN sf
           """.stripMargin, 
      Map[String,Object](
        SchemaFieldProperties.id -> schemaFieldId.toString,
        SchemaFieldProperties.title -> schemaField.title,
        SchemaFieldProperties.isControlled -> schemaField.isControlled.asInstanceOf[Object],
        SchemaFieldProperties.inputElement -> schemaField.inputElement,
        SchemaFieldProperties.inputStyle -> schemaField.inputStyle,
        SchemaFieldProperties.displayElement -> schemaField.displayElement,
        SchemaFieldProperties.displayStyle -> schemaField.displayStyle,
        SchemaFieldProperties.order -> index.asInstanceOf[Object],
        SchemaFieldProperties.createdAt -> now.asInstanceOf[Object],
        SchemaFieldProperties.updatedAt -> now.asInstanceOf[Object],
        "schemaId" -> schemaId.toString,
        "dataTypeId" -> schemaField.dataType.id.toString
      )
    )
    val field = SchemaField(
      schemaFieldId, schemaField.title, schemaField.dataType,
      schemaField.inputElement, schemaField.inputStyle, schemaField.displayElement, schemaField.displayStyle,
      schemaField.isControlled, schemaField.terms
    )
    entityRepo.setVocabularyTermsForSchemaField(field, schemaField.terms, tx) match {
      case Left(errors) => {
        if (field.dataType.id == schemaId) {
          throw new Exception("Data type and schema type are the same. This is not allowed.")
        } else {
          throw new Exception(errors.toString())
          SchemaField(null, "Fake", null, "", "", "", "", false, List())
        }
      }
      case Right(terms) => {
        SchemaField(
          schemaFieldId, schemaField.title, schemaField.dataType,
          schemaField.inputElement, schemaField.inputStyle, schemaField.displayElement, schemaField.displayStyle,
          schemaField.isControlled, terms
        )
      }
    }
  }

  def getById(id:UUID):Option[Schema] = db.withTransaction(tx => {
    val schemaFields = tx.run(
      s"""
         |MATCH (schema:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {${SchemaProperties.id}}
         |})<-[schemaFieldLink:${SchemaFieldProperties.schema}]-(schemaField: ${SchemaFieldProperties.label})
         |  -[dataType:${SchemaFieldProperties.dataType}]->(dataTypeSchema: ${SchemaProperties.label})
         |
         |OPTIONAL MATCH  (dataTypeSchema)<-[:${SchemaProperties.owns}]-(owner:${UserProperties.label})
         |RETURN schemaField, dataTypeSchema, owner, schemaField.${SchemaFieldProperties.isControlled} as ${SchemaFieldProperties.isControlled}
         |ORDER BY schemaFieldLink.${SchemaFieldProperties.order} ASC
       """.stripMargin, Map[String,AnyRef](
        SchemaFieldProperties.id -> id.toString
      )).map(row => {
      val ownerInfo = row.get("owner")
      val owner = nodeToUser(if (ownerInfo != null) ownerInfo.asNode() else null)
      val dataTypeSchema = nodeToSchema(row.get("dataTypeSchema").asNode(), List(), owner)
      val isControlled = row.get(SchemaFieldProperties.isControlled).asBoolean()
      val schemaFieldNode = row.get("schemaField").asNode()
      val schemaFieldId = UUID.fromString(schemaFieldNode.get(SchemaFieldProperties.id).asString())
      val terms = if (isControlled) {
        entityRepo.getTermsForSchemaField(schemaFieldId)
      } else {
        List()
      }
      nodeToSchemaField(row.get("schemaField").asNode(), dataTypeSchema, terms)
    })
    val schemaBase = tx.run(
      s"""
         |MATCH (schema:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {${SchemaProperties.id}}
         |})
         |
         |OPTIONAL MATCH (schema)<-[:${SchemaProperties.owns}]-(owner:${UserProperties.label})
         |WHERE NOT schema:${SchemaProperties.deleted}
         |RETURN schema, owner
       """.stripMargin, Map[String,AnyRef](
        SchemaProperties.id -> id.toString
      )).map(row => {
      nodeToSchema(row.get("schema").asNode(), schemaFields.toList, 
        {
          val userNodeRow = row.get("owner")
          if (userNodeRow.isNull()) {
            null
          } else {
            nodeToUser(userNodeRow.asNode())
          }
        }
      )
    }).toList
    schemaBase.headOption
  })

  def getByIdWithVersionId(id:UUID, versionId:UUID):Option[Schema] = ???

  private def update(tx:Transaction, schema: Schema) = {
    // TODO: handle link to project in calling function
    // The reason for this is that we presume that only the user who is working
    // on the schema wants the updated version of the schema
    val now = new Date().getTime
    val newVersionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (currentSchema:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {${SchemaProperties.id}}
         |})<-[:${SchemaProperties.owns}]-(owner:${UserProperties.label}),
         |(updatingUser:${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |REMOVE currentSchema:${SchemaProperties.current}
         |WITH currentSchema, owner, updatingUser
         |CREATE (newSchema:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : currentSchema.${SchemaProperties.id},
         |  ${SchemaProperties.versionId} : {${SchemaProperties.versionId}},
         |  ${SchemaProperties.title} : {${SchemaProperties.title}},
         |  ${SchemaProperties.appliesTo} : {${SchemaProperties.appliesTo}},
         |  ${SchemaProperties.isControlled} : {${SchemaProperties.isControlled}},
         |  ${SchemaProperties.createdAt} : currentSchema.createdAt,
         |  ${SchemaProperties.updatedAt} : {${SchemaProperties.updatedAt}}
         |})-[:${SchemaProperties.previousVersion}]->(currentSchema),
         |  (newSchema)<-[:${SchemaProperties.owns}]-(owner),
         |  (newSchema)-[:${SchemaProperties.updatedBy}]->(updatingUser)
         |
         |WITH currentSchema, newSchema
         |MATCH (currentSchema)-[:${SchemaProperties.project}]->(project:${ProjectProperties.label})
         |CREATE (newSchema)-[:${SchemaProperties.project}]->(project)
         """.stripMargin, Map[String,AnyRef](
        SchemaProperties.id -> schema.id.toString,
        "userId" -> schema.owner.uuid.toString,

        SchemaProperties.versionId -> newVersionId.toString,
        SchemaProperties.title -> schema.title,
        SchemaProperties.appliesTo -> schema.appliesTo,
        SchemaProperties.isControlled -> schema.isControlled.asInstanceOf[Object],
        SchemaProperties.updatedAt -> now.asInstanceOf[Object]
      )
    )
    schema.fields.zipWithIndex.foreach({
      case (field, index) => createField(schema.id, field, now, index, tx)
    })

    schema
  }

  def save(schema:Schema):Schema = db.withTransaction(tx => {
    val result = if (schema.id == null) {
      create(tx, schema)
    } else {
      update(tx, schema)
    }
    tx.success()
    result
  })

  def list: List[Schema] = db.withSession(session => {
    session.run(
      s"""
         |MATCH (schema:${SchemaProperties.label}:${SchemaProperties.current})
         |RETURN schema.id
       """.stripMargin).map(row => getById(UUID.fromString(row.get("schema.id").asString())).get).toList
  })

  def listForProject(project:Project): List[Schema] = db.withSession(session => {
    session.run(
      s"""
         |MATCH (:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {${ProjectProperties.id}}
         |})<-[:${SchemaProperties.project}]-(schema:${SchemaProperties.label}:${SchemaProperties.current})
         |RETURN schema.id
       """.stripMargin, Map(
        ProjectProperties.id -> project.id.toString
      )).map(row => getById(UUID.fromString(row.get("schema.id").asString())).get).toList
  })

  // If someone splits off and creates a new version, then copy it and break the history

  def linkToProject(schema:Schema, project:Project) = db.withTransaction { tx =>
    /**
      * Delete links between the current version of the project and any other versions
      * of the schema
      * Create link between this project (current version) and the latest version
      * of the schema
      */
    tx.run(
      s"""
         |MATCH (project:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId}
         |}), (schema:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {schemaId}
         |})
         |CREATE (schema)-[:${SchemaProperties.project}]->(project)
       """.stripMargin, Map(
        "schemaId" -> schema.id.toString,
        "projectId" -> project.id.toString
      )
    )
    tx.success()
  }

  def delete(schema:Schema, deletingUser:User) = db.withTransaction( tx => {
    val newVersionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH
         |  (schema:${SchemaProperties.label}:${SchemaProperties.current} {
         |    ${SchemaProperties.id} : {${SchemaProperties.id}}
         |  }),
         |  (deletingUser:${UserProperties.label} {
         |    ${UserProperties.id}: {deletingUserId}
         |  })
         |
         |REMOVE schema:${SchemaProperties.current}
         |CREATE
         |  (newSchema:${SchemaProperties.label}:${SchemaProperties.current}:${SchemaProperties.deleted} {
         |    ${SchemaProperties.id} : {${SchemaProperties.id}},
         |    ${SchemaProperties.nodeId} : {newVersionId}
         |  }),
         |  (newSchema)-[:${SchemaProperties.deletedBy}]->(deletingUser),
         |  (newSchema)-[:${SchemaProperties.previousVersion}]->(schema)
       """.stripMargin, Map[String,AnyRef](
        SchemaProperties.id -> schema.id.toString,
        "deletingUserId" -> deletingUser.uuid.toString,
        "newVersionId" -> newVersionId.toString
      ))
    tx.success()
  })

  def listVersions(schema:Schema, pageNo:Int=0, rpp:Int=25):Iterator[Version[Schema]] = db.withTransaction( tx => {
    tx.run(
      s"""
         |MATCH (schema:${SchemaProperties.label} {
         |  ${SchemaProperties.id} : {schemaId}
         |})-[action:${SchemaProperties.versionAction}]->(user:${UserProperties.label})
         |RETURN schema, action, user
         |ORDER BY ${SchemaProperties.updatedAt}
         |SKIP ${pageNo * rpp}
         |LIMIT ${rpp}
       """.stripMargin, Map(
        "schemaId" -> schema.id.toString
      )
    ).map( row => {
      val schemaNode = row.get("schema").asNode()
      val fields = tx.run(
        s"""
           |MATCH (:${SchemaProperties.label} {
           |  ${SchemaProperties.id} : {schemaId},
           |  ${SchemaProperties.versionId} : {versionId}
           |})<-[schemaFieldRelation:${SchemaFieldProperties.schema}]-(schemaField:${SchemaFieldProperties.label})
           |    -[:${SchemaFieldProperties.dataType}]->(dataType:${SchemaProperties.label}),
           |  (dataType)<-[:${SchemaProperties.owns}]-(dataTypeOwner:${UserProperties.label})
           |RETURN schemaField, dataType, dataTypeOwner
           |ORDER BY schemaFieldRelation.${SchemaFieldProperties.order}
         """.stripMargin
      ).map(row => {
        nodeToSchemaField(
          row.get("schemaField").asNode(),
          nodeToSchema(row.get("dataType").asNode(), List(), nodeToUser(row.get("dataTypeOwner").asNode())),
          List()
        )
      })
      val userNode = row.get("user").asNode()
      val action = row.get("action").asString()

      new Version[Schema] {
        override val item = nodeToSchema(schemaNode, fields.toList, nodeToUser(userNode))
        override val versionId = UUID.fromString(schemaNode.get(SchemaProperties.versionId).asString())
        override val updatedAt = new Date(schemaNode.get(SchemaProperties.updatedAt).asLong())
        override val action = schemaNode.get(SchemaProperties.versionAction).asString()
      }

    })
  })

  def revertTo(schema:Schema, idOfVersionToRestore:UUID, revertingUser:User) = db.withTransaction(tx => {
    val now = new Date().getTime
    val newVersionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (currentVersion:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {schemaId}
         |})
         |  <-[:${SchemaProperties.owns}]-(owner:${UserProperties.label}),
         |(versionToRestore:${SchemaProperties.label} {
         |  ${SchemaProperties.id} : {schemaId},
         |  ${SchemaProperties.versionId} : {versionId}
         |})<-[:${SchemaFieldProperties.schema}]-(schemaFieldToRestore),
         |(revertingUser:${UserProperties.label} {
         |  ${UserProperties.id} : {revertingUserId}
         |}),
         |REMOVE currentVersion:${SchemaProperties.current}
         |CREATE (newVersion:${SchemaProperties.label}:${SchemaProperties.current} {
         |  ${SchemaProperties.id} : {schemaId},
         |  ${SchemaProperties.title} : versionToRestore.${SchemaProperties.title},
         |  ${SchemaProperties.versionId} : {newVersionId},
         |  ${SchemaProperties.appliesTo} : versionToRestore.${SchemaProperties.appliesTo},
         |  ${SchemaProperties.isControlled} : versionToRestore.${SchemaProperties.isControlled},
         |  ${SchemaProperties.createdAt} : versionToRestore.${SchemaProperties.createdAt},
         |  ${SchemaProperties.updatedAt} : {now}
         |}),
         |  (newVersion)-[:${SchemaProperties.revertedBy}]->(revertingUser),
         |  (newVersion)<-[:${SchemaProperties.owns}]-(owner),
         |  (newVersion)-[:${SchemaProperties.isReversionOf}]->(versionToRestore)
         |
         |// Update fields
         |MATCH (versionToRestore)
         |        <-[schemaFieldLink:${SchemaFieldProperties.schema}]-(schemaField:${SchemaFieldProperties.label})
         |        -[:${SchemaFieldProperties.dataType}]->(dataType)
         |CREATE (newSchemaField:${SchemaFieldProperties.label} {
         |  ${SchemaFieldProperties.id} : schemaFieldToRestore.${SchemaFieldProperties.id},
         |  ${SchemaFieldProperties.title} : schemaFieldToRestore.${SchemaFieldProperties.title},
         |  ${SchemaFieldProperties.isControlled} : schemaFieldRestore.${SchemaFieldProperties.isControlled},
         |  ${SchemaFieldProperties.inputElement} : schemaFieldRestore.${SchemaFieldProperties.inputElement},
         |  ${SchemaFieldProperties.inputStyle} : schemaFieldRestore.${SchemaFieldProperties.inputStyle},
         |  ${SchemaFieldProperties.displayElement} : schemaFieldRestore.${SchemaFieldProperties.displayElement},
         |  ${SchemaFieldProperties.displayStyle} : schemaFieldRestore.${SchemaFieldProperties.displayStyle}
         |}),
         |  (newSchemaField)-[:${SchemaFieldProperties.schema} {
         |    ${SchemaFieldProperties.order} : schemaFieldLink.${SchemaFieldProperties.order}
         |  }]->(newVersion),
         |  (newSchemaField)-[:${SchemaFieldProperties.dataType}]->(dataType)
       """.stripMargin, Map[String,AnyRef](
        "schemaId" -> schema.id.toString,
        "revertngUserId" -> revertingUser.uuid.toString,
        "newVersionId" -> newVersionId.toString,
        "now" -> now.asInstanceOf[Object]
      )
    )
    tx.success()
  })

}


trait ConvertsNodesToSchemas {

  def nodeToSchemaField(schemaField:Node, schema:Schema, terms:List[Entity]):SchemaField = SchemaField(
    UUID.fromString(schemaField.get(SchemaFieldProperties.id).asString()),
    schemaField.get(SchemaFieldProperties.title).asString(),
    schema,
    schemaField.get(SchemaFieldProperties.inputElement).asString(),
    schemaField.get(SchemaFieldProperties.inputStyle).asString(),
    schemaField.get(SchemaFieldProperties.displayElement).asString(),
    schemaField.get(SchemaFieldProperties.displayStyle).asString(),
    schemaField.get(SchemaFieldProperties.isControlled).asBoolean(),
    terms
  )

  def nodeToSchema(schemaNode:Node, schemaFields:List[SchemaField], owner:User):Schema =
    Schema(
      UUID.fromString(schemaNode.get(SchemaProperties.id).asString()),
      schemaNode.get(SchemaProperties.title).asString(),
      schemaNode.get(SchemaProperties.appliesTo).asString(),
      schemaFields,
      schemaNode.get(SchemaProperties.isControlled).asBoolean(),
      owner
    )
}


