package schemas

import java.util.UUID

import accessControl.User
import annotations.{Entity, EntityProperties}

/**
 * Created by dave on 10/14/15.
 */

case class Schema(id:UUID, title:String, appliesTo:String, fields:List[SchemaField], isControlled:Boolean, owner:User) {

  private val fieldNames = fields.foldLeft(Map[String,SchemaField]()) {
    case (acc, field) => acc.updated(field.title, field)
  }

  def apply(fieldName:String) = fieldNames(fieldName)

  private val fieldNodeIds = fields.foldLeft(Map[UUID,SchemaField]()) {
    case (acc, field) => acc.updated(field.id, field)
  }

  def apply(fieldId:UUID) = fieldNodeIds(fieldId)

  def this(title:String, appliesTo:String, fields:List[SchemaField], isControlled:Boolean, creator:User) = this(null, title, appliesTo, fields, isControlled, creator)

  def validate(entity:Map[String,AnyRef]):ValidationStatus = {
    val requiredFieldNames = fields.map(_.title).toSet
    val extraFields = entity.keySet.diff(Set(EntityProperties.id, EntityProperties.versionId)).diff(requiredFieldNames)
    val missingFields = requiredFieldNames.diff(entity.keySet)
    // do all data types match?
    val invalidFields = fields.filterNot({
      case SchemaField(_, fieldKey, dataType, _, _, _, _, isControlled, _) => missingFields.contains(fieldKey) || entity.contains(fieldKey) && (dataType match {
        case Schema(_, "String", _, Nil, isControlled, creator) => entity(fieldKey).asInstanceOf[String].length > 0
        case Schema(_, "Int", _, Nil, isControlled, creator) => try {
            entity(fieldKey).asInstanceOf[Int]
            true
          } catch {
              case ex:ClassCastException => {
                try {
                  entity(fieldKey).asInstanceOf[String].toInt
                  true
                } catch {
                  case ex:NumberFormatException => false
                }
              }
          }
          // TODO: Check for controlled vocabulary
        case _ => true
      })
    }).map(_.title).toSet

    if (extraFields.nonEmpty || missingFields.nonEmpty || invalidFields.nonEmpty) {
      SchemaValidationError(extraFields, missingFields, invalidFields)
    } else {
      Pass
    }
  }

}

class ValidationStatus
case class SchemaValidationError(extraFields:Set[String], missingFields:Set[String], invalidFields:Set[String]) extends ValidationStatus
case object Pass extends ValidationStatus


case class SchemaField(id: UUID, title:String, dataType:Schema, inputElement:String, inputStyle:String, displayElement:String, displayStyle:String,
                       isControlled:Boolean, terms:List[Entity]
                      ) {

  def this(id: UUID, title:String, dataType:Schema, isControlled:Boolean) = this(id, title, dataType, null, null, null, null, isControlled, List())
  def this(title:String, dataType:Schema, isControlled:Boolean) = this(null, title, dataType, isControlled)

}

