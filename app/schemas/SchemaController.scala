package schemas

import java.util.UUID
import javax.inject.{Inject, Singleton}

import accessControl.{AuthenticationService, User, UserRepository}
import annotations.{Entity, EntityRepository, RendersEntitiesAsJson}
import common.JsonResponses
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.{Action, Controller}
import play.api.data._
import projects.{ProjectService, ProjectUserRights}


/**
 * Created by dave on 11/13/15.
 */
trait SchemaController extends RendersEntitiesAsJson with JsonResponses {

  this: Controller =>

  val logger = Logger("SchemaController")

  val projectService: ProjectService
  val schemaRepo: SchemaRepository
  val entityRepo: EntityRepository
  val userRepo: UserRepository
  val authService: AuthenticationService

  import play.api.libs.json.Reads._
  import play.api.libs.functional.syntax._
  import play.api.data.Forms._
  import play.api.data.validation.Constraints._


  implicit val referencedSchemaFieldReads:Reads[ReferencedSchemaField] = (
    (JsPath \ "id").readNullable[UUID] and (JsPath \ "title").read[String] and (JsPath \ "typeId").read[UUID] and
      (JsPath \ "inputElement").read[String] and (JsPath \ "inputStyle").read[String] and (JsPath \ "displayElement").read[String] and (JsPath \ "displayStyle").read[String]
      and (JsPath \ "isControlled").read[Boolean]
  )(ReferencedSchemaField.fromWeb _)

  implicit val referencedSchemaReads:Reads[ReferencedSchema] = (
    (JsPath \ "id").read[UUID] and (JsPath \ "title").read[String] and (JsPath \ "appliesTo").read[String] and
      (JsPath \ "fields").read[List[ReferencedSchemaField]] and (JsPath \ "isControlled").read[Boolean]
  )(ReferencedSchema.apply _)

  implicit val entityWrites:Writes[EntityWithReferencedSchema] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "schema").write[UUID] and (JsPath \ "values").write[Map[String,String]]
    )(unlift(EntityWithReferencedSchema.unapply))

  implicit val referencedSchemaFieldWrites:Writes[ReferencedSchemaField] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "title").write[String] and (JsPath \ "typeId").write[UUID] and
      (JsPath \ "inputElement").write[String] and (JsPath \ "inputStyle").write[String] and (JsPath \ "displayElement").write[String]
      and (JsPath \ "displayStyle").write[String]
      and (JsPath \ "isControlled").write[Boolean] and (JsPath \ "values").write[List[EntityWithReferencedSchema]]
    )(unlift(ReferencedSchemaField.unapply))

  def entityWithReferencedSchemaFromEntity(entity:Entity):EntityWithReferencedSchema = EntityWithReferencedSchema(
    entity.id,
    entity.schema.id,
    entity.data
  )

  def entityFromEntityWithReferencedSchema(ews:EntityWithReferencedSchema, schema:Schema) = Entity(ews.id, schema, ews.data)

  def schemaFieldFromReferencedSchemaField(referenced:ReferencedSchemaField):Option[SchemaField] =
    schemaRepo.getById(referenced.typeId) match {
      case Some(innerSchema) => Some(SchemaField(
        referenced.id, referenced.title, innerSchema,
        referenced.inputElement, referenced.inputStyle, referenced.displayElement, referenced.displayStyle,
        referenced.isControlled, referenced.values.map(entityFromEntityWithReferencedSchema(_, innerSchema))))
      case None => None
    }

  def referencedSchemaFromSchema(schema:Schema) = ReferencedSchema(schema.id, schema.title, schema.appliesTo,
    schema.fields.map (f => ReferencedSchemaField(f.id, f.title, f.dataType.id,
      f.inputElement, f.inputStyle, f.displayElement, f.displayStyle,
      f.isControlled, f.terms.map(entityWithReferencedSchemaFromEntity))),
    schema.isControlled
  )


  def entity2JsObject(entity:Entity):JsObject = {
    Json.obj(
      "id" -> entity.id,
      "schemaId" -> entity.schema.id,
      "data" -> entity.data
    )
  }

  def entityWithReferencedSchema2JsObject(entity:EntityWithReferencedSchema):JsObject = {
    Json.obj(
      "id" -> entity.id,
      "schemaId" -> entity.schemaId,
      "data" -> entity.data
    )
  }

  def referencedSchemaField2JsObject(rfs:ReferencedSchemaField) = {
    Json.obj(
      "id" -> rfs.id,
      "title" -> rfs.title,
      "typeId" -> rfs.typeId,
      "isControlled" -> rfs.isControlled,
      "values" -> rfs.values.map(entityWithReferencedSchema2JsObject)

    )
  }

  implicit val referencedSchemaWrites: Writes[ReferencedSchema] = (
    (JsPath \ "id").write[UUID] and (JsPath \ "title").write[String] and (JsPath \ "appliesTo").write[String]
      and (JsPath \ "fields").write[List[ReferencedSchemaField]] and (JsPath \ "isControlled").write[Boolean]
  )(unlift(ReferencedSchema.unapply _))

  implicit val newSchemaFieldReads:Reads[NewSchemaField] = (
    (JsPath \ "title").read[String] and (JsPath \ "typeId").read[UUID] and
      (JsPath \ "inputElement").read[String] and (JsPath \ "inputStyle").read[String] and (JsPath \ "displayElement").read[String] and (JsPath \ "displayStyle").read[String]
      and (JsPath \ "isControlled").read[Boolean]
  )(NewSchemaField.apply _)

  implicit val newSchemaReads:Reads[NewSchema] = (
     (JsPath \ "title").read[String] and (JsPath \ "appliesTo").read[String] and (JsPath \ "fields").read[List[NewSchemaField]]
      and (JsPath \ "isControlled").read[Boolean]
  )(NewSchema.apply _)

  def create(projectId: UUID) = projectService.PermissionRequiredAction(projectId, ProjectUserRights.canAddNewSchemas)(parse.json) { implicit request =>
    val json = request.body
    val title = (json \ "title").as[String]
    val isControlled = (json \ "isControlled").as[Boolean]
    val appliesTo = (json \ "appliesTo").as[String]
    val fields = (json \ "fields").as[List[JsObject]].map(
      field => FieldData(
        (field \ "title").as[String],
        (field \ "typeId").as[UUID],
        (field \ "inputElement").as[String],
        (field \ "inputStyle").as[String],
        (field \ "displayElement").as[String],
        (field \ "displayStyle").as[String],
        // TODO: handle controlled vocabulary fields
        (field \ "isControlled").as[Boolean],
        (field \ "values").as[List[Map[String,String]]]
      )
    )

    val referencedSchemas = fields.foldLeft(Map[UUID, Option[Schema]]()) {
      case (acc, field) => if (acc.contains(field.typeId)) {
        acc
      } else {
        acc.updated(field.typeId, schemaRepo.getById(field.typeId))
      }
    }

    val missingFields = referencedSchemas.filter(f => f._2.isEmpty)
    val filteredReferencedSchemas = referencedSchemas.filter(f => f._2.isDefined).map({
      case (key, value) => key -> value.head
    })

    if (missingFields.isEmpty) {
      val schemaToCreate = Schema(
        null, title, appliesTo, fields.map(f => {
            val fieldType = filteredReferencedSchemas(f.typeId)
            SchemaField(null, f.title, fieldType,
              f.inputElement, f.inputStyle, f.displayElement, f.displayStyle,
              f.isControlled, f.values.map(values => Entity(null, fieldType, values))
            )
        }),
        isControlled, request.user
      )
      val createdSchema = schemaRepo.save(schemaToCreate)
      schemaRepo.linkToProject(createdSchema, request.project)
      Created(Json toJson referencedSchemaFromSchema(createdSchema))
    } else {
      BadRequest(Json.obj(
        "error" -> s"Fields do not exist",
        "missingFields" ->  missingFields.keys
      ))
    }
  }

  def update(projectId:UUID, schemaId:UUID) = projectService.PermissionRequiredAction(projectId, ProjectUserRights.canEditOthersSchemas)(parse.json) { implicit request =>
    schemaRepo.getById(schemaId) match {
      case None => NotFound(error("Schema does not exist"))
      case Some(schema) => {
        val json = request.body
        val title = (json \ "title").as[String]
        val isControlled = (json \ "isControlled").as[Boolean]
        val appliesTo = (json \ "appliesTo").as[String]
        val fields = (json \ "fields").as[List[JsObject]].map(
          field => (
            (field \ "id").getOrElse(null),
            (field \ "title").as[String],
            (field \ "typeId").as[UUID],
            (field \ "inputElement").as[String],
            (field \ "inputStyle").as[String],
            (field \ "displayElement").as[String],
            (field \ "displayStyle").as[String],
            (field \ "isControlled").as[Boolean],
            (field \ "values").asOpt[List[Map[String,String]]]
          )
        )

        val referencedSchemas = fields.foldLeft(Map[UUID, Option[Schema]]()) {
            case (acc, (_, _, typeId, _, _, _, _, _, _)) => if (acc.contains(typeId)) {
              acc
          } else {
            acc.updated(typeId, schemaRepo.getById(typeId))
          }
        }

        val missingFields = referencedSchemas.filter(f => f._2.isEmpty)
        val filteredReferencedSchemas = referencedSchemas.filter(f => f._2.isDefined).map({
          case (key, value) => key -> value.head
        })

        if (missingFields.isEmpty) {
          val schemaToUpdate = Schema(
            schemaId, title, appliesTo, fields.map({
              case(possibleId, title, typeId, inputElement, inputStyle, displayElement, displayStyle, isControlled, values) => {
                val fieldType = filteredReferencedSchemas(typeId)
                val id = if (possibleId == null) null else UUID.fromString(possibleId.as[String])
                SchemaField(id, title, fieldType,
                  inputElement, inputStyle, displayElement, displayStyle,
                  isControlled, values.getOrElse(List()).map(values => Entity(null, fieldType, values))
                )
              }
            }),
            isControlled, request.user
          )
          val updatedSchema = schemaRepo.save(schemaToUpdate)
          Ok(Json toJson referencedSchemaFromSchema(updatedSchema))
        } else {
          BadRequest(Json.obj(
            "error" -> s"Fields do not exist",
            "missingFields" ->  missingFields.keys
          ))
        }
      }
    }
  }

  def get(id: UUID) = Action {
    schemaRepo getById id match {
      case Some(schema) => {
        Ok(Json toJson referencedSchemaFromSchema(schema))
      }
      case None => NotFound(Json.obj("message" -> s"Schema ${id} not found"))
    }
  }

  def list = Action {
    Ok(
      Json.toJson(schemaRepo.list.map(referencedSchemaFromSchema))
    )
  }

  def listForProject(projectId: UUID) = projectService.ProjectRequiredAction(projectId) { implicit request =>
    val schemaList = schemaRepo listForProject(request.project)
    val schemaJson = Json toJson(schemaList.map(s => referencedSchemaFromSchema(s)))
    Ok(Json.obj("projectId" -> projectId, "schemas" -> schemaJson))
  }

  def getBaseTypes = Action {
    Ok(Json toJson schemaRepo.getBaseTypes.map(s => referencedSchemaFromSchema(s)))
  }

  def listVersions(schemaId:UUID, page:Int, rpp:Int) = authService.LoginRequiredAction { implicit request =>
    schemaRepo.getById(schemaId) match {
      case None => NotFound(missingObject("Schema", schemaId))
      case Some(schema) => Ok(Json.obj(
        "id" -> schemaId.toString,
        "versions" -> schemaRepo.listVersions(schema, page, rpp).map(version => Json.obj(
          "version" -> Json.obj(
            "versionId" -> version.versionId.toString,
            "updatedAt" -> version.updatedAt,
            "action" -> version.action
          ),
          "schema" -> referencedSchemaFromSchema(version.item)
        )).toList)
      )
    }
  }

  def getForProjectAndId(projectId:UUID, schemaId:UUID) = get(schemaId)

  def delete(projectId:UUID, schemaId: UUID) = projectService.PermissionRequiredAction(projectId, ProjectUserRights.canDeleteSchemas) { implicit request =>
    schemaRepo getById schemaId match {
      case Some(schema) => {
        schemaRepo.delete(schema, request.user)
        Ok(Json.obj("message" -> s"Schema ${schema.id} (${schema.title}) deleted"))
      }
      case None => NotFound(Json.obj("message" -> s"Schema ${schemaId} not found"))
    }
  }

}

case class FieldData(title:String, typeId:UUID,
  inputElement:String, inputStyle:String, displayElement:String, displayStyle:String,
  isControlled:Boolean, values:List[Map[String, String]]
)
case class ReferencedSchema(id:UUID, title:String, appliesTo:String, fields:List[ReferencedSchemaField], isControlled:Boolean)
case class ReferencedSchemaField(id:UUID, title:String, typeId:UUID,
                                 inputElement:String, inputStyle:String, displayElement:String, displayStyle:String,
                                 isControlled:Boolean, values:List[EntityWithReferencedSchema]) {
  def this(title:String, typeId:UUID,
           inputElement:String, inputStyle:String, displayElement:String, displayStyle:String,
           isControlled:Boolean, values:List[EntityWithReferencedSchema]) = this(null, title, typeId, inputElement, inputStyle, displayElement, displayStyle, isControlled, values)
}

case class EntityWithReferencedSchema(id:UUID, schemaId:UUID, data:Map[String,String]) {
  def this(schemaId:UUID, data:Map[String,String]) = this(null, schemaId, data)
}

case class NewSchema(title:String, appliesTo:String, fields:List[NewSchemaField], isControlled:Boolean)
case class NewSchemaField(title:String, typeId:UUID,
                          inputElement:String, inputStyle:String, displayElement:String, displayStyle:String,
                          isControlled:Boolean)

case object ReferencedSchemaField {
  val NO_ID = null

  def fromWeb(possibleId:Option[UUID], title:String, typeId:UUID, inputElement:String, inputStyle:String, displayElement:String, displayStyle:String, isControlled:Boolean) = possibleId match {
    case Some(id) => ReferencedSchemaField(id, title, typeId, inputElement, inputStyle, displayElement, displayStyle, isControlled:Boolean, List())
    case None => ReferencedSchemaField(NO_ID, title, typeId, inputElement, inputStyle, displayElement, displayStyle, isControlled:Boolean, List())
  }
}



@Singleton
class DefaultController @Inject()(
   val projectService: ProjectService,
   val schemaRepo:SchemaRepository,
   val entityRepo:EntityRepository,
   val userRepo:UserRepository,
   val authService:AuthenticationService
) extends Controller with SchemaController
