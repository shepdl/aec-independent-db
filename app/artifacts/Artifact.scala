package artifacts

import java.io.File
import java.util.UUID

import accessControl.{Ownable, User}
import common.Resource


/**
 * Created by dave on 11/6/15.
 */
case class Artifact(id:UUID, title:String, owner:User) extends Ownable with Resource {
  override val resourceName: String = "Artifact"
  def this(title:String, owner:User) = this(null, title, owner)
}


case class Model(id:UUID, title:String, format:String, files:List[ModelFile], previewImageData:String) extends Resource {
  override val resourceName: String = "Model"
}

case class ModelFile(id:UUID, filename:String, fileType:String, file:File)

case class Image(id:UUID, title:String, filename:String, fileType:String, file:File)

