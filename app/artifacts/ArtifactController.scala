package artifacts

import java.security.MessageDigest
import javax.inject.{Inject, Singleton}

import play.api.libs.json._
import play.api.mvc.{Action, BodyParsers, Controller}
import projects.{ProjectRepository, ProjectService, ProjectUserRights}
import play.api.data._
import play.api.data.Forms._
import play.api.libs.functional.syntax._
import java.io.File
import java.util.UUID

import accessControl.{AuthenticationService, RendersUserAsJson, User}
import common.JsonResponses
import play.api.i18n.MessagesApi
import play.api.i18n.I18nSupport

/**
 * Created by dave on 11/12/15.
 */
trait ArtifactController extends I18nSupport with JsonResponses with RendersUserAsJson {

  this: Controller =>

  val authenticationService:AuthenticationService
  val projectRepo:ProjectRepository
  val projectService:ProjectService
  val artifactRepo:ArtifactRepository
  val mediaRepo:MediaRepository

  implicit val messagesApi:MessagesApi

  val artifactReads: Reads[Artifact] = (
    (JsPath \ "id").read[UUID] and (JsPath \ "title").read[String] and (JsPath \ "owner").read[User]
  )(Artifact.apply _)

  val artifactForm:Form[ArtifactData] = Form(
    mapping(
      "title" -> nonEmptyText
    )(ArtifactData.apply)(ArtifactData.unapply)
  )

  def toJson(artifact:Artifact):JsObject = Json obj (
    "id" -> artifact.id,
    "title" -> artifact.title,
    "models" -> JsArray(mediaRepo.getModelsForArtifact(artifact).map(toJson))
  )

  def toJson(model:Model):JsObject = Json obj(
    "id" -> model.id,
    "title" -> model.title,
//    "files" -> model.files.map(f => s"/models/${model.id}/$f")
    "files" -> model.files.map(modelFile => s"/models/${model.id}/${modelFile.id}.${modelFile.fileType}"),
    "previewImage" -> model.previewImageData
  )


  def get(id:UUID) = Action {
    artifactRepo getById id match {
      case Some(artifact) => Ok(toJson(artifact))
      case None => NotFound(error(s"Artifact ${id} not found"))
    }
  }

  def getForProject(activityId:UUID) = projectService.ProjectRequiredAction(activityId) { request =>
    val artifacts = artifactRepo.getForProject(request.project)
    Ok(JsArray(artifacts.map({
      artifact => toJson(artifact) ++ Json.obj(
        "models" -> mediaRepo.getModelsForArtifact(artifact).map(toJson)
      )
    })))
  }

  val ACCEPTABLE_SORT_FIELDS = Set(ArtifactProperties.createdAt, ArtifactProperties.updatedAt, ArtifactProperties.title)
  def listNotInProject(activityId:UUID, page:Int, sortBy:String, rpp:Int) = projectService.ProjectRequiredAction(activityId) { request =>
    if (ACCEPTABLE_SORT_FIELDS.contains(sortBy)) {
      Ok(Json.obj(
        "artifacts" -> artifactRepo.listNotInProject(request.project, page, sortBy, rpp).map(toJson)
      ))
    } else {
      BadRequest(error(
        s"Unrecognized sort field $sortBy"
      ))
    }
  }

  /**
   *
   * Workflow: we expect the user to enter the form data, and then select a file, and then hit "Upload"
   * The client will then submit the create request, which contains the temporary filename.
   * Then, the client will submit the file (without additional user interaction), which will POST to the
   * uploadMedia handler. That will return a filename for the file.
   *
   * @param activityId
   * @return
   */
  def create(activityId:UUID) = projectService.AuthenticatedProjectAction(activityId)(parse.urlFormEncoded) { implicit request =>
    artifactForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(formWithErrors.errorsAsJson)
      },
      artifactData => {
        val unsavedArtifact = Artifact(null, artifactData.title, request.user)
        val savedArtifact = artifactRepo.save(unsavedArtifact)
        artifactRepo.linkToProject(savedArtifact, request.project)
        Created(toJson(savedArtifact))
      }
    )
  }

  def createWithFile(activityId:UUID, artifactId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canAddNewArtifacts
  )(parse.multipartFormData) { implicit request =>
    if (request.body.dataParts.contains("title")) {
      val title = request.body.dataParts("title")
      val format = request.body.dataParts("format").head
      val files = request.body.files.map {
        case (file) => {
          val extension = file.filename.split("\\.").last
          ModelFile(
            null, file.filename, extension, file.ref.file
          )
        }
      }
      if (files.isEmpty) {
        BadRequest(error("File missing"))
      } else {
        artifactRepo.getById(artifactId) match {
          case None => NotFound(error(s"Artifact $artifactId not found"))
          case Some(artifact) => {
            val title = request.body.dataParts("title").head
            val modelData = Model(
              null, title, format, files.toList, null
            )
            val model = mediaRepo.save(modelData)
            val modelWithFile = mediaRepo.addModelToArtifact(artifact, model)
            Created(toJson(model))
          }
        }
      }
    } else {
      BadRequest(error("title missing"))
    }
  }

  def update(activityId:UUID, artifactId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canEditOthersArtifacts
  )(parse.json) { implicit request =>
    val artifactResult = request.body.validate[Artifact](artifactReads)
    artifactResult.fold(
      errors => BadRequest(Json.obj("status" -> "KO", "message" -> JsError.toFlatJson(errors))),
      artifactData => {
        artifactRepo.getById(artifactId) match {
          case None => NotFound(s"Artifact ${artifactId} does not exist")
          case Some(artifact) => {
            val newArtifact = Artifact(artifactId, artifactData.title, request.user)
            artifactRepo.save(newArtifact)
            Ok(Json.obj("status" -> "OK", "message" -> s"${artifact.title} updated"))
          }
        }
      }
    )
  }

  def linkToProject(activityId:UUID, artifactId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canAddNewArtifacts
  ) { implicit request =>
    artifactRepo.getById(artifactId) match {
      case None => NotFound(error(s"Artifact $artifactId not found"))
      case Some(artifact) => {
        artifactRepo.linkToProject(artifact, request.project)
        Ok(Json.obj(
          "message" -> "Link successful",
          "projectId" -> request.project.id,
          "artifactId" -> artifact.id
        ))
      }
    }
  }

  def unlinkFromProject(activityId:UUID, artifactId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canDeleteArtifacts
    ) { implicit request =>
    artifactRepo.getById(artifactId) match {
      case None => NotFound(error(s"Artifact ${artifactId} not found"))
      case Some(artifact) => {
        artifactRepo.unlinkFromProject(artifact, request.project)
        Ok(Json.obj(
          "message" -> "Unlinked",
          "projectId" -> request.project.id,
          "artifactId" -> artifact.id
        ))
      }
    }
  }

  def delete(activityId: UUID, id:UUID) = projectService.PermissionRequiredAction(activityId, ProjectUserRights.canDeleteArtifacts) { implicit request =>
    artifactRepo.getById(id) match {
      case None => NotFound(Json.obj("message" -> s"Artifact ${id} not found"))
      case Some(artifact) => {
        artifactRepo.delete(artifact, request.user)
        Ok(Json.obj("status" -> "OK", "message" -> s"${artifact.title} deleted"))
      }
    }
  }

  def listVersions(id:UUID, start:Int=0, rpp:Int=25) = projectService.PermissionRequiredAction(id, ProjectUserRights.canEditOthersArtifacts) { implicit request =>
    artifactRepo.getById(id) match {
      case None => NotFound(Json.obj("message" -> s"Artifact ${id} not found"))
      case Some(_) => Ok(Json.obj(
        "artifactId" -> id,
        "versions" -> artifactRepo.listVersions(id, start, rpp).map {
          version => Json.obj(
            "version" -> Json.obj(
              "versionId" -> version.versionId.toString,
              "updatedAt" -> version.updatedAt,
              "action" -> version.action
            ),
            "artifact" -> toJson(version.item)
          )
        })
      )
    }
  }

}


case class ArtifactData(title:String)
case class ModelStub(id:Long)


@Singleton
class DefaultController @Inject()(
   val authenticationService: AuthenticationService,
   val projectRepo:ProjectRepository,
   val projectService: ProjectService,
   val artifactRepo: ArtifactRepository,
   val mediaRepo:MediaRepository,
   override val messagesApi:MessagesApi
) extends ArtifactController with Controller

