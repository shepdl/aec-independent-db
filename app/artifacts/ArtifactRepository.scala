package artifacts

import java.util.{Date, UUID}
import javax.inject.Inject

import accessControl.{ConvertsNodesToUsers, OwnableProperties, User, UserProperties}
import common._
import org.neo4j.driver.v1.Transaction
import org.neo4j.driver.v1.types.Node
import projects.{Project, ProjectProperties}

/**
 * Created by dave on 11/12/15.
 */

object ArtifactProperties extends GraphEntityProperties with VersionedGraphEntityProperties
  with OwnableProperties with TimestampsProperties {
  val label = "Artifact"
  val title = "title"

  val project = "Project"

}

class ArtifactRepository @Inject() (db:DatabaseService) extends ConvertsNodesToArtifacts {

  import scala.collection.JavaConversions._

  def getById(id:UUID):Option[Artifact] = db.withTransaction { tx =>
    tx.run(
      s"""
         |MATCH (a:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : {${ArtifactProperties.id}}
         |})<-[:${ArtifactProperties.owns}]-(u:${UserProperties.label})
         |WHERE NOT a:${ArtifactProperties.deleted}
         |RETURN a, u
       """.stripMargin, Map(
        ArtifactProperties.id -> id.toString
      )
    ).map(row => nodeToArtifact(row.get("a").asNode(), row.get("u").asNode())).toIterable.headOption
  }

  def save(artifact:Artifact):Artifact = db.withTransaction(tx => {
    val savedArtifact = save(artifact, tx)
    tx.success()
    savedArtifact
  })

  def save(artifact: Artifact, tx:Transaction):Artifact = {
    val now = new Date().getTime
    val versionId = UUID.randomUUID()
    val id = if (artifact.id == null) {
      val newId = UUID.randomUUID()
      val result = tx.run(
        s"""
           |MATCH (u:${UserProperties.label} {
           |  ${UserProperties.id} : {userId}
           |})
           |CREATE (a:${ArtifactProperties.label}:${ArtifactProperties.current} {
           |  ${ArtifactProperties.id} : {artifactId},
           |  ${ArtifactProperties.title} : {${ArtifactProperties.title}},
           |  ${ArtifactProperties.createdAt} : {${ArtifactProperties.createdAt}},
           |  ${ArtifactProperties.updatedAt} : {${ArtifactProperties.updatedAt}},
           |  ${ArtifactProperties.versionId} : {${ArtifactProperties.versionId}},
           |  ${ArtifactProperties.versionAction} : "${VersionActions.original}"
           |})<-[:${ArtifactProperties.owns} {
           |  ${ArtifactProperties.createdAt} : {${ArtifactProperties.createdAt}},
           |  ${ArtifactProperties.updatedAt} : {${ArtifactProperties.updatedAt}}
           |}]-(u)
           |RETURN a
         """.stripMargin, Map[String,AnyRef](
          "userId" -> artifact.owner.uuid.toString,
          "artifactId" -> newId.toString,
          ArtifactProperties.title -> artifact.title,
          ArtifactProperties.createdAt -> now.asInstanceOf[Object],
          ArtifactProperties.updatedAt -> now.asInstanceOf[Object],
          ArtifactProperties.versionId -> versionId.toString
        )
      )
      newId
    } else {
      /**
        * Create new artifact
        * Link all models to this new artifact
        * Link projects to this new artifact
        */
      val result = tx.run(
        s"""
           |MATCH (oldArtifact:${ArtifactProperties.label}:${ArtifactProperties.current} {
           |  ${ArtifactProperties.id} : {artifactId}
           |}),
           |(user:${UserProperties.label} {
           |  ${UserProperties.id} : {userId}
           |})
           |
           |REMOVE oldArtifact:${ArtifactProperties.current}
           |CREATE (newArtifact:${ArtifactProperties.label}:${ArtifactProperties.current} {
           |  ${ArtifactProperties.id} : oldArtifact.${ArtifactProperties.id},
           |  ${ArtifactProperties.title} : {${ArtifactProperties.title}},
           |  ${ArtifactProperties.createdAt} : oldArtifact.${ArtifactProperties.createdAt},
           |  ${ArtifactProperties.updatedAt} : {${ArtifactProperties.updatedAt}},
           |  ${ArtifactProperties.versionId} : {${ArtifactProperties.versionId}},
           |  ${ArtifactProperties.versionAction} : "${VersionActions.updated}"
           |}),
           |  (newArtifact)-[:${ArtifactProperties.previousVersion}]->(oldArtifact),
           |  (newArtifact)<-[:${ArtifactProperties.owns}]-(user)
           |
           |WITH oldArtifact, newArtifact
           |MATCH
           |  (oldArtifact)-[:${ArtifactProperties.project}]->(project:${ProjectProperties.current})
           |CREATE
           |  (newArtifact)-[:${ArtifactProperties.project}]->(project)
           |
           |WITH oldArtifact, newArtifact
           |MATCH
           |  (oldArtifact)<-[:${ModelProperties.artifact}]-(model:${ModelProperties.label})
           |CREATE
           |  (newArtifact)<-[:${ModelProperties.artifact}]-(model)
           |
         """.stripMargin, Map[String,AnyRef](
          "artifactId" -> artifact.id.toString,
          "userId" -> artifact.owner.uuid.toString,
          ArtifactProperties.title -> artifact.title,
          ArtifactProperties.updatedAt -> now.asInstanceOf[Object],
          ArtifactProperties.versionId -> versionId.toString
        )
      )
      artifact.id
    }
    Artifact(id, artifact.title, artifact.owner)
  }

  def delete(artifact: Artifact, user:User) = db.withTransaction(tx => {
    /**
      * Create new version of the artifact that has the "deleted" property
      * Link all projects to this version
      */
    val versionId = UUID.randomUUID()
    val now = new Date().getTime
    val results = tx.run(
      s"""
         |MATCH (oldArtifact:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : {artifactId}
         |})<-[:${ArtifactProperties.owns}]-(owner:${UserProperties.label}),
         |(deletingUser: ${UserProperties.label} {
         |  ${UserProperties.id} : {userId}
         |})
         |REMOVE oldArtifact:${ArtifactProperties.current}
         |CREATE (newArtifact:${ArtifactProperties.label}:${ArtifactProperties.current}:${ArtifactProperties.deleted} {
         |  ${ArtifactProperties.id} : {artifactId},
         |  ${ArtifactProperties.versionId} : {${ArtifactProperties.versionId}},
         |  ${ArtifactProperties.updatedAt} : {${ArtifactProperties.updatedAt}},
         |  ${ArtifactProperties.versionAction} : {${ArtifactProperties.versionAction}}
         |})-[:${ArtifactProperties.deletedBy}]->(deletingUser),
         |  (newArtifact)<-[:${ArtifactProperties.owns}]-(owner),
         |  (newArtifact)-[:${ArtifactProperties.previousVersion}]->(oldArtifact)
       """.stripMargin, Map[String,AnyRef](
        "artifactId" -> artifact.id.toString,
        ArtifactProperties.versionId -> versionId.toString,
        "userId" -> user.uuid.toString,
        ArtifactProperties.updatedAt -> now.asInstanceOf[Object],
        ArtifactProperties.versionAction -> VersionActions.deleted
      )
    )
    tx.success()
  })

  def linkToProject(artifact:Artifact, project: Project) = db.withTransaction { tx =>

    /**
      * Create a new version of the project that has this artifact
      */
    val now = new Date().getTime.toString
    tx.run(
      s"""
         |MATCH (p:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId}
         |}), (a:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : {artifactId}
         |})
         |CREATE (a)-[:${ArtifactProperties.project} {
         |  ${ArtifactProperties.createdAt} : {${ArtifactProperties.createdAt}},
         |  ${ArtifactProperties.updatedAt} : {${ArtifactProperties.updatedAt}}
         |}]->(p)
       """.stripMargin, Map(
        "projectId" -> project.id.toString,
        "artifactId" -> artifact.id.toString,
        ArtifactProperties.createdAt -> now,
        ArtifactProperties.updatedAt -> now
      )
    )
    tx.success()
  }

  def unlinkFromProject(artifact:Artifact, project:Project) = db.withTransaction { tx => {
    /**
      * Create a new version of the project that does not have this artifact
      */
    tx.run(
      s"""
         |MATCH (a:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : {artifactId}
         |})-[o:${ArtifactProperties.project}]->(:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId}
         |})
         |DELETE o
       """.stripMargin, Map(
        "artifactId" -> artifact.id.toString,
        "projectId" -> project.id.toString
      )
    )
    tx.success()
  }}

  def getForProject(project:Project):List[Artifact] = db.withTransaction { tx =>

    /**
      * Add 'Current' to the Project label and the 'artifact' label
      */
    tx.run(
      s"""
         |MATCH (p:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {${ProjectProperties.id}}
         |})<-[:${ArtifactProperties.project}]-(a:${ArtifactProperties.label}:${ArtifactProperties.current})
         |  <-[:${ArtifactProperties.owns}]-(u:${UserProperties.label})
         |WHERE NOT a:${ArtifactProperties.deleted}
         |RETURN a,u
       """.stripMargin, Map(
        ProjectProperties.id -> project.id.toString
      )).map(row => nodeToArtifact(
      row.get("a").asNode(), row.get("u").asNode()
    )).toList
  }

  def listNotInProject(project:Project, page:Int, sortBy:String, rpp:Int):List[Artifact] = db.withTransaction(tx => {
    val skip = page * rpp
    tx.run(
      s"""
         |MATCH (project:${ProjectProperties.label}:${ProjectProperties.current} {
         |  ${ProjectProperties.id} : {projectId}
         |}), (artifact:${ArtifactProperties.label}:${ArtifactProperties.current})
         |  <-[:${ArtifactProperties.owns}]-(user: ${UserProperties.label})
         |WHERE NOT (project)<-[:${ArtifactProperties.project}]-(artifact)
         |RETURN artifact, user
         |SKIP ${skip} LIMIT ${rpp}
       """.stripMargin, Map(
        "projectId" -> project.id.toString
      )
    ).map(row => {
      nodeToArtifact(row.get("artifact").asNode(), row.get("user").asNode())
    }).toList
  })

  /**
    * Return list of versions for an artifact
    *
    * @param id The artifact ID
    * @param page
    * @param rpp
    * @return
    */
  def listVersions(id:UUID, page:Int, rpp:Int):Seq[Version[Artifact]] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (artifact:${ArtifactProperties.label} {
         |  ${ArtifactProperties.id} : {${ArtifactProperties.id}}
         |})<-[:${ArtifactProperties.owns}]-(user:${UserProperties.label})
         |RETURN artifact, user
         |ORDER BY artifact.${ArtifactProperties.updatedAt} DESC
         |SKIP ${page * rpp}
         |LIMIT $rpp
       """.stripMargin, Map(
        ArtifactProperties.id -> id.toString
      )
    ).map(row => {
      val artifactNode = row.get("artifact").asNode()
      val userNode = row.get("user").asNode()
      new Version[Artifact] {
        override val item = nodeToArtifact(artifactNode, userNode)
        override val versionId: UUID = UUID.fromString(artifactNode.get(ArtifactProperties.versionId).asString())
        override val updatedAt: Date = new Date(artifactNode.get(ArtifactProperties.updatedAt).asLong())
        override val action: String = artifactNode.get(ArtifactProperties.versionAction).asString()
      }
    }).toSeq
  })


  def revertTo(artifact:Artifact, idOfVersionToRestore:UUID, revertingUser:User) = db.withTransaction( tx => {
    /**
      * Copy over old node
      * Remove current label from old node
      *
      */
    val now = new Date().getTime
    val newVersionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH (currentVersion:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : {artifactId}
         |})<-[:${ArtifactProperties.owns}]-(owner:${UserProperties.label}),
         |  (versionToRestore:${ArtifactProperties.label} {
         |      ${ArtifactProperties.versionId} : {idOfVersionToRestore}
         |  }),
         |  (revertingUser: ${UserProperties.label} {
         |    ${UserProperties.id} : {revertingUserId}
         |  })
         |REMOVE currentVersion:${ArtifactProperties.current}
         |
         |CREATE (newVersion:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : versionToRestore.${ArtifactProperties.id},
         |  ${ArtifactProperties.title} : versionToRestore.${ArtifactProperties.title},
         |  ${ArtifactProperties.createdAt} : versionToRestore.${ArtifactProperties.createdAt},
         |  ${ArtifactProperties.updatedAt} : {now},
         |  ${ArtifactProperties.versionId} : {versionId},
         |  ${ArtifactProperties.versionAction} : "${VersionActions.reverted}"
         |}),
         |  (newVersion)<-[:${ArtifactProperties.owns}]-(owner),
         |  (newVersion)-[:${ArtifactProperties.previousVersion}]->(currentVersion),
         |  (newVersion)-[:${ArtifactProperties.revertedBy}]->(revertingUser),
         |  (newVersion)-[:${ArtifactProperties.isReversionOf}]->(versionToRestore)
         |
         |WITH currentVersion, versionToRestore, newVersion
         |MATCH
         |  (currentVersion)-[:${ArtifactProperties.project}]->(project:${ProjectProperties.label})
         |CREATE
         |  (newVersion)-[:${ArtifactProperties.project}]->(project)
         |
         |WITH currentVersion, versionToRestore, newVersion
         |MATCH
         |  (versionToRestore)<-[:${ModelProperties.artifact}]-(model:${ModelProperties.label})
         |CREATE
         |  (newVersion)<-[:${ModelProperties.artifact}]-(model)
       """.stripMargin, Map(
        "artifactId" -> artifact.id.toString,
        "idOfVersionToRestore" -> idOfVersionToRestore.toString,
        "revertingUserId" -> revertingUser.uuid.toString,
        "versionId" -> newVersionId.toString,
        "now" -> now.asInstanceOf[Object]
      )
    )
    tx.success()
  })

}


trait ConvertsNodesToArtifacts extends ConvertsNodesToUsers {

  implicit def nodeToArtifact(artifactNode:Node, ownerNode:Node):Artifact = {
    Artifact(
      UUID.fromString(artifactNode.get(ArtifactProperties.id).asString()),
      artifactNode.get(ArtifactProperties.title).asString(),
      nodeToUser(ownerNode)
    )
  }

}
