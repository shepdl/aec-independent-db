package artifacts

import java.io.File
import java.util.{Base64, UUID}
import javax.inject.{Inject, Singleton}

import accessControl.AuthenticationService
import common.JsonResponses
import play.api.libs.json._
import play.api.mvc.{Action, Controller}
import projects.{ProjectService, ProjectUserRights}
import play.api.data.Forms._
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

/**
 * Created by dave on 11/13/15.
 */
trait MediaController extends JsonResponses {

  this: Controller =>

  val projectService:ProjectService
  val artifactRepo:ArtifactRepository
  val mediaRepo:MediaRepository
  val authService:AuthenticationService

//  val imageReads: Reads[Image] = (
//    (JsPath \ "id").read[UUID] and (JsPath \ "title").read[String] and (JsPath \ "filename").read[String]
//      and (JsPath \ "fileType").read[String]
//    )(Image.apply _)
//
//  implicit val imageWrites: Writes[Image] = (
//    (JsPath \ "id").write[UUID] and (JsPath \ "title").write[String] and (JsPath \ "filename").write[String]
//      and (JsPath \ "fileType").write[String]
//    )(unlift(Image.unapply _))

  def toJson(model:Model) = Json obj (
    "id" -> model.id,
    "title" -> model.title,
    "files" -> JsArray(model.files.map(f => JsString(
      s"/models/${model.id}/${f.id}.${f.fileType}".replace("//", "/")
    )))
  )

  def addImage(projectId:UUID, artifactId:UUID) = projectService.PermissionRequiredAction(
    projectId, ProjectUserRights.canAddNewArtifacts
  )(parse.multipartFormData) { request =>
    request.body.file("image").map { image => {
        val item = image.ref

        artifactRepo.getById(artifactId) match {
          case Some(artifact) => {
            val fileExtension = image.filename.split(".").last
            val imageFile = Image(null, image.key, image.filename, fileExtension, image.ref.file)
            // TODO: Actually save image
            val mediaObject = mediaRepo.addImageToArtifact(artifact, imageFile)
            Ok(Json toJson Json.obj(
              "message" -> "Object added"
            ))
          }
          case None => NotFound(s"Project ${artifactId} not found")
        }
      }
      }.getOrElse(Redirect("/404.html").flashing(
      "error" -> "Missing file"
    ))
  }

  def getModel(artifactId:UUID, modelId:UUID) = Action {
    mediaRepo.getById(modelId) match {
      case None => NotFound(Json toJson Map("Error" -> "Artifact not found"))
      case Some(model) => Ok(toJson(model))
    }
  }

  def getModelFile(modelId:UUID, name:String) = Action {
    mediaRepo.getById(modelId) match {
      case None => NotFound(error("Model does not exist"))
      // TODO: this is a crummy way to de-URLencode a string. Need to fix this.
      case Some(model) => mediaRepo.getModelFile(model, name.replace("%20", " ")) match {
        case None => NotFound(error("Model file not found"))
        case Some(modelFile) => Ok.sendFile(modelFile.file)
      }
    }
  }

  def getModelFile(modelId:UUID, modelFileId:UUID, name:String) = Action {
    // TODO: this is a crummy way to de-URLencode a string. Need to fix this.
    mediaRepo.getById(modelId) match {
      case None => NotFound(error("Model does not exist"))
      case Some(model) => mediaRepo.getModelFile(model, name) match {
        case None => NotFound(error("Model file not found"))
        case Some(modelFile) => Ok.sendFile(modelFile.file)
      }
    }
  }

  def addModelFilesForArtifact(activityId:UUID, artifactId:UUID) = projectService.PermissionRequiredAction(
    activityId, ProjectUserRights.canAddNewArtifacts
  )(parse.multipartFormData) { implicit request =>
    artifactRepo.getById(artifactId) match {
      case None => NotFound(error(s"Artifact ${artifactId} not found"))
      case Some(artifact) => {
        val modelFiles = request.body.files.foldLeft(Map[String,File]()) {
          case (acc, modelFile) => acc.updated(modelFile.filename, modelFile.ref.file)
        }
        val title = request.body.dataParts("title").head
        val format = request.body.dataParts("format").head
        val model = mediaRepo.save(Model(null, title, format, modelFiles.map({
          case (filename, file) => {
            val fileType = filename.split("\\.").last
            ModelFile(null, filename, fileType, file)
          }
        }).toList, null))
        mediaRepo.addModelToArtifact(artifact, model)
        Ok(Json.obj(
          "artifactId" -> artifact.id,
          "projectId" -> activityId,
          "model" -> Json.obj(
            "id" -> model.id,
            "title" -> model.title,
            "files" -> model.files.map(file => Json.obj(
              "id" -> file.id,
              "filename" -> file.filename,
              "type" -> file.fileType
            ))
          )
        ))
      }
    }
  }

  def deleteModel(projectId:UUID, artifactId:UUID, modelId:UUID) = projectService.PermissionRequiredAction(
    projectId, ProjectUserRights.canDeleteArtifacts
  ) {request =>
    artifactRepo.getById(artifactId) match {
      case None => NotFound(Json toJson Map("Error" -> "Artifact not found"))
      case Some(artifact) => {
        mediaRepo.getById(modelId) match {
          case None => NotFound(Json toJson Map("Error" -> "Model not found"))
          case Some(model) => {
            mediaRepo.removeModelFromArtifact(artifact, model)
            Ok(Json toJson Map("message" -> "Model deleted"))
          }
        }
      }
    }
  }

  def setModelPreviewImage(projectId: UUID, artifactId: UUID, modelId: UUID) = projectService.PermissionRequiredAction(
    projectId, ProjectUserRights.canAddNewArtifacts
  )(parse.json(10000000)) { implicit request =>
    mediaRepo.getById(modelId) match {
      case None => NotFound(Json toJson Map("Error" -> "Model not found"))
      case Some(model) => {
        (request.body \ "imageData").asOpt[String] match {
          case None => BadRequest(error("Missing image data body"))
          case Some(imageData) => {
            mediaRepo.setPreviewImage(model, imageData)
            Ok(message("Model preview image updated"))
          }
        }
      }
    }
  }

  def getPreview(modelId: UUID) = Action { implicit request =>
    mediaRepo.getById(modelId) match {
      case None => NotFound(Json toJson Map("Error" -> "Model not found"))
      case Some(model) => {
        mediaRepo.getPreviewImage(model) match {
          case None => NotFound(Json toJson Map("Error" -> "Image not found"))
          case Some(imageData) => {
            val decoded = Base64.getDecoder().decode(imageData)
            Ok(decoded)
          }
        }
      }
    }
  }

  def getModelPreviewImage(projectId: UUID, artifactId: UUID, modelId: UUID) = Action { implicit request =>
    mediaRepo.getById(modelId) match {
      case None => NotFound(Json toJson Map("Error" -> "Model not found"))
      case Some(model) => {
        mediaRepo.getPreviewImage(model) match {
          case None => NotFound("Not found")
          case Some(imageData) => Ok(imageData)
        }
      }
    }
  }

  def listVersions(artifactId:UUID, modelId:UUID, start:Int=0, rpp:Int=0) = authService.LoginRequiredAction { implicit request =>
    mediaRepo.getById(modelId) match {
      case None => NotFound(Json.obj("message" -> s"Model ${modelId} not found"))
      case Some(model) => Ok(Json.obj(
        "id" -> modelId,
        "versions" -> mediaRepo.listVersions(model).map { version =>
          Json.obj(
            "version" -> Json.obj(
              "versionId" -> version.versionId.toString,
              "updatedAt" -> version.updatedAt,
              "action" -> version.action
            ),
            "model" -> toJson(version.item)
          )
        })
      )
    }
  }

}

@Singleton
class DefaultMediaController @Inject()(
  override val artifactRepo:ArtifactRepository,
  override val mediaRepo:MediaRepository,
  override val authService: AuthenticationService,
  override val projectService: ProjectService

  )
  extends MediaController with Controller
