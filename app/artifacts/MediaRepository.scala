package artifacts

import java.io.File
import java.nio.file.{Files, Paths, StandardCopyOption}
import java.util.{Date, UUID}
import javax.inject.Inject

import accessControl.{User, UserProperties}
import common._
import org.neo4j.driver.v1.types.Node
import play.api.{Configuration, Logger}

/**
  * Created by dave on 11/13/15.
  */
object ModelProperties extends GraphEntityProperties with VersionedGraphEntityProperties with TimestampsProperties {

  val label = "Model"

  val title = "title"
  val format = "format"
  val imageData = "imageData"

  val artifact = "Artifact"
}

object ImageProperties extends GraphEntityProperties with TimestampsProperties {

  val label = "Image"

  val title = "title"
  val filename = "filename"
  val filetype = "fileType"

  val artifact = "Artifact"
}

object ModelFileProperties extends GraphEntityProperties with TimestampsProperties {

  val label = "ModelFile"

  val filename = "filename"
  val fileType = "fileType"

  val model = "Model"

}

class MediaRepository @Inject() (config:Configuration, db:DatabaseService, artifactRepo:ArtifactRepository) extends ConvertsNodesToMedia {

  val logger = Logger("MediaRepository")

  lazy val mediaFilePath = config.getString("media.resourcePath").getOrElse("/app-data/")
  def stripMediaPathPrefix(nodeId: Long, filePath:String) =
    filePath.replace(s"$mediaFilePath/", "").replace(s"model-$nodeId-", "")

  def mediaDir(model:Model) = s"""$mediaFilePath/${model.id}/"""
  def mediaPath(modelId:UUID, modelFileId:UUID, modelFileType:String):String = s"""$mediaFilePath/${modelId}/${modelFileId}.${modelFileType}"""
  def mediaPath(model:Model, modelFile:ModelFile):String = mediaPath(model.id, modelFile.id, modelFile.fileType)
  def mediaPath(modelId:UUID, modelFile:ModelFile):String = mediaPath(modelId, modelFile.id, modelFile.fileType)

  val path = new File(mediaFilePath)

  if (!path.exists()) {
    Files.createDirectories(path.toPath)
  }

  import scala.collection.JavaConversions._

  def getById(id:UUID):Option[Model] = db.withTransaction { tx => {
    val modelFiles = tx.run(
      s"""MATCH (:${ModelProperties.label} {
         |  ${ModelProperties.id} : {modelId}
         |})<-[:${ModelFileProperties.model}]-(mf:${ModelFileProperties.label})
         |RETURN mf
         |ORDER BY mf.${ModelFileProperties.filename}
       """.stripMargin, Map(
        "modelId" -> id.toString
      )
    ).map(row => nodeToModelFile(row.get("mf").asNode(), mediaPath(id, _, _))
    ).toList
    val models = tx.run(
      s"""
         |MATCH (m:${ModelProperties.label} {
         |  ${ModelProperties.id} : {${ModelProperties.id}}
         |})
         |RETURN m
       """.stripMargin, Map(
        ModelProperties.id -> id.toString
      )
    ).map(row => nodeToModel(row.get("m").asNode(), modelFiles)).toList
    models.headOption
  }}

  def save(model:Model) = db.withTransaction( tx => {
    val now = new Date().getTime.toString
    val versionId = UUID.randomUUID()
    val newModel = if (model.id == null) {
      val modelId = UUID.randomUUID()
      tx.run(
        s"""
           |CREATE (m:${ModelProperties.label}:${ModelProperties.current} {
           |  ${ModelProperties.id} : {${ModelProperties.id}},
           |  ${ModelProperties.versionId} : {${ModelProperties.versionId}},
           |  ${ModelProperties.title} : {${ModelProperties.title}},
           |  ${ModelProperties.format} : {${ModelProperties.format}},
           |  ${ModelProperties.createdAt} : {${ModelProperties.createdAt}},
           |  ${ModelProperties.updatedAt} : {${ModelProperties.updatedAt}}
           |})
           |RETURN m
         """.stripMargin, Map(
          ModelProperties.id -> modelId.toString,
          ModelProperties.versionId -> versionId.toString,
          ModelProperties.title -> model.title,
          ModelProperties.format -> model.format,
          ModelProperties.createdAt -> now,
          ModelProperties.updatedAt -> now
        )
      )
      val modelFiles = model.files.map(modelFile => {
        val modelFileId = UUID.randomUUID()
        tx.run(
          s"""
             |MATCH (m:${ModelProperties.label}:${ModelProperties.current} {
             |  ${ModelProperties.id} : {modelId}
             |})
             |CREATE (m)<-[:${ModelFileProperties.model}]-(:${ModelFileProperties.label} {
             |  ${ModelFileProperties.id} : {${ModelFileProperties.id}},
             |  ${ModelFileProperties.filename} : {${ModelFileProperties.filename}},
             |  ${ModelFileProperties.fileType} : {${ModelFileProperties.fileType}},
             |  ${ModelFileProperties.createdAt} : {${ModelFileProperties.createdAt}},
             |  ${ModelFileProperties.updatedAt} : {${ModelFileProperties.updatedAt}}
             |})
         """.stripMargin, Map(
            "modelId" -> modelId.toString,
            ModelFileProperties.id -> modelFileId.toString,
            ModelFileProperties.filename -> modelFile.filename,
            ModelFileProperties.fileType -> modelFile.fileType,
            ModelFileProperties.createdAt -> now,
            ModelFileProperties.updatedAt -> now
          )
        )
        val newModelFile = ModelFile(
          modelFileId, modelFile.filename, modelFile.fileType, modelFile.file
        )
        ModelFile(modelFileId, modelFile.filename, modelFile.fileType, new File(mediaPath(modelId, newModelFile)))
        newModelFile
      })
      val newModel = Model(
        modelId, model.title, model.format, modelFiles, null
      )
      new File(mediaDir(newModel)).mkdirs()
      modelFiles.foreach(modelFile => {
        Files.copy(
          modelFile.file.toPath,
          Paths.get(mediaPath(newModel, modelFile)),
          StandardCopyOption.REPLACE_EXISTING
        )
      })
      newModel
    } else {
      tx.run(
        s"""
           |MATCH (oldModel:${ModelProperties.label}:${ModelProperties.current} {
           |  ${ModelProperties.id} : {${ModelProperties.id}}
           |})<-[:${ModelFileProperties.model}]-(modelFile:${ModelFileProperties.label})
           |(oldModel)-[artifactLink:${ModelProperties.artifact}]->(artifact:${ArtifactProperties.label})
           |REMOVE oldModel:${ModelProperties.current}
           |REMOVE artifactLink
           |CREATE (newModel:${ModelProperties.label}:${ModelProperties.current} {
           |  ${ModelProperties.id} : {${ModelProperties.id}},
           |  ${ModelProperties.versionId} : {${ModelProperties.versionId}},
           |  ${ModelProperties.title} : {${ModelProperties.title}},
           |  ${ModelProperties.format} : {${ModelProperties.format}},
           |  ${ModelProperties.createdAt} : oldModel.${ModelProperties.createdAt},
           |  ${ModelProperties.updatedAt} : {${ModelProperties.updatedAt}}
           |}),
           |  (newModel)-[:${ModelProperties.previousVersion}]->(oldModel),
           |  (newModel)<-[:${ModelFileProperties.model}]-(modelFile),
           |  (newModel)-[:${ModelProperties.artifact}]->(artifact)
         """.stripMargin, Map(
          ModelProperties.id -> model.id.toString,
          ModelProperties.versionId -> versionId.toString,
          ModelProperties.title -> model.title,
          ModelProperties.format -> model.format,
          ModelProperties.updatedAt -> now
        )
      )
      model
    }
    tx.success()
    newModel
  })

  def addModelToArtifact(artifact:Artifact, model:Model) = db.withTransaction(tx => {
    val now = new Date().getTime.toString
    // Create new version of artifact
    val newArtifact = artifactRepo.save(artifact, tx)
    tx.run(
      s"""
         |MATCH (a:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : {artifactId}
         |}), (m:${ModelProperties.label}:${ModelProperties.current} {
         |  ${ModelProperties.id} : {modelId}
         |})
         |
         |CREATE (a)<-[al:${ModelProperties.artifact} {
         |  ${ModelProperties.createdAt} : {${ModelProperties.createdAt}},
         |  ${ModelProperties.updatedAt} : {${ModelProperties.updatedAt}}
         |}]-(m)
         |RETURN al
       """.stripMargin, Map(
        "artifactId" -> artifact.id.toString,
        "modelId" -> model.id.toString,
        ModelProperties.createdAt -> now,
        ModelProperties.updatedAt -> now
      )
    )
    tx.success()
  })

  def addImageToArtifact(artifact:Artifact, image:Image) = db.withTransaction(tx => {
    val now = new Date().getTime.toString
    val id = UUID.randomUUID()
    tx.run(
      s"""
         |CREATE (:${ArtifactProperties.label} {
         |  ${ArtifactProperties.id} : {artifactId}
         |})<-[a:${ImageProperties.artifact} {
         |  ${ImageProperties.createdAt} : {${ImageProperties.createdAt}},
         |  ${ImageProperties.updatedAt} : {${ImageProperties.updatedAt}}
         |}]-(:${ImageProperties.label} {
         |  ${ImageProperties.id} : {imageId}
         |})
         |RETURN a
       """.stripMargin, Map(
        "artifactId" -> artifact.id.toString,
        "imageId" -> image.id.toString,
        ImageProperties.createdAt -> now,
        ImageProperties.updatedAt -> now
      )
    )
    tx.success()
  })

  def removeModelFromArtifact(artifact:Artifact, model:Model) = db.withTransaction( tx => {
    model.files.foreach(modelFile => modelFile.file.delete())
    // Save old version of artifact
    val newArtifact = artifactRepo.save(artifact)
    tx.run(
      s"""
          |MATCH (:${ArtifactProperties.label}:${ArtifactProperties.current} {
          |  ${ArtifactProperties.id} : {artifactId}
          |})<-[ma:${ModelProperties.artifact}]-(:${ModelProperties.label} {
          |  ${ModelProperties.id} : {modelId}
          |})
          |DELETE ma
       """.stripMargin, Map(
        "artifactId" -> artifact.id.toString,
        "modelId" -> model.id.toString
      )
    )
    // TODO: Delete files
  })

  def deleteModel(model:Model, deletingUser:User) = db.withTransaction( tx => {
    val newVersionId = UUID.randomUUID()
    /**
      * Create new "deleted" version of model
      * Unlink from artifacts
      */
    tx.run(
      s"""
         |MATCH (oldModel:${ModelProperties.label}:${ModelProperties.current} {
         |  ${ModelProperties.id} : {modelId}
         |}),  (deletingUser:${UserProperties.label} {
         |    ${UserProperties.id} : {userId}
         |  })
         |REMOVE oldModel:${ModelProperties.current}
         |CREATE (newModel:${ModelProperties.label}:${ModelProperties.deleted} {
         |  ${ModelProperties.id} : {modelId},
         |  ${ModelProperties.versionId} : {versionId}
         |})-[:${ModelProperties.deletedBy}]->(deletingUser)
       """.stripMargin, Map(
      "modelId" -> model.id.toString,
      "userId" -> deletingUser.uuid.toString,
      "versionId" -> newVersionId.toString
    ))
  })

  def getImagesForArtifact(artifact:Artifact): List[Image] = db.withTransaction({ tx =>
    tx.run(
      s"""
         |MATCH (:${ArtifactProperties.label} {
         |  ${ArtifactProperties.id} : {${ArtifactProperties.id}}
         |})<-[:${ImageProperties.artifact}]-(image:${ImageProperties.label})
         |RETURN image
       """.stripMargin, Map(
        ArtifactProperties.id -> artifact.id.toString
      )).map(
      row => nodeToImage(row.get("image").asNode())
    ).toList
  })

  def getModelsForArtifact(artifact: Artifact): List[Model] = db.withTransaction( tx => {
    tx.run(
      s"""
         |MATCH (:${ArtifactProperties.label}:${ArtifactProperties.current} {
         |  ${ArtifactProperties.id} : {${ArtifactProperties.id}}
         |})<-[:${ModelProperties.artifact}]-(model:${ModelProperties.label})
         |  <-[:${ModelFileProperties.model}]-(modelFile:${ModelFileProperties.label})
         |RETURN model, modelFile
       """.stripMargin, Map(
        ArtifactProperties.id -> artifact.id.toString
      )
    ).foldLeft(Map[UUID,(Model,List[ModelFile])]()) {
      case (acc, row) => {
        val modelNode = row.get("model").asNode()
        val modelFileNode = row.get("modelFile").asNode()
        val modelUUID = UUID.fromString(modelNode.get(ModelProperties.id).asString())
        val modelFile = nodeToModelFile(modelFileNode, mediaPath(modelUUID, _, _))
        if (acc.contains(modelUUID)) {
          val (model, modelFiles) = acc(modelUUID)
          acc.updated(modelUUID, (model, modelFile :: modelFiles))
        } else {
          acc.updated(modelUUID, (nodeToModel(modelNode, List()), List(modelFile)))
        }
      }
    }.map({
      case (_, (model, modelFiles)) => Model(model.id, model.title, model.format, modelFiles, model.previewImageData)
    }).toList
  })

  def getModelFile(model:Model, modelFileId: UUID): Option[ModelFile] = db.withTransaction( tx => {
    val modelFiles = tx.run(
      s"""
         |MATCH (modelFile:${ModelFileProperties.label} {
         |  ${ModelFileProperties.id} : {${ModelFileProperties.id}}
         |})
         |RETURN modelFile
       """.stripMargin, Map(
        ModelFileProperties.id -> modelFileId.toString
      )
    ).map(row => nodeToModelFile(row.get("modelFile").asNode(), mediaPath(model.id, _, _)))
    if (modelFiles.isEmpty) {
      logger.error(s"Model file not found: ${model.id}/${modelFileId}")
      None
    } else {
      val modelFile = modelFiles.next()
      if (modelFile.file.exists()) {
        Some(modelFile)
      } else {
        logger.error(s"File ${model.id}/${modelFileId} missing or not found")
        None
      }
    }
  })

  def getModelFile(model:Model, name:String):Option[ModelFile] = db.withTransaction(tx => {
    val possibleModelFile = tx.run(
      s"""
         |MATCH (model:${ModelProperties.label}:${ModelProperties.current} {
         |  ${ModelProperties.id} : {${ModelProperties.id}}
         |})<-[:${ModelFileProperties.model}]-(modelFile:${ModelFileProperties.label} {
         |  ${ModelFileProperties.filename} : {${ModelFileProperties.filename}}
         |})
         |RETURN modelFile
       """.stripMargin, Map(
        ModelProperties.id -> model.id.toString,
        ModelFileProperties.filename -> name
      )
    )

    if (possibleModelFile.hasNext) {
      Some(nodeToModelFile(possibleModelFile.next().get("modelFile").asNode(), mediaPath(model.id, _, _)))
    } else {
      try {
        getModelFile(model, UUID.fromString(name.split("\\.").head))
      } catch {
        case _:Throwable => None
      }
    }
  })

  def setPreviewImage(model: Model, imageData: String) = db.withTransaction(tx => {
    val now = new Date().getTime.toString
    tx.run(
      s"""
         |MATCH (model:${ModelProperties.label} {
         |  ${ModelProperties.id} : {modelId}
         |})
         |WITH model
         |  SET model.${ModelProperties.imageData} = {imageData},
         |      model.${ModelProperties.updatedAt} = {now}
       """.stripMargin, Map(
        "modelId" -> model.id.toString,
        "imageData" -> imageData,
        "now" -> now
      )
    )
    tx.success()
  })

  def getPreviewImage(model:Model):Option[String] = db.withTransaction(tx => {
    val results = tx.run(
      s"""
         |MATCH (model:${ModelProperties.label} {
         |  ${ModelProperties.id} : {${ModelProperties.id}}
         |})
         |RETURN model.${ModelProperties.imageData} AS imageData
       """.stripMargin, Map(
        ModelProperties.id -> model.id.toString
      )).map(row => row.get("imageData").asString())
    if (results.hasNext) {
      Some(results.next())
    } else {
      None
    }
  })

  def listVersions(model:Model, page:Int=0, rpp:Int=25):Iterable[Version[Model]] = db.withTransaction(tx => {
    tx.run(
      s"""
         |MATCH (model:${ModelProperties.label} {
         |  ${ModelProperties.id} : {modelId}
         |})<-[:${ModelFileProperties.model}]-(modelFile:${ModelFileProperties.label})
         |ORDER BY model.${ModelProperties.updatedAt}
         |LIMIT $rpp
         |OFFSET ${page * rpp}
         |RETURN model.${ModelProperties.id} AS modelId, model, modelFile
       """.stripMargin, Map(
        "modelId" -> model.id
      )
    ).foldLeft(Map[UUID,(Node,List[Node])]()) {
      case (modelList, row) => {
        val modelId = UUID.fromString(row.get("modelId").asString())
        val modelNode = row.get("model").asNode()
        val modelFileNode = row.get("modelFile").asNode()
        if (modelList.contains(modelId)) {
          modelList.updated(modelId, (modelNode,
                modelFileNode :: modelList(modelId)._2
            )
          )
        } else {
          modelList.updated(modelId, (modelNode, List(modelFileNode)))
        }
      }
    }.map({
      case (key, (modelNode, modelFileNodes)) => {
        new Version[Model] {
          override val item = nodeToModel(modelNode, modelFileNodes.map(modelFileNode => nodeToModelFile(modelFileNode, mediaPath(key, _, _))))
          override val versionId = UUID.fromString(modelNode.get(ModelProperties.versionId).asString())
          override val updatedAt: Date = new Date(modelNode.get(ModelProperties.updatedAt).asLong())
          override val action: String = "action"
        }
      }
    })
  })

  /**
    * Revert to a previous version of the model
    *
    * PLEASE NOTE: this will link the reverted version of the model to the
    *   artifact to which the current version is linked! We presume this is the most
    *   logical course of action.
    * @param model
    * @param versionId
    * @param revertingUser
    * @return
    */
  def revertToVersion(model:Model, versionId:UUID, revertingUser:User) = db.withTransaction(tx => {
    val now = new Date().getTime
    val newVersionId = UUID.randomUUID()
    tx.run(
      s"""
         |MATCH
         |  (currentVersion:${ModelProperties.label}:${ModelProperties.current} {
         |    ${ModelProperties.id}: {modelId}
         |  })-[:${ModelProperties.artifact}]->(artifact),
         |  (versionToRestore:${ModelProperties.label} {
         |    ${ModelProperties.id} : {modelId},
         |    ${ModelProperties.versionId} : {oldVersionId}
         |  }),
         |  (revertingUser:${UserProperties.label} {
         |    ${UserProperties.id} : {userId}
         |  })
         |REMOVE
         |  currentVersion.${ModelProperties.current}
         |CREATE
         |  (newVersion:${ModelProperties.label}:${ModelProperties.current} {
         |    ${ModelProperties.id} : {modelId},
         |    ${ModelProperties.versionId} : {newVersionId},
         |    ${ModelProperties.format} : versionToRestore.${ModelProperties.format},
         |    ${ModelProperties.title} : versionToRestore.${ModelProperties.title},
         |    ${ModelProperties.createdAt} : versionToRestore.${ModelProperties.createdAt},
         |    ${ModelProperties.updatedAt} : {now}
         |  })
         |  (newVersion)-[:${ModelProperties.revertedBy}]->(revertingUser),
         |  (newVersion)-[:${ModelProperties.artifact}]->(artifact),
         |  (newVersion)-[:${ModelProperties.isReversionOf}]->(versionToRestore),
         |  (newVersion)-[:${ModelProperties.previousVersion}]->(currentVersion),
         |
         |WITH currentVersion, versionToRestore, newVersion
         |MATCH
         |  (oldVersion)<-[:${ModelFileProperties.model}]-(modelFile:${ModelFileProperties.label}),
         |CREATE
         |  (newVersion)<-[:${ModelFileProperties.model}]-(modelFile)
       """.stripMargin, Map(
        "modelId" -> model.id.toString,
        "versionId" -> versionId.toString,
        "newVersionId" -> newVersionId.toString,
        "userId" -> revertingUser.uuid.toString,
        "now" -> now.asInstanceOf[Object]
      )
    )
  })
}


trait ConvertsNodesToMedia {

  def nodeToImage(node:Node):Image = Image(
    UUID.fromString(node.get(ImageProperties.id).asString()),
    node.get(ImageProperties.title).asString(),
    node.get(ImageProperties.filename).asString(),
    node.get(ImageProperties.filetype).asString(),
    new File(
      node.get(ImageProperties.filename).asString()
    )
  )

  def nodeToModel(node:Node, filenames:List[ModelFile]):Model = Model(
    UUID.fromString(node.get(ModelProperties.id).asString()),
    node.get(ModelProperties.title).asString(),
    node.get(ModelProperties.format).asString(),
    filenames,
    {
      val imageData = node.get(ModelProperties.imageData)
      if (imageData == null) {
        ""
      } else {
        imageData.asString()
      }
    }
  )

  def nodeToModelFile(node:Node, filenameBuilder:(UUID, String) => String):ModelFile = {
    val modelFileId = UUID.fromString(node.get(ModelFileProperties.id).asString())
    val fileType = node.get(ModelFileProperties.fileType).asString()
    ModelFile(modelFileId, node.get(ModelFileProperties.filename).asString(),
      fileType, new File(filenameBuilder(modelFileId, fileType))
    )
  }

}
