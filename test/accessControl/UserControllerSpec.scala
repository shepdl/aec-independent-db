package accessControl

import common.{DatabaseService, TestIHVApplication}
import play.api.Application
import play.api.i18n.MessagesApi
import play.api.mvc.Controller
import projects.ProjectRepository

/**
  * Created by dave on 1/27/17.
  */
trait BasicUser {

  val userRepo:UserRepository

  val name = "Lara Croft"
  val email = "lcroft@ox.ac.uk"
  val password = "tombraider#1"

  val user = userRepo.save(new User(name, email))

  userRepo.setPasswordForUser(user, password)

}

trait HasUserRepository {

  val app:Application

  val userRepo = app.injector.instanceOf[UserRepository]

}

trait UserControllerSpec extends TestIHVApplication with HasUserRepository with BasicUser with ExtraUsers {

  val controller = new UserController with Controller {
    override val authentication = app.injector.instanceOf[AuthenticationService]
    override val messagesApi = app.injector.instanceOf[MessagesApi]
    override val userRepo = app.injector.instanceOf[UserRepository]
    override val projectRepo = app.injector.instanceOf[ProjectRepository]
  }

}


