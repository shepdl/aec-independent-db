package accessControl

import java.util.UUID

import common.{DatabaseService, HasDatabase, TestIHVApplication}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class UserRepoCRUDSpec extends Specification {

  "A User Repository" should {

    "create a new user when the user is new and set the user's password" in new UserRepoTestCase {
      user.uuid must not beNull

      userRepo.setPasswordForUser(user, password)

      val foundUser = userRepo.getById(user.uuid).get

      foundUser.name mustEqual name
      foundUser.email mustEqual email
    }

    "update a user when the user is not new" in new UserRepoTestCase {
      val newName = "Lara L. Croft"
      val newEmail = "lcroft@cambridge.ac.uk"
      val updatedUser = User(
        user.uuid, newName, newEmail
      )

      userRepo.save(updatedUser)
      userRepo.setPasswordForUser(updatedUser, "idon'tlikechangingpasswords")

      val foundUser = userRepo.getById(user.uuid).get

      foundUser.name mustEqual newName
      foundUser.email mustEqual newEmail
    }

    "return a user when looking up a user by email and password" in new UserRepoTestCase {
      userRepo.setPasswordForUser(user, password)

      val userResult = userRepo.getByEmailAndPassword(email, password)
      userResult must beSome
      val foundUser = userResult.get

      foundUser.uuid mustEqual user.uuid
      foundUser.name mustEqual user.name
    }

    "return a 'Not Found' message when the wrong email is given" in new UserRepoTestCase {
      userRepo.setPasswordForUser(user, password)

      val userResult = userRepo.getByEmailAndPassword("thewrongemail", password)
      userResult must beNone
    }

    "return a 'Not Found' message when the wrong password is given" in new UserRepoTestCase {
      userRepo.setPasswordForUser(user, password)

      val userResult = userRepo.getByEmailAndPassword(email, "thewrongpassword")
      userResult must beNone
    }

    "return a 'Not Found' message when the user's ID does not exist" in new UserRepoTestCase {
      val foundUser = userRepo.getById(UUID.randomUUID())
      foundUser must beNone
    }

    "delete a user when requested" in new UserRepoTestCase {
      userRepo.delete(user)

      val missingUser = userRepo.getById(user.uuid)
      missingUser must beNone
    }

  }

}

trait UserRepoTestCase extends TestIHVApplication with HasUserRepository with BasicUser with ExtraUsers


