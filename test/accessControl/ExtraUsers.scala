package accessControl

/**
  * Created by dave on 1/27/17.
  */
trait ExtraUsers {
  val userRepo:UserRepository

  val jones = userRepo.save(new User(
    "Indiana Jones", "ijones@iu.edu"
  ))
  val langdon = userRepo.save(new User(
    "Robert Langdon", "rlangdon@harvard.edu"
  ))
}
