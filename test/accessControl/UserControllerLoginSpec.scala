package accessControl

import common.{DatabaseService, HasDatabase}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.i18n.MessagesApi
import play.api.mvc.{Controller, Results}
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class UserControllerLoginSpec extends PlaySpecification with Results with JsonMatchers {

  "A User Controller" should {
    "log the user in when supplied with the correct username and password" in new UserControllerSpec  {

      val data = Map(
        "email" -> Seq(email),
        "password" -> Seq(password)
      )

      val loginRequest = FakeRequest(POST, s"/users/login", FakeHeaders(), data)
      val loginResponse = controller.login()(loginRequest)

      status(loginResponse) mustEqual OK
      session(loginResponse).data.keys must contain("userId")
      val cookie = cookies(loginResponse).apply("PLAY_SESSION")

      val whoAmIRequest = FakeRequest(GET, s"/users/whoAmI").withCookies(cookie)
      val whoAmIResponse = controller.whoAmI()(whoAmIRequest)

      status(whoAmIResponse) mustEqual OK
      val whoAmIResponseJson = contentAsString(whoAmIResponse)

      whoAmIResponseJson must /("user")/("id" -> user.uuid.toString)
      whoAmIResponseJson must /("user")/("name" -> user.name)
      whoAmIResponseJson must /("user")/("email" -> user.email)
    }

    "fail to log the user in when the username is incorrect" in new UserControllerSpec {
      val data = Map(
        "email" -> Seq(email + "extra"),
        "password" -> Seq(password)
      )

      val loginRequest = FakeRequest(POST, s"/users/login", FakeHeaders(), data)
      val loginResponse = controller.login()(loginRequest)

      status(loginResponse) mustEqual NOT_FOUND
      session(loginResponse).data.keys must not contain("userId")

      val whoAmIRequest = FakeRequest(GET, s"/users/whoAmI")
      val whoAmIResponse = controller.whoAmI()(whoAmIRequest)

      status(whoAmIResponse) mustEqual FORBIDDEN
    }

    "fail to log the user in when the password is incorrect" in new UserControllerSpec {
      val data = Map(
        "email" -> Seq(email),
        "password" -> Seq(password + "extra")
      )

      val loginRequest = FakeRequest(POST, s"/users/login", FakeHeaders(), data)
      val loginResponse = controller.login()(loginRequest)

      status(loginResponse) mustEqual NOT_FOUND
      session(loginResponse).data.keys must not contain("userId")

      val whoAmIRequest = FakeRequest(GET, s"/users/whoAmI")
      val whoAmIResponse = controller.whoAmI()(whoAmIRequest)

      status(whoAmIResponse) mustEqual FORBIDDEN
    }

  }

}



