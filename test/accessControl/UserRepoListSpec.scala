package accessControl

import common.TestIHVApplication
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._


@RunWith(classOf[JUnitRunner])
class UserRepoListSpec extends Specification {

  "A User Repository" should {
    "sort all fields by 'created at' by default" in new UserRepoListTestCase {
      val result = userRepo.list(0, "blah", 5)
      result must haveSize(5)
      result(0) mustEqual userRepo.ADMIN_USER
      result(1) mustEqual users(0)
      result(2) mustEqual users(1)
      result(3) mustEqual users(2)
      result(4) mustEqual users(3)
    }
    "sort all fields by name alphabetically when requested" in new UserRepoListTestCase {
      val result = userRepo.list(0, UserProperties.name, 5)
      result must haveSize(5)
      result(0) mustEqual userRepo.ADMIN_USER
      result(1) mustEqual users(4)
      result(2) mustEqual users(1)
      result(3) mustEqual users(0)
      result(4) mustEqual users(2)
    }
    "return 3 users when page is 1 and rpp is 3" in new UserRepoListTestCase {
      val result = userRepo.list(1, "createdAt", 3)
      result must haveSize(3)
      result(0) mustEqual users(2)
      result(1) mustEqual users(3)
    }
    "return 3 users when page is 0 and rpp is 3" in new UserRepoListTestCase {
      val result = userRepo.list(0, "createdAt", 3)
      result must haveSize(3)
      result(0) mustEqual userRepo.ADMIN_USER
      result(1) mustEqual users(0)
      result(2) mustEqual users(1)
    }
  }

}


trait UserRepoListTestCase extends TestIHVApplication with HasUserRepository {
  val userRepo:UserRepository

  val users = {
    List(
      new User("Lara Croft", "lcroft@ox.ac.uk"),
      new User("Indiana Jones", "jones@iu.edu"),
      new User("Robert Langdon", "rlangdon@harvard.edu"),
      new User("Steve Smith", "smith@nowhere.edu"),
      new User("Bob Anderson", "jones@nowhere.edu")
    ).map(userRepo.save)
  }

}
