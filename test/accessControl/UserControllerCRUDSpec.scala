package accessControl

import java.util.UUID

import common.{HasDatabase, TestIHVApplication}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.i18n.MessagesApi
import play.api.libs.json.Json
import play.api.mvc.{Controller, Results}
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class UserControllerCRUDSpec extends PlaySpecification with Results with JsonMatchers {

  "A User Controller" should {

    "create a user when supplied with a name, email address, and password" in new UserControllerCRUDTest {
      val data = Map(
        "name" -> name,
        "email" -> email,
        "password" -> password
      ).map({
        case (k, v) => k -> Seq(v)
      })
      val createRequest = FakeRequest(POST, s"/join", FakeHeaders(), data)
      val createResponse = controller.join()(createRequest)

      status(createResponse) mustEqual CREATED
      val createResponseJson = contentAsString(createResponse)

      val userId = (Json.parse(createResponseJson) \ "id").as[UUID]
      createResponseJson must /("name" -> name)
      createResponseJson must /("email" -> email)

      val getRequest = FakeRequest(GET, s"/users/${userId}")
      val getResponse = controller.getById(userId)(getRequest)

      status(getResponse) mustEqual OK

      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("id" -> userId.toString)
      getResponseJson must /("name" -> name)
      getResponseJson must /("email" -> email)
    }

    val name = "Lara Croft"
    val email = "lcroft@ox.ac.uk"
    val password = "#1tombraider"

    "fail to create a user when the user's email address is missing" in new UserControllerCRUDTest {
      val data = Map(
        "name" -> name,
        "password" -> password
      ).map({
        case (k, v) => k -> Seq(v)
      })
      val createRequest = FakeRequest(POST, s"/join", FakeHeaders(), data)
      val createResponse = controller.join()(createRequest)
      status(createResponse) mustEqual BAD_REQUEST
    }

    "fail to create a user when the user's name is missing" in new UserControllerCRUDTest {
      val data = Map(
        "email" -> email,
        "password" -> password
      ).map({
        case (k, v) => k -> Seq(v)
      })
      val createRequest = FakeRequest(POST, s"/join", FakeHeaders(), data)
      val createResponse = controller.join()(createRequest)

      status(createResponse) mustEqual BAD_REQUEST
    }

    "fail to create a user when the user's password is missing" in new UserControllerCRUDTest {
      val data = Map(
        "name" -> name,
        "email" -> email
      ).map({
        case (k, v) => k -> Seq(v)
      })
      val createRequest = FakeRequest(POST, s"/join", FakeHeaders(), data)
      val createResponse = controller.join()(createRequest)

      status(createResponse) mustEqual BAD_REQUEST
    }

    "update the user when the user is logged in" in new UserControllerCRUDTest {
      val userId = user.uuid
      val newName = "Lara L. Croft"
      val newEmail = "lcroft@cambridge.ac.uk"
      val payload = Json.parse(
        s"""
           |{
           |  "id" : "${userId}",
           |  "name" : "${newName}",
           |  "email" : "${newEmail}"
           |}
         """.stripMargin)
      val updateRequest = FakeRequest(PUT, s"/users/${userId}").withBody(payload).withSession(
        "userId" -> userId.toString
      )
      val updateResponse = controller.update(userId)(updateRequest)

      status(updateResponse) mustEqual OK
      val updateResponseJson = contentAsString(updateResponse)

      updateResponseJson must /("name" -> newName)
      updateResponseJson must /("email" -> newEmail)
    }

    "fail to update the user when the user is not logged in" in new UserControllerCRUDTest {
      val userId = user.uuid
      val newName = "Lara L. Croft"
      val newEmail = "lcroft@cambridge.ac.uk"
      val payload = Json.parse(
        s"""
           |{
           |  "id" : "${userId}",
           |  "name" : "${newName}",
           |  "email" : "${email}"
           |}
         """.stripMargin
      )
      val updateRequest = FakeRequest(PUT, s"/users/${userId}").withBody(payload)
      val updateResponse = controller.update(userId)(updateRequest)

      status(updateResponse) mustEqual FORBIDDEN
    }

    "get a user when supplied with the correct ID" in new UserControllerCRUDTest {
      val getRequest = FakeRequest(GET, s"/users/${user.uuid}")
      val getResponse = controller.getById(user.uuid)(getRequest)

      status(getResponse) mustEqual OK
      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("name" -> name)
      getResponseJson must /("email" -> email)
    }

    "fail to get a user when supplied with an invalid ID" in new UserControllerCRUDTest {
      val fakeId = UUID.randomUUID()
      val getRequest = FakeRequest(GET, s"/users/${fakeId}")
      val getResponse = controller.getById(fakeId)(getRequest)

      status(getResponse) mustEqual NOT_FOUND
    }

    "delete a user when the user is an administrator" in new UserControllerCRUDTest {
      val userId = user.uuid
      val deleteRequest = FakeRequest(DELETE, s"/users/${userId}").withSession(
        "userId" -> user.uuid.toString
      )
      val deleteResponse = controller.delete(userId)(deleteRequest)

      status(deleteResponse) mustEqual OK

      val getRequest = FakeRequest(GET, s"/users/${userId}").withSession(
        "userId" -> user.uuid.toString
      )
      val getResponse = controller.getById(userId)(getRequest)

      status(getResponse) mustEqual NOT_FOUND
    }

    "return a 404 when trying to delete a user that does not exist" in new UserControllerCRUDTest {
      val userId = UUID.randomUUID()
      val deleteRequest = FakeRequest(DELETE, s"/users/${userId}").withSession(
        "userId" -> user.uuid.toString
      )
      val deleteResponse = controller.delete(userId)(deleteRequest)

      status(deleteResponse) mustEqual NOT_FOUND
    }

  }

}


trait UserControllerCRUDTest extends TestIHVApplication with HasUserRepository with BasicUser with UserControllerSpec
