package annotations

import java.io.File

import accessControl.{BasicUser, HasUserRepository}
import artifacts.{HasArtifactRepo, HasMediaRepo, Model, ModelFile}
import common.TestIHVApplication
import play.api.Application
import projects.HasProjectRepo
import schemas.HasSchemaRepo

/**
  * Created by dave on 1/30/17.
  */
trait HasAnnotationRepository {
  val app:Application
  val annotationRepo = app.injector.instanceOf[AnnotationRepository]
}


trait ModelTest extends HasArtifactRepo with HasMediaRepo {

  val model = mediaRepo.save(
    Model(
      null, "Mock model", "dae", List(
        ModelFile(null, "test.dae", "dae", new File("test.dae")),
        ModelFile(null, "test.jpeg", "jpeg", new File("test.jpeg"))
      ),
      ""
    )
  )

  val project = createMockProject
  val artifact = createMockArtifact

  artifactRepo.linkToProject(artifact, project)
  mediaRepo.addModelToArtifact(artifact, model)

}
trait AnnotationRepositoryTest extends TestIHVApplication with HasUserRepository with BasicUser
  with HasProjectRepo with HasSchemaRepo with HasEntityRepo with EntityTestCaseSchemas
  with HasAnnotationRepository
