package annotations

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class AnnotationRepositoryUpdateSpec extends Specification {

  trait AnnotationRepositoryUpdateTest extends AnnotationRepositoryTest with ModelTest {
    val eras = List(4,5,6,7).map(i => entityRepo.save(
      Entity(null, eraSchema, Map("name" -> s"Era ${i}"))
    ))
    val geometryPoints = List((0.0, 0.0, 0.0), (1.0, 1.0, 1.0), (2.0, 2.0, 2.0), (0.0, 0.0, 0.0))
    val data = Map( "era" -> eras(0).id.toString )

    val original = annotationRepo.save(
      Annotation(
        null,
        project, model,
        Entity(null, builtDuringSchema, data),
        geometryPoints,
        user,
        ""
      )
    )
  }

  "When updating, an Annotation Repository" should {

    "update an annotation by changing some of the fields of the entity" in new AnnotationRepositoryUpdateTest {

      val updated = annotationRepo.save(
        Annotation(
          original.id,
          original.project,
          original.model,
          Entity(
            original.entity.id,
            original.entity.schema,
            Map(
              "era" -> eras(1).id.toString
            )
          ),
          original.location,
          original.creator,
          original.imageData
        )
      )

      updated.id mustEqual original.id
      updated.project mustEqual project
      updated.model mustEqual model
      updated.location mustEqual geometryPoints
      updated.entity.id mustEqual original.entity.id
      updated.entity.schema mustEqual original.entity.schema
      updated.entity.data("era") mustEqual eras(1).id.toString

    }

    "update an annotation by changing the location" in new AnnotationRepositoryUpdateTest {

      val newGeometry = List((1.0, 2.0, 3.0), (4.0, 5.0, 6.0), (1.0, 2.0, 3.0))

      val updated = annotationRepo.save(
        Annotation(
          original.id,
          original.project,
          original.model,
          original.entity,
          newGeometry,
          original.creator,
          original.imageData
        )
      )

      updated.id mustEqual original.id
      updated.project mustEqual project
      updated.model mustEqual model
      updated.location mustEqual newGeometry
      updated.entity.id mustEqual original.entity.id
      updated.entity.schema mustEqual original.entity.schema
      updated.entity.data("era") mustEqual eras(0).id.toString
    }

    /*
    "reject an update to an annotation if the entity is empty" in new AnnotationRepoSpec {
      val eras = List(4,5,6,7).map(i => entityRepo.create(eraSchema, Map("name" -> s"Era ${i}")).left.get)
      val geometryPoints = List((0.0, 0.0, 0.0), (1.0, 1.0, 1.0), (2.0, 2.0, 2.0), (0.0, 0.0, 0.0))
      val data = Map( "era" -> eras(0).id.toString )

      val original = annotationRepo.create(project.id, model.id, builtDuringSchema.id, geometryPoints, data).left.get

      annotationRepo.update(model, builtDuringSchema,
        new Annotation(original.id, project, model,
          new Entity(original.entity.id, builtDuringSchema, Map()),
          geometryPoints
        )
      ) match {
        case Left(annotation) => failure("For some reason, an empty set of data caused an annotation to be created")
        case Right(errors) => success
      }

    }
    */

//    "reject an update if a string property is empty"
//    "reject an update if a numeric property cannot be cast to a number"

  }

}
