package annotations

import java.io.File

import accessControl.{BasicUser, HasUserRepository}
import artifacts._
import common._
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.Application
import play.api.libs.json.JsString
import play.api.test._
import play.api.test.Helpers._
import projects.{HasProjectRepo, ProjectRepository}
import schemas.{HasSchemaRepo, Pass, SchemaRepository}


@RunWith(classOf[JUnitRunner])
class AnnotationRepositoryCreateSpec extends Specification {

  trait AnnotationRepositoryCreateTest extends AnnotationRepositoryTest with ModelTest

  "When creating, an Annotation Repository" should {

    "create and retrieve an annotation tied to a specific location if that annotation matches the schema" in new AnnotationRepositoryCreateTest {

      val geometry = List((0.0, 0.0, 0.0), (1.0, 1.0, 1.0), (2.0, 2.0, 2.0), (0.0, 0.0, 0.0))

      val data = Map(
        "name" -> "Horus",
        "origin" -> "Thebes"
      )

      val annotationCreated = annotationRepo.save(
        Annotation(
          null, project, model, Entity(
            null, figureSchema, data
          ), geometry, user, ""
        )
      )

      annotationCreated.project mustEqual project
      annotationCreated.model.id mustEqual model.id
      annotationCreated.location mustEqual geometry
      annotationCreated.entity.schema.id mustEqual figureSchema.id
      annotationCreated.entity.data("name") mustEqual data("name")
      annotationCreated.entity.data("origin") mustEqual data("origin")

      val annotationRetrievedOption = annotationRepo.getById(annotationCreated.id)

      annotationRetrievedOption mustNotEqual None

      val annotationRetrieved = annotationRetrievedOption.get
      annotationRetrieved.project mustEqual project
      annotationRetrieved.model.id mustEqual model.id
      annotationRetrieved.location mustEqual geometry
      annotationRetrieved.entity.schema.id mustEqual figureSchema.id
      annotationRetrieved.entity.data("name") mustEqual data("name")
      annotationRetrieved.entity.data("origin") mustEqual data("origin")
    }

    "create and retrieve an annotation tied to a specific location if the annotation matches the schema and it is a reference type" in new AnnotationRepositoryCreateTest {

      val eras = List(4,5,6,7).map(i => entityRepo.save(Entity(
        null, eraSchema, Map("name" -> s"Era ${i}")
      )))

      val geometryPoints = List((0.0, 0.0, 0.0), (1.0, 1.0, 1.0), (2.0, 2.0, 2.0), (0.0, 0.0, 0.0))

      val data = Map(
        "era" -> eras(0).id.toString
      )

      builtDuringSchema.validate(data) match {
        case Pass => success
        case _ => failure
      }

      val annotation = annotationRepo.save(Annotation(
        null, project, model,
        Entity(
          null,
          builtDuringSchema,
          data
        ),
        geometryPoints,
        user,
        ""
      ))

      annotation.project mustEqual project
      annotation.model mustEqual model
      annotation.location mustEqual geometryPoints
      annotation.entity.schema.id mustEqual builtDuringSchema.id
      annotation.entity.data("era") mustEqual eras(0).id.toString

    }

//    "create and retrieve a whole model annotation if that annotation matches the schema"

//    "reject an annotation with a 400 if the entity does not match the schema"

//    "reject an annotation with a controlled vocabulary item that does not exist"
//    "create and retrieve an annotation with a controlled vocabulary"

  }

}



