package annotations

import org.junit.runner.RunWith
import org.specs2.mutable._
import org.specs2.runner._

import play.api.libs.json._
import play.api.test._
import play.api.test.Helpers._


/**
  * Created by daveshepard on 3/19/16.
  */
@RunWith(classOf[JUnitRunner])
class AnnotationControllerDeleteSpec extends Specification {

  trait AnnotationControllerDeleteTest extends AnnotationControllerTest with Horus {
    val annotation = annotationRepo.save(Annotation(
      null, project, model,
      Entity(null, instanceOfDFSchema, Map(
        "figure" -> horus.id.toString,
        "figure color" -> "gold"
      )),
      List(
        (0.0, 4.0, 5.0),
        (1.0, 2.0, 3.0),
        (0.0, 4.0, 5.0)
      ),
      user,
      ""
    ))
  }

  "When deleting, an Annotation Controller" should {

    "delete an annotation and not retrieve the object afterwards" in new AnnotationControllerDeleteTest {

      val deleteRequest = FakeRequest(DELETE, individualInstanceURL(annotation.id))
        .withSession("userId" -> user.uuid.toString)

      val deleteResponse = controller.delete(model.id, instanceOfFigureSchema.title, instanceOfFigureSchema.id, annotation.id)(deleteRequest)

      status(deleteResponse) mustEqual OK

      annotationRepo.getById(annotation.id) match {
        case Some(a) => failure(s"Annotation ${a.id} still exists")
        case _ => success
      }

      entityRepo.getById(annotation.entity.id) match {
        case Some(e) => failure(s"Entity ${e.id} still exists!")
        case _ => success
      }

    }
  }

}
