package annotations

import accessControl.{GroupRepository, UserRepository}
import artifacts.{ArtifactRepository, MediaRepository}
import fictionalArchaeologists.MiscDiscoveriesScenario
import play.api.test.PlaySpecification
import projects.{Project, ProjectRepository}
import schemas.{Schema, SchemaRepository}

/**
  * Created by daveshepard on 3/27/17.
  */
class VersioningTest extends PlaySpecification {

  "An AnnotationRepository" should {

    "Creating a new annotation and updating it creates two versions that are returned in the correct order" in new AnnotationVersioningTest {
      val updatedAnnotation = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> astaroth.id.toString,
            "event" -> "transmigration",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))

      val annotationList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)
      annotationList must haveSize(2)
      annotationList(0).item mustEqual(updatedAnnotation)
      annotationList(1).item mustEqual(sampleLocatedAnnotation)
    }

    "Creating an annotation and updating it twice creates three versions" in new AnnotationVersioningTest {
      val updatedAnnotation1 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> astaroth.id.toString,
            "event" -> "transmigration",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))
      val updatedAnnotation2 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> cthulu.id.toString,
            "event" -> "unforseeable change",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))

      val annotationList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)
      annotationList must haveSize(3)
      annotationList(0).item mustEqual(updatedAnnotation2)
      annotationList(1).item mustEqual(updatedAnnotation1)
      annotationList(2).item mustEqual(sampleLocatedAnnotation)
    }

    "Deleting an annotation creates a new version" in new AnnotationVersioningTest {
      val updatedAnnotation1 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> astaroth.id.toString,
            "event" -> "transmigration",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))
      val updatedAnnotation2 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> cthulu.id.toString,
            "event" -> "unforseeable change",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))

      annotationRepo.delete(sampleLocatedAnnotation, danforth)

      val annotationList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)
      annotationList must haveSize(4)
      annotationList(1).item mustEqual(updatedAnnotation2)
      annotationList(2).item mustEqual(updatedAnnotation1)
      annotationList(3).item mustEqual(sampleLocatedAnnotation)
    }

    "Reverting to the immediate previous version works correctly" in new AnnotationVersioningTest {
      val updatedAnnotation1 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> astaroth.id.toString,
            "event" -> "transmigration",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))
      val updatedAnnotation2 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> cthulu.id.toString,
            "event" -> "unforseeable change",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))

      val preReversionVersionList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)

      annotationRepo.revertToVersion(sampleLocatedAnnotation.id, preReversionVersionList(1).versionId, danforth)

      val annotationList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)
      annotationList must haveSize(4)
      annotationList(0).item mustEqual(updatedAnnotation1)
      annotationList(1).item mustEqual(updatedAnnotation2)
      annotationList(2).item mustEqual(updatedAnnotation1)
      annotationList(3).item mustEqual(sampleLocatedAnnotation)
    }

    "Reverting to the version two versions ago works correctly" in new AnnotationVersioningTest {
      val updatedAnnotation1 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> astaroth.id.toString,
            "event" -> "transmigration",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))
      val updatedAnnotation2 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> cthulu.id.toString,
            "event" -> "unforseeable change",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))

      val preReversionVersionList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)

      annotationRepo.revertToVersion(sampleLocatedAnnotation.id, preReversionVersionList(2).versionId, danforth)

      val annotationList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)
      annotationList must haveSize(4)
      annotationList(0).item mustEqual(sampleLocatedAnnotation)
      annotationList(1).item mustEqual(updatedAnnotation2)
      annotationList(2).item mustEqual(updatedAnnotation1)
      annotationList(3).item mustEqual(sampleLocatedAnnotation)
    }

    "Reverting to a version prior to a deletion works" in new AnnotationVersioningTest {
      val updatedAnnotation1 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> astaroth.id.toString,
            "event" -> "transmigration",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))
      val updatedAnnotation2 = annotationRepo.save(Annotation(
        sampleLocatedAnnotation.id,
        sampleProject, cthuluModel, Entity(
          sampleLocatedAnnotation.entity.id,
          sampleLocatedAnnotation.entity.schema,
          Map(
            "deity" -> cthulu.id.toString,
            "event" -> "unforseeable change",
            "interpretation" -> "Chaos and famine",
            "location" -> "Malibu"
          )
        ),
        List(
          (0.1, 1.1, 2.1),
          (3.1, 4.5, 3.6)
        ),
        danforth,
        null
      ))

      annotationRepo.delete(sampleLocatedAnnotation, danforth)

      val preReversionVersionList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)

      annotationRepo.revertToVersion(sampleLocatedAnnotation.id, preReversionVersionList(2).versionId, danforth)

      val annotationList = annotationRepo.listVersions(sampleLocatedAnnotation.id, 0, 25)
      annotationList must haveSize(5)
      annotationList(0).item mustEqual(sampleLocatedAnnotation)
      annotationList(1).action mustEqual AnnotationProperties.deleted
      annotationList(2).item mustEqual(updatedAnnotation2)
      annotationList(3).item mustEqual(updatedAnnotation1)
      annotationList(4).item mustEqual(sampleLocatedAnnotation)
    }

//    "Reverting to a “deleted” version returns an error"

  }

}


trait AnnotationVersioningTest extends MiscDiscoveriesScenario {

  val sampleProject = projectRepo.save(Project(null, "Test Project"))
  val sampleUnlocatedAnnotation = annotationRepo.save(
    Annotation(
      null, sampleProject, cthuluModel, Entity(
        null,
        elderGodFigureSchema,
        Map(
          "deity" -> cthulu.id.toString,
          "material" -> "Ebonite"
        )
      ),
      List(),
      danforth,
      null
    )
  )

  val sampleLocatedAnnotation = annotationRepo.save(
    Annotation(
      null, sampleProject, cthuluModel, Entity(
        null,
        illuminatiSecretSchema,
        Map(
          "deity" -> cthulu.id.toString,
          "event" -> "appearance",
          "interpretation" -> "Cthulu will appear for 1.8 minutes",
          "location" -> "All places simultaneously"
        )
      ),
      List(
        (0.0, 1.0, 2.0),
        (3.0, 4.0, 5.0),
        (6.0, 7.0, 8.0),
        (0.0, 1.0, 2.0)
      ),
      danforth,
      null
    )
  )
}
