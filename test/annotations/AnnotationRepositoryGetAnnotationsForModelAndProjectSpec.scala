package annotations

import java.util.UUID

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class AnnotationRepositoryGetAnnotationsForModelAndProjectSpec extends Specification {

  trait AnnotationRepositoryGetAnnotationsForModelAndProjectTest extends AnnotationRepositoryTest with ModelTest {


  }

  "When querying for annotations from models and projects, an AnnotationRepository" should {

    "retrieve all annotations for a model" in new AnnotationRepositoryGetAnnotationsForModelAndProjectTest {
      val artifacts = List(createMockArtifactWithFile, createMockArtifactWithFile)
      val models = artifacts.map(mediaRepo.getModelsForArtifact(_).head)

      val eras = List(4,5,6,7).map(i => entityRepo.save(Entity(
        null, eraSchema, Map("name" -> s"Era ${i}"))
      ))
      val geometryPoints = 0 to eras.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })

      val builtDuringAnnotations = eras.indices map(i =>
        annotationRepo.save(Annotation(
          null, project, models(i % 2),
          Entity(null, builtDuringSchema, Map( "era" -> eras(i).id.toString )),
          geometryPoints(i),
          user, ""
        )
        ))

      val annotationsMapped = builtDuringAnnotations.foldLeft(Map[UUID,Annotation]()) {
        case (acc, annotation) => acc.updated(annotation.id, annotation)
      }
      artifacts.map(artifact => {
        val model = mediaRepo.getModelsForArtifact(artifact).head
        val annotations = annotationRepo.getForModel(model)
        annotations.length mustEqual eras.size / 2

        annotations.zipWithIndex.map({
          case (annotation, index) => {
            val otherAnnotation = annotationsMapped(annotation.id)
            annotation.model mustEqual model

            annotation.id mustEqual otherAnnotation.id
            annotation.entity.schema.id mustEqual otherAnnotation.entity.schema.id
            annotation.location mustEqual otherAnnotation.location
            annotation.project mustEqual otherAnnotation.project
            annotation.entity.id mustEqual otherAnnotation.entity.id
          }
        })
      })
    }

    "retrieve all annotations for a project" in new AnnotationRepositoryGetAnnotationsForModelAndProjectTest {
      val projects = List(createMockProject, createMockProject)

      val eras = List(4,5,6,7).map(i => entityRepo.save(Entity(null, eraSchema, Map("name" -> s"Era ${i}"))))
      val geometryPoints = eras.indices map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      }
        )

      val builtDuringAnnotations = eras.indices map(i =>
        annotationRepo.save(Annotation(
          null, projects(i % 2), model,
          Entity(null, builtDuringSchema, Map(
              "era" -> eras(i).id.toString
            )
          ),
          geometryPoints(i),
          user, ""
        )
      ))

      val annotationsMapped = builtDuringAnnotations.foldLeft(Map[UUID,Annotation]()) {
        case (acc, annotation) => acc.updated(annotation.id, annotation)
      }

      projects.map(project => {
        val annotations = annotationRepo.getForProject(project)
        annotations.length mustEqual eras.size / 2

        annotations.map(annotation => {
          val otherAnnotation = annotationsMapped(annotation.id)
          annotation.project mustEqual project

          annotation.id mustEqual otherAnnotation.id
          annotation.entity.schema.id mustEqual otherAnnotation.entity.schema.id
          annotation.location mustEqual otherAnnotation.location
          annotation.model.id mustEqual model.id
          annotation.entity.id mustEqual otherAnnotation.entity.id
        })
      })
    }

    "retrieve all annotations for a model in a project" in new AnnotationRepositoryGetAnnotationsForModelAndProjectTest {
      val projects = List(createMockProject, createMockProject)
      val artifacts = List(createMockArtifactWithFile, createMockArtifactWithFile)
      val models = artifacts.map(mediaRepo.getModelsForArtifact(_).head)

      val eras = List(4, 5, 6, 7, 8, 9).map(i => entityRepo.save(Entity(null, eraSchema, Map("name" -> s"Era ${i}"))))
      val geometryPoints = 0 to eras.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })

      val builtDuringAnnotations = eras.indices map(i =>
        annotationRepo.save(Annotation(null,
          projects(i % 2), models((i + 1) % 2),
          Entity(null, builtDuringSchema, Map( "era" -> eras(i).id.toString )),
            geometryPoints(i), user, ""
        ))
      )

      val annotationsMapped = builtDuringAnnotations.foldLeft(Map[UUID,Annotation]()) {
        case (acc, annotation) => acc.updated(annotation.id, annotation)
      }

      annotationRepo.getForModelAndProject(models.head, projects.head) must beEmpty

      annotationRepo.getForModelAndProject(models(0), projects(1)) match {
        case List() => failure("Annotations not found")
        case annotations => {
          annotations.length mustEqual eras.size / 2

          annotations.foreach { annotation =>
            val otherAnnotation = annotationsMapped(annotation.id)
            annotation.project mustEqual projects(1)

            annotation.id mustEqual otherAnnotation.id
            annotation.entity.schema.id mustEqual otherAnnotation.entity.schema.id
            annotation.location mustEqual otherAnnotation.location
            annotation.model mustEqual models(0)
            annotation.entity.id mustEqual otherAnnotation.entity.id
          }
        }
      }

      annotationRepo.getForModelAndProject(models(1), projects(1)) must beEmpty

      annotationRepo.getForModelAndProject(models(1), projects(0)) match {
        case List() => failure("Annotations not found")
        case annotations => {
          annotations must haveSize(eras.size / 2)

          annotations.foreach { annotation =>
            val otherAnnotation = annotationsMapped(annotation.id)
            annotation.project mustEqual projects(0)

            annotation.id mustEqual otherAnnotation.id
            annotation.entity.schema.id mustEqual otherAnnotation.entity.schema.id
            annotation.location mustEqual otherAnnotation.location
            annotation.model mustEqual models(1)
            annotation.entity.id mustEqual otherAnnotation.entity.id
          }
        }
      }

    }

    "retrieve all annotations with a particular schema for a model" in new AnnotationRepositoryGetAnnotationsForModelAndProjectTest {
      val projects = List(createMockProject, createMockProject)

      val eras = List(4, 5, 6, 7, 8, 9).map(i => entityRepo.save(
        Entity(null, eraSchema, Map("name" -> s"Era ${i}"))
      ))
      val geometryPoints = 0 to eras.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })

      val builtDuringAnnotations = eras.indices map(i =>
        annotationRepo.save(Annotation(
          null, projects(i % 2), model,
          Entity(null, builtDuringSchema, Map( "era" -> eras(i).id.toString )),
          geometryPoints(i),
          user, ""
          )
        )
      )

      val eraAnnotations = eras.indices map(i =>
        annotationRepo.save(Annotation(
          null, projects(i % 2), model,
          Entity(null, eraSchema, Map( "name" -> s"era ${i + 10}" )),
            geometryPoints(i),
          user, ""
      )))

      annotationRepo.getForModelAndSchema(model, eraSchema) match {
        case List() => failure("No Era annotations found")
        case annotations => {
          annotations must haveSize(eras.size)
          annotations.foreach(_.entity.schema mustEqual eraSchema)
        }
      }

      annotationRepo.getForModelAndSchema(model, builtDuringSchema) match {
        case List() => failure("No BuiltDuring annotations found")
        case annotations => {
          annotations must haveSize(builtDuringAnnotations.size)
          annotations.foreach(_.entity.schema.id mustEqual builtDuringSchema.id)
        }
      }

    }

    "retrieve all annotations with a particular schema for a model and a project" in new AnnotationRepositoryGetAnnotationsForModelAndProjectTest {
      val projects = List(createMockProject, createMockProject)

      val eras = List(4, 5, 6, 7, 8, 9).map(i => entityRepo.save(Entity(null, eraSchema, Map("name" -> s"Era ${i}"))))
      val geometryPoints = 0 to eras.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })

      val builtDuringAnnotations = eras.indices map(i =>
        annotationRepo.save(Annotation(
          null, projects(i % 2), model,
          Entity(null, builtDuringSchema, Map( "era" -> eras(i).id.toString)),
          geometryPoints(i),
          user, ""
        ))
      )

      val eraAnnotations = eras.indices map(i =>
        annotationRepo.save(Annotation(
          null, projects(i % 2), model,
          Entity(null, eraSchema, Map( "name" -> s"era ${i + 10}" )),
          geometryPoints(i),
          user, ""
        ))
      )

      annotationRepo.getForModelAndProjectAndSchema(model, projects(0), eraSchema) match {
        case List() => failure("No Era annotations found")
        case annotations => {
          annotations must haveSize(eras.size / 2)
          annotations.foreach(_.entity.schema mustEqual eraSchema)
          annotations.foreach(_.project mustEqual projects(0))
        }
      }

      annotationRepo.getForModelAndProjectAndSchema(model, projects(1), builtDuringSchema) match {
        case List() => failure("No BuiltDuring annotations found")
        case annotations => {
          annotations must haveSize(eras.size  / 2)
          annotations.foreach(_.entity.schema.id mustEqual builtDuringSchema.id)
          annotations.foreach(_.project mustEqual projects(1))
        }
      }
    }

  }

}
