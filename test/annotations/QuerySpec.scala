package annotations

import annotations.search.AnnotationRepositorySearchTest
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class QuerySpec extends Specification {

  "A Query" should {

    "create the correct Cypher to match an object with base types, and only the matching objects" in new AnnotationRepositorySearchTest {
      val qualifier = annotationRepo.qualifierFactory

      val query = Query(
        None, None, List(Qualifier(colorSchema, List(
          AttributeQualifier(colorSchema.fields(0), qualifier.equality, "red")
        ))
        ), Set()
      )

      val cypherQuery = annotationRepo.toCypher(query)

      cypherQuery must contain(s"(a0:Annotation)-[:${AnnotationProperties.entity}]->(q0_0)")
      cypherQuery must contain(s"(a0:Annotation)-[:OnMedia]->(m:Model)")
      cypherQuery must contain(s"""q0_0.`name` = "${redColor.data("name")}"""")
    }

    "create the correct Cypher to match an object with node and base types, and only the matching objects" in new AnnotationRepositorySearchTest {

      val qualifier = annotationRepo.qualifierFactory
      val descriptionSearchText = "abcdef%"

      val query = Query(
        None, None, List(Qualifier(wigSchema, List(
          AttributeQualifier(wigSchema.fields(0), qualifier.equality, greenColor.id.toString)
        ))), Set()
      )

      val cypherQuery = annotationRepo.toCypher(query)

      cypherQuery must contain(s"(q0_0)<--(fe0)" )
      cypherQuery must contain(s"(a0:Annotation)-[:${AnnotationProperties.entity}]->(fe0)")
      cypherQuery must contain(s"(a0:Annotation)-[:OnMedia]->(m:Model)")
    }

    "create the correct Cypher to match whole-model annotations with one schema" in new AnnotationRepositorySearchTest {
      val era = eras(1)

      val qualifier = annotationRepo.qualifierFactory
      val query = Query(
        None, None, List(Qualifier(eraInstanceSchema, List(
          AttributeQualifier(eraInstanceSchema.fields(0), qualifier.equality, era.id.toString)
        ))), Set()
      )

      val cypherQuery = annotationRepo.toCypher(query)

      cypherQuery must contain(s"(q0_0)<--(fe0)")
      cypherQuery must contain(s"(a0:Annotation)-[:${AnnotationProperties.entity}]->(fe0),")
      cypherQuery must contain(s"""q0_0.id = "${era.id}" """)

    }

    "create the correct Cypher to match whole-model annotations with two schemas" in new AnnotationRepositorySearchTest {
      val era = eras(1)
      val location = locations(2)

      val qualifier = annotationRepo.qualifierFactory
      val query = Query(
        None, None, List(
          Qualifier(eraInstanceSchema, List(AttributeQualifier(eraInstanceSchema.fields(0), qualifier.equality, era.id.toString))),
          Qualifier(locationInstanceSchema, List(AttributeQualifier(locationInstanceSchema.fields(0), qualifier.equality, location.id.toString)))
        ), Set()
      )

      val cypherQuery = annotationRepo.toCypher(query)

      List(
        s"(q0_0)<--(fe0)",
        s"(a0:Annotation)-[:${AnnotationProperties.entity}]->(fe0)",
        s"(q1_0)<--(fe1)",
        s"(a1:Annotation)-[:${AnnotationProperties.entity}]->(fe1)",
        s"""q0_0.id = "${era.id}" """,
        s""" q1_0.id = "${location.id}" """
      ).foreach(string => {
        cypherQuery must contain(string)
      })
    }

    "create the correct Cypher to match whole-model annotations with two schemas and one base type" in new AnnotationRepositorySearchTest {

      val descriptionQualifier = "abcdef"
      val era = eras(1)
      val location = locations(2)

      val eraInstance = entityRepo.save(Entity(null, eraInstanceSchema, Map(
        "era" -> era.id.toString,
        "description" -> s"qwerty$descriptionQualifier qwer",
        "comments" -> "Other data"
      )))

      val qualifier = annotationRepo.qualifierFactory
      val query = Query(
        None, None, List(
          Qualifier(eraInstanceSchema, List(
            AttributeQualifier(eraInstanceSchema.fields(0), qualifier.equality, era.id.toString),
            AttributeQualifier(eraInstanceSchema.fields(1), qualifier.like, s"%$descriptionQualifier%")
          )),
          Qualifier(locationInstanceSchema, List(AttributeQualifier(locationInstanceSchema.fields(0), qualifier.equality, location.id.toString)))
        ), Set()
      )

      val cypherQuery = annotationRepo.toCypher(query)

      List(
        s"(q0_0)<--(fe0)",
        s"(a0:Annotation)-[:${AnnotationProperties.entity}]->(fe0)",
        s"(q1_0)<--(fe1)",
        s"(a1:Annotation)-[:${AnnotationProperties.entity}]->(fe1)",
        s"""q0_0.id = "${era.id}" """,
        s"""q1_0.id = "${location.id}" """,
        s"""fe0.`${eraInstanceSchema.fields(1).title}` =~ "%${descriptionQualifier}%""""
      ).foreach(string => {
        cypherQuery must contain(string)
      })
    }

  }

}

