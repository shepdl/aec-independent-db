package annotations

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json.{JsObject, JsString, JsValue, Json}


@RunWith(classOf[JUnitRunner])
class AnnotationControllerGetAnnotationsForProjectAndModelAndSchemaSpec extends Specification {

  "When querying, an AnnotationController" should {
    "retrieve all annotations for a model in a project that have the same schema" in new AnnotationControllerTest {
      val artifacts = List(createMockArtifactWithFile, createMockArtifactWithFile)
      val models = artifacts.map(mediaRepo.getModelsForArtifact(_).head)
      val projects = List(createMockProject, createMockProject)

      val figures = List(
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 1", "origin" -> "Atlanta"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 2", "origin" -> "Bath"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 3", "origin" -> "Camden")))
      )

      val geometryPoints = 0 to figures.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })


      val figureAnnotations = List(
        annotationRepo.save(Annotation(null, projects(0), models(1),
          Entity(null,
            instanceOfFigureSchema,
            Map(
              "figure" -> figures(0).id.toString,
              "condiments" -> "something",
              "bottle count" -> 0.toString
            )
          ),
          geometryPoints(0),
         user, ""
        )),
        annotationRepo.save(Annotation(null, projects(0), models(1),
          Entity(null,
            instanceOfFigureSchema,
            Map(
            "figure" -> figures(1).id.toString,
            "condiments" -> "something",
            "bottle count" -> 2.toString
          )),
          geometryPoints(0),
          user, ""
        )),
        annotationRepo.save(Annotation(null, projects(1), models(0),
          Entity(null,
            instanceOfFigureSchema,
            Map(
            "figure" -> figures(2).id.toString,
            "condiments" -> "something",
            "bottle count" -> 2.toString
          )),
          geometryPoints(0),
          user, ""
        )
      ))

      val getRequestP0A0 = FakeRequest(GET, s"/activities/${projects(0).id}/models/${models(0).id}/${figureSchema.title}-${figureSchema.id}/annotations")
      val getResponseP0A0 = controller.getForProjectAndModelAndSchema(projects(0).id, models(0).id, figureSchema.title, figureSchema.id)(getRequestP0A0)
      status(getResponseP0A0) mustEqual OK

      val p0A0JsonAnnotations = (Json.parse(contentAsString(getResponseP0A0)) \ "annotations").as[Seq[JsObject]]
      p0A0JsonAnnotations.size mustEqual 0


      val getRequestP0A1 = FakeRequest(GET, s"/activities/${projects(0).id}/models/${models(1).id}/${instanceOfFigureSchema.title}-${instanceOfFigureSchema.id}/annotations")
      val getResponseP0A1 = controller.getForProjectAndModelAndSchema(projects(0).id, models(1).id, instanceOfFigureSchema.title, instanceOfFigureSchema.id)(getRequestP0A1)
      status(getResponseP0A1) mustEqual OK

      val p0A1JsonAnnotations = (contentAsJson(getResponseP0A1) \ "annotations").as[Seq[JsObject]]
      p0A1JsonAnnotations.size mustEqual 2


      val getRequestP1A0 = FakeRequest(GET, s"/activities/${projects(1).id}/models/${models(0).id}/${instanceOfFigureSchema.title}-${instanceOfFigureSchema.id}/annotations")
      val getResponseP1A0 = controller.getForProjectAndModelAndSchema(projects(1).id, models(0).id, instanceOfFigureSchema.title, instanceOfFigureSchema.id)(getRequestP1A0)
      status(getResponseP1A0) mustEqual OK

      val p1A0JsonAnnotations = (contentAsJson(getResponseP1A0) \ "annotations").as[Seq[JsObject]]
      p1A0JsonAnnotations.size mustEqual 1


      val getRequestP1A1 = FakeRequest(GET, s"/activities/${projects(1).id}/models/${models(1).id}/${instanceOfFigureSchema.title}-${instanceOfFigureSchema.id}/annotations")
      val getResponseP1A1 = controller.getForProjectAndModelAndSchema(projects(1).id, models(1).id, instanceOfFigureSchema.title, instanceOfFigureSchema.id)(getRequestP1A1)
      status(getResponseP1A1) mustEqual OK

      val p1A1JsonAnnotations = (contentAsJson(getResponseP1A1) \ "annotations").as[Seq[JsObject]]
      (p1A1JsonAnnotations).size mustEqual 0
    }


  }

}
