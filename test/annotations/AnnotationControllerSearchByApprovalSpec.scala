package annotations

import accessControl._
import annotations.search.AnnotationRepositorySearchByAnnotationTestCase
import backend.approvals.ApprovalRepository
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.Application
import play.api.libs.json.Json
import play.api.mvc.Controller
import play.api.test.Helpers._
import play.api.test._
import projects.ProjectRepository
import schemas.SchemaRepository

trait AnnotationControllerSearchTestCase {
  val app:Application

  val controller = new AnnotationSearchController with Controller {
    override val entityRepo: EntityRepository =  app.injector.instanceOf[EntityRepository]
    override val projectRepo = app.injector.instanceOf[ProjectRepository]
    override val schemaRepo: SchemaRepository = app.injector.instanceOf[SchemaRepository]
    override val annotationRepo: AnnotationRepository = app.injector.instanceOf[AnnotationRepository]
    override val groupRepo = app.injector.instanceOf[GroupRepository]
    override val approvalRepo = app.injector.instanceOf[ApprovalRepository]
  }

  def doSearch(jsonBody:String) = {
    val searchRequest = FakeRequest(POST, "/annotations/search", FakeHeaders(), Json.parse(jsonBody))
    contentAsString(controller.search()(searchRequest))
  }

}

@RunWith(classOf[JUnitRunner])
class AnnotationControllerSearchByApprovalSpec extends PlaySpecification with JsonMatchers {

  trait AnnotationControllerSearchTest extends AnnotationRepositorySearchByAnnotationTestCase with AnnotationControllerSearchTestCase

  "When searching by approvals, an Annotation Repository" should {

    "return one annotation when only one annotation has been approved, but there are two that have not been approved" in new AnnotationControllerSearchTest {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)

      val response = doSearch(
        s"""
         |{
         |  "request" : {
         |    "project" : [],
         |    "entities" : [],
         |    "approvedBy": [
         |      "${fictionalArchaeologists.id}"
         |    ]
         |  }
         |}
         """.stripMargin
      )

      response must */("annotations").andHave(size(1))
    }

    "return no annotations when no annotations have been approved and one group is queried" in new AnnotationControllerSearchTest {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [],
           |    "approvedBy": [
           |      "${fictionalArchaeologists.id}"
           |    ]
           |  }
           |}
         """.stripMargin
      )

      response must */("result").andHave(size(0))
    }

    "return no annotations when no annotations have been approved and two groups are queried" in new AnnotationControllerSearchTest {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [],
           |    "approvedBy": [
           |      "${fictionalArchaeologists.id}",
           |      "${hogwarts.id}"
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must */("result").andHave(size(0))
    }

    "return an annotation when it has been approved by multiple groups, but only one group is being queried" in new AnnotationControllerSearchTest {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      approvalRepo.approve(approvedAnnotation1, hogwarts, potter)
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [],
           |    "approvedBy": [
           |      "${fictionalArchaeologists.id}"
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must */("annotations").andHave(size(1))
    }

    "return an annotation when it has been approved by one group, but two groups are queried" in new AnnotationControllerSearchTest {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [],
           |    "approvedBy": [
           |      "${fictionalArchaeologists.id}",
           |      "${hogwarts.id}"
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must */("annotations").andHave(size(1))
    }


    "return two annotations when each has been approved by one group, but not another" in new AnnotationControllerSearchTest {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      approvalRepo.approve(approvedAnnotation2, hogwarts, potter)
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [],
           |    "approvedBy": [
           |      "${fictionalArchaeologists.id}",
           |      "${hogwarts.id}"
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must */("annotations").andHave(size(2))
    }

    "return one annotation when each has been approved by one group, but not another, and a filter has been applied" in new AnnotationControllerSearchTest {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      approvalRepo.approve(approvedAnnotation2, hogwarts, potter)
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |    {
           |      "type" : "quality",
           |      "schemaId" : "${wigSchema.id}",
           |      "fieldName" : "${wigSchema("material").id}",
           |      "queryType" : "equality",
           |      "value" : "${plasterMaterial.id}"
           |    }],
           |    "approvedBy": [
           |      "${fictionalArchaeologists.id}",
           |      "${hogwarts.id}"
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must */("annotations").andHave(size(1))
    }

  }

}
