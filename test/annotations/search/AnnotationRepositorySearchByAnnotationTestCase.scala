package annotations.search

import accessControl.{ExtraUsers, Group, GroupRepository, User}
import annotations.{Annotation, Entity}
import backend.approvals.ApprovalRepository

/**
  * Created by dave on 2/3/17.
  */
trait AnnotationRepositorySearchByAnnotationTestCase extends AnnotationRepositorySearchTest with ExtraUsers {
  val approvalRepo = app.injector.instanceOf[ApprovalRepository]
  val groupRepo = app.injector.instanceOf[GroupRepository]

  val fictionalArchaeologists = groupRepo.save(Group(null, "Fictional Archaeologists", "http://example.com/"))
  groupRepo.addUser(fictionalArchaeologists, jones, "ADMIN")
  groupRepo.addUser(fictionalArchaeologists, langdon, "ADMIN")

  val hogwarts = groupRepo.save(Group(null, "Hogwarts Academy", "http://hogwarts.ac.uk/"))
  val potter = userRepo.save(User(null, "Harry Potter", "hpotter@hogwarts.ac.uk"))
  groupRepo.addUser(hogwarts, potter, "ADMIN")

  val randomGeometry = List(
    (0.0, 0.0, 1.0), (1.0, 2.0, 3.0), (4.0, 5.0, 6.0)
  )

  val approvedAnnotation1 = annotationRepo.save(
    Annotation(
      null, project, model,
      Entity(null, wigSchema, Map(
        "color" -> greenColor.id.toString,
        "material" -> plasterMaterial.id.toString,
        "description" -> "This is a test"
      )),
      randomGeometry, user, ""
    )
  )
  val approvedAnnotation2 = annotationRepo.save(
    Annotation(
      null, project, model,
      Entity(null, wigSchema, Map(
        "color" -> redColor.id.toString,
        "material" -> woodMaterial.id.toString,
        "description" -> "A wood wig"
      )),
      randomGeometry, user, ""
    )
  )

  val unapprovedAnnotation1 = annotationRepo.save(
    Annotation(
      null, project, model,
      Entity(null, wigSchema, Map(
        "color" -> redColor.id.toString,
        "material" -> plasterMaterial.id.toString,
        "description" -> "This will not be approved"
      )),
      randomGeometry, user, ""
    )
  )
  val unapprovedAnnotation2 = annotationRepo.save(
    Annotation(
      null, project, model,
      Entity(null, wigSchema, Map(
        "color" -> greenColor.id.toString,
        "material" -> woodMaterial.id.toString,
        "description" -> "This will not be approved either"
      )),
      randomGeometry, user, ""
    )
  )
}
