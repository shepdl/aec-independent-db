package annotations.search

import annotations.{Annotation, AnnotationRepositoryTest, Entity, ModelTest}
import schemas.{Schema, SchemaField}

/**
  * Created by dave on 4/14/16.
  */
trait AnnotationRepositorySearchTest extends AnnotationRepositoryTest with ModelTest {

  val colorSchema = schemaRepo.save(Schema(
    null, "Color", "entity", List(
      SchemaField(null, "name", stringSchemaNode, false, List()),
      SchemaField(null, "hexCode", stringSchemaNode, false, List())
    ), false, user)
  )
  val greenColor = entityRepo.save(Entity(null, colorSchema, Map(
    "name" -> "green", "hexCode" -> "#00FF00"
  )))
  val redColor = entityRepo.save(Entity(null, colorSchema, Map(
    "name" -> "red", "hexCode" -> "#FF0000"
  )))


  val materialSchema = schemaRepo.save(Schema(null, "Material", "entity", List(
    SchemaField(null, "name", stringSchemaNode, false, List()),
    SchemaField(null, "density", longSchemaNode, false, List())), false, user)
  )
  val plasterMaterial = entityRepo.save(Entity(null, materialSchema, Map(
    "name" -> "plaster", "density" -> "1"
  )))
  val woodMaterial = entityRepo.save(Entity(null, materialSchema, Map(
    "name" -> "wood", "density" -> "4"
  )))


  val wigSchema = schemaRepo.save(Schema(
    null, "Wig", "annotation", List(
      SchemaField(null, "color", colorSchema, false, List()),
      SchemaField(null, "material", materialSchema, false, List()),
      SchemaField(null, "description", stringSchemaNode, false, List())), false, user)
  )

  val eras = (0 to 10).map({
    i => {
      entityRepo.save(Entity(null, eraSchema, Map("name" -> s"Era $i")))
    }
  }).toList

  val eraInstanceSchema = schemaRepo.save(Schema(
    null, "Era Instance", "either", List(
      SchemaField(null, "era", eraSchema, false, List()),
      SchemaField(null, "comments", stringSchemaNode, false, List())),
    false, user))

  val locationSchema = schemaRepo.save(Schema(null, "Location", "either",
    List(
      SchemaField(null, "name", stringSchemaNode, false, List()),
      SchemaField(null, "latitude", longSchemaNode, false, List()),
      SchemaField(null, "longitude", longSchemaNode, false, List())
    ),
    isControlled = false,
    user
  ))
  val locationInstanceSchema = schemaRepo.save(Schema(
    null, "Location Instance", "either", List(
      SchemaField(null, "location", locationSchema, false, List())), false, user)
  )
  val locations = List(
    "Thebes", "Cairo", "Saqqara"
  ).map(n => entityRepo.save(Entity(null, locationSchema,
    Map("name" -> n, "latitude" -> n.hashCode.toLong.toString, "longitude" -> (n.hashCode.toLong * 100).toString))
  ))


  val decorationSchema = schemaRepo.save(Schema(
    null, "Decoration", "annotation", List(
      SchemaField(null, "color", colorSchema, false, List()),
      SchemaField(null, "material", materialSchema, false, List())
    ),
    false, user
  ))


  val nowhere = List((0.0, 0.0, 0.0))

  annotationRepo.save(Annotation(
    null, project, model,
    Entity(null, decorationSchema, Map(
      "color" -> greenColor.id.toString,
      "material" -> plasterMaterial.id.toString
    )), nowhere, user, ""
  ))

}
