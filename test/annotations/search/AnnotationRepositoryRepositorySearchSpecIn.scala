package annotations.search

import annotations.{Annotation, Entity}
import schemas.{Schema, SchemaField}

/**
  * Created by dave on 2/3/17.
  */
trait AnnotationRepositoryRepositorySearchSpecIn extends AnnotationRepositorySearchTest {

  /**
    * Coffin 1:
    *   - Horus figure annotation
    *   - - name: Horus
    *   - - origin: Thebes
    *   - Elvis figure annotation
    *   - - name: Elvis
    *   - - origin: Tennessee
    *   - John Lennon figure annotation
    *   - - name: John Lennon
    *   - - origin: Liverpool
    *   - Wig annotation
    *   - - material: plaster
    *   - - color: red
    *   - 21st dynasty era instance annotation
    *   - - era: 21st dynasty
    *   - - comments: "Really hard to tell"
    *   - Thebes location instance
    *   - - location: Thebes
    *   - - comments: "Evidence 1"
    *   - Thebes location instance
    *   - - location: Thebes
    *   - - comments: "Evidence 2"
    *   - Thebes location instance
    *   - - location: Thebes
    *   - - comments: "Evidence 3"
    *
    *
    * Coffin 2:
    *   - Horus figure annotation
    *   - - name: Horus
    *   - - origin: Thebes
    *   - Wig annotation
    *   - - material: wood
    *   - - color: green
    *   - Wig annotation
    *   - - material: wood
    *   - - color: red
    *   - Wig annotation
    *   - - material: wood
    *   - - color: orange
    *   - Thebes location instance
    *   - - location: Thebes
    *   - - comments: "Something else"
    *
    */

  val artifact1 = createMockArtifactWithFile
  val artifact2 = createMockArtifactWithFile

  val orangeColor = entityRepo.save(Entity(null, colorSchema, Map("name" -> "orange", "hexCode" -> "#FFFF00")))


  val horus = entityRepo.save(Entity(null, figureSchema, Map("name" -> "Horus", "origin" -> "Thebes")))
  val elvis = entityRepo.save(Entity(null, figureSchema, Map("name" -> "Elvis", "origin" -> "Tennessee")))
  val lennon = entityRepo.save(Entity(null, figureSchema, Map("name" -> "John Lennon", "origin" -> "Liverpool")))

  val thebes = entityRepo.save(Entity(null, locationSchema, Map("name" -> "Thebes", "latitude" -> 2.toString, "longitude" -> 4.toString)))
  val newLocationInstance = schemaRepo.save(Schema(
    null, "Location Instance", "annotation", List(
      SchemaField(null, "location", locationSchema, false, List()),
      SchemaField(null, "comments", stringSchemaNode, false, List())),
    false, user)
  )


  val af1Model = mediaRepo.getModelsForArtifact(artifact1).head
  val af2Model = mediaRepo.getModelsForArtifact(artifact2).head

  val genericLocation = List((1.0, 2.0, 3.0), (2.0, 3.0, 4.0), (1.0, 2.0, 3.0))

  val dynasty21 = entityRepo.save(Entity(null, eraSchema, Map("name" -> "21st Dynasty")))

  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(null, figureSchema, Map("name" -> "Horus", "origin" -> "Thebes")),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(null, figureSchema, Map("name" -> "Elvis", "origin" -> "Tennessee")),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(null, figureSchema, Map("name" -> "John Lennon", "origin" -> "Liverpool")),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(null, eraInstanceSchema, Map("era" -> dynasty21.id.toString, "comments" -> "Really hard to tell")),
    nowhere,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(
      null,
      wigSchema,
      Map("material" -> plasterMaterial.id.toString,
        "color" -> redColor.id.toString,
        "description" -> "Very colorful wig")
    ),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(null,
      newLocationInstance,
      Map("location" -> thebes.id.toString, "comments" -> "Evidence 1")
    ),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(null,
      newLocationInstance,
      Map("location" -> thebes.id.toString, "comments" -> "Evidence 2")
    ),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af1Model,
    Entity(null,
      newLocationInstance,
      Map("location" -> thebes.id.toString, "comments" -> "Evidence 3")
    ),
    genericLocation,
    user, ""
  ))


  annotationRepo.save(Annotation(null, project, af2Model,
    Entity(null,
      figureSchema,
      Map("name" -> "Horus", "origin" -> "Thebes")
    ),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af2Model,
    Entity(null,
      wigSchema,
      Map("material" -> woodMaterial.id.toString, "color" -> greenColor.id.toString, "description" -> "Very colorful wig")
    ),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af2Model,
    Entity(null,
      wigSchema,
      Map("material" -> woodMaterial.id.toString, "color" -> redColor.id.toString, "description" -> "Very colorful wig")
    ),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af2Model,
    Entity(null,
      wigSchema,
      Map("material" -> woodMaterial.id.toString, "color" -> orangeColor.id.toString, "description" -> "Very colorful wig")
    ),
    genericLocation,
    user, ""
  ))
  annotationRepo.save(Annotation(null, project, af2Model,
    Entity(null,
      newLocationInstance,
      Map("location" -> thebes.id.toString, "comments" -> "Something else")
    ),
    genericLocation,
    user, ""
  ))

}
