package annotations.search

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

/**
  * Created by dave on 2/3/17.
  */
@RunWith(classOf[JUnitRunner])
class AnnotationRepositorySearchByApprovalSpec extends Specification {

  "When searching by approvals, an Annotation Repository" should {

    "return one annotation when only one annotation has been approved, but there are two that have not been approved" in new AnnotationRepositorySearchByAnnotationTestCase {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)

      val qualifierFactory = annotationRepo.qualifierFactory
      val query = annotationRepo.queryFactory.create.addApprovedByGroup(fictionalArchaeologists)
      val result = annotationRepo.search(query)
      result must haveSize(1)

      val firstResult = result.head
      firstResult.annotations.head.id mustEqual approvedAnnotation1.id
    }

    "return no annotations when no annotations have been approved and one group is queried" in new AnnotationRepositorySearchByAnnotationTestCase {
      val qualifierFactory = annotationRepo.qualifierFactory
      val query = annotationRepo.queryFactory.create.addApprovedByGroup(fictionalArchaeologists)
      val result = annotationRepo.search(query)
      result must beEmpty
    }

    "return no annotations when no annotations have been approved and two groups are queried" in new AnnotationRepositorySearchByAnnotationTestCase {
      val qualifierFactory = annotationRepo.qualifierFactory
      val query = annotationRepo.queryFactory.create
        .addApprovedByGroup(fictionalArchaeologists)
        .addApprovedByGroup(hogwarts)
      val result = annotationRepo.search(query)
      result must beEmpty
    }

    "return an annotation when it has been approved by multiple groups, but only one group is being queried" in new AnnotationRepositorySearchByAnnotationTestCase {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      approvalRepo.approve(approvedAnnotation1, hogwarts, potter)
      val qualifierFactory = annotationRepo.qualifierFactory
      val query = annotationRepo.queryFactory.create.addApprovedByGroup(fictionalArchaeologists)
      val result = annotationRepo.search(query)
      result must haveSize(1)
    }

    "return an annotation when it has been approved by one group, but two groups are queried" in new AnnotationRepositorySearchByAnnotationTestCase {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      val qualifierFactory = annotationRepo.qualifierFactory
      val query = annotationRepo.queryFactory.create
        .addApprovedByGroup(fictionalArchaeologists)
        .addApprovedByGroup(hogwarts)
      val result = annotationRepo.search(query)
      result must haveSize(1)
    }

    "return two annotations when each has been approved by one group, but not another" in new AnnotationRepositorySearchByAnnotationTestCase {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      approvalRepo.approve(approvedAnnotation2, hogwarts, potter)
      val qualifierFactory = annotationRepo.qualifierFactory
      val query = annotationRepo.queryFactory.create
        .addApprovedByGroup(fictionalArchaeologists)
        .addApprovedByGroup(hogwarts)
      val result = annotationRepo.search(query)
      result.head.annotations must haveSize(2)
    }

    "return one annotation when it has been approved by one group, and there is also a filter on it" in new AnnotationRepositorySearchByAnnotationTestCase {
      approvalRepo.approve(approvedAnnotation1, fictionalArchaeologists, jones)
      approvalRepo.approve(approvedAnnotation2, hogwarts, potter)
      val qualifierFactory = annotationRepo.qualifierFactory
      val query = annotationRepo.queryFactory.create
          .addQualifier(
            qualifierFactory.createForSchema(wigSchema)
              .addAttribute(wigSchema("material"), qualifierFactory.equality, plasterMaterial.id.toString)
          )
        .addApprovedByGroup(fictionalArchaeologists)
        .addApprovedByGroup(hogwarts)
      val result = annotationRepo.search(query)
      result must haveSize(1)
      result.head.annotations must haveSize(1)
    }

  }

}
