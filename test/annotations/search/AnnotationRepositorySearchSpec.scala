package annotations.search

import org.junit.runner.RunWith
import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner

/**
  * Created by dave on 2/3/17.
  */
@RunWith(classOf[JUnitRunner])
class AnnotationRepositorySearchSpec extends Specification {

  "An Annotation Repository, when querying, " should {

    // Base types
    "return one model with one annotation with base types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory
      val figureName = "Elvis"
      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(figureSchema)
            .addAttribute(figureSchema("name"), qualifierFactory.equality, figureName)
      )
      val result = annotationRepo.search(query2)
      result.length mustEqual 1
      val firstModel = result.head
      firstModel.model.id mustEqual af1Model.id
      firstModel.annotations.length mustEqual 1
      val firstAnnotation = firstModel.annotations.head
      firstAnnotation.entity.schema mustEqual figureSchema
      firstAnnotation.entity.data("name") mustEqual figureName
    }

    "return one model with three annotations with base types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory
      val comments = "Evidence"
      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(newLocationInstance)
          .addAttribute(newLocationInstance("comments"), qualifierFactory.like, s".*$comments.*")
      )
      val result = annotationRepo.search(query2)
      result.length mustEqual 1
      val firstModel = result.head
      firstModel.model.id mustEqual af1Model.id
      firstModel.annotations.length mustEqual 3
      val firstAnnotation = firstModel.annotations.head
      firstModel.annotations.foreach(a => a.entity.schema.id mustEqual newLocationInstance.id)
    }

    "return two models with one annotation each with base types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory
      val figureName = "Horus"
      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(figureSchema)
          .addAttribute(figureSchema("name"), qualifierFactory.equality, figureName)
      )
      val result = annotationRepo.search(query2)
      result.length mustEqual 2
    }


    // Node types
    "return one model with one annotation with node types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory
      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(wigSchema)
        .addAttribute(wigSchema("material"), qualifierFactory.equality, plasterMaterial.id.toString)
        .addAttribute(wigSchema("color"), qualifierFactory.equality, redColor.id.toString)
      )

      val result = annotationRepo.search(query2)
      result.length mustEqual 1
      val firstResult = result.head
      firstResult.model.id mustEqual af1Model.id
      firstResult.annotations.length mustEqual 1
      firstResult.annotations(0).entity("material") mustEqual plasterMaterial.id.toString
      firstResult.annotations(0).entity("color") mustEqual redColor.id.toString
    }

    "return one model with three annotations with node types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory
      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(wigSchema)
          .addAttribute(wigSchema("material"), qualifierFactory.equality, woodMaterial.id.toString)
      )

      val result = annotationRepo.search(query2)
      result.length mustEqual 1
      val firstResult = result.head
      firstResult.model.id mustEqual af2Model.id
      firstResult.annotations.length mustEqual 3
    }
    "return two models with two annotations each with node types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory
      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(wigSchema)
          .addAttribute(wigSchema("color"), qualifierFactory.equality, redColor.id.toString)
      )

      val result = annotationRepo.search(query2)
      result.length mustEqual 2
    }

    // Whole model annotations
    "return one model with two location-less annotations with node types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory
      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(newLocationInstance)
          .addAttribute(newLocationInstance("location"), qualifierFactory.equality, thebes.id.toString)
          .addAttribute(newLocationInstance("comments"), qualifierFactory.like, "*2")
      )
      val query3 = query.addQualifier(
        qualifierFactory.createForSchema(eraInstanceSchema)
          .addAttribute(eraInstanceSchema("era"), qualifierFactory.equality, dynasty21.id.toString)
      )

      val result = annotationRepo.search(query3)
      result.length mustEqual 1
    }

    "return two models with one location-less annotation (on one) and three (on the other), with node types" in new AnnotationRepositoryRepositorySearchSpecIn {
      val query = annotationRepo.queryFactory.create
      val qualifierFactory = annotationRepo.qualifierFactory

      val query2 = query.addQualifier(
        qualifierFactory.createForSchema(locationInstanceSchema)
          .addAttribute(locationInstanceSchema("location"), qualifierFactory.equality, thebes.id.toString)
      )

      val result = annotationRepo.search(query2)
      result.length mustEqual 2

      val model1Result = result.filter(_.model.id == af1Model.id).head
      val model2Result = result.filter(_.model.id == af2Model.id).head

      model1Result.annotations.length mustEqual 3
      model2Result.annotations.length mustEqual 1
    }

  }
}
