package annotations

import java.util.UUID

import org.junit.runner.RunWith
import org.specs2.matcher.JsonMatchers
import org.specs2.mutable._
import org.specs2.runner._
import play.api.libs.json._
import play.api.mvc.Results
import play.api.test._
import play.api.test.Helpers._


/**
  * Created by daveshepard on 3/18/16.
  */
@RunWith(classOf[JUnitRunner])
class AnnotationControllerUpdateSpec extends PlaySpecification with Results with JsonMatchers {

  trait AnnotationControllerUpdateTest extends AnnotationControllerTest with Horus {
    val annotation = annotationRepo.save(Annotation(
      null, project, model,
      Entity(null, instanceOfDFSchema, Map(
        "figure" -> horus.id.toString,
        "figure color" -> "orange"
      )),
      List(
        (0.0, 4.0, 5.0),
        (1.0, 2.0, 3.0),
        (0.0, 4.0, 5.0)
      ),
      user,
      ""
    ))
  }

  "When updating, an AnnotationController" should {

    "update an annotation with a new entity" in new AnnotationControllerUpdateTest {
      val json = Json.parse(
        s"""
           |{
           |  "id" : "${annotation.id}",
           |  "location" : [
           |    [10.0, 4.0, 5.0], [1.0, 21.0, 3.0], [0.0, 4.0, 512.0]
           |  ],
           |  "entity" : {
           |    "id" : "${annotation.entity.id}",
           |    "schema" : { "id" : "${instanceOfDFSchema.id}", "title" : "${instanceOfDFSchema.title}" },
           |    "data" : {
           |      "figure" : "${osiris.id}",
           |      "figure color" : "red"
           |    }
           |  }
           |}
           """.stripMargin)
      val updateRequest = FakeRequest(PUT, individualFigureURL(annotation.id), FakeHeaders(), json)
        .withSession("userId" -> user.uuid.toString)

      val updateResponse = controller.update(
        project.id, model.id, instanceOfDFSchema.title, instanceOfDFSchema.id, annotation.id
      ).apply(updateRequest)

      status(updateResponse) mustEqual OK
      val createResponseJson = contentAsString(updateResponse)

      createResponseJson must /("entity") /("data") /("figure" -> osiris.id.toString)
      createResponseJson must /("entity") /("data") /("figure color" -> "red")

      val annotationId = (Json.parse(createResponseJson) \ "id").as[UUID]

      val getRequest = FakeRequest(GET, individualFigureURL(annotationId))
      val getResponse =
        controller.get(model.id, instanceOfDFSchema.title, instanceOfDFSchema.id, annotationId)(
        getRequest
      )
      status(getResponse) mustEqual OK
      val getResponseJson = contentAsString(getResponse)
      getResponseJson must /("id" -> annotationId.toString)
      getResponseJson must /("entity") /("schema") /("id" -> instanceOfDFSchema.id.toString)
      getResponseJson must /("entity") /("data") /("figure" -> osiris.id.toString)
      getResponseJson must /("entity") /("data") /("figure color" -> "red")

      getResponseJson must /("location").andHave(size(3))
    }

    "update an annotation with a new location" in new AnnotationControllerUpdateTest {
      val json = Json.parse(
        s"""
           |{
           |  "id" : "${annotation.id}",
           |  "location" : [
           |    [10.0, 4.0, 5.0], [1.0, 21.0, 3.0], [0.0, 4.0, 512.0]
           |  ],
           |  "entity" : {
           |    "id" : "${annotation.entity.id}",
           |    "schema" : { "id" : "${instanceOfDFSchema.id}", "title" : "${instanceOfDFSchema.title}" },
           |    "data" : {
           |      "figure" : "${annotation.entity("figure")}",
           |      "figure color" : "orange"
           |    }
           |  }
           |}
           """.stripMargin)
      val updateRequest = FakeRequest(PUT, individualFigureURL(annotation.id), FakeHeaders(), json)
        .withSession("userId" -> user.uuid.toString)

      val updateResponse = controller.update(project.id, model.id, instanceOfDFSchema.title, instanceOfDFSchema.id, annotation.id)(
          updateRequest
        )

      status(updateResponse) mustEqual OK
      val createResponseJson = contentAsString(updateResponse)

      createResponseJson must /("entity")/("data")/("figure" -> horus.id.toString)
      createResponseJson must /("entity")/("data") /("figure color" -> "orange")

      val annotationId = (Json.parse(createResponseJson) \ "id").as[UUID]

      val getRequest = FakeRequest(GET, individualFigureURL(annotationId))
      val getResponse =
        controller.get(model.id, instanceOfDFSchema.title, instanceOfDFSchema.id, annotationId)(
        getRequest
      )
      status(getResponse) mustEqual OK
      val getResponseJson = contentAsString(getResponse)
      getResponseJson must /("id" -> annotationId.toString)

      getResponseJson must /("entity")/("data")/("figure" -> horus.id.toString)
      getResponseJson must /("entity")/("data")/("figure color" -> "orange")

//      (getResponseJson \ "location")(0)(0) mustEqual JsNumber(10)
//      (getResponseJson \ "location")(0)(1) mustEqual JsNumber(4)
//      (getResponseJson \ "location")(0)(2) mustEqual JsNumber(5)
//
//      (getResponseJson \ "location")(1)(0) mustEqual JsNumber(1)
//      (getResponseJson \ "location")(1)(1) mustEqual JsNumber(21)
//      (getResponseJson \ "location")(1)(2) mustEqual JsNumber(3)
//
//      (getResponseJson \ "location")(2)(0) mustEqual JsNumber(0)
//      (getResponseJson \ "location")(2)(1) mustEqual JsNumber(4)
//      (getResponseJson \ "location")(2)(2) mustEqual JsNumber(512)
    }

    "reject an update to an annotation if it does not have an entity" in new AnnotationControllerUpdateTest {
      val json = Json.parse(
        s"""
           |{
           |  "id" : "${annotation.id}",
           |  "location" : [
           |    [10.0, 4.0, 5.0], [1.0, 21.0, 3.0], [0.0, 4.0, 512.0]
           |  ]
           |}
           """.stripMargin
      )
      val updateRequest = FakeRequest(PUT, individualFigureURL(annotation.id), FakeHeaders(), json)
        .withSession("userId" -> user.uuid.toString)

      val updateResponse =
        controller.update(project.id, model.id, instanceOfDFSchema.title, instanceOfDFSchema.id, annotation.id)(
        updateRequest
      )

      status(updateResponse) mustEqual BAD_REQUEST

      val annotationId = annotation.id

      val getRequest = FakeRequest(GET, individualFigureURL(annotationId))
      val getResponse =
        controller.get(model.id, instanceOfDFSchema.title, instanceOfDFSchema.id, annotationId)(
          getRequest
        )
      status(getResponse) mustEqual OK
      val getResponseJson = contentAsString(getResponse)
      getResponseJson must /("id" -> annotationId.toString)
      getResponseJson must /("entity") /("schema")/("id" -> instanceOfDFSchema.id.toString)
      getResponseJson must /("entity")/("data")/("figure" -> horus.id.toString)
      getResponseJson must /("entity")/("data")/("figure color" -> "orange")

//      (getResponseJson \ "location")(0)(0) mustEqual JsNumber(0)
//      (getResponseJson \ "location")(0)(1) mustEqual JsNumber(4)
//      (getResponseJson \ "location")(0)(2) mustEqual JsNumber(5)
//
//      (getResponseJson \ "location")(1)(0) mustEqual JsNumber(1)
//      (getResponseJson \ "location")(1)(1) mustEqual JsNumber(2)
//      (getResponseJson \ "location")(1)(2) mustEqual JsNumber(3)
//
//      (getResponseJson \ "location")(2)(0) mustEqual JsNumber(0)
//      (getResponseJson \ "location")(2)(1) mustEqual JsNumber(4)
//      (getResponseJson \ "location")(2)(2) mustEqual JsNumber(5)
    }

  }
}
