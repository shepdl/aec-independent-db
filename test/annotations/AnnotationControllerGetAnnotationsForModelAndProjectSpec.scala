package annotations

import java.util.UUID

import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.test._


@RunWith(classOf[JUnitRunner])
class AnnotationControllerGetAnnotationsForModelAndProjectSpec extends PlaySpecification with JsonMatchers {

  "When querying for annotations from models and projects, an AnnotationController" should {

    "retrieve all annotations for a model" in new AnnotationControllerTest {
      val artifacts = List(createMockArtifactWithFile, createMockArtifactWithFile)
      val models = artifacts.map(mediaRepo.getModelsForArtifact(_).head)

      val figures = List(
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 1", "origin" -> "Atlanta"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 2", "origin" -> "Bath"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 3", "origin" -> "Camden")))
      )

      val geometryPoints = 0 to figures.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })

      val figureAnnotations = figures.indices map(i =>
        annotationRepo.save(Annotation(null, project, models(i % 2),
          Entity(null, instanceOfFigureSchema,
            Map(
              "figure" -> figures(i).id.toString,
              "condiments" -> "something",
              "bottle count" -> i.toString
            )
          ),
          geometryPoints(i),
          user, ""
      )))

      val annotationsMapped = figureAnnotations.foldLeft(Map[UUID,Annotation]()) {
        case (acc, annotation) => acc.updated(annotation.id, annotation)
      }

      models.foreach(model => {
        val getRequest = FakeRequest(GET, s"/models/${model.id}/annotations")
        val getResponse = controller.getForModel(model.id)(getRequest)

        status(getResponse) mustEqual OK

//        val modelsJson = contentAsJson(getResponse)
        val figures = annotationsMapped.map(_._2.entity.data("figure"))
        val condiments = annotationsMapped.map(_._2.entity.data("condiments"))
        val bottleCounts = annotationsMapped.map(_._2.entity.data("bottle count"))

        val modelsJson = contentAsString(getResponse)
        modelsJson must */("figure").andHave(oneOf(figures))
        modelsJson must */("condiments").andHave(oneOf(condiments))
        modelsJson must */("bottle count").andHave(oneOf(bottleCounts))
        /*
        (modelsJson \ "annotations").as[Seq[JsObject]] foreach { annotation =>
          val annotationId = (annotation \ "id").as[Long]
          val originalAnnotation = annotationsMapped(annotationId)
          (annotation \ "entity" \ "data" \ "figure") mustEqual JsString(originalAnnotation.entity.data("figure"))
          (annotation \ "entity" \ "data" \ "condiments") mustEqual JsString(originalAnnotation.entity.data("condiments"))
          (annotation \ "entity" \ "data" \ "bottle count") mustEqual JsString(originalAnnotation.entity.data("bottle count"))
        }
        */

      })
    }

    "retrieve all annotations for a project" in new AnnotationControllerTest {
      val projects = List(createMockProject, createMockProject)

      val figures = List(
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 1", "origin" -> "Atlanta"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 2", "origin" -> "Bath"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 3", "origin" -> "Camden")))
      )

      val geometryPoints = 0 to figures.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })

      val figureAnnotations = figures.indices map(i =>
        annotationRepo.save(Annotation(null, projects(i % 2), model,
          Entity(null,
            instanceOfFigureSchema,
            Map(
              "figure" -> figures(i).id.toString,
              "condiments" -> "something",
              "bottle count" -> i.toString
            )
          ),
          geometryPoints(i),
          user, ""
        ))
      )

      val annotationsMapped = figureAnnotations.foldLeft(Map[UUID,Annotation]()) {
        case (acc, annotation) => acc.updated(annotation.id, annotation)
      }

      projects.foreach(project => {
        val getRequest = FakeRequest(GET, s"/activities/${project.id}/annotations")
        val getResponse = controller.getForProject(project.id)(getRequest)

        status(getResponse) mustEqual OK

        val figures = annotationsMapped.map(_._2.entity.data("figure"))
        val condiments = annotationsMapped.map(_._2.entity.data("condiments"))
        val bottleCounts = annotationsMapped.map(_._2.entity.data("bottle count"))

        val modelsJson = contentAsString(getResponse)
        modelsJson must */("figure").andHave(oneOf(figures))
        modelsJson must */("condiments").andHave(oneOf(condiments))
        modelsJson must */("bottle count").andHave(oneOf(bottleCounts))

        /*
        val modelsJson = contentAsJson(getResponse)
        (modelsJson \ "annotations").as[Seq[JsObject]] foreach { annotation =>
          val annotationId = (annotation \ "id").as[Long]
          val originalAnnotation = annotationsMapped(annotationId)
          (annotation \ "entity" \ "data" \ "figure") mustEqual JsString(originalAnnotation.entity.data("figure"))
          (annotation \ "entity" \ "data" \ "condiments") mustEqual JsString(originalAnnotation.entity.data("condiments"))
          (annotation \ "entity" \ "data" \ "bottle count") mustEqual JsString(originalAnnotation.entity.data("bottle count"))
        }
        */

      })
    }

    "retrieve all annotations for a model in a project" in new AnnotationControllerTest {
      val artifacts = List(createMockArtifactWithFile, createMockArtifactWithFile)
      val models = artifacts.map(mediaRepo.getModelsForArtifact(_).head)
      val projects = List(createMockProject, createMockProject)

      val figures = List(
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 1", "origin" -> "Atlanta"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 2", "origin" -> "Bath"))),
        entityRepo.save(Entity(null, figureSchema, Map("name" -> s"Figure 3", "origin" -> "Camden")))
      )

      val geometryPoints = 0 to figures.length map (i0 => {
        val i = i0.toDouble
        List((i, i, i), (i + 1, i + 1, i + 1), (i + 2, i + 2, i + 2), (i, i, i))
      })


      val figureAnnotations = List(
        annotationRepo.save(Annotation(null, projects(0), models(1),
          Entity(null,
            instanceOfFigureSchema,
            Map(
              "figure" -> figures(0).id.toString,
              "condiments" -> "something",
              "bottle count" -> 0.toString
            )
          ),
          geometryPoints(0),
          user, ""
        )),
        annotationRepo.save(Annotation(null, projects(0), models(1),
          Entity(null,
          instanceOfFigureSchema,
          Map(
            "figure" -> figures(1).id.toString,
            "condiments" -> "something",
            "bottle count" -> 2.toString
          )),
          geometryPoints(0),
          user, ""
        )),
        annotationRepo.save(Annotation(null, projects(1), models(0),
          Entity(null,
          instanceOfFigureSchema,
          Map(
            "figure" -> figures(2).id.toString,
            "condiments" -> "something",
            "bottle count" -> 2.toString
          )),
          geometryPoints(0),
          user, ""
        ))
      )

      /*
      val getRequestP0A0 = FakeRequest(GET, s"/activities/${projects(0).id}/models/${models(0).id}/annotations")
      val getResponseP0A0 = Helpers.call(annotationController.getForModelAndProject(models(0).id, projects(0).id), getRequestP0A0)
      status(getResponseP0A0) mustEqual OK

      val p0A0JsonAnnotations = (contentAsJson(getResponseP0A0) \ "annotations").as[Seq[JsObject]]
      p0A0JsonAnnotations.size mustEqual 0

      val getRequestP0A1 = FakeRequest(GET, s"/activities/${projects(0).id}/models/${models(1).id}/annotations")
      val getResponseP0A1 = Helpers.call(annotationController.getForModelAndProject(models(1).id, projects(0).id), getRequestP0A1)
      status(getResponseP0A1) mustEqual OK

      val p0A1JsonAnnotations = (contentAsJson(getResponseP0A1) \ "annotations").as[Seq[JsObject]]
      p0A1JsonAnnotations.size mustEqual 2
      (p0A1JsonAnnotations(0) \ "entity" \ "data" \ "figure") mustEqual JsString(figures(0).id.toString)
      (p0A1JsonAnnotations(0) \ "entity" \ "data" \ "condiments") mustEqual JsString("something")
      (p0A1JsonAnnotations(0) \ "entity" \ "data" \ "bottle count") mustEqual JsString(0.toString)

      (p0A1JsonAnnotations(1) \ "entity" \ "data" \ "figure") mustEqual JsString(figures(1).id.toString)
      (p0A1JsonAnnotations(1) \ "entity" \ "data" \ "condiments") mustEqual JsString("something")
      (p0A1JsonAnnotations(1) \ "entity" \ "data" \ "bottle count") mustEqual JsString(2.toString)


      val getRequestP1A0 = FakeRequest(GET, s"/activities/${projects(1).id}/models/${models(0).id}/annotations")
      val getResponseP1A0 = Helpers.call(annotationController.getForModelAndProject(models(0).id, projects(1).id), getRequestP1A0)
      status(getResponseP1A0) mustEqual OK

      val p1A0JsonAnnotations = (contentAsJson(getResponseP1A0) \ "annotations").as[Seq[JsObject]]
      p1A0JsonAnnotations.size mustEqual 1
      (p1A0JsonAnnotations(0) \ "entity" \ "data" \ "figure") mustEqual JsString(figures(2).id.toString)
      (p1A0JsonAnnotations(0) \ "entity" \ "data" \ "condiments") mustEqual JsString("something")
      (p1A0JsonAnnotations(0) \ "entity" \ "data" \ "bottle count") mustEqual JsString(2.toString)


      val getRequestP1A1 = FakeRequest(GET, s"/activities/${projects(1).id}/models/${models(1).id}/annotations")
      val getResponseP1A1 = Helpers.call(annotationController.getForModelAndProject(models(1).id, projects(1).id), getRequestP1A1)
      status(getResponseP1A1) mustEqual OK

      val p1A1JsonAnnotations = (contentAsJson(getResponseP1A1) \ "annotations").as[Seq[JsObject]]
      (p1A1JsonAnnotations).size mustEqual 0
      */

    }

  }

}
