package annotations

import java.util.UUID

import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.mvc.Results
import play.api.test._


@RunWith(classOf[JUnitRunner])
class EntityControllerCreateSpec extends PlaySpecification with Results with JsonMatchers {

  "When creating, an Entity Controller" should {
    "create and retrieve an entity if it corresponds to a schema with only simple types" in new EntityControllerTest {
      val data = Map(
        "name" -> Seq("Horus"),
        "origin" -> Seq("Thebes")
      )
      val url = s"/activities/${mockProject.id}/${figureSchema.title}-${figureSchema.id}"
      val newEntityRequest = FakeRequest(POST, url, FakeHeaders(), data)
        .withSession("userId" -> user.uuid.toString)

      val createResponse = controller.create(mockProject.id, figureSchema.title, figureSchema.id)(newEntityRequest)

      status(createResponse) mustEqual CREATED
      val responseJson = contentAsJson(createResponse)
      val entityId = (responseJson \ "id").as[UUID]
      val responseToCheck = contentAsString(createResponse)
      responseToCheck must /("data") /("name" -> data("name").head)
      responseToCheck must /("data") /("origin" -> data("origin").head)

      val getRequest = FakeRequest(GET, s"/${figureSchema.title}-${figureSchema.id}/$entityId").withHeaders(("Fake", "Fake"))
      val getResponse = controller.getById(figureSchema.title, figureSchema.id, entityId)(getRequest)
      val getResponseJson = contentAsString(getResponse)
      getResponseJson must /("id" -> entityId.toString)
      getResponseJson must /("data")/("name" -> data("name").head)
      getResponseJson must /("data")/("origin" -> data("origin").head)
    }

    "create and retrieve an entity if it corresponds to a schema with reference and simple types" in new EntityControllerTest {
      val data = Map(
        "name" -> "Horus",
        "origin" -> "Thebes"
      )

      val horus = entityRepo.save(Entity(null, figureSchema, data))

      val instance = Map(
        "figure" -> Seq(horus.id.toString),
        "figure color" -> Seq("red")
      )

      val newEntityRequest = FakeRequest(
        POST,
        s"/activities/${mockProject.id}/${instanceOfDFSchema.title}-${instanceOfDFSchema.id}",
        FakeHeaders(),
        instance
      )
        .withSession("userId" -> user.uuid.toString)

      val createResponse = controller.create(mockProject.id, instanceOfDFSchema.title, instanceOfDFSchema.id)(newEntityRequest)

      status(createResponse) mustEqual CREATED
      val responseJson = contentAsJson(createResponse)
      val entityId = (responseJson \ "id").as[UUID]
      val responseToCheck = contentAsString(createResponse)
      responseToCheck must /("data")/("figure" -> instance("figure").head)
      responseToCheck must /("data")/("figure color" -> instance("figure color").head)

      val getRequest = FakeRequest(GET, s"/${instanceOfDFSchema.title}-${instanceOfDFSchema.id}/$entityId").withHeaders(("Fake", "Fake"))
      val getResponse = controller.getById(instanceOfDFSchema.title, instanceOfDFSchema.id, entityId)(getRequest)
      val getResponseJson = contentAsString(getResponse)
      println(getResponseJson)
      getResponseJson must /("id" -> entityId.toString)
      getResponseJson must /("data") /("figure" -> instance("figure").head)
      getResponseJson must /("data") /("figure color" -> instance("figure color").head)
    }

    "return a Bad Request when an entity has an extra field" in new EntityControllerTest {
      val data = Map(
        "name" -> "Horus",
        "origin" -> "Thebes"
      )

      val horus = entityRepo.save(Entity(null, figureSchema, data))

      val instance = Map(
        "divine figure" -> Seq(horus.id.toString),
        "color" -> Seq("red"),
        "an extra field" -> Seq("Whoops!")
      )

      val newEntityRequest = FakeRequest(POST,
        s"/activities/${mockProject.id}/${instanceOfDFSchema.title}-${instanceOfDFSchema.id}",
        FakeHeaders(),
        instance
      )
        .withSession("userId" -> user.uuid.toString)

      val createResponse = controller.create(mockProject.id, instanceOfDFSchema.title, instanceOfDFSchema.id)(newEntityRequest)

      status(createResponse) mustEqual BAD_REQUEST
    }

    "return a Bad Request when an entity is missing a field" in new EntityControllerTest {
      val data = Map(
        "name" -> "Horus",
        "origin" -> "Thebes"
      )

      val horus = entityRepo.save(Entity(null, figureSchema, data))
      val instance = Map(
        "divine figure" -> Seq(horus.id.toString)
      )

      val newEntityRequest = FakeRequest(POST,
        s"/activities/${mockProject.id}/${instanceOfDFSchema.title}-${instanceOfDFSchema.id}",
        FakeHeaders(),
        instance
      )
        .withSession("userId" -> user.uuid.toString)

      val createResponse = controller.create(mockProject.id, instanceOfDFSchema.title, instanceOfDFSchema.id)(newEntityRequest)

      status(createResponse) mustEqual BAD_REQUEST
    }

  }

}




