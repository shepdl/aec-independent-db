package annotations

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class EntityRepositoryDeleteSpec extends Specification {

  "When deleting an entity, an Entity Repository" should {
    "delete an entity with simple types and all its relations" in new EntityRepoTest {
      val data = Map(
        "name" -> "Horus",
        "origin" -> "Thebes"
      )

      val entityCreated = entityRepo.save(Entity(null, figureSchema, data))

      val deletionStatus = entityRepo.delete(entityCreated, user)

      deletionStatus mustEqual true


      entityRepo.getById(entityCreated.id) match {
        case Some(entity) => failure("Entity not deleted")
        case None => success("Entity successfully deleted")
      }

    }

    "delete an entity with reference types but not delete the things being referenced" in new EntityRepoTest with Horus {

      val idf = entityRepo.save(Entity(null, instanceOfDFSchema, Map(
        "figure" -> horus.id.toString,
        "figure color" -> "green")
      ))

      val deletionStatus = entityRepo.delete(idf, user)

      entityRepo.getById(idf.id) match {
        case Some(entity) => failure("Instance of divine figure not deleted")
        case None => success("Instance of divine figure successfully deleted")
      }

      entityRepo.getById(horus.id) match {
        case Some(entity) => success("Divine figure still present")
        case None => success("No divine figure found")
      }
    }
  }

}
