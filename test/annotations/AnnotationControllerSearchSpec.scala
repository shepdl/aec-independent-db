package annotations

import accessControl.GroupRepository
import annotations.search.AnnotationRepositoryRepositorySearchSpecIn
import backend.approvals.ApprovalRepository
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.libs.json._
import play.api.mvc.Controller
import play.api.test.Helpers._
import play.api.test._
import projects.ProjectRepository
import schemas.SchemaRepository


@RunWith(classOf[JUnitRunner])
class AnnotationControllerSearchSpec extends PlaySpecification with JsonMatchers {

  "An AnnotationController Search Spec" should {

    "return one model with one annotation with base types" in new AnnotationControllerSearchSpecIn {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${figureSchema.id}",
           |        "fieldName" : "${figureSchema("name").id}",
           |        "queryType" : "equality",
           |        "value" : "${elvis.data("name")}"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin)

      response must /("result") /#(0) /("model") /("id" -> af1Model.id.toString)
      response must /("result") /#(0)/("annotations")/#(0)/("entity")/("schema")/("id" -> figureSchema.id.toString)
      response must /("result") /#(0)/("annotations")/#(0)/("entity")/("data")/("name" -> elvis.data("name"))
    }

    "return one model with three annotations with base types" in new AnnotationControllerSearchSpecIn {
      val comments = "Evidence"
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${newLocationInstance.id}",
           |        "fieldName" : "${newLocationInstance("comments").id}",
           |        "queryType" : "like",
           |        "value" : ".*${comments}.*"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin)

      response must /("result")/#(0)/("model")/("id" -> af1Model.id.toString)
    }

    "not return any results when an equality qualifier is used on a text match" in new AnnotationControllerSearchSpecIn {
      val figureName = "Horus"
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${figureSchema.id}",
           |        "fieldName" : "${figureSchema("name").id}",
           |        "queryType" : "equality",
           |        "value" : ".*${figureName}.*"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin)

      val modelIds = response must */("result").andHave(size(0))

    }

    "return two models with one annotation each with base types" in new AnnotationControllerSearchSpecIn {
      val figureName = "Horus"
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${figureSchema.id}",
           |        "fieldName" : "${figureSchema("name").id}",
           |        "queryType" : "like",
           |        "value" : ".*${figureName}.*"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin)

      response must */("model")/("id" -> contain(af1Model.id.toString))
      response must */("model")/("id" -> contain(af2Model.id.toString))

    }


    // Node types
    "return one model with one annotation with node types" in new AnnotationControllerSearchSpecIn {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${wigSchema.id}",
           |        "fieldName" : "${wigSchema("material").id}",
           |        "queryType" : "equality",
           |        "value" : "${plasterMaterial.id}"
           |      },
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${wigSchema.id}",
           |        "fieldName" : "${wigSchema("color").id}",
           |        "queryType" : "equality",
           |        "value" : "${redColor.id}"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin
      )

      response must /("result")/#(0)/("annotations")/#(0)/("entity")/("data")/("material" -> plasterMaterial.id.toString)
      response must /("result")/#(0)/("annotations")/#(0)/("entity")/("data")/("color" -> redColor.id.toString)
    }

    "return one model with three annotations with node types" in new AnnotationControllerSearchSpecIn {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${wigSchema.id}",
           |        "fieldName" : "${wigSchema("material").id}",
           |        "queryType" : "equality",
           |        "value" : "${woodMaterial.id}"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin
      )

      response must */("annotations").andHave(size(3))

    }

    "return two models with two annotations each with node types" in new AnnotationControllerSearchSpecIn {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${wigSchema.id}",
           |        "fieldName" : "${wigSchema("color").id}",
           |        "queryType" : "equality",
           |        "value" : "${redColor.id}"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must /("result").andHave(size(2))
    }

    // Whole model annotations
    "return one model with two location-less annotations with node types and one base type" in new AnnotationControllerSearchSpecIn {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${newLocationInstance.id}",
           |        "fieldName" : "${newLocationInstance("location").id}",
           |        "queryType" : "equality",
           |        "value" : "${thebes.id}"
           |      },
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${newLocationInstance.id}",
           |        "fieldName" : "${newLocationInstance("comments").id}",
           |        "queryType" : "like",
           |        "value" : ".*2"
           |      },
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${eraInstanceSchema.id}",
           |        "fieldName" : "${eraInstanceSchema("era").id}",
           |        "queryType" : "equality",
           |        "value" : "${dynasty21.id}"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must /("result").andHave(size(1))
    }

    "return one model with two location-less annotations, one with a node type and one with a base type" in new AnnotationControllerSearchSpecIn {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${newLocationInstance.id}",
           |        "fieldName" : "${newLocationInstance("comments").id}",
           |        "queryType" : "like",
           |        "value" : ".*2"
           |      },
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${eraInstanceSchema.id}",
           |        "fieldName" : "${eraInstanceSchema("era").id}",
           |        "queryType" : "equality",
           |        "value" : "${dynasty21.id}"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must /("result").andHave(size(1))
    }

    "return two models with one location-less annotation (on one) and three (on the other), with node types" in new AnnotationControllerSearchSpecIn {
      val response = doSearch(
        s"""
           |{
           |  "request" : {
           |    "project" : [],
           |    "entities" : [
           |      {
           |        "type" : "quality",
           |        "schemaId" : "${locationInstanceSchema.id}",
           |        "fieldName" : "${locationInstanceSchema("location").id}",
           |        "queryType" : "equality",
           |        "value" : "${thebes.id}"
           |      }
           |    ]
           |  }
           |}
         """.stripMargin
      )
      response must /("result").andHave(size(2))
    }

  }

}

trait AnnotationControllerSearchSpecIn extends AnnotationRepositoryRepositorySearchSpecIn {

  val controller = new AnnotationSearchController with Controller {
    override val entityRepo: EntityRepository = app.injector.instanceOf[EntityRepository]
    override val projectRepo = app.injector.instanceOf[ProjectRepository]
    override val schemaRepo: SchemaRepository = app.injector.instanceOf[SchemaRepository]
    override val annotationRepo: AnnotationRepository = app.injector.instanceOf[AnnotationRepository]
    override val groupRepo = app.injector.instanceOf[GroupRepository]
    override val approvalRepo = app.injector.instanceOf[ApprovalRepository]
  }

  def doSearch(jsonBody:String) = {
    val searchRequest = FakeRequest(POST, "/annotations/search").withJsonBody(Json.parse(jsonBody))
    contentAsString(Helpers.call(controller.search, searchRequest))
  }
}
