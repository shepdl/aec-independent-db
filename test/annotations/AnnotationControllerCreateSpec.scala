package annotations

import java.util.UUID

import artifacts.{HasArtifactRepo, HasMediaRepo}
import common.HasDatabase
import org.junit.runner.RunWith
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.libs.json.{JsNumber, JsString, Json}
import play.api.mvc.{Controller, Results}
import play.api.test.{FakeRequest, _}
import projects.HasProjectRepo

/**
  * Created by daveshepard on 3/19/16.
  */
@RunWith(classOf[JUnitRunner])
class AnnotationControllerCreateSpec extends PlaySpecification with Results with JsonMatchers {

  trait AnnotationControllerCreateTest extends AnnotationControllerTest with Horus

  "When creating, an AnnotationController" should {
    "create and retrieve an annotation tied to a specific location if that annotation matches the schema and has only simple types" in new AnnotationControllerCreateTest {
      val data = Map(
        "data[0].name" -> Seq("name"),
        "data[0].value" -> Seq("Horus"),
        "data[1].name" -> Seq("origin"),
        "data[1].value" -> Seq("Thebes"),
        "location[0].x" -> Seq("0.0"),
        "location[0].y" -> Seq("4.0"),
        "location[0].z" -> Seq("5.0"),
        "location[1].x" -> Seq("1.0"),
        "location[1].y" -> Seq("2.0"),
        "location[1].z" -> Seq("3.0"),
        "location[2].x" -> Seq("0.0"),
        "location[2].y" -> Seq("4.0"),
        "location[2].z" -> Seq("5.0")
      )

      val createRequest = FakeRequest(POST, createFigureURL, FakeHeaders(), data)
        .withSession("userId" -> user.uuid.toString)
      val createResponse = controller.create(
        project.id, model.id, figureSchema.title, figureSchema.id
      )(createRequest)

      status(createResponse) mustEqual CREATED
      val createResponseJson = contentAsString(createResponse)

      createResponseJson must /("entity")/("data") /("name" -> horus("name"))
      createResponseJson must /("entity")/("data") /("origin" -> horus("origin"))

      createResponseJson must /("location").andHave(size(3))
//      createResponseJson must /("location")/#(0)/#(0) -> 0.0)
//      createResponseJson must /("location")/#(0)/#(0 -> 4.0)
//      createResponseJson must /("location")/#(0)/#(0 -> 5.0)

//      createResponseJson must /("location").andHave(exactly(0.0, 4.0, 5.0))
//      (createResponseJson \ "location")(0)(0) mustEqual JsNumber(0.0)
//      (createResponseJson \ "location")(0)(1) mustEqual JsNumber(4.0)
//      (createResponseJson \ "location")(0)(2) mustEqual JsNumber(5.0)

//      (createResponseJson \ "location")(1)(0) mustEqual JsNumber(1.0)
//      (createResponseJson \ "location")(1)(1) mustEqual JsNumber(2.0)
//      (createResponseJson \ "location")(1)(2) mustEqual JsNumber(3.0)
//      (createResponseJson \ "location")(2)(0) mustEqual JsNumber(0.0)
//      (createResponseJson \ "location")(2)(1) mustEqual JsNumber(4.0)
//      (createResponseJson \ "location")(2)(2) mustEqual JsNumber(5.0)

      val annotationId = (Json.parse(createResponseJson) \ "id").as[UUID]

      val getRequest = FakeRequest(GET, individualFigureURL(annotationId))
      val getResponse = controller.get(
        model.id, figureSchema.title, figureSchema.id, annotationId
      )(getRequest)

      status(getResponse) mustEqual OK
      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("id" -> annotationId.toString)
      getResponseJson must /("entity")/("schema")/("id" -> figureSchema.id.toString)
      getResponseJson must /("entity")/("data")/("name" -> horus("name"))
      getResponseJson must /("entity")/("data")/("origin" -> horus("origin"))

//        /("location")/#(0).andHave(exactly(0.0, 4.0, 5.0))
      // TODO: implement location

//        /("location").andHave(exactly(
//          Seq(0.0, 4.0, 5.0),
//          Seq(1.0, 2.0, 3.0),
//          Seq(0.0, 4.0, 5.0)
//        ))

    }

    "create and retrieve an annotation tied to a specific location if that annotation matches the schema and has reference types" in new AnnotationControllerCreateTest {
      val instance = Map(
        "data[0].name" -> Seq("figure"),
        "data[0].value" -> Seq(horus.id.toString),
        "data[1].name" -> Seq("condiments"),
        "data[1].value" -> Seq("sirachia"),
        "data[2].name" -> Seq("bottle count"),
        "data[2].value" -> Seq("6"),
        "location[0].x" -> Seq("0.0"),
        "location[0].y" -> Seq("4.0"),
        "location[0].z" -> Seq("5.0"),
        "location[1].x" -> Seq("1.0"),
        "location[1].y" -> Seq("2.0"),
        "location[1].z" -> Seq("3.0"),
        "location[2].x" -> Seq("0.0"),
        "location[2].y" -> Seq("4.0"),
        "location[2].z" -> Seq("5.0")
      )

      val createRequest = FakeRequest(POST, createInstanceURL, FakeHeaders(), instance)
        .withSession("userId" -> user.uuid.toString)
      val createResponse = controller.create(
        project.id, model.id, instanceOfFigureSchema.title, instanceOfFigureSchema.id
      )(createRequest)

      status(createResponse) mustEqual CREATED
      val createResponseJson = contentAsString(createResponse)

      createResponseJson must /("entity")/("data") /("figure" -> horus.id.toString)
      createResponseJson must /("entity")/("data") /("condiments" -> "sirachia")
      createResponseJson must /("entity")/("data") /("bottle count" -> "6")

      val annotationId = (Json.parse(createResponseJson) \ "id").as[UUID]

      val getRequest = FakeRequest(GET, individualFigureURL(annotationId))
      val getResponse = controller.get(model.id, figureSchema.title, figureSchema.id, annotationId)(
        getRequest
      )
      status(getResponse) mustEqual OK
      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("id" -> annotationId.toString)
      getResponseJson must /("entity") /("schema")/("id" -> instanceOfFigureSchema.id.toString)
      getResponseJson must /("entity") /("data") /("figure" -> horus.id.toString)
      getResponseJson must /("entity") /("data") /("condiments" -> "sirachia")
      getResponseJson must /("entity") /("data") /("bottle count" -> "6")
    }

    "reject an annotation if it does not refer to an existing artifact" in new AnnotationControllerTest {
      val horus = Map(
        "data.name" -> Seq("Horus"),
        "data.origin" -> Seq("Thebes"),
        "location[0].x" -> Seq("0.0"),
        "location[0].y" -> Seq("4.0"),
        "location[0].z" -> Seq("5.0"),
        "location[1].x" -> Seq("1.0"),
        "location[1].y" -> Seq("2.0"),
        "location[1].z" -> Seq("3.0"),
        "location[2].x" -> Seq("0.0"),
        "location[2].y" -> Seq("4.0"),
        "location[2].z" -> Seq("5.0")
      )

      val createRequest = FakeRequest(POST, createFigureURL, FakeHeaders(), horus)
        .withSession("userId" -> user.uuid.toString)
      val createResponse =
        controller.create(UUID.randomUUID(), model.id, figureSchema.title, figureSchema.id)(
        createRequest
      )

      status(createResponse) mustEqual NOT_FOUND
      val createResponseJson = contentAsString(createResponse)
      createResponseJson must /("message" -> s"Project does not exist")
    }

    "reject an annotation if it does not refer to an existing project" in new AnnotationControllerTest {

      val horus = Map(
        "data.name" -> Seq("Horus"),
        "data.origin" -> Seq("Thebes"),
        "location[0].x" -> Seq("0.0"),
        "location[0].y" -> Seq("4.0"),
        "location[0].z" -> Seq("5.0"),
        "location[1].x" -> Seq("1.0"),
        "location[1].y" -> Seq("2.0"),
        "location[1].z" -> Seq("3.0"),
        "location[2].x" -> Seq("0.0"),
        "location[2].y" -> Seq("4.0"),
        "location[2].z" -> Seq("5.0")
      )

      val createRequest = FakeRequest(POST, createFigureURL, FakeHeaders(), horus)
        .withSession("userId" -> user.uuid.toString)
      val createResponse =
        controller.create(UUID.randomUUID(), model.id, figureSchema.title, figureSchema.id)(
        createRequest
      )

      status(createResponse) mustEqual NOT_FOUND
      val createResponseJson = contentAsString(createResponse)
      createResponseJson must /("message" -> s"Project does not exist")
    }

  }

}


