package annotations

import accessControl.{BasicUser, HasUserRepository, User}
import common.TestIHVApplication
import play.api.Application
import projects.HasProjectRepo
import schemas.{HasSchemaRepo, Schema, SchemaField, SchemaRepository}

/**
  * Created by dave on 3/17/16.
  */
trait HasEntityRepo {
  val app:Application

  val entityRepo = app.injector.instanceOf[EntityRepository]
}

class EntityRepoTest extends TestIHVApplication with HasUserRepository with BasicUser
  with HasProjectRepo with HasSchemaRepo with HasEntityRepo with EntityTestCaseSchemas

trait EntityTestCaseSchemas {

  val schemaRepo:SchemaRepository
  val stringSchemaNode:Schema
  val user:User

  val figureSchema = schemaRepo.save(
    Schema(null, "Divine Figure", "entity", List(
      SchemaField(null, "name", stringSchemaNode, false, List()),
      SchemaField(null, "origin", stringSchemaNode, false, List())
    ), false, user))

  val eraSchema = schemaRepo.save(
    Schema(null, "Era", "entity", List(
      SchemaField(null, "name", stringSchemaNode, false, List())
    ), false, user)
  )

  val builtDuringSchema = schemaRepo.save(Schema(
    null, "Built During", "entity", List(
      SchemaField(null, "era", eraSchema, false, List())
    ), false, user)
  )

  val instanceOfDFSchema = schemaRepo.save(Schema(null, "Divine Figure Instance", "annotation", List(
    SchemaField(null, "figure", figureSchema, false, List()),
    SchemaField(null, "figure color", stringSchemaNode, false, List())
  ), false, user)
  )

}
