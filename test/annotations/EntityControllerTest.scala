package annotations

import accessControl.{AuthenticationService, BasicUser, HasUserRepository}
import common.TestIHVApplication
import play.api.mvc.Controller
import projects.{HasProjectRepo, ProjectRepository, ProjectService}
import schemas.{HasSchemaRepo, Schema, SchemaField, SchemaRepository}

/**
  * Created by dave on 3/17/16.
  */
trait EntityControllerTest extends TestIHVApplication with HasUserRepository with BasicUser
  with HasProjectRepo with HasSchemaRepo with HasEntityRepo with EntityTestCaseSchemas {

  val mockProject = createMockProject

  val controller = new EntityController with Controller {
    override val entityRepo: EntityRepository = app.injector.instanceOf[EntityRepository]
    override val schemaRepo: SchemaRepository = app.injector.instanceOf[SchemaRepository]
    override val authenticationService: AuthenticationService = app.injector.instanceOf[AuthenticationService]
    override val projectService = app.injector.instanceOf[ProjectService]
  }

}
