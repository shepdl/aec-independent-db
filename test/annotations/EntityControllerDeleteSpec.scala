package annotations

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.libs.json.JsString
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class EntityControllerDeleteSpec extends PlaySpecification with JsonMatchers {

  trait EntityControllerDeleteTest extends EntityControllerTest with Horus

  "When deleting, an entity controller" should {

    "delete an entity with simple and reference types, but not delete the things being referenced" in new EntityControllerDeleteTest {
      val idf = entityRepo.save(Entity(
        null, instanceOfDFSchema, Map(
          "figure" -> horus.id.toString,
          "figure color" -> "green"
        ))
      )

      val deleteRequest = FakeRequest(DELETE, s"/${instanceOfDFSchema.title}-${instanceOfDFSchema.id}/${idf.id}").withHeaders(("Fake", "Fake"))

      val deleteResponse = controller.delete(instanceOfDFSchema.title, instanceOfDFSchema.id, idf.id)(deleteRequest)

      status(deleteResponse) mustEqual OK
      val deleteJson = contentAsString(deleteResponse)
      deleteJson must /("message" -> s"${instanceOfDFSchema.title} #${instanceOfDFSchema.id} deleted")

      entityRepo.getById(idf.id) match {
        case Some(entity) => failure("Instance of divine figure not deleted")
        case None => success("Instance of divine figure successfully deleted")
      }

      entityRepo.getById(horus.id) match {
        case Some(entity) => success("Divine figure still present")
        case None => success("No divine figure found")
      }

      val getRequest = FakeRequest(GET, s"/${figureSchema.title}-${figureSchema.id}/${idf.id}").withHeaders(("Fake", "Fake"))
      val getResponse = controller.getById(figureSchema.title, figureSchema.id, idf.id)(getRequest)
      status(getResponse) mustEqual NOT_FOUND
    }

  }

}
