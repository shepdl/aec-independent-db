package annotations

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._
import schemas.{Pass, Schema, SchemaField}


@RunWith(classOf[JUnitRunner])
class EntityRepositoryUpdateSpec extends Specification {

  "When updating an entity, the Entity Repository" should {

    "update an entity with new simple values"  in new EntityRepoTest {

      val data = Map(
        "name" -> "Horus",
        "origin" -> "Thebes"
      )

      figureSchema.validate(data) match {
        case Pass => success
        case _ => failure
      }

      val entityCreated = entityRepo.save(Entity(null, figureSchema, data))

      entityCreated.schema.id mustEqual figureSchema.id
      entityCreated.data("name") mustEqual data("name")
      entityCreated.data("origin") mustEqual data("origin")

      val entityRetrievedOption = entityRepo.getById(entityCreated.id)

      entityRetrievedOption mustNotEqual None

      val entityRetrieved = entityRetrievedOption.get
      entityRetrieved.schema.id mustEqual figureSchema.id
      entityRetrieved.data("name") mustEqual data("name")
      entityRetrieved.data("origin") mustEqual data("origin")

      val newData = Map(
        "name" -> "Horus 2: Electric Boogaloo",
        "origin" -> "Cairo"
      )

      val updatedEntity = Entity(entityRetrieved.id, figureSchema, newData)

      entityRepo.save(updatedEntity)

      val entityRetrieved1 = entityRepo.getById(entityRetrieved.id).get
      entityRetrieved1.schema.id mustEqual figureSchema.id
      entityRetrieved1.data("name") mustEqual newData("name")
      entityRetrieved1.data("origin") mustEqual newData("origin")

    }

    "update an entity with new reference values" in new EntityRepoTest with Horus {
      val df1Data = Map(
        "figure" -> horus.id.toString,
        "figure color" -> "Thebes"
      )

      val df2Data = Map(
        "figure" -> osiris.id.toString,
        "figure color" -> "Cairo"
      )

      val df2 = entityRepo.save(Entity(null, instanceOfDFSchema, df2Data))

      val df1 = entityRepo.save(Entity(null, instanceOfDFSchema, df1Data))
      val iDFData = Map("figure" -> df1.id.toString, "figure color" -> "green")
      val dfReference = entityRepo.save(Entity(null, instanceOfDFSchema, iDFData))
      val updateData = Map(
        "figure" -> df2.id.toString,
        "figure color" -> "gold"
      )
      val dfReferenceUpdate = Entity(dfReference.id, instanceOfDFSchema, updateData)

      val updatedDF2 = entityRepo.save(dfReferenceUpdate)
      updatedDF2.schema.id mustEqual instanceOfDFSchema.id
      updatedDF2.data("figure") mustEqual updateData("figure")
      updatedDF2.data("figure color") mustEqual updateData("figure color")

    }


//    "delete an entity"
  }

}
