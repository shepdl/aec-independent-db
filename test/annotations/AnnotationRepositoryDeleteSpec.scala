package annotations

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._

import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class AnnotationRepositoryDeleteSpec extends Specification {

  "When deleting, an Annotation Repository" should {

    trait AnnotationRepositoryDeleteTest extends AnnotationRepositoryTest with ModelTest {
      val eras = List(4,5,6,7).map(i => entityRepo.save(
        Entity(null, eraSchema, Map("name" -> s"Era ${i}"))
      ))
      val geometryPoints = List((0.0, 0.0, 0.0), (1.0, 1.0, 1.0), (2.0, 2.0, 2.0), (0.0, 0.0, 0.0))
      val data = Map( "era" -> eras(0).id.toString )

      val original = annotationRepo.save(
        Annotation(
          null,
          project, model,
          Entity(null, builtDuringSchema, data),
          geometryPoints,
          user,
          ""
        )
      )
    }

    "delete an annotation and not be able to retrieve a deleted annotation" in new AnnotationRepositoryDeleteTest {

      annotationRepo.delete(original, user)

      annotationRepo.getById(original.id) match {
        case Some(annotation) => failure("Retrieved a deleted annotation")
        case None => success
      }

    }

  }

}
