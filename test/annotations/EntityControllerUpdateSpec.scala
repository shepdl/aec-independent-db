package annotations

import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.libs.json.{JsString, Json}
import play.api.test._
import play.api.test.Helpers._
import schemas.Schema

trait Horus {
  val entityRepo:EntityRepository
  val figureSchema:Schema

  val horus = entityRepo.save(Entity(
    null, figureSchema, Map(
      "name" -> "Horus",
      "origin" -> "Thebes"
    )
  ))

  val osiris = entityRepo.save(Entity(
      null, figureSchema, Map(
      "name" -> "Osiris",
      "origin" -> "Cairo"
    )
  ))

}

@RunWith(classOf[JUnitRunner])
class EntityControllerUpdateSpec extends PlaySpecification with JsonMatchers {

  trait EntityControllerUpdateTest extends EntityControllerTest with Horus

  "When update, an Entity Controller" should {
    "update an entity with only simple types" in new EntityControllerUpdateTest {

      val data = Json.parse(
          s"""
             |{
             |  "id" : "${horus.id}",
             |  "schema" : { "id" : "${figureSchema.id}", "title" : "${figureSchema.title}"},
             |  "data" : {
             |    "name" : "Bob",
             |    "origin" : "Atlanta"
             |  }
             |}
           """.stripMargin
        )
      val updateRequest = FakeRequest(PUT, s"/${figureSchema.title}-${figureSchema.id}/${horus.id}", FakeHeaders(), data)
        .withSession("userId" -> user.uuid.toString)

      val updateResponse = controller.update(figureSchema.title, figureSchema.id, horus.id)(updateRequest)

      status(updateResponse) mustEqual OK

      val entityId = horus.id
      val getRequest = FakeRequest(GET, s"/${figureSchema.title}-${figureSchema.id}/$entityId").withHeaders(("Fake", "Fake"))
      val getResponse = Helpers.call(controller.getById(figureSchema.title, figureSchema.id, entityId), getRequest)
      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("id" -> entityId.toString)
      getResponseJson must /("data") /("name" -> "Bob")
      getResponseJson must /("data") /("origin" -> "Atlanta")

    }

    "update an entity with simple and reference types" in new EntityControllerUpdateTest {

      val body = Json.parse(
          s"""
             |{
             |  "id" : "${horus.id}",
             |  "schema" : { "id" : "${instanceOfDFSchema.id}", "title" : "${instanceOfDFSchema.title}"},
             |  "data" : {
             |    "figure" : "${osiris.id}",
             |    "figure color" : "gold"
             |  }
             |}
           """.stripMargin
        )
      val updateRequest = FakeRequest(PUT, s"/${instanceOfDFSchema.title}-${instanceOfDFSchema.id}/${horus.id}", FakeHeaders(), body)
        .withSession("userId" -> user.uuid.toString)

      val updateResponse = controller.update(instanceOfDFSchema.title, instanceOfDFSchema.id, horus.id)(updateRequest)

      status(updateResponse) mustEqual OK
      val updateResponseJson = contentAsString(updateResponse)
      updateResponseJson must /("id" -> horus.id.toString)
      updateResponseJson must /("schema") /("id" -> instanceOfDFSchema.id.toString)
      updateResponseJson must /("data") /("figure" -> osiris.id.toString)
      updateResponseJson must /("data") /("figure color" -> "gold")

      val entityId = horus.id
      val getRequest = FakeRequest(GET, s"/${instanceOfDFSchema.title}-${instanceOfDFSchema.id}/$entityId").withHeaders(("Fake", "Fake"))
      val getResponse = controller.getById(instanceOfDFSchema.title, instanceOfDFSchema.id, entityId)(getRequest)
      val getResponseJson = contentAsString(getResponse)

      getResponseJson must /("id" -> entityId.toString)
      getResponseJson must /("data") / ("figure" -> osiris.id.toString)
      getResponseJson must /("data") / ("figure color" -> "gold")
    }

    "not update and return a Bad Request when an entity has an extra field" in new EntityControllerUpdateTest {

      val data = Json.parse(
          s"""
             |{
             |  "id" : "${osiris.id}",
             |  "schema" : { "id" : "${figureSchema.id}", "title" : "${figureSchema.title}"},
             |  "data" : {
             |    "name" : "Osiris 2",
             |    "origin" : "Cairo",
             |    "extra field" : "whoops!"
             |  }
             |}
           """.stripMargin
        )
      val updateRequest = FakeRequest(PUT, s"/${figureSchema.title}-${figureSchema.id}/${osiris.id}", FakeHeaders(), data)
        .withSession("userId" -> user.uuid.toString)

      val updateResponse = controller.update(figureSchema.title, figureSchema.id, osiris.id)(updateRequest)

      status(updateResponse) mustEqual BAD_REQUEST

      val entityId = osiris.id
      val getRequest = FakeRequest(GET, s"/${figureSchema.title}-${figureSchema.id}/$entityId").withHeaders(("Fake", "Fake"))
      val getResponse = controller.getById(figureSchema.title, figureSchema.id, entityId)(getRequest)
      val getResponseJson = contentAsString(getResponse)
      getResponseJson must /("id" -> entityId.toString)
      getResponseJson must /("data") / ("name" -> osiris.data("name"))
      getResponseJson must /("data") / ("origin" -> osiris.data("origin"))
    }

    "not update and return a Bad Request when an entity is missing a field" in new EntityControllerUpdateTest {

      val data = Json.parse(
          s"""
             |{
             |  "id" : "${horus.id}",
             |  "schema" : { "id" : "${figureSchema.id}", "title" : "${figureSchema.title}"},
             |  "data" : {
             |    "name" : "Horus 2"
             |  }
             |}
           """.stripMargin
        )
      val updateRequest = FakeRequest(PUT, s"/${figureSchema.title}-${figureSchema.id}/${horus.id}", FakeHeaders(), data)
        .withSession("userId" -> user.uuid.toString)

      val updateResponse = controller.update(figureSchema.title, figureSchema.id, horus.id)(updateRequest)

      status(updateResponse) mustEqual BAD_REQUEST

      val entityId = horus.id
      val getRequest = FakeRequest(GET, s"/${figureSchema.title}-${figureSchema.id}/$entityId").withHeaders(("Fake", "Fake"))
      val getResponse = controller.getById(figureSchema.title, figureSchema.id, entityId)(getRequest)
      val getResponseJson = contentAsString(getResponse)
      getResponseJson must /("id" -> entityId.toString)
      getResponseJson must /("data") / ("name" -> "Horus")
      getResponseJson must /("data") / ("origin" -> "Thebes")
    }

  }

}



