package annotations

import java.util.UUID

import accessControl.{AuthenticationService, BasicUser, HasUserRepository}
import artifacts.{ArtifactRepository, HasArtifactRepo, HasMediaRepo, MediaRepository}
import backend.approvals.ApprovalRepository
import common.TestIHVApplication
import play.api.i18n.MessagesApi
import play.api.mvc.Controller
import projects.{HasProjectRepo, ProjectRepository, ProjectService, ProjectUserRights}
import schemas.{HasSchemaRepo, Schema, SchemaField, SchemaRepository}

/**
  * Created by dave on 1/30/17.
  */
class AnnotationControllerTest extends TestIHVApplication with HasUserRepository with BasicUser
  with HasArtifactRepo with HasMediaRepo
  with HasProjectRepo with HasSchemaRepo with HasEntityRepo with EntityTestCaseSchemas
  with HasAnnotationRepository with ModelTest {

  val controller = new AnnotationController with Controller {
    override val projectRepo = app.injector.instanceOf[ProjectRepository]
    override val entityRepo = app.injector.instanceOf[EntityRepository]
    override val schemaRepo = app.injector.instanceOf[SchemaRepository]
    override val artifactRepo = app.injector.instanceOf[ArtifactRepository]
    override val approvalRepo = app.injector.instanceOf[ApprovalRepository]
    override val mediaRepo = app.injector.instanceOf[MediaRepository]
    override val authenticationService = app.injector.instanceOf[AuthenticationService]
    override val projectService = app.injector.instanceOf[ProjectService]
    override val annotationRepo = app.injector.instanceOf[AnnotationRepository]

    override def messagesApi = app.injector.instanceOf[MessagesApi]
  }

  val instanceOfFigureSchema = schemaRepo.save(Schema(
    null, "Instance of Divine Figure", "annotation", List(
      SchemaField(null, "figure", figureSchema, false, List()),
      SchemaField(null, "condiments", stringSchemaNode, false, List()),
      SchemaField(null, "bottle count", longSchemaNode, false, List())
    ),
    false, user)
  )

  projectRepo.setRightsInProjectForUser(project, user, ProjectUserRights.ALL_RIGHTS)

  val createFigureURL = s"/activities/${project.id}/artifacts/${model.id}/${figureSchema.title}-${figureSchema.id}"
  val createInstanceURL = s"/activities/${project.id}/artifacts/${model.id}/${instanceOfFigureSchema.title}-${instanceOfFigureSchema.id}"
  def individualFigureURL(annotationId:UUID) = s"/artifacts/${model.id}/${figureSchema.title}-${figureSchema.id}/$annotationId"
  def individualInstanceURL(annotationId:UUID) = s"/artifacts/${model.id}/${instanceOfFigureSchema.title}-${instanceOfFigureSchema.id}/$annotationId"

}
