package annotations

import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import schemas.{Pass, Schema, SchemaValidationError}


trait EraTestCases {

  val entityRepo:EntityRepository
  val eraSchema:Schema

  val eras = List("Era 1", "Era 2", "Era 3", "Era 4").map({
    e => entityRepo.save(Entity(null, eraSchema, Map("name" -> e)))
  })

}

@RunWith(classOf[JUnitRunner])
class EntityRepositoryCreateSpec extends Specification {

  "When creating an entity, an Entity Repository" should {

    "create and retrieve an entity if it corresponds to a schema with only simple types" in new EntityRepoTest {

      val data = Map(
        "name" -> "Horus",
        "origin" -> "Thebes"
      )

      val entityCreated = entityRepo.save(Entity(null, figureSchema, data))

      entityCreated.schema.id mustEqual figureSchema.id
      entityCreated.data("name") mustEqual data("name")
      entityCreated.data("origin") mustEqual data("origin")

      val entityRetrievedOption = entityRepo.getById(entityCreated.id)

      entityRetrievedOption mustNotEqual None

      val entityRetrieved = entityRetrievedOption.get
      entityRetrieved.schema.id mustEqual figureSchema.id
      entityRetrieved.data("name") mustEqual data("name")
      entityRetrieved.data("origin") mustEqual data("origin")
    }

    "create and retrieve an entity if it corresponds to a schema with reference types" in new EntityRepoTest with EraTestCases {

      // Scenario: eras
      // Expects:
      """
        |{
        |  "id" : 0,
        |  "schema" : { "id": 0, "name": "Built During"}
        |  "data" : {
        |   "era" : { "id" : 0, "schema" : { "id" : 0, "name" : "Era"}, dataId: 0}
        |  }
      """.stripMargin

      val data = Map("era" -> eras(0).id.toString)

      builtDuringSchema.validate(data) match {
        case Pass => success
        case SchemaValidationError(_, _, _) => failure
      }

      val entityCreated = entityRepo.save(Entity(null, builtDuringSchema, data))

      entityCreated.schema.id mustEqual builtDuringSchema.id
      entityCreated.data("era") mustEqual eras(0).id.toString

    }

    "create and retrieve an entity if it corresponds to an entity with both simple and reference types" in new EntityRepoTest with EraTestCases {

      val data = Map("era" -> eras(0).id.toString)

      builtDuringSchema.validate(data) match {
        case Pass => success
        case SchemaValidationError(_, _, _) => failure("Validation fails")
      }

      val entityCreated = entityRepo.save(Entity(null, builtDuringSchema, data))
      entityCreated.schema.id mustEqual builtDuringSchema.id
      entityCreated.data("era") mustEqual eras(0).id.toString
    }

    "reject creating an entity when it has an extra field" in new EntityRepoTest with EraTestCases {

      val data = Map(
        "era" -> eras(0).id.toString,
        "what" -> "A thing",
        "what2" -> "Yet one more thing"
      )
      builtDuringSchema.validate(data) match {
        case Pass => failure
        case errors:SchemaValidationError => {
          errors.extraFields mustEqual Set("what", "what2")
          errors.missingFields mustEqual Set()
          errors.invalidFields mustEqual Set()
        }
      }
    }

    "reject creating an entity when it is missing a field" in new EntityRepoTest with EraTestCases {

      val data = Map("name" -> "horus")
      figureSchema.validate(data) match {
        case Pass => failure
        case errors:SchemaValidationError => {

          errors.extraFields mustEqual Set()
          errors.missingFields mustEqual Set("origin")
          errors.invalidFields mustEqual Set()
        }
      }
    }

    /*
    "reject creating an entity when a reference field points to an invalid value" in new EntitySpec {
      val eraSchema = schemaRepo.create("Era", List(
        ("name", stringSchemaNode.id)
      ))
      val builtDuringSchema = schemaRepo.create("Built During", List(
        ("era", eraSchema.id),
        ("what", stringSchemaNode.id)
      ))

      val eras = List("Era 1", "Era 2", "Era 3", "Era 4").map({
        e => entityRepo.create(eraSchema, Map("name" -> e)).left.get
      })

      val data = Map("era" -> (eras(0).id + 100000).toString, "what" -> "Hello")
      entityRepo.create(builtDuringSchema, data) match {
        case Left(entity) => 1 mustEqual 2
        case Right(errors) => {
          errors.extraFields mustEqual Set()
          errors.missingFields mustEqual Set()
          errors.invalidFields mustEqual Set("era")
        }
      }
    }
    */

//    "create an entity with a controlled vocabulary"

//    "reject an annotation if it a string property is empty"
//    "reject an annotation if it a numeric property cannot be cast to a number"
  }

}

