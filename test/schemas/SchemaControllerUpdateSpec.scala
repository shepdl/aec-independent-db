package schemas

/**
 * Created by dave on 3/10/16.
 */
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.libs.json._
import play.api.mvc.Results
import play.api.test._


@RunWith(classOf[JUnitRunner])
class SchemaControllerUpdateSpec extends PlaySpecification with Results with JsonMatchers {

  "When updating, a SchemaController " should {

    "update a schema by changing the schema's name" in new SchemaControllerTest {
      val schemaName1 = "DemoSchemaUnchanged"

      val field1Name = "field1"
      val field1Type = stringSchemaNode

      val schema = schemaRepo.save(Schema(
        null, schemaName1, "either", List(
          SchemaField(null, field1Name, field1Type, false, List())
        ),
        isControlled = false, user
      ))

      val schemaId = schema.id
      val schemaTitle = schema.title
      val fieldId = schema.fields(0).id

      val schemaName2 = "DemoSchemaChanged"
      val jsonToSubmit = Json.parse(
        s"""
           | {
           |    "id": "$schemaId",
           |    "title": "$schemaName2",
           |    "appliesTo" : "either",
           |    "isControlled" : false,
           |    "fields" : [
           |      {"title": "$field1Name", "typeId": "${field1Type.id}", "id" : "$fieldId", "isControlled": false}
           |    ]
           | }
         """.stripMargin)

      val updateRequest = FakeRequest(POST, s"/activities/${project.id}/schemas/${schemaId}", FakeHeaders(), jsonToSubmit)
//        .withJsonBody(jsonToSubmit)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = controller.update(project.id, schemaId)(updateRequest)

      status(updateResponse) mustEqual OK
      val getRequest = FakeRequest(GET, s"/schemas/${schemaId}").withHeaders(("Blank", "Blank"))
      val postUpdate = controller.get(schemaId)(getRequest)

      status(postUpdate) mustEqual OK
      val getPostUpdateJson = contentAsString(postUpdate)

      getPostUpdateJson must /("id" -> schemaId.toString)
      getPostUpdateJson must /("title" -> schemaName2)
      getPostUpdateJson must /("isControlled" -> false)
      getPostUpdateJson must /("fields") /#(0) /("id" -> fieldId.toString)
      getPostUpdateJson must /("fields") /#(0) /("title" -> field1Name)
      getPostUpdateJson must /("fields") /#(0) /("typeId" -> field1Type.id.toString)
      getPostUpdateJson must /("fields") /#(0) /("isControlled" -> false)
    }

    "update a schema by adding a new field" in new SchemaControllerTest {
      val schemaName = "DemoSchema"

      val field1Name = "field1"
      val field1Type = stringSchemaNode


      val schema = schemaRepo.save(Schema(
        null, schemaName, "annotation", List(
          SchemaField(null, field1Name, field1Type, false, List())),
        isControlled = false, user)
      )

      val schemaId = schema.id
      val field1Id = schema.fields(0).id

      val field2Name = "field2"
      val field2Type = longSchemaNode


      val jsonToSubmit = Json.parse(
        s"""
           | {
           |    "id": "$schemaId",
           |    "title": "$schemaName",
           |    "appliesTo" : "whole",
           |    "isControlled" : false,
           |    "fields" : [
           |      {"title": "$field1Name", "typeId": "${field1Type.id.toString}", "id" : "$field1Id", "isControlled" : false},
           |      {"title": "$field2Name", "typeId": "${field2Type.id.toString}", "isControlled" : false}
           |    ]
           | }
         """.stripMargin)

      val updateRequest = FakeRequest(POST, s"/activities/${project.id}/schemas/${schemaId}", FakeHeaders(), jsonToSubmit)
//        .withJsonBody(jsonToSubmit)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = controller.update(project.id, schemaId)(updateRequest)

      status(updateResponse) mustEqual OK
      val getRequest = FakeRequest(GET, s"/schemas/${schemaId}").withHeaders(("Blank", "Blank"))
      val postUpdate = controller.get(schemaId)(getRequest)

      status(postUpdate) mustEqual OK
      val getPostUpdateJson = contentAsString(postUpdate)

      getPostUpdateJson must /("id" -> schemaId.toString)
      getPostUpdateJson must /("title" -> schemaName)
      getPostUpdateJson must /("isControlled" -> false)

      getPostUpdateJson must /("fields") /# (0) /("id" -> field1Id.toString)
      getPostUpdateJson must /("fields") /# (0) /("title" -> field1Name)
      getPostUpdateJson must /("fields") /# (0) /("typeId" -> field1Type.id.toString)
      getPostUpdateJson must /("fields") /# (0) /("isControlled" -> false)

//      getPostUpdateJson must /("fields") /# (1) /("id" -> field1Id.toString)
      getPostUpdateJson must /("fields") /# (1) /("title" -> field2Name)
      getPostUpdateJson must /("fields") /# (1) /("typeId" -> field2Type.id.toString)
      getPostUpdateJson must /("fields") /# (1) /("isControlled" -> false)

    }

//    "not allow two fields to have the same name"

    "update a schema by removing an existing field" in new SchemaControllerTest {
      val schemaName = "DemoSchema"

      val field1Name = "field1"
      val field1Type = stringSchemaNode
      val field2Name = "field2"
      val field2Type = longSchemaNode

      val schema = schemaRepo.save(Schema(
        null, schemaName, "annoation", List(
          SchemaField(null, field1Name, field1Type, false, List()),
          SchemaField(null, field2Name, field2Type, false, List())
        ), isControlled = false, user
      ))

      val schemaId = schema.id
      val field1Id = schema.fields(0).id

      val jsonToSubmit = Json.parse(
        s"""
         | {
         |    "id": "$schemaId",
         |    "title": "$schemaName",
         |    "appliesTo" : "annotation",
         |    "isControlled" : false,
         |    "fields" : [
         |      {"title": "$field1Name", "typeId": "${field1Type.id.toString}", "id" : "$field1Id", "isControlled" : false}
         |    ]
         | }
         """.stripMargin)

      val updateRequest = FakeRequest(POST, s"/activities/${project.id}/schemas/${schemaId}", FakeHeaders(), jsonToSubmit) //.withJsonBody(jsonToSubmit)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = controller.update(project.id, schemaId)(updateRequest)

      status(updateResponse) mustEqual OK
      val getRequest = FakeRequest(GET, s"/schemas/${schemaId}").withHeaders(("Blank", "Blank"))
      val postUpdate = controller.get(schemaId)(getRequest)

      status(postUpdate) mustEqual OK
      val getPostUpdateJson = contentAsString(postUpdate)

      getPostUpdateJson must /("id" -> schemaId.toString)
      getPostUpdateJson must /("title" -> schemaName)
      getPostUpdateJson must /("isControlled" -> false)
      getPostUpdateJson must /("fields") /#(0) /("id" -> field1Id.toString)
      getPostUpdateJson must /("fields") /#(0) /("title" -> field1Name)
      getPostUpdateJson must /("fields") /#(0) /("typeId" -> field1Type.id.toString)
      getPostUpdateJson must /("fields") /#(0) /("isControlled" -> false)
    }

    "update a schema by changing the name of an existing field" in new SchemaControllerTest {

      val schemaName = "DemoSchema"

      val field1Name = "field1"
      val field1Type = stringSchemaNode
      val field2Name = "field2"
      val field2Type = longSchemaNode

      val schema = schemaRepo.save(Schema(
        null, schemaName, "annotation",
        List(
          SchemaField(null, field1Name, field1Type, false, List()),
          SchemaField(null, field2Name, field2Type, false, List())
        ),
        isControlled = false,
        user
      ))

      val schemaId = schema.id
      val field1Id = schema.fields(0).id

      val field1UpdatedName = "Updated title"

      val jsonToSubmit = Json.parse(
        s"""
           | {
           |    "id": "$schemaId",
           |    "title": "$schemaName",
           |    "appliesTo" : "annotation",
           |    "isControlled" : false,
           |    "fields" : [
           |      {"title": "$field1UpdatedName", "typeId": "${field1Type.id.toString}", "id" : "$field1Id", "isControlled" : false}
           |    ]
           | }
         """.stripMargin)

      val updateRequest = FakeRequest(POST, s"/activities/${project.id}/schemas/${schemaId}", FakeHeaders(), jsonToSubmit)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = controller.update(project.id, schemaId)(updateRequest)

      status(updateResponse) mustEqual OK
      val getRequest = FakeRequest(GET, s"/schemas/${schemaId}").withHeaders(("Blank", "Blank"))
      val postUpdate = controller.get(schemaId)(getRequest)

      status(postUpdate) mustEqual OK
      val getPostUpdateJson = contentAsString(postUpdate)

      getPostUpdateJson must /("id" -> schemaId.toString)
      getPostUpdateJson must /("title" -> schemaName)
      getPostUpdateJson must /("isControlled" -> false)
      getPostUpdateJson must /("fields") /#(0) /("id" -> field1Id.toString)
      getPostUpdateJson must /("fields") /#(0) /("title" -> field1UpdatedName)
      getPostUpdateJson must /("fields") /#(0) /("typeId" -> field1Type.id.toString)
      getPostUpdateJson must /("fields") /#(0) /("isControlled" -> false)

    }

    "update a schema by changing the type of an existing field" in new SchemaControllerTest {

      val schemaName = "DemoSchema"

      val field1Name = "field1"
      val field1Type = stringSchemaNode
      val field2Name = "field2"
      val field2Type = longSchemaNode

      val schema = schemaRepo.save(Schema(null,
        schemaName, "annotation", List(
          SchemaField(null, field1Name, field1Type, false, List()),
          SchemaField(null, field2Name, field2Type, false, List())
        ),
        isControlled = false,
        user
      ))

      val schemaId = schema.id
      val field1Id = schema.fields(0).id

      val field1UpdatedName = "Updated title"
      val field1UpdatedTypeId = longSchemaNode.id

      val jsonToSubmit = Json.parse(
        s"""
           | {
           |    "id": "$schemaId",
           |    "title": "$schemaName",
           |    "appliesTo" : "annotation",
           |    "isControlled" : false,
           |    "fields" : [
           |      {"title": "$field1UpdatedName", "typeId": "$field1UpdatedTypeId", "id" : "$field1Id", "isControlled" : false}
           |    ]
           | }
         """.stripMargin)

      val updateRequest = FakeRequest(POST, s"/activities/${project.id}/schemas/${schemaId}", FakeHeaders(), jsonToSubmit)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = controller.update(project.id, schemaId)(updateRequest)

      status(updateResponse) mustEqual OK
      val getRequest = FakeRequest(GET, s"/schemas/${schemaId}").withHeaders(("Blank", "Blank"))
      val postUpdate = controller.get(schemaId)(getRequest)

      status(postUpdate) mustEqual OK
      val getPostUpdateJson = contentAsString(postUpdate)
      getPostUpdateJson must
        /("id" -> schemaId.toString)
        /("title" -> schemaName)
        /("isControlled" -> false)
        /("fields") /#(0)
          /("id" -> field1Id.toString)
          /("title" -> field1UpdatedName)
          /("typeId" -> field1Type.id.toString)
          /("isControlled" -> false)
    }


    "update a schema by changing the order of fields" in new SchemaControllerTest {

      val schemaName = "DemoSchema"

      val field1Name = "field1"
      val field1Type = stringSchemaNode
      val field2Name = "field2"
      val field2Type = longSchemaNode

      val schema = schemaRepo.save(Schema(
        null, schemaName, "annotation", List(
          SchemaField(null, field1Name, field1Type, false, List()),
          SchemaField(null, field2Name, field2Type, false, List())
        ),
        isControlled = false,
        user
      ))

      val schemaId = schema.id
      val field1Id = schema.fields(0).id
      val field2Id = schema.fields(1).id

      val jsonToSubmit = Json.parse(
        s"""
           | {
           |    "id": "$schemaId",
           |    "title": "$schemaName",
           |    "appliesTo" : "annotation",
           |    "isControlled" : false,
           |    "fields" : [
           |      {"title": "$field2Name", "typeId": "${field2Type.id.toString}", "id" : "$field2Id", "isControlled" : false},
           |      {"title": "$field1Name", "typeId": "${field1Type.id.toString}", "id" : "$field1Id", "isControlled" : false}
           |    ]
           | }
         """.stripMargin)

      val updateRequest = FakeRequest(PUT, s"/activities/${project.id}/schemas/${schemaId}", FakeHeaders(), jsonToSubmit)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = controller.update(project.id, schemaId)(updateRequest)

      status(updateResponse) mustEqual OK
      val getRequest = FakeRequest(GET, s"/schemas/${schemaId}").withHeaders(("Blank", "Blank"))
      val postUpdate = controller.get(schemaId)(getRequest)

      status(postUpdate) mustEqual OK
      val getPostUpdateJson = contentAsString(postUpdate)
      getPostUpdateJson must /("id" -> schemaId.toString)
      getPostUpdateJson must /("title" -> schemaName)
      getPostUpdateJson must /("isControlled" -> false)

      getPostUpdateJson must /("fields") /#(0) /("id" -> field2Id.toString)
      getPostUpdateJson must /("fields") /#(0) /("title" -> field2Name)
      getPostUpdateJson must /("fields") /#(0) /("typeId" -> field2Type.id.toString)
      getPostUpdateJson must /("fields") /#(0) /("isControlled" -> false)

      getPostUpdateJson must /("fields") /#(1) /("id" -> field1Id.toString)
      getPostUpdateJson must /("fields") /#(1) /("title" -> field1Name)
      getPostUpdateJson must /("fields") /#(1) /("typeId" -> field1Type.id.toString)
      getPostUpdateJson must /("fields") /#(1) /("isControlled" -> false)
    }

    "update a schema by changing the order of fields and adding fields at the same time" in new SchemaControllerTest {

      val schemaName = "DemoSchema"

      val field1Name = "field1"
      val field1Type = stringSchemaNode
      val field2Name = "field2"
      val field2Type = longSchemaNode

      val schema = schemaRepo.save(Schema(
        null, schemaName, "annotation", List(
          SchemaField(null, field1Name, field1Type, false, List()),
          SchemaField(null, field2Name, field2Type, false, List())
        ),
        isControlled = false,
        user
      ))

      val schemaId = schema.id
      val field1Id = schema.fields(0).id
      val field2Id = schema.fields(1).id

      val field3Name = "field3"
      val field3Type = stringSchemaNode

      val jsonToSubmit = Json.parse(
        s"""
           | {
           |    "id": "$schemaId",
           |    "title": "$schemaName",
           |    "appliesTo" : "annotation",
           |    "isControlled" : false,
           |    "fields" : [
           |      {"title": "$field2Name", "typeId": "${field2Type.id.toString}", "id" : "$field2Id", "isControlled" : false},
           |      {"title": "$field3Name", "typeId": "${field3Type.id.toString}", "isControlled" : false},
           |      {"title": "$field1Name", "typeId": "${field1Type.id.toString}", "id" : "$field1Id", "isControlled" : false}
           |    ]
           | }
         """.stripMargin)

      val updateRequest = FakeRequest(POST, s"/activities/${project.id}/schemas/${schemaId}", FakeHeaders(), jsonToSubmit)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = controller.update(project.id, schemaId)(updateRequest)

      status(updateResponse) mustEqual OK
      val getRequest = FakeRequest(GET, s"/schemas/${schemaId}").withHeaders(("Blank", "Blank"))
      val postUpdate = controller.get(schemaId)(getRequest)

      status(postUpdate) mustEqual OK
      val getPostUpdateJson = contentAsString(postUpdate)
      getPostUpdateJson must /("id" -> schemaId.toString)
      getPostUpdateJson must /("title" -> schemaName)
      getPostUpdateJson must /("isControlled" -> false)

      getPostUpdateJson must /("fields") /#(2) /("id" -> field1Id.toString)
      getPostUpdateJson must /("fields") /#(2) /("title" -> field1Name)
      getPostUpdateJson must /("fields") /#(2) /("typeId" -> field1Type.id.toString)
      getPostUpdateJson must /("fields") /#(2) /("isControlled" -> false)
      getPostUpdateJson must /("fields") /#(1) /("title" -> field3Name)
      getPostUpdateJson must /("fields") /#(1) /("typeId" -> field3Type.id.toString)
      getPostUpdateJson must /("fields") /#(1) /("isControlled" -> false)

      getPostUpdateJson must /("fields") /#(0) /("id" -> field2Id.toString)
      getPostUpdateJson must /("fields") /#(0) /("title" -> field2Name)
      getPostUpdateJson must /("fields") /#(0) /("typeId" -> field2Type.id.toString)
      getPostUpdateJson must /("fields") /#(0) /("isControlled" -> false)

    }
  }


}
