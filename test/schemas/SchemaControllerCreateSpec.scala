package schemas

/**
 * Created by dave on 3/10/16.
 */

import java.util.UUID

import accessControl._
import annotations.EntityRepository
import common.TestIHVApplication
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.libs.json._
import play.api.mvc.{Controller, Results}
import play.api.test._
import projects.{HasProjectRepo, ProjectService, ProjectUserRights}


@RunWith(classOf[JUnitRunner])
class SchemaControllerCreateSpec extends PlaySpecification with Results with JsonMatchers {

  "The SchemaController " should {
    "create a schema with a single base field" in new SchemaControllerTest {

      val schemaName = "DemoSchema"
      val field1Name = "field1"
      val field1TypeId = stringSchemaNode.id

      val body = Json.parse(
        s"""
           |{
           |  "title" : "$schemaName",
           |  "appliesTo" : "whole",
           |  "isControlled" : false,
           |  "fields" : [
           |      {
           |      "title" : "$field1Name",
           |      "typeId" : "${field1TypeId}",
           |      "isControlled" : false,
           |      "values" : []
           |    }
           |  ]
           |}
         """.stripMargin
      )

      val creationRequest = FakeRequest(POST, s"/activities/${project.id}/schemas", FakeHeaders(), body)
        .withSession("userId" -> user.uuid.toString)

      val creationResponse = controller.create(project.id)(creationRequest)

      status(creationResponse) mustEqual CREATED
      val responseJSON = contentAsString(creationResponse)
      responseJSON must /("title" -> schemaName)
      responseJSON must /("fields") /#(0)
      responseJSON must /("fields") /#(0) /("title" -> field1Name)
      responseJSON must /("fields") /#(0) /("typeId" -> field1TypeId.toString)
      responseJSON must /("fields") /#(0) /("isControlled" -> false)
    }

    "create a schema with multiple base fields" in new SchemaControllerTest {

      val schemaName = "DemoSchemaWith2Fields"
      val field1Name = "field1"
      val field1Type = stringSchemaNode.id
      val field2Name = "field2"
      val field2Type = longSchemaNode.id

      val data = Json.parse(
        s"""
           |{
           |  "title" : "$schemaName",
           |  "appliesTo" : "either",
           |  "isControlled" : false,
           |  "fields" : [
           |    {
           |      "title" : "$field1Name",
           |      "typeId" : "$field1Type",
           |      "isControlled" : false,
           |      "values" : []
           |    },
           |    {
           |      "title" : "$field2Name",
           |      "typeId" : "$field2Type",
           |      "isControlled" : false,
           |      "values" : []
           |    }
           |  ]
           |}
         """.stripMargin
      )

      val creationRequest = FakeRequest(POST, s"/activities/${project.id}/schemas", FakeHeaders(), data)
        .withSession("userId" -> user.uuid.toString)
      val creationResponse = controller.create(project.id)(creationRequest)

      status(creationResponse) mustEqual 201
      val responseJSON = contentAsString(creationResponse)

      responseJSON must /("title" -> schemaName)
      responseJSON must /("isControlled" -> false)
      responseJSON must /("fields") /#(0) /("title" -> field1Name)
      responseJSON must /("fields") /#(0) /("typeId" -> field1Type.toString)
      responseJSON must /("fields") /#(0) /("isControlled" -> false)
      responseJSON must /("fields") /#(1) /("title" -> field2Name)
      responseJSON must /("fields") /#(1) /("typeId" -> field2Type.toString)
      responseJSON must /("fields") /#(1) /("isControlled" -> false)

    }

    "refuse a schema that uses an invalid schema type" in new SchemaControllerTest {

      val schemaName = "DemoSchemaWith2Fields"
      val field1Name = "field1"
      val field1Type = stringSchemaNode.id
      val field2Name = "field2"
      val field2Type = UUID.randomUUID()

      val body = Json.parse(s"""
           |{
           |  "title" : "$schemaName",
           |  "appliesTo" : "whole",
           |  "isControlled" : false,
           |  "fields" : [
           |    {
           |      "title" : "$field1Name",
           |      "typeId" : "$field1Type",
           |      "isControlled" : false,
           |      "values" : []
           |    },
           |    {
           |      "title" : "$field2Name",
           |      "typeId" : "$field2Type",
           |      "isControlled" : false,
           |      "values" : []
           |    }
           |  ]
           |}
         """.stripMargin
      )

      val creationRequest = FakeRequest(POST, s"/activities/${project.id}/schemas", FakeHeaders(), body)
        .withSession("userId" -> user.uuid.toString)
      val creationResponse = controller.create(project.id)(creationRequest)

      status(creationResponse) mustEqual BAD_REQUEST
      val errorMessage = contentAsString(creationResponse)

      errorMessage must /("error" -> "Fields do not exist")

    }
  }

}


