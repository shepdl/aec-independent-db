package schemas

import common.HasDatabase
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class SchemaRepositoryIsBaseTypeSpec extends Specification {

  "When dealing with base types, a Schema Repository" should {

    "always have base types defined" in new SchemaRepoTestCase {
      val retrievedStringSchemaNode = schemaRepo.getById(stringSchemaNode.id).get

      retrievedStringSchemaNode.id mustEqual stringSchemaNode.id
      val retrievedLongSchemaNode = schemaRepo.getById(longSchemaNode.id).get
      retrievedLongSchemaNode.id mustEqual longSchemaNode.id
    }

    "match fields that are base types" in new SchemaRepoTestCase {
      val newSchema = schemaRepo.save(Schema(null, "Person", "entity", List(
        SchemaField(null, "firstName", stringSchemaNode, false, List()),
        SchemaField(null, "lastName", stringSchemaNode, false, List()),
        SchemaField(null, "age", longSchemaNode, false, List())
      ), false, testUser))

      schemaRepo.isBaseType(newSchema.fields(0).dataType) mustEqual(true)
      schemaRepo.isBaseType(newSchema.fields(1).dataType) mustEqual(true)
      schemaRepo.isBaseType(newSchema.fields(1).dataType) mustEqual(true)
    }

    "not match fields that are not base types" in new SchemaRepoTestCase {
      val colorSchema = schemaRepo.save(Schema(null, "Color", "entity", List(
        SchemaField(null, "name", stringSchemaNode, false, List())), false, testUser
      ))
      val personSchema = schemaRepo.save(Schema(null, "Person", "entity", List(
        SchemaField(null, "firstName", stringSchemaNode, false, List()),
        SchemaField(null, "lastName", stringSchemaNode, false, List()),
        SchemaField(null, "eye color", colorSchema, false, List())
      ), false, testUser))


      schemaRepo.isBaseType(personSchema) mustEqual false
    }
  }

}
