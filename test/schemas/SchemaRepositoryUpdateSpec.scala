package schemas

/**
 * Created by dave on 3/9/16.
 */

import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._

@RunWith(classOf[JUnitRunner])
class SchemaRepositoryUpdateSpec extends Specification {

  "For updating a Schema, the Schema Repository should" in {

    "update a schema by adding a new field" in new SchemaRepoTestCase {
      val firstSchema = createBaseSchema

      val newSchema = Schema(
        firstSchema.id,
        firstSchema.title,
        firstSchema.appliesTo,
        firstSchema.fields ++ List(SchemaField(null, "dateOfBirth", stringSchemaNode, false, List())),
        false,
        testUser
      )

      val updatedSchema = schemaRepo.save(newSchema)

      updatedSchema.id mustEqual newSchema.id
      updatedSchema.fields.size mustEqual 3
      updatedSchema.fields(0).title mustEqual "firstName"
      updatedSchema.fields(0).id mustEqual firstSchema.fields(0).id
      updatedSchema.fields(0).dataType mustEqual stringSchemaNode

      updatedSchema.fields(1).title mustEqual "lastName"
      updatedSchema.fields(1).id mustEqual firstSchema.fields(1).id
      updatedSchema.fields(0).dataType mustEqual stringSchemaNode

      updatedSchema.fields(2).title mustEqual "dateOfBirth"
      updatedSchema.fields(2).dataType mustEqual stringSchemaNode
    }

    "update a schema by removing an existing field" in new SchemaRepoTestCase {
      val firstSchema = createBaseSchema

      val newSchema = Schema(
        firstSchema.id,
        firstSchema.title,
        firstSchema.appliesTo,
        List(firstSchema.fields(1)),
        false,
        testUser
      )

      val updatedSchema = schemaRepo.save(newSchema)

      updatedSchema.id mustEqual newSchema.id
      updatedSchema.fields.size mustEqual 1

      updatedSchema.fields(0).title mustEqual "lastName"
      updatedSchema.fields(0).id mustEqual firstSchema.fields(1).id
      updatedSchema.fields(0).dataType mustEqual firstSchema.fields(1).dataType

    }

    "update a schema by changing the name of an existing field" in new SchemaRepoTestCase {
      val firstSchema = createBaseSchema

      val firstField = firstSchema.fields.head

      val newFieldName = "EnglishName"

      val newSchema = Schema(
        firstSchema.id,
        firstSchema.title,
        firstSchema.appliesTo,
        List(
          SchemaField(firstField.id, newFieldName, firstField.dataType, false, List()),
          firstSchema.fields(1)
        ),
        false,
        testUser
      )

      val updatedSchema = schemaRepo.save(newSchema)

      updatedSchema.id mustEqual newSchema.id
      updatedSchema.fields.size mustEqual 2

      updatedSchema.fields(0).id mustEqual firstField.id
      updatedSchema.fields(0).title mustEqual newFieldName
      updatedSchema.fields(0).dataType mustEqual firstField.dataType

      updatedSchema.fields(1).id mustEqual firstSchema.fields(1).id
      updatedSchema.fields(1).title mustEqual firstSchema.fields(1).title
      updatedSchema.fields(1).dataType mustEqual firstSchema.fields(1).dataType

    }

    "update a schema by changing the type of an existing field" in new SchemaRepoTestCase {
      val firstSchema = createBaseSchema

      val firstField = firstSchema.fields.head

      val newFieldType = longSchemaNode

      val newSchema = Schema(
        firstSchema.id,
        firstSchema.title,
        firstSchema.appliesTo,
        List(
          SchemaField(firstField.id, firstField.title, newFieldType, false, List()),
          firstSchema.fields(1)
        ),
        false,
        testUser
      )

      val updatedSchema = schemaRepo.save(newSchema)

      updatedSchema mustEqual newSchema

      updatedSchema.id mustEqual newSchema.id
      updatedSchema.fields.size mustEqual 2

      updatedSchema.fields(0).id mustEqual firstField.id
      updatedSchema.fields(0).title mustEqual firstField.title
      updatedSchema.fields(0).dataType mustEqual newFieldType

      updatedSchema.fields(1).id mustEqual firstSchema.fields(1).id
      updatedSchema.fields(1).title mustEqual firstSchema.fields(1).title
      updatedSchema.fields(1).dataType mustEqual firstSchema.fields(1).dataType

    }

  }
}
