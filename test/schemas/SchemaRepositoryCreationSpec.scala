package schemas

import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test._



@RunWith(classOf[JUnitRunner])
class SchemaRepositoryCreationSpec extends Specification {

  "The Schema Repository" should {

    "create and retrieve a schema with simple types correctly" in new SchemaRepoTestCase {

      val schemaCreated = buildBODInscriptionSchema

      val schemaRetrieved = schemaRepo.getById(schemaCreated.id).get

      val schemaTitle = "BookOfTheDeadInscription"
      val expected = Schema(schemaRetrieved.id, schemaTitle, schemaRetrieved.appliesTo, List(), false, testUser)

      schemaRetrieved mustEqual schemaCreated


      schemaRetrieved.title mustEqual schemaTitle

      schemaRetrieved.fields.size mustEqual 2
      schemaRetrieved.fields(0).title mustEqual "number"
      schemaRetrieved.fields(0).dataType.id mustEqual longSchemaNode.id
      schemaRetrieved.fields(0).isControlled mustEqual false

      schemaRetrieved.fields(1).title mustEqual "originalText"
      schemaRetrieved.fields(1).dataType.id mustEqual stringSchemaNode.id
      schemaRetrieved.fields(1).isControlled mustEqual false

    }

    "create and retrieve a schema with reference types correctly" in new SchemaRepoTestCase {

      val firstSchema = buildBODInscriptionSchema

      val secondSchemaTitle = "RefersToBOD"
      val stringField = "aStringField"
      val refField = "theRefField"
      val secondSchemaFields = List(
        SchemaField(null, stringField, stringSchemaNode, false, List()),
        SchemaField(null, refField, firstSchema, false, List())
      )

      val secondSchemaCreated = schemaRepo.save(Schema(null, secondSchemaTitle, "entity", secondSchemaFields, false, testUser))

      val secondSchemaRetrieved = schemaRepo.getById(secondSchemaCreated.id).get

      secondSchemaCreated.id mustEqual secondSchemaRetrieved.id
      secondSchemaCreated.title mustEqual secondSchemaRetrieved.title
      secondSchemaCreated.isControlled mustEqual secondSchemaRetrieved.isControlled
      secondSchemaCreated.owner mustEqual secondSchemaRetrieved.owner

      secondSchemaCreated.fields.size mustEqual 2
      secondSchemaCreated.fields(0).title mustEqual stringField
      secondSchemaCreated.fields(0).dataType.id mustEqual stringSchemaNode.id
      secondSchemaCreated.fields(1).isControlled mustEqual false

      secondSchemaCreated.fields(1).title mustEqual refField
      secondSchemaCreated.fields(1).dataType.id mustEqual firstSchema.id
      secondSchemaCreated.fields(1).isControlled mustEqual false

      secondSchemaRetrieved.fields.size mustEqual 2
      secondSchemaRetrieved.fields(0).title mustEqual stringField
      secondSchemaRetrieved.fields(0).dataType.id mustEqual stringSchemaNode.id
      secondSchemaRetrieved.fields(1).isControlled mustEqual false

      secondSchemaRetrieved.fields(1).title mustEqual refField
      secondSchemaRetrieved.fields(1).dataType.id mustEqual firstSchema.id
      secondSchemaRetrieved.fields(1).isControlled mustEqual false

    }

    "create and retrieve a schema with a field with controlled vocabulary" in new WithApplication with SchemaRepoTestCase {

      val schemaTitle = "Wig"
      val schemaCreated = schemaRepo.save(Schema(null,
        schemaTitle, "annotation",
        List(
          SchemaField(null, "color", stringSchemaNode, true, List()),
          SchemaField(null, "notes", stringSchemaNode, false, List())
        ), false, testUser))

      val colorTerms = List(
        "red", "green", "blue", "purple", "gold", "yellow", "orange", "black"
      )

      val schemaRetrieved = schemaRepo.getById(schemaCreated.id).get
      val expected = Schema(
        schemaRetrieved.id, schemaTitle, schemaRetrieved.appliesTo, List(), false, testUser
      )

      schemaRetrieved mustEqual schemaCreated


      schemaRetrieved.title mustEqual schemaTitle

      schemaRetrieved.fields.size mustEqual 2
      schemaRetrieved.fields(0).title mustEqual "color"
      schemaRetrieved.fields(0).dataType.id mustEqual stringSchemaNode.id
      schemaRetrieved.fields(0).isControlled mustEqual true

      schemaRetrieved.fields(1).title mustEqual "notes"
      schemaRetrieved.fields(1).dataType.id mustEqual stringSchemaNode.id
      schemaRetrieved.fields(1).isControlled mustEqual false

    }

//    "fail to create a schema with a field that is invalid for the controlled vocabulary"

  }
}
