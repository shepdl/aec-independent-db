package schemas

import accessControl.{GroupRepository, UserRepository}
import annotations.{AnnotationRepository, EntityRepository}
import artifacts.{ArtifactRepository, MediaRepository}
import fictionalArchaeologists.{FictionalArchaeologists, MiscDiscoveriesScenario}
import org.junit.runner.RunWith
import org.specs2.runner.JUnitRunner
import play.api.test.PlaySpecification
import projects.ProjectRepository

/**
  * Created by daveshepard on 3/27/17.
  */
@RunWith(classOf[JUnitRunner])
class VersioningSpec extends PlaySpecification{

  "A SchemaRepository" should {

    "Creating a schema and updating it creates two versions that are returned in the correct order" in new VersioningTest {
      val updatedSchema = schemaRepo.save(Schema(
        deityOrderSchema.id, "Order of Deity", "annotation", List(SchemaField(
          deityOrderSchema.fields(0).id, "Name", stringSchema, false, List()
        )), false, pabodie
      ))

      val schemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList
      schemaList must haveSize(2)
      schemaList(0).item mustEqual updatedSchema
      schemaList(1).item mustEqual deityOrderSchema
      schemaList(0).versionId mustNotEqual schemaList(1).versionId
    }

    "Creating a schema and updating it twice creates three versions" in new VersioningTest {
      val updatedSchema1 = schemaRepo.save(Schema(
        deityOrderSchema.id, "Order of Deity", "annotation", List(SchemaField(
          deityOrderSchema.fields(0).id, "Name", stringSchema, false, List()
        )), false, pabodie
      ))
      val updatedSchema2 = schemaRepo.save(Schema(
        deityOrderSchema.id, "The Order of the Deity", "annotation", List(
          SchemaField(deityOrderSchema.fields(0).id, "Another name", stringSchema, false, List()),
          SchemaField(null, "Authority name", stringSchema, false, List())
        ), false, pabodie
      ))

      val schemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList
      schemaList must haveSize(3)
      schemaList(0).item mustEqual updatedSchema2
      schemaList(1).item mustEqual updatedSchema1
      schemaList(2).item mustEqual deityOrderSchema
    }

    "Deleting a schema with two previous versions creates three versions" in new VersioningTest {
      val updatedSchema1 = schemaRepo.save(Schema(
        deityOrderSchema.id, "Order of Deity", "annotation", List(SchemaField(
          deityOrderSchema.fields(0).id, "Name", stringSchema, false, List()
        )), false, pabodie
      ))

      schemaRepo.delete(deityOrderSchema, pabodie)

      val schemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList
      schemaList must haveSize(3)
      val deletedVersion = schemaList(0)
      deletedVersion.action mustEqual SchemaProperties.deleted
      schemaList(1).item mustEqual updatedSchema1
      schemaList(2).item mustEqual deityOrderSchema
    }

    "Reverting to the immediate previous version works correctly and contains the correct fields" in new VersioningTest {
      val updatedSchema1 = schemaRepo.save(Schema(
        deityOrderSchema.id, "Order of Deity", "annotation", List(SchemaField(
          deityOrderSchema.fields(0).id, "Name", stringSchema, false, List()
        )), false, pabodie
      ))

      val schemaListPreRevert = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList
      schemaRepo.revertTo(deityOrderSchema, schemaListPreRevert(1).versionId, pabodie)
      val schemaPostRevert = schemaRepo.getById(deityOrderSchema.id)
      schemaPostRevert mustEqual deityOrderSchema

      val schemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList
      schemaList must haveSize(3)
      schemaList(0).item mustEqual deityOrderSchema
      schemaList(1).item mustEqual updatedSchema1
      schemaList(2).item mustEqual deityOrderSchema
    }

    "Reverting to the version two versions ago works correctly" in new VersioningTest {
      val updatedSchema1 = schemaRepo.save(Schema(
        deityOrderSchema.id, "Order of Deity", "annotation", List(SchemaField(
          deityOrderSchema.fields(0).id, "Name", stringSchema, false, List()
        )), false, pabodie
      ))
      val updatedSchema2 = schemaRepo.save(Schema(
        deityOrderSchema.id, "The Order of the Deity", "annotation", List(
          SchemaField(deityOrderSchema.fields(0).id, "Another name", stringSchema, false, List()),
          SchemaField(null, "Authority name", stringSchema, false, List())
        ), false, pabodie
      ))

      val schemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList
      schemaRepo.revertTo(deityOrderSchema, schemaList(1).versionId, pabodie)
      val retrieved = schemaRepo.getById(deityOrderSchema.id)
      retrieved mustEqual Some(schemaList(1).item)

      val postReversionSchemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList

      schemaList(0).item mustEqual updatedSchema1
      schemaList(1).item mustEqual updatedSchema2
      schemaList(2).item mustEqual updatedSchema1
      schemaList(3).item mustEqual deityOrderSchema
    }

    "Reverting to a version prior to a deletion works" in new VersioningTest {
      val updatedSchema1 = schemaRepo.save(Schema(
        deityOrderSchema.id, "Order of Deity", "annotation", List(SchemaField(
          deityOrderSchema.fields(0).id, "Name", stringSchema, false, List()
        )), false, pabodie
      ))
      val updatedSchema2 = schemaRepo.save(Schema(
        deityOrderSchema.id, "The Order of the Deity", "annotation", List(
          SchemaField(deityOrderSchema.fields(0).id, "Another name", stringSchema, false, List()),
          SchemaField(null, "Authority name", stringSchema, false, List())
        ), false, pabodie
      ))

      val schemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList
      schemaRepo.revertTo(deityOrderSchema, schemaList(1).versionId, pabodie)
      val retrieved = schemaRepo.getById(deityOrderSchema.id)
      retrieved mustEqual Some(schemaList(1).item)

      val postReversionSchemaList = schemaRepo.listVersions(deityOrderSchema, 0, 25).toList

      schemaList(0).item mustEqual updatedSchema1
      schemaList(1).item mustEqual updatedSchema2
      schemaList(2).item mustEqual updatedSchema1
      schemaList(3).item mustEqual deityOrderSchema
    }

//    "Reverting to a “deleted” version returns an error"

//    "Creating a schema, creating some annotations, and updating the schema returns that the current version of the schema is incorrect"

  }


}

trait VersioningTest extends MiscDiscoveriesScenario
