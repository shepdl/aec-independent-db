package schemas

import annotations.Entity
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._


@RunWith(classOf[JUnitRunner])
class SchemaRepositorySchemaFieldControlledVocabSpec extends Specification {

  "The Schema Repository" should {

    "create a schema with a field with controlled vocabulary" in new SchemaRepoTestCase {
      val schemaTitle = "Wig"
      val colorSchema = schemaRepo.save(Schema(null, "Colors", "entity", List(
        SchemaField(null, "name", stringSchemaNode, false, List())), isControlled = true, testUser))
      val colorTerms = List(
        "red", "green", "blue", "purple", "gold", "yellow", "orange", "black"
      ).map(
        colorName => entityRepo.save(Entity(null, colorSchema, Map("name" -> colorName)))
      )
      val schemaCreated = schemaRepo.save(Schema(null,
        schemaTitle, "annotation",
        List(
          SchemaField(null, "color", colorSchema, true, colorTerms),
          SchemaField(null, "notes", stringSchemaNode, false, List())),
        false, testUser)
      )

      val schemaRetrievedOpt = schemaRepo.getById(schemaCreated.id)

      schemaRetrievedOpt must beSome[Schema]

      val schemaRetrieved = schemaRetrievedOpt.get

      schemaRetrieved("notes").isControlled must beFalse
      schemaRetrieved("color").isControlled must beTrue
      schemaRetrieved("color").terms must haveSize(colorTerms.size)

      schemaRetrieved("color").terms.zip(colorTerms).filterNot({
        case (retrieved, original) => retrieved.schema == colorSchema && retrieved.data("name") == original("name")
      }) must beEmpty
    }

    "update a schema's fields and make sure controlled vocabulary status is preserved" in new SchemaRepoTestCase {
      val schemaTitle = "Wig"
      val colorSchema = schemaRepo.save(
        Schema(null, "Colors", "entity",
          List(SchemaField(null, "name", stringSchemaNode, false, List())
          ), isControlled = true, testUser)
      )
      val colorTerms = List(
        "red", "green", "blue", "purple", "gold", "yellow", "orange", "black"
      ).map(
        colorName => entityRepo.save(Entity(null, colorSchema, Map("name" -> colorName)))
      )
      val schemaCreated = schemaRepo.save(Schema(
        null, schemaTitle, "annotation",
        List(
          SchemaField(null, "color", colorSchema, true, colorTerms),
          SchemaField(null, "notes", stringSchemaNode, false, List())
        ), false, testUser
      )
      )

      val newFields = List(
        schemaCreated("notes"),
        schemaCreated("color")
      )

      val newSchema = Schema(
        schemaCreated.id,
        schemaCreated.title,
        "either",
        newFields,
        schemaCreated.isControlled,
        testUser
      )

      val schemaRetrievedOpt = schemaRepo.getById(schemaCreated.id)
      val schemaRetrieved = schemaRetrievedOpt.get

      schemaRetrieved("notes").isControlled must beFalse
      schemaRetrieved("color").isControlled must beTrue
      schemaRetrieved("color").terms must haveSize(colorTerms.size)

      schemaRetrieved("color").terms.zip(colorTerms).filterNot({
        case (retrieved, original) => retrieved.schema == colorSchema && retrieved.data("name") == original("name")
      }) must beEmpty
    }

    "update a schema by adding, removing, and reorder terms in a field with controlled vocabulary" in new SchemaRepoTestCase {
      val schemaTitle = "Wig"
      val colorSchema = schemaRepo.save(Schema(
        null, "Colors", "entity",
        List(
          SchemaField(null, "name", stringSchemaNode, false, List())
        ), isControlled = false, testUser
      ))
      val colorTerms = List(
        "red", "green", "blue", "purple", "gold", "yellow", "orange", "black"
      ).map(
        colorName => entityRepo.save(Entity(null, colorSchema, Map("name" -> colorName)))
      )

      val schemaCreated = schemaRepo.save(Schema(
        null, schemaTitle, "annotation", List(
        SchemaField(null, "color", colorSchema, true, colorTerms),
        SchemaField(null, "notes", stringSchemaNode, false, List())),
        isControlled = false,
        testUser
      ))


      val schemaRetrieved = schemaRepo.getById(schemaCreated.id).get

      val currentTerms = schemaRetrieved("color").terms

      val reorderedTerms = List(
        currentTerms(2),
        currentTerms(0),
        Entity(null, currentTerms(0).schema, Map("name" -> "azure")),
        currentTerms(4),
        Entity(null, currentTerms(0).schema, Map("name" -> "ecru")),
        currentTerms(6)
      )

      val schemaUpdated = Schema(
        schemaCreated.id,
        schemaCreated.title,
        schemaCreated.appliesTo,
        List(
          // NOTE: Fields are reordered!
          schemaCreated.fields(1),
          SchemaField(
            schemaCreated.fields(0).id,
            schemaCreated.fields(0).title,
            schemaCreated.fields(0).dataType,
            schemaCreated.fields(0).isControlled,
            reorderedTerms
          )
        ),
        schemaCreated.isControlled,
        testUser
      )

      schemaRepo.save(schemaUpdated)

      val updatedRetrievedSchemaOpt = schemaRepo.getById(schemaCreated.id)
      updatedRetrievedSchemaOpt must beSome[Schema]

      val updatedRetrievedSchema = updatedRetrievedSchemaOpt.get

      val updatedTerms = updatedRetrievedSchema("color").terms
      updatedTerms aka "The number of terms in the color vocabulary" must haveSize(reorderedTerms.size)
      updatedTerms(0).data("name") mustEqual reorderedTerms(0).data("name")
      updatedTerms(1).data("name") mustEqual reorderedTerms(1).data("name")
      updatedTerms(2).data("name") mustEqual "azure"
      updatedTerms(3).data("name") mustEqual reorderedTerms(3).data("name")
      updatedTerms(4).data("name") mustEqual "ecru"
      updatedTerms(5).data("name") mustEqual reorderedTerms(5).data("name")
    }


    /*
    "convert a schema from controlled to un-controlled and delete all associated terms"
    "convert schema from uncontrolled to controlled make sure that all existing values for that field are now parts of the controlled term"
    "delete controlled vocab once schema is deleted"
    */

  }

}
