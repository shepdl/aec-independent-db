package schemas

/**
 * Created by dave on 3/9/16.
 */
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.specification.{BeforeAfterEach, Scope}
import play.api.test.WithApplication

@RunWith(classOf[JUnitRunner])
class SchemaRepositoryDeletionSpec extends Specification {

  "For deleting a Schema, the Schema Repository should" in {

    "delete a schema correctly" in new SchemaRepoTestCase {
      val sampleSchema = createBaseSchema
      schemaRepo.getById(sampleSchema.id) mustEqual Some(sampleSchema)
      schemaRepo.delete(sampleSchema, testUser)
      schemaRepo.getById(sampleSchema.id) mustEqual None
    }
  }

}
