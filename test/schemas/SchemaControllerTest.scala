package schemas

import accessControl.{AuthenticationService, BasicUser, HasUserRepository, UserRepository}
import annotations.EntityRepository
import common.TestIHVApplication
import play.api.mvc.Controller
import projects.{HasProjectRepo, ProjectService, ProjectUserRights}

/**
  * Created by dave on 1/30/17.
  */
class SchemaControllerTest extends TestIHVApplication with HasUserRepository with BasicUser with HasProjectRepo with HasSchemaRepo {

  val controller = new SchemaController with Controller {
    override val projectService = app.injector.instanceOf[ProjectService]
    override val entityRepo: EntityRepository = app.injector.instanceOf[EntityRepository]
    override val authService: AuthenticationService = app.injector.instanceOf[AuthenticationService]
    override val schemaRepo: SchemaRepository = app.injector.instanceOf[SchemaRepository]
    override val userRepo: UserRepository = app.injector.instanceOf[UserRepository]
  }

  val project = createMockProject
  projectRepo.setRightsInProjectForUser(project, user, ProjectUserRights.ALL_RIGHTS)

}
