package schemas

import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._


@RunWith(classOf[JUnitRunner])
class SchemaValidationSpec extends Specification {

  trait SchemaValidationSpec extends SchemaRepoTestCase

  "A schema validator should" should {

    "pass when all required fields are present" in new SchemaValidationSpec  {
      val eraSchema = schemaRepo.save(Schema(null, "Era", "entity", List(
        SchemaField(null, "name", stringSchemaNode, false, List())
      ), false, user))
      val builtDuringSchema = schemaRepo.save(Schema(null,
        "Built During", "whole", List(
        SchemaField(null, "era", eraSchema, false, List()),
        SchemaField(null, "what", stringSchemaNode, false, List())
      ), false, user))

      val data = Map[String,AnyRef]("era" -> "Any era",
        "what" -> "A thing"
      )

      builtDuringSchema.validate(data) mustEqual Pass
    }

    "reject creating an entity when it has an extra field" in new SchemaValidationSpec {
      val eraSchema = schemaRepo.save(Schema(null, "Era", "entity", List(
        SchemaField(null, "name", stringSchemaNode, false, List())
      ), false, user))
      val builtDuringSchema = schemaRepo.save(Schema(null, "Built During", "whole", List(
        SchemaField(null, "era", eraSchema, false, List()),
        SchemaField(null, "what", stringSchemaNode, false, List())
      ), false, user))

      val data = Map[String,AnyRef]("era" -> "Any era",
        "what" -> "A thing", "what2" -> "Yet one more thing"
      )

      builtDuringSchema.validate(data) match {
        case Pass => failure("Validation passed when it should not have")
        case errors:SchemaValidationError => {
          errors.extraFields mustEqual Set("what2")
          errors.missingFields mustEqual Set()
          errors.invalidFields mustEqual Set()
        }
      }
    }

    "reject creating an entity when it is missing a field" in new SchemaValidationSpec {
      val eraSchema = schemaRepo.save(Schema(null, "Era", "entity", List(
        SchemaField(null, "name", stringSchemaNode, false, List())
      ), false, user))
      val builtDuringSchema = schemaRepo.save(Schema(null, "Built During", "whole", List(
        SchemaField(null, "era", eraSchema, false, List()),
        SchemaField(null, "what", stringSchemaNode, false, List())
      ), false, user))

      val data = Map("era" -> "Any era")
      builtDuringSchema.validate(data) match {
        case Pass => failure("Validation passed when it should not have")
        case errors:SchemaValidationError => {
          errors.extraFields mustEqual Set()
          errors.missingFields mustEqual Set("what")
          errors.invalidFields mustEqual Set()
        }
      }
    }

    /*
    "reject creating an entity when a reference field points to an invalid value" in new HasSchemaRepo {
      val eraSchema = schemaRepo.create("Era", List(
        ("name", stringSchemaNode.id)
      ))
      val builtDuringSchema = schemaRepo.create("Built During", List(
        ("era", eraSchema.id),
        ("what", stringSchemaNode.id)
      ))

      val eras = List("Era 1", "Era 2", "Era 3", "Era 4").map({
        e => entityRepo.create(eraSchema, Map("name" -> e)).left.get
      })

      val data = Map("era" -> (eras(0).id + 100000).toString, "what" -> "Hello")
      entityRepo.create(builtDuringSchema, data) match {
        case Left(entity) => 1 mustEqual 2
        case Right(errors) => {
          errors.extraFields mustEqual Set()
          errors.missingFields mustEqual Set()
          errors.invalidFields mustEqual Set("era")
        }
      }
    }
    */


  }

}
