package schemas

/**
 * Created by dave on 5/11/15.
 */

import accessControl._
import annotations.HasEntityRepo
import common.TestIHVApplication
import play.api.Application
import projects.HasProjectRepo

trait HasSchemaRepo {

  val app:Application
  val schemaRepo = app.injector.instanceOf[SchemaRepository]


  val stringSchemaNode = schemaRepo.baseTypes("String")
  val longSchemaNode = schemaRepo.baseTypes("Int")

}


trait SchemaRepoTestCase extends TestIHVApplication with HasUserRepository with BasicUser
  with HasProjectRepo with HasSchemaRepo with HasEntityRepo {

  val testUser = user

  def buildBODInscriptionSchema = {
    val title = "BookOfTheDeadInscription"
    val fields = List(
      SchemaField(null, "number" , longSchemaNode, false, List()),
      SchemaField(null, "originalText", stringSchemaNode, false, List())
    )

    schemaRepo.save(
      Schema(null, title, "annotation", fields, false, testUser)
    )
  }

  def createBaseSchema:Schema = {
    val title = "Person"
    val fields = List(
      SchemaField(null, "firstName", stringSchemaNode, false, List()),
      SchemaField(null, "lastName", stringSchemaNode, false, List())
    )

    schemaRepo.save(
      Schema(null, title, "annotation", fields, false, testUser)
    )
  }

}
