package common

import play.api.Configuration
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.WithApplication

/**
  * Created by dave on 1/27/17.
  */
trait TestIHVApplication extends WithApplication {

  override val app = new GuiceApplicationBuilder()
    .configure(
      Configuration(
        "play.crypto.secret" -> "changeme",
        "db.default.url" -> "bolt://localhost",
        "db.default.username" -> "neo4j",
        "db.default.password" -> "qwe123",
        "media.resourcePath" -> "./artifact-uploads/"
      )
    ).overrides(bind[DatabaseService].to[TestNeo4jDatabase])
    .build()

}
