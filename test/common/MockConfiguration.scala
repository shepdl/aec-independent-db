package common

import play.api.Configuration

/**
  * Created by dave on 12/12/16.
  */
trait MockConfiguration {

  val config = Configuration.from(Map(
    "play.crypto.secret" -> "Fake secret",
    "play.i18n.langs" -> Seq("en"),
    "media.resourcePath" -> "/tmp/ih-viewer-test/",
    "db.default.url" -> "bolt://localhost:7474/",
    "db.default.username" -> "neo4j",
    "db.default.password" -> "qwe123"
  ))

}
