package common

import javax.inject.Inject
import com.google.inject.Singleton

import org.neo4j.driver.v1.Config.EncryptionLevel
import org.neo4j.driver.v1._
import play.api.Configuration

/**
  * Created by dave on 12/12/16.
  */
@Singleton
class TestNeo4jDatabase @Inject()(config:Configuration) extends DatabaseService {

  private val dbPath = config.getString("db.default.url").get
  private val dbUsername = config.getString("db.default.username").get
  private val dbPassword = config.getString("db.default.password").get

  private val neo4jConfig = Config.build().withEncryptionLevel(EncryptionLevel.NONE).toConfig

  val driver = GraphDatabase.driver(dbPath, AuthTokens.basic(dbUsername, dbPassword), neo4jConfig)

  def clear = {
    val session = driver.session()
    val tx = session.beginTransaction()
    tx.run(s"MATCH (n) DETACH DELETE n")
    tx.success()
    session.close()
    tx.close()
  }

  clear

  def destroy = {
    driver.close()
  }

}
