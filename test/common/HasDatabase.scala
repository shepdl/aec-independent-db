package common

import org.specs2.mutable.{BeforeAfter}

/**
  * Created by dave on 12/12/16.
  */
trait HasDatabase extends BeforeAfter {

  val dbService = new TestNeo4jDatabase(null)

  override def before = {

  }

  override def after: Any = {
    println("Clearing after")
    dbService.clear
    dbService.destroy
  }
}
