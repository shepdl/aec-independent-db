package artifacts

import java.io.PrintWriter

import accessControl.User
import play.api.{Application, Configuration}
import play.api.libs.Files.TemporaryFile
import play.api.test.{FakeApplication, WithApplication}
import projects.{HasProjectRepo, ProjectRepository}

/**
 * Created by dave on 3/11/16.
 */
trait HasArtifactRepo extends HasProjectRepo {

  val app:Application
  val user:User

  val artifactRepo = app.injector.instanceOf[ArtifactRepository]

  val artifactTitle = "Demo Artifact"
  def createMockArtifact = artifactRepo.save(Artifact(null, artifactTitle, user))
}

trait HasMediaRepo {

  val app:Application

  val artifactRepo:ArtifactRepository

  val mediaRepo = app.injector.instanceOf[MediaRepository]

  def createMockArtifact:Artifact

  val fileContent =
    s"""
       |<sampleXml>
       |  <test>Hello!</test>
       |</sampleXml>
         """.stripMargin
  def createMockArtifactWithFile = {
    val artifact = createMockArtifact
    val tempFile = TemporaryFile("test", ".dae")
    val writer = new PrintWriter(tempFile.file)
    writer.write(fileContent)
    writer.close()
    val model = mediaRepo.save(Model(
      null, "model title", "dae", List(
        ModelFile(null, "test.dae", "dae", tempFile.file)
      ), ""
    ))
    mediaRepo.addModelToArtifact(artifact, model)
    artifact
  }
}
