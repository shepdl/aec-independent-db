package artifacts

import common.{HasDatabase, TestIHVApplication}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.test._
import play.api.test.Helpers._
import projects.HasProjectRepo
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.charset.Charset

import accessControl.{BasicUser, HasUserRepository}
import play.api.Application
import schemas.HasSchemaRepo

class ArtifactRepositoryTest extends TestIHVApplication with HasUserRepository with BasicUser with HasProjectRepo
  with HasSchemaRepo with HasArtifactRepo with HasMediaRepo

@RunWith(classOf[JUnitRunner])
class Neo4jArtifactRepositoryCreateSpec extends Specification {

  import scala.collection.JavaConversions._

  def makeFile(id:Long) = {
    val file = new File(
      Files.createTempFile(s"test-file-$id", ".jpg").toUri
    )
    file.deleteOnExit()
    file
  }

  "When creating an artifact, an Artifact Repository" should {

    "create and retrieve an artifact correctly" in new ArtifactRepositoryTest {
      val project = createMockProject
      val artifactCreated = createMockArtifact
      artifactCreated.title mustEqual artifactTitle

      val artifactRetrieved = artifactRepo.getById(artifactCreated.id).get
      artifactRetrieved mustEqual artifactCreated
      artifactRetrieved.title mustEqual artifactTitle
    }

    /*
    "add an image to an artifact, and retrieve it" in new ArtifactRepositoryTest {
      val project = createMockProject
      val artifactTitle = "Demo Artifact"
      val artifactCreated = artifactRepo.save(Artifact(null, artifactTitle, user))
      artifactCreated.title mustEqual artifactTitle

      val artifactRetrieved = artifactRepo.getById(artifactCreated.id).get
      artifactRetrieved mustEqual artifactCreated
      artifactRetrieved.title mustEqual artifactTitle

      val testFile = makeFile(artifactCreated.id)
      val imageSubmitted = mediaRepo.addImageToArtifact(artifactCreated, testFile)
      val imagesLoaded = mediaRepo.getImagesForArtifact(artifactCreated)
      imagesLoaded.length mustEqual 1
      imagesLoaded(0).id mustEqual imageSubmitted.id
    }

    "add two images to an artifact, and retrieve them" in new ArtifactRepositoryTest {
      val project = createMockProject
      val artifactTitle = "Demo Artifact"
      val artifactCreated = artifactRepo.create(project, artifactTitle)
      artifactCreated.title mustEqual artifactTitle

      val artifactRetrieved = artifactRepo.get(artifactCreated.id).get
      artifactRetrieved mustEqual artifactCreated
      artifactRetrieved.title mustEqual artifactTitle

      val testFile = makeFile(artifactCreated.id)
      val imageSubmitted = mediaRepo.addImageToArtifact(artifactCreated, testFile)
      val imagesLoaded = mediaRepo.getImagesForArtifact(artifactCreated)
      imagesLoaded.length mustEqual 1
      imagesLoaded(0).id mustEqual imageSubmitted.id

      val testFile2 = makeFile(artifactCreated.id)
      val imageSubmitted2 = mediaRepo.addImageToArtifact(artifactCreated, testFile2)
      val imagesLoaded2 = mediaRepo.getImagesForArtifact(artifactCreated)
      imagesLoaded2.length mustEqual 2
      val imageLoadedIds = imagesLoaded2.map(_.id).toSet
      val imagesSubmittedIds = Set(imageSubmitted.id, imageSubmitted2.id)
      imageLoadedIds mustEqual imagesSubmittedIds
    }
    */


//    "remove an image from an artifact"

    "add a model to an artifact, and retrieve it" in new ArtifactRepositoryTest {
      val project = createMockProject

      val artifactCreated = createMockArtifactWithFile

      val modelsLoaded = mediaRepo.getModelsForArtifact(artifactCreated)
      modelsLoaded must haveSize(1)

      // val modelFile = mediaRepo.getModelFile(modelsLoaded(0), modelsLoaded(0).files.head.filename).get
      // Files.readAllLines(modelFile.file.toPath, Charset.forName("utf-8")).mkString("\n") mustEqual fileContent
    }


    /*
    "remove a model from an artifact"

    "retrieve an artifact with two pictures and two models"

    "update an artifact when the artifact title has changed"

    "unlink an artifact from a project"
    "link an artifact to a project"
    "prevent linking an artifact to a project a second time"

    "delete an artifact when the artifact has no projects left to it"
    */

  }

}


