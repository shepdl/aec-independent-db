package artifacts

import java.io.File

import accessControl.{GroupRepository, UserRepository}
import annotations.{AnnotationRepository, EntityRepository}
import common.VersionActions
import fictionalArchaeologists.MiscDiscoveriesScenario
import play.api.test.PlaySpecification
import projects.ProjectRepository
import schemas.{Schema, SchemaRepository}

/**
  * Created by daveshepard on 3/27/17.
  */
class VersioningTest extends PlaySpecification {

  "An ArtifactRepository" should {

    "Creating a artifact and updating it creates two versions that are returned in the correct order" in new ArtifactVersioningTest {
      val updatedCthuluStatue = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(3)
      artifactList(0).item mustEqual updatedCthuluStatue
      artifactList(1).item mustEqual cthuluStatue
      artifactList(2).item mustEqual cthuluStatue
      artifactList(0).versionId mustNotEqual artifactList(1).versionId
    }

    "Creating an artifact and updating it twice creates three version" in new ArtifactVersioningTest {
      val updatedCthuluStatue1 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))
      val updatedCthuluStatue2 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans, Louisiana", cthuluStatue.owner
      ))

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(4)
      artifactList(0).item mustEqual updatedCthuluStatue2
      artifactList(1).item mustEqual updatedCthuluStatue1
      artifactList(2).item mustEqual cthuluStatue
      artifactList(0).versionId mustNotEqual artifactList(1).versionId
    }

    "Adding a new model creates a new version" in new ArtifactVersioningTest {
      val updatedCthuluStatue1 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))
      val newModel = mediaRepo.save(Model(
        null, "Test model", "obj", List(
          ModelFile(null, "testFile.obj", "obj", new File("testFile.obj")),
          ModelFile(null, "testFile.jpg", "jpg", new File("testFile.jpg"))
        ), null
      ))
      mediaRepo.addModelToArtifact(cthuluStatue, newModel)

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(4)
    }

    "Deleting a model creates a new version" in new ArtifactVersioningTest {
      val updatedCthuluStatue1 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))
      val newModel = mediaRepo.save(Model(
        null, "Test model", "obj", List(
          ModelFile(null, "testFile.obj", "obj", new File("testFile.obj")),
          ModelFile(null, "testFile.jpg", "jpg", new File("testFile.jpg"))
        ), null
      ))
      mediaRepo.addModelToArtifact(cthuluStatue, newModel)
      mediaRepo.removeModelFromArtifact(cthuluStatue, newModel)

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(5)
      artifactList(0).action mustEqual VersionActions.updated
    }

    "Deleting a version with three previous versions creates four versions" in new ArtifactVersioningTest {
      val updatedCthuluStatue1 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))
      val newModel = mediaRepo.save(Model(
        null, "Test model", "obj", List(
          ModelFile(null, "testFile.obj", "obj", new File("testFile.obj")),
          ModelFile(null, "testFile.jpg", "jpg", new File("testFile.jpg"))
        ), null
      ))
      mediaRepo.addModelToArtifact(cthuluStatue, newModel)

      artifactRepo.delete(cthuluStatue, pabodie)

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(5)
      artifactList(0).action mustEqual ArtifactProperties.deleted
    }

    "Reverting to the immediate previous version works correctly" in new ArtifactVersioningTest {
      val updatedCthuluStatue1 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))
      val newModel = mediaRepo.save(Model(
        null, "Test model", "obj", List(
          ModelFile(null, "testFile.obj", "obj", new File("testFile.obj")),
          ModelFile(null, "testFile.jpg", "jpg", new File("testFile.jpg"))
        ), null
      ))
      mediaRepo.addModelToArtifact(cthuluStatue, newModel)

      val preReversionVersions = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactRepo.revertTo(cthuluStatue, preReversionVersions(1).versionId, pabodie)

      val currentVersion = artifactRepo.getById(cthuluStatue.id).get
      currentVersion mustEqual updatedCthuluStatue1

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(5)
      artifactList(0).item mustEqual updatedCthuluStatue1
    }

    "Reverting to the version two versions ago works correctly" in new ArtifactVersioningTest {
      val updatedCthuluStatue1 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))
      val newModel = mediaRepo.save(Model(
        null, "Test model", "obj", List(
          ModelFile(null, "testFile.obj", "obj", new File("testFile.obj")),
          ModelFile(null, "testFile.jpg", "jpg", new File("testFile.jpg"))
        ), null
      ))
      mediaRepo.addModelToArtifact(cthuluStatue, newModel)

      val preReversionVersions = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactRepo.revertTo(cthuluStatue, preReversionVersions(2).versionId, pabodie)

      val currentVersion = artifactRepo.getById(cthuluStatue.id).get
      currentVersion mustEqual cthuluStatue

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(5)
      artifactList(0).item mustEqual cthuluStatue
    }

    "Reverting to a version prior to a deletion works" in new ArtifactVersioningTest {
      val updatedCthuluStatue1 = artifactRepo.save(Artifact(
        cthuluStatue.id, "Cthulu Statue from New Orleans", cthuluStatue.owner
      ))
      val newModel = mediaRepo.save(Model(
        null, "Test model", "obj", List(
          ModelFile(null, "testFile.obj", "obj", new File("testFile.obj")),
          ModelFile(null, "testFile.jpg", "jpg", new File("testFile.jpg"))
        ), null
      ))
      mediaRepo.addModelToArtifact(cthuluStatue, newModel)
      artifactRepo.delete(cthuluStatue, pabodie)

      val preReversionVersions = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactRepo.revertTo(cthuluStatue, preReversionVersions(3).versionId, pabodie)

      val currentVersion = artifactRepo.getById(cthuluStatue.id).get
      currentVersion mustEqual cthuluStatue

      val artifactList = artifactRepo.listVersions(cthuluStatue.id, 0, 25)
      artifactList must haveSize(6)
      artifactList(0).item mustEqual cthuluStatue
    }

//    "Reverting to a “deleted” version returns an error"

  }

}

trait ArtifactVersioningTest extends MiscDiscoveriesScenario
