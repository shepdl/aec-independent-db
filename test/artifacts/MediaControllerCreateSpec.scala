package artifacts

import java.nio.charset.Charset

import common.{HasDatabase, TestIHVApplication}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import play.api.libs.Files.TemporaryFile
import play.api.mvc.MultipartFormData.FilePart
import play.api.mvc.{Controller, MultipartFormData}
import play.api.test._
import play.api.test.Helpers._
import projects.{HasProjectRepo, ProjectService}
import java.io.{BufferedWriter, File, PrintWriter}
import java.nio.file.{Files, Paths, StandardOpenOption}

import accessControl.{AuthenticationService, BasicUser, HasUserRepository}
import schemas.HasSchemaRepo


class MediaControllerTest extends TestIHVApplication with HasUserRepository with BasicUser with HasProjectRepo
  with HasSchemaRepo with HasArtifactRepo with HasMediaRepo {

  val mediaController = new MediaController with Controller {
    override val artifactRepo = app.injector.instanceOf[ArtifactRepository]
    override val mediaRepo: MediaRepository = app.injector.instanceOf[MediaRepository]
    override val projectService: ProjectService = app.injector.instanceOf[ProjectService]
    override val authService: AuthenticationService = app.injector.instanceOf[AuthenticationService]
  }
}


@RunWith(classOf[JUnitRunner])
class MediaControllerCreateSpec extends Specification {

  "When creating, a Media Controller" should {

    "create media for an existing artifact" in new MediaControllerTest {
      val project = createMockProject
      val artifact = createMockArtifact

      artifactRepo.linkToProject(artifact, project)

      override val fileContent =
        s"""
           |<sampleXml>
           |  <test>Hello!</test>
           |</sampleXml>
         """.stripMargin
      val tempFile = TemporaryFile("test", ".dae")
      val writer = new PrintWriter(tempFile.file)
      writer.write(fileContent)
      writer.close()

      val formData = MultipartFormData[TemporaryFile](
        dataParts = Map(),
        files = Seq(FilePart[TemporaryFile](
          key = "file",
          filename = tempFile.file.getName,
          contentType = Some("application/xml"),
          ref = tempFile
        )),
        badParts = Seq()
      )

      val createRequest = FakeRequest(POST, s"/activities/${project.id}/artifacts/${artifact.id}/models")
          .withMultipartFormDataBody(formData)
        .withSession("userId" -> user.uuid.toString)

//      val createResponse = Helpers.call(mediaController.uploadModel, createRequest)
    }

//    "fail to create media when creating media for a non-existant artifact"

  }

}

