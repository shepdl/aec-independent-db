package artifacts

import java.io.PrintWriter
import java.util.UUID

import accessControl.{AuthenticationService, BasicUser, HasUserRepository}
import common.{DatabaseService, HasDatabase, TestIHVApplication}
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.i18n.MessagesApi
import play.api.libs.Files.TemporaryFile
import play.api.libs.json.{JsString, Json}
import play.api.mvc.{Controller, MultipartFormData, Results}
import play.api.test._
import projects.{HasProjectRepo, ProjectRepository, ProjectService, ProjectUserRights}
import schemas.HasSchemaRepo


class ArtifactControllerTest extends TestIHVApplication with HasUserRepository with BasicUser with HasProjectRepo
  with HasSchemaRepo with HasArtifactRepo with HasMediaRepo {

  val artifactController = new ArtifactController with Controller {
    override val projectRepo: ProjectRepository = app.injector.instanceOf[ProjectRepository]
    override implicit val messagesApi: MessagesApi = app.injector.instanceOf[MessagesApi]
    override val artifactRepo: ArtifactRepository = app.injector.instanceOf[ArtifactRepository]
    override val mediaRepo: MediaRepository = app.injector.instanceOf[MediaRepository]
    override val authenticationService: AuthenticationService = app.injector.instanceOf[AuthenticationService]
    override val projectService: ProjectService = app.injector.instanceOf[ProjectService]
  }

  val mediaController = new MediaController with Controller {
    override val artifactRepo = app.injector.instanceOf[ArtifactRepository]
    override val mediaRepo: MediaRepository = app.injector.instanceOf[MediaRepository]
    override val projectService: ProjectService = app.injector.instanceOf[ProjectService]
    override val authService: AuthenticationService = app.injector.instanceOf[AuthenticationService]
  }
}


@RunWith(classOf[JUnitRunner])
class ArtifactControllerCreateSpec extends PlaySpecification with Results with JsonMatchers {

  import common.MultipartFormDataWritable.anyContentAsMultipartFormWritable

  "When creating, an artifact controller " should {

    "create and retrieve a new artifact with a file" in new ArtifactControllerTest {
      val project = createMockProject
      projectRepo.setRightsInProjectForUser(project, user, ProjectUserRights.ALL_RIGHTS)
      override val fileContent =
        s"""
           |<sampleXml>
           |  <test>Hello!</test>
           |</sampleXml>
         """.stripMargin
      val tempFile = TemporaryFile("test", ".dae")
      val writer = new PrintWriter(tempFile.file)
      writer.write(fileContent)
      writer.close()

      val artifactFormData = Map(
        "title" -> Seq(artifactTitle)
      )

      val artifactCreationRequest = FakeRequest(POST, s"/activities/${project.id}/artifacts", FakeHeaders(), artifactFormData)
        .withSession("userId" -> user.uuid.toString)

      val artifactCreateResponse = artifactController.create(project.id)(artifactCreationRequest)

      status(artifactCreateResponse) mustEqual 201
      val artifactCreateResponseJson = contentAsJson(artifactCreateResponse)
      val artifactId = (artifactCreateResponseJson \ "id").as[UUID]

      val modelTitle = "Test Model"


      val formData = MultipartFormData[TemporaryFile](
        dataParts = Map(
          "title" -> Seq(modelTitle),
          "format" -> Seq("dae")
        ),
        files = Seq(MultipartFormData.FilePart[TemporaryFile](
          key = "model",
          filename = tempFile.file.getName,
          contentType = Some("application/xml"),
          ref = tempFile
        )),
        badParts = Seq.empty
      )

      val modelCreateRequest = FakeRequest(POST, s"/activities/${project.id}/artifacts/${artifactId}", FakeHeaders(), formData)
        .withSession("userId" -> user.uuid.toString)
      val modelCreateResponse = artifactController.createWithFile(project.id, artifactId)(modelCreateRequest)

      status(modelCreateResponse) mustEqual CREATED

      val modelCreateResponseJson = contentAsString(modelCreateResponse)
      modelCreateResponseJson must /("title" -> modelTitle)
      modelCreateResponseJson must /("files").andHave(size(1))

      val modelId = (contentAsJson(modelCreateResponse) \ "id" ).as[UUID]

      val modelRequest = FakeRequest(GET, s"/artifacts/${artifactId}/models/$modelId").withHeaders(("Fake", "Fake"))
      val modelResponse = artifactController.getForProject(project.id)(modelRequest)
      val modelResponseJson = contentAsString(modelResponse)
      modelResponseJson must /#(0) /("models") /#(0) /("title" -> "Test Model")
      val modelFilename = (Json.parse(
        modelResponseJson
      ) \(0) \("models")\(0)\("files")\(0)\("filename")).as[String]

      val mediaRequest = FakeRequest(GET, s"/artifacts/${modelFilename}").withHeaders("Fake" -> "Fake")
      val mediaResponse = mediaController.getModelFile(modelId, modelFilename)(mediaRequest)

      status(mediaResponse) mustEqual OK
      val responseFile = contentAsString(mediaResponse)
      responseFile mustEqual fileContent
    }

    /*
    "create a new artifact, and retrieve it with links to all media linked to it"
    "update an artifact's title"
    "add new images to an artifact"
    "replace the model for the artifact"
    "delete an image from an artifact"
    "delete an artifact"
    */

  }

}


/*
class TestArtifactController (override val projectRepo:ProjectRepository,
    override val artifactRepo:ArtifactRepository,
    override val mediaRepo:MediaRepository
  ) extends ArtifactController with Controller


trait HasArtifactController extends HasProjectRepo with HasArtifactRepo with HasMediaRepo {
  val artifactController = new TestArtifactController(projectRepo, artifactRepo, mediaRepo)
}
*/

