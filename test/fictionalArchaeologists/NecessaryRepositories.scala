package fictionalArchaeologists

import accessControl.{GroupRepository, UserRepository}
import annotations.{AnnotationRepository, EntityRepository}
import artifacts.{ArtifactRepository, MediaRepository}
import common.TestIHVApplication
import projects.ProjectRepository
import schemas.HasSchemaRepo

/**
  * Created by dave on 4/5/17.
  */
trait NecessaryRepositories extends TestIHVApplication with HasSchemaRepo {
  val projectRepo: ProjectRepository = app.injector.instanceOf[ProjectRepository]
  val userRepo: UserRepository = app.injector.instanceOf[UserRepository]
  val artifactRepo: ArtifactRepository = app.injector.instanceOf[ArtifactRepository]
  val mediaRepo: MediaRepository = app.injector.instanceOf[MediaRepository]
  val annotationRepo: AnnotationRepository = app.injector.instanceOf[AnnotationRepository]
  val entityRepo: EntityRepository = app.injector.instanceOf[EntityRepository]
  val groupRepo: GroupRepository = app.injector.instanceOf[GroupRepository]

  val stringSchema = stringSchemaNode

}
