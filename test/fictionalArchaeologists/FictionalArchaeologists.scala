package fictionalArchaeologists

import java.util.UUID

import accessControl._

/**
  * Created by daveshepard on 3/23/17.
  */
trait FictionalArchaeologists {

  val userRepo:UserRepository
  val groupRepo:GroupRepository

  val croft = userRepo.save(
    new User(null, "Lara Croft", "croft@ox.ac.uk")
  )

  val jones = userRepo.save(
    new User(null, "Indiana Jones", "ijones@iu.edu")
  )

  val langdon = userRepo.save(
    new User(null, "Robert Langdon", "langdon@harvard.edu")
  )

  val fictionalArchaeologists = groupRepo.save(new Group(
    null, "Fictional Archaeologists", "http://fa.com/"
  ))

  groupRepo.addUser(fictionalArchaeologists, croft, GroupMembershipProperties.roles.admin)
  groupRepo.addUser(fictionalArchaeologists, jones, GroupMembershipProperties.roles.member)

}
