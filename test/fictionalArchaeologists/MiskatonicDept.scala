package fictionalArchaeologists

import accessControl._

/**
  * Created by daveshepard on 3/23/17.
  */
trait MiskatonicDept {

  val userRepo:UserRepository
  val groupRepo:GroupRepository

  val pabodie = userRepo.save(
    new User(null, "Frank H. Pabodie", "pabodie@miskatonic.edu")
  )

  val danforth = userRepo.save(
    new User(null, "Peter Danforth", "danforth@miskatonic.edu")
  )

  val carter = userRepo.save(
    new User(null, "Randolph Carter", "carter@gmail.com")
  )

  val miskatonic = groupRepo.save(Group(null, "Miskatonic University Department of Weird Studies", "dws.miskatonic.edu"))

  groupRepo.addUser(miskatonic, pabodie, GroupMembershipProperties.roles.admin)
  groupRepo.addUser(miskatonic, danforth, GroupMembershipProperties.roles.member)

}
