package fictionalArchaeologists

import accessControl.User
import annotations.{Entity, EntityRepository}
import schemas.{Schema, SchemaField, SchemaRepository}

/**
  * Created by daveshepard on 3/23/17.
  */
trait ElderGods {

  val schemaRepo:SchemaRepository
  val entityRepo:EntityRepository
  val stringSchema:Schema
  val pabodie:User

  val deityOrderSchema = schemaRepo.save(Schema(
    null, "Deity Order", "entity", List(
      SchemaField(null, "name", stringSchema, false, List())
    ), false, pabodie
  ))

  val elderGods = entityRepo.save(Entity(
    null, deityOrderSchema, Map(
      "name" -> "Elder Gods"
    )
  ))

  val mythicalFigure = entityRepo.save(Entity(
    null, deityOrderSchema, Map(
      "name" -> "Mythical Figure"
    )
  ))

  val deitySchema = schemaRepo.save(new Schema(
    null, "Deity", "either", List(
      SchemaField(null, "name", stringSchema, false, List()),
      SchemaField(null, "order", stringSchema, true, List(elderGods, mythicalFigure))
    ), false, pabodie
  ))

  val cthulu = entityRepo.save(Entity(
    null, deitySchema, Map(
      "name" -> "Cthulu",
      "order" -> elderGods.id.toString
    )
  ))

  val dagon = entityRepo.save(Entity(
    null, deitySchema, Map(
      "name" -> "Dagon",
      "order" -> elderGods.id.toString
    )
  ))

  val astaroth = entityRepo.save(Entity(
    null, deitySchema, Map(
      "name" -> "Astaroth",
      "order" -> elderGods.id.toString
    )
  ))

  val elderGodList = List(cthulu, dagon, astaroth)

  val shaitan = entityRepo.save(Entity(
    null, deitySchema, Map(
      "name" -> "Shaitan",
      "order" -> mythicalFigure.id.toString
    )
  ))

  val elderGodFigureSchema = schemaRepo.save(Schema(
    null, "Elder God Figure", "whole", List(
      SchemaField(null, "deity", deitySchema, false, List()),
      SchemaField(null, "material", stringSchema, false, List())
    ), false, pabodie
  ))

  val illuminatiSecretSchema = schemaRepo.save(Schema(
    null, "Secret of the Illuminati", "annotation", List(
      SchemaField(null, "deity", deitySchema, true, elderGodList),
      SchemaField(null, "event", stringSchema, false, List()),
      SchemaField(null, "interpretation", stringSchema, false, List()),
      SchemaField(null, "location", stringSchema, false, List())
    ), false, pabodie
  ))

}
