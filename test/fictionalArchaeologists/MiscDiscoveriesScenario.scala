package fictionalArchaeologists

import annotations.AnnotationRepository
import artifacts._
import projects.ProjectRepository
import schemas.Schema

/**
  * Created by daveshepard on 3/23/17.
  */
trait MiscDiscoveriesScenario extends NecessaryRepositories with FictionalArchaeologists
  with MiskatonicDept with ElderGods {


  val ark = artifactRepo.save(
    Artifact(null, "Ark of the Covenant", jones)
  )

  val arkModel = mediaRepo.save(
    Model(null, "Ark Top", "obj", List(
      ModelFile(null, "base.obj", "obj", null),
      ModelFile(null, "base.mtl", "mtl", null),
      ModelFile(null, "texture1.jpg", "jpg", null)
    ), null)
  )

  mediaRepo.addModelToArtifact(ark, arkModel)

  val kingInYellow = artifactRepo.save(
    Artifact(null, "Box of Manuscript for the King in Yellow", pabodie)
  )

  val kingInYellowModel = mediaRepo.save(
    Model(null, "Manuscript Model Box", "obj", List(
      ModelFile(null, "base.obj", "obj", null)
    ), null)
  )

  mediaRepo.addModelToArtifact(kingInYellow, kingInYellowModel)

  val cthuluStatue = artifactRepo.save(
    Artifact(null, "Cthulu Statue", pabodie)
  )

  val cthuluModel = mediaRepo.save(
    Model(null, "Cthulu Statue Model", "obj", List(
      ModelFile(null, "cthuluBase.obj", "obj", null),
      ModelFile(null, "cthuluTexture.jpg", "jpg", null)
    ), null)
  )

  mediaRepo.addModelToArtifact(cthuluStatue, cthuluModel)

}
