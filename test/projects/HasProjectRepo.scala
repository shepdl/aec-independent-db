package projects

import play.api.Application

/**
 * Created by dave on 3/11/16.
 */
trait HasProjectRepo {

  val app:Application
  val projectRepo = app.injector.instanceOf[ProjectRepository]

  def createMockProject = {
    val project = projectRepo.save(Project(null, "Demo Project"))
    project
  }

}
