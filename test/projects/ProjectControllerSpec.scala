package projects

import java.util.UUID

import accessControl.{AuthenticationService, BasicUser, HasUserRepository, UserRepository}
import backend.approvals.ApprovalRepository
import common.TestIHVApplication
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import org.specs2.runner._
import play.api.i18n.MessagesApi
import play.api.libs.json._
import play.api.mvc.Controller
import play.api.test._


trait ProjectControllerTest extends TestIHVApplication with HasUserRepository with BasicUser with HasProjectRepo {

  val controller = new ProjectController with Controller {
    override val projectRepo: ProjectRepository = app.injector.instanceOf[ProjectRepository]
    override val userRepo: UserRepository = app.injector.instanceOf[UserRepository]
    override val messagesApi: MessagesApi = app.injector.instanceOf[MessagesApi]
    override val authentication: AuthenticationService = app.injector.instanceOf[AuthenticationService]
    override val approvalRepo: ApprovalRepository = app.injector.instanceOf[ApprovalRepository]
    override val projectService: ProjectService = app.injector.instanceOf[ProjectService]
  }

}

@RunWith(classOf[JUnitRunner])
class ProjectControllerSpec extends PlaySpecification with JsonMatchers {

  "A Project Controller" should {

    "create and retrieve a project correctly" in new ProjectControllerTest {

      val projectName = "Demo Project"

      val createRequest = FakeRequest(POST, "/activities").withFormUrlEncodedBody(
        ("title", projectName)
      ).withSession("userId" -> user.uuid.toString)
      val createResponse = Helpers.call(controller.create, createRequest)

      status(createResponse) mustEqual CREATED
      val responseJson = contentAsString(createResponse)
      responseJson must /("title" -> projectName)
    }

    "update a project's title" in new ProjectControllerTest {
      val projectName1 = "Demo Project Unchanged"
      val createRequest = FakeRequest(POST, "/activities").withFormUrlEncodedBody(
        ("title", projectName1)
      ).withSession("userId" -> user.uuid.toString)
      val createResponse = Helpers.call(controller.create, createRequest)
      val responseJson = contentAsJson(createResponse)
      val projectId = (responseJson \ "id").as[UUID]

      val projectName2 = "Demo Project Changed"
      val newJson = Json.parse(
        s"""
           |{
           |  "id" : "${projectId}",
           |  "title" : "$projectName2"
           |}
         """.
          stripMargin)

      val updateRequest = FakeRequest(PUT, s"/activities/${projectId}").withJsonBody(newJson)
        .withSession("userId" -> user.uuid.toString)
      val updateResponse = Helpers.call(controller.update(projectId), updateRequest)

      status(updateResponse) mustEqual OK
      val updateResponseAsJson = contentAsString(updateResponse)
      updateResponseAsJson must /("id" -> projectId.toString)
      updateResponseAsJson must /("title" -> projectName2)
    }

    "delete a project" in new ProjectControllerTest {
      val projectName = "Demo Project"

      val createRequest = FakeRequest(POST, "/activities").withFormUrlEncodedBody(
        ( "title", projectName )
      ).withSession("userId" -> user.uuid.toString)
      val createResponse = Helpers.call(controller.create,  createRequest)

      status(createResponse) mustEqual CREATED
      val responseJson = contentAsString(createResponse)
      responseJson must /("title" -> projectName)
      val responseJson2 = contentAsJson(createResponse)
      val projectId = (  responseJson2 \ "id").as[UUID]

      val deleteRequest = FakeRequest(DELETE, s"/activities/$projectId")
        .withHeaders(("Fake", "fake"))
        .withSession("userId" -> user.uuid.toString)
      val deleteResponse = Helpers.call(controller.delete(projectId), deleteRequest)

      status(deleteResponse) mustEqual OK

      val getRequest = FakeRequest(GET, s"/activities/$projectId").withHeaders(("Fake", "fake"))
      val getResponse =  Helpers.call(controller.get(projectId), getRequest)

      status(getResponse) mustEqual NOT_FOUND
    }
  }

}


