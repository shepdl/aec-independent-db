package projects

import java.util.UUID

import accessControl.{GroupRepository, UserRepository}
import annotations.{AnnotationRepository, EntityRepository}
import artifacts.{ArtifactRepository, MediaRepository}
import common.{HasDatabase, TestIHVApplication, VersionActions}
import fictionalArchaeologists.MiscDiscoveriesScenario
import org.specs2.mutable._
import org.specs2.runner._
import org.junit.runner._
import org.specs2.matcher.JsonMatchers
import play.api.i18n.MessagesApi
import play.api.libs.json.Json
import play.api.mvc.{Controller, Results}
import play.api.test._
import play.api.test.Helpers._
import schemas.{Schema, SchemaRepoTestCase, SchemaRepository}

/**
  * Created by daveshepard on 3/27/17.
  */
@RunWith(classOf[JUnitRunner])
class ProjectRepositoryVersioningTest extends PlaySpecification {

  "A Project Repository" should {

    "contain one version of a project when it has been newly created" in new MiscDiscoveriesScenario {
      val newProject = projectRepo.save(Project(null, "Test Project"))
      val projectList = projectRepo.listVersions(newProject.id, 0, 10)
      projectList must haveSize(1)
      projectList.head.item mustEqual newProject
    }

    "contain two versions of a project when it has been updated once" in new MiscDiscoveriesScenario {
      val newProject = projectRepo.save(Project(null, "Test Project"))
      val updatedProject = Project(newProject.id, "Test Project 1")
      projectRepo.save(updatedProject)
      val projectList = projectRepo.listVersions(newProject.id, 0, 10)
      projectList must haveSize(2)
      projectList(0).item mustEqual updatedProject
      projectList(1).item mustEqual newProject
      projectList(0).versionId mustNotEqual projectList(1).versionId
    }

    "contain three versions of a project when it has been updated twice" in new MiscDiscoveriesScenario {
      val newProject = projectRepo.save(Project(null, "Test Project"))
      Thread.sleep(100)
      val updatedProject1 = Project(newProject.id, "Test Project 1")
      projectRepo.save(updatedProject1)
      val updatedProject2 = Project(newProject.id, "Test Project 2")
      Thread.sleep(100)
      projectRepo.save(updatedProject2)
      val projectList = projectRepo.listVersions(newProject.id, 0, 10)
      projectList must haveSize(3)
      projectList(0).item mustEqual updatedProject2
      projectList(0).action mustEqual VersionActions.updated
      projectList(1).item mustEqual updatedProject1
      projectList(1).action mustEqual VersionActions.updated
      projectList(2).item mustEqual newProject
      projectList(2).action mustEqual VersionActions.original
      projectList(0).versionId mustNotEqual projectList(1).versionId
    }


    // Deletion tests ...
    "contain two versions of a project when it has been created and then deleted" in new MiscDiscoveriesScenario {
      val newProject = projectRepo.save(Project(null, "Test Project"))
      projectRepo.delete(newProject, pabodie)
      val projectList = projectRepo.listVersions(newProject.id, 0, 10)
      projectList must haveSize(2)
      projectList(0).action mustEqual ProjectProperties.deleted
      projectList(1).item mustEqual newProject
      projectList(0).versionId mustNotEqual projectList(1).versionId
    }

    "contain three versions of a project when it has been created, updated once, and then deleted" in new MiscDiscoveriesScenario {
      val newProject = projectRepo.save(Project(null, "Test Project"))
      val updatedProject = Project(newProject.id, "Test Project 1")
      projectRepo.save(updatedProject)
      projectRepo.delete(updatedProject, pabodie)
      val projectList = projectRepo.listVersions(newProject.id, 0, 10)
      projectList must haveSize(3)
      projectList(0).item mustEqual updatedProject
      projectList(0).action mustEqual ProjectProperties.deleted
      projectList(1).item mustEqual updatedProject
      projectList(2).item mustEqual newProject
      projectList(0).versionId mustNotEqual projectList(1).versionId
    }

    "revert correctly to contain four versions of a project when it has been created, updated once, deleted, and then reverted" in new MiscDiscoveriesScenario {
      val newProject = projectRepo.save(Project(null, "Test Project"))
      val updatedProject = Project(newProject.id, "Test Project 1")
      projectRepo.save(updatedProject)
      val projectListBeforeDeletion = projectRepo.listVersions(newProject.id, 0, 10)
      projectListBeforeDeletion must haveSize(2)
      projectRepo.delete(updatedProject, pabodie)
      projectRepo.revertTo(newProject.id, projectListBeforeDeletion(0).versionId, pabodie)
      val projectListAfterDeletion = projectRepo.listVersions(newProject.id, 0, 10)
      projectListAfterDeletion must haveSize(4)
      projectListAfterDeletion(0).item mustEqual updatedProject
      projectListAfterDeletion(1).item mustEqual updatedProject
      projectListAfterDeletion(1).action mustEqual ProjectProperties.deleted
      projectListAfterDeletion(2).item mustEqual updatedProject
      projectListAfterDeletion(3).item mustEqual newProject
      val currentProject = projectRepo.getById(newProject.id).get
      currentProject mustEqual updatedProject
    }

//    "fail to revert to a deleted version of a project"
  }

}


