package projects

import accessControl.{BasicUser, HasUserRepository}
import common.{HasDatabase, TestIHVApplication}
import org.junit.runner._
import org.specs2.mutable._
import org.specs2.runner._
import play.api.test._


@RunWith(classOf[JUnitRunner])
class ProjectRepositorySpec extends Specification {

  "A Project Repository" should {

    "create a project when supplied with the title" in new ProjectRepositoryTest {

      val projectTitle = "Demo Project"
      val projectCreated = projectRepo.save(Project(null, projectTitle))
      val projectRetrieved = projectRepo.getById(projectCreated.id).get

      projectRetrieved mustEqual projectCreated
      projectRetrieved.title mustEqual projectTitle
    }

    "update a project when supplied with a new title" in new ProjectRepositoryTest {

      val projectCreated = createMockProject
      projectRepo.setRightsInProjectForUser(projectCreated, user, ProjectUserRights.ALL_RIGHTS)

      val projectTitleUpdated = "Demo Project Changed"
      val projectUpdateData = Project(projectCreated.id, projectTitleUpdated)
      val projectUpdated = projectRepo.save(projectUpdateData)
      val projectRetrieved = projectRepo.getById(projectCreated.id).get

      projectRetrieved.id mustEqual projectCreated.id
      projectRetrieved.title mustEqual projectTitleUpdated
    }

    "delete a project when sent the proper request" in new ProjectRepositoryTest {
      val projectCreated = createMockProject
      projectRepo.delete(projectCreated, user)
      projectRepo.getById(projectCreated.id) mustEqual None
    }

  }

}

trait ProjectRepositoryTest extends TestIHVApplication with HasUserRepository with HasProjectRepo with BasicUser
