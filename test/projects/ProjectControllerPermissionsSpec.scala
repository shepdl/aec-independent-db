package projects

import org.junit.runner._
import org.specs2.runner._
import accessControl.{AuthenticationService, GroupRepository, UserRepository}
import annotations.{AnnotationRepository, EntityRepository}
import artifacts.{ArtifactRepository, MediaRepository}
import backend.approvals.ApprovalRepository
import common.TestIHVApplication
import fictionalArchaeologists.MiscDiscoveriesScenario
import org.specs2.matcher.JsonMatchers
import play.api.i18n.MessagesApi
import play.api.libs.json.Json
import play.api.mvc.{Controller, Results}
import play.api.test._
import schemas.{HasSchemaRepo, Schema, SchemaRepository}

/**
  * Created by daveshepard on 3/27/17.
  */

@RunWith(classOf[JUnitRunner])
class ProjectControllerPermissionsSpec extends PlaySpecification with Results with JsonMatchers {

  "A ProjectController should" in {

    // Editing metadata
    "allow a user with the canEditMetadata permission to update its title" in new ProjectPermissionsTestCase {
      val data = Json.parse(
        s"""
           |{
           |  "id" : "${sampleProject.id}",
           |  "title" : "Test Project 2"
           |}
         """.stripMargin)
      val updateRequest = FakeRequest(POST, s"/projects/${sampleProject.id}")
          .withBody(data)
          .withSession("userId" -> pabodie.uuid.toString)
      val updateResponse = controller.update(sampleProject.id)(updateRequest)

      status(updateResponse) mustEqual OK
    }

    "not allow a user with other rights, but not canEditMetadata, to update the title" in new ProjectPermissionsTestCase {
      val data = Json.parse(
        s"""
           |{
           |  "id" : "${sampleProject.id}",
           |  "title" : "Test Project 2"
           |}
         """.stripMargin)
      projectRepo.setRightsInProjectForUser(sampleProject, danforth, Set(
        ProjectUserRights.canAddAnnotations,
        ProjectUserRights.canDeleteAnnotations
      ))
      val updateRequest = FakeRequest(POST, s"/projects/${sampleProject.id}")
          .withBody(data)
        .withSession("userId" -> danforth.uuid.toString)
      val updateResponse = controller.update(sampleProject.id)(updateRequest)
      status(updateResponse) mustEqual FORBIDDEN
    }

    "not allow a user with no rights in the project to update the title" in new ProjectPermissionsTestCase {
      val data = Json.parse(
        s"""
           |{
           |  "id" : "${sampleProject.id}",
           |  "title" : "Test Project 2"
           |}
         """.stripMargin)
      val updateRequest = FakeRequest(POST, s"/projects/${sampleProject.id}")
          .withBody(data)
        .withSession("userId" -> carter.uuid.toString)
      val updateResponse = controller.update(sampleProject.id)(updateRequest)
      status(updateResponse) mustEqual FORBIDDEN
    }

    // Adding users
    "allow a user with canAddUsers to add new users" in new ProjectPermissionsTestCase {
      val payload = Json.parse(
        s"""
           |{
           |  "rights" : [
           |    "${ProjectUserRights.canAddAnnotations}",
           |    "${ProjectUserRights.canDeleteAnnotations}",
           |    "${ProjectUserRights.canEditOthersAnnotations}"
           |  ]
           |}
         """.stripMargin
      )
      val updateRequest = FakeRequest(PUT, s"/activities/${sampleProject.id}/permissions/${danforth.uuid}")
          .withSession("userId" -> pabodie.uuid.toString)
          .withBody(payload)
      val updateResponse = controller.setRolesInProjectForUser(sampleProject.id, danforth.uuid)(updateRequest)
      status(updateResponse) mustEqual OK
      projectRepo.getRightsInProject(sampleProject, danforth) mustEqual Set(
        ProjectUserRights.canAddAnnotations,
        ProjectUserRights.canDeleteAnnotations,
        ProjectUserRights.canEditOthersAnnotations
      )
    }

    "not allow a user who has some rights, but not canAddUsers, to add new users" in new ProjectPermissionsTestCase {
      val payload = Json.parse(
        s"""
           |{
           |  "rights" : [
           |    "${ProjectUserRights.canAddAnnotations}",
           |    "${ProjectUserRights.canDeleteAnnotations}",
           |    "${ProjectUserRights.canEditOthersAnnotations}"
           |  ]
           |}
         """.stripMargin
      )
      val updateRequest = FakeRequest(PUT, s"/activities/${sampleProject.id}/permissions/${danforth.uuid}")
        .withSession("userId" -> danforth.uuid.toString)
        .withBody(payload)
      val updateResponse = controller.setRolesInProjectForUser(sampleProject.id, danforth.uuid)(updateRequest)
      status(updateResponse) mustEqual FORBIDDEN
      projectRepo.getRightsInProject(sampleProject, danforth) mustEqual Set()
    }

    "not allow a user with no rights in the project to add users" in new ProjectPermissionsTestCase {
      val payload = Json.parse(
        s"""
           |{
           |  "rights" : [
           |    "${ProjectUserRights.canAddAnnotations}",
           |    "${ProjectUserRights.canDeleteAnnotations}",
           |    "${ProjectUserRights.canEditOthersAnnotations}"
           |  ]
           |}
         """.stripMargin
      )
      val updateRequest = FakeRequest(PUT, s"/activities/${sampleProject.id}/permissions/${danforth.uuid}")
        .withSession("userId" -> carter.uuid.toString)
        .withBody(payload)
      val updateResponse = controller.setRolesInProjectForUser(sampleProject.id, danforth.uuid)(updateRequest)
      status(updateResponse) mustEqual FORBIDDEN
      projectRepo.getRightsInProject(sampleProject, danforth) mustEqual Set()
    }

  }

}


trait ProjectPermissionsTestCase extends TestIHVApplication with HasSchemaRepo with MiscDiscoveriesScenario {

  val sampleProject = projectRepo.save(Project(null, "Test Project"))
  projectRepo.setRightsInProjectForUser(sampleProject, pabodie, ProjectUserRights.ALL_RIGHTS)

  val controller = new ProjectController with Controller {
    override val projectRepo: ProjectRepository = app.injector.instanceOf[ProjectRepository]
    override val messagesApi: MessagesApi = app.injector.instanceOf[MessagesApi]
    override val authentication: AuthenticationService = app.injector.instanceOf[AuthenticationService]
    override val userRepo: UserRepository = app.injector.instanceOf[UserRepository]
    override val approvalRepo: ApprovalRepository = app.injector.instanceOf[ApprovalRepository]
    override val projectService: ProjectService = app.injector.instanceOf[ProjectService]
  }
}
